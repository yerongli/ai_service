# variables:
# name of the node, a count
# nodelink used to link similar items
# parent vaiable used to refer to the parent of the node in the tree
# node contains an empty dictionary for the children in the node
import argparse
import collections, os, json, re, operator, timeit, pymongo
from loc_recognizer import LocTrie
from unionfind import UnionFind


class TreeNode:
    def __init__(self, name_value, num_occur, parent_node):
        self.name = name_value  # Item name
        self.count = num_occur  # Occurrence count
        self.nodeLink = None    # Next tree node
        self.parent = parent_node  # Needs to be updated
        self.children = {}      # children nodes

    def inc(self, num_occur):
        """
        Increments the count variable with a given amount
        :param num_occur: The amount to be added
        :return: None
        """
        self.count += num_occur


def create_tree(dataset, min_sup=3):
    """
    Create a frequency pattern tree with transaction records and minimum supporting number
    :param dataset: Input dataset, a list of tuples, individual transaction and corresponding occurrence count
    :param min_sup: Minimum supporting number
    :return: Root of the FP-tree and corresponding header-table dictionary
    """
    header_table = {}
    '''
        Go over dataset twice
    '''
    # Go over dataset for the first time:
    for trans in dataset:  # First pass counts frequency of occurrences
        for item in trans:
            header_table[item] = header_table.get(item, 0) + dataset[trans]
    for k in list(header_table):  # Remove items appear minSup
        if header_table[k] < min_sup:
            del (header_table[k])
    freq_item_set = set(header_table.keys())
    if len(freq_item_set) == 0:
        return None, None  # if no items meet min support --> return empty tree and header table
    for k in header_table:
        header_table[k] = [header_table[k], None]  # reformat headerTable to use Node link
    ret_tree = TreeNode('Null Set', 1, None)  # Create a null root
    for tranSet, count in dataset.items():  # Go over dataset for second the time
        local_d = {}
        # Go over dataset for the second time:
        # reorder items in individual transaction with respect to occurrence counts
        for item in tranSet:
            if item in freq_item_set:
                local_d[item] = header_table[item][0]
        if len(local_d) > 0:
            orderedItems = [v[0] for v in sorted(local_d.items(), key=lambda p: p[1], reverse=True)]
            update_tree(orderedItems, ret_tree, header_table, count)  # populate tree with ordered freq itemset
    return ret_tree, header_table


def update_tree(items, in_tree, header_table, count):
    """
    :param items:
    :param in_tree:
    :param header_table:
    :param count:
    :return:
    """
    # check if orderedItems[0] in ret_tree.children
    if items[0] in in_tree.children:
        in_tree.children[items[0]].inc(count)  # increment count
    else:  # add items[0] to inTree.children
        in_tree.children[items[0]] = TreeNode(items[0], count, in_tree)
        if not header_table[items[0]][1]:  # update header table
            header_table[items[0]][1] = in_tree.children[items[0]]
        else:
            update_header(header_table[items[0]][1], in_tree.children[items[0]])
    if len(items) > 1:  # call update_tree() with remaining ordered items
        update_tree(items[1::], in_tree.children[items[0]], header_table, count)


def update_header(node_to_test, target_node):
    """
    Update header table : Add a new node to an existing link list
    :param node_to_test: One of existing node in the link list
    :param target_node: A new node to add
    :return: None
    """
    while node_to_test.nodeLink:
        node_to_test = node_to_test.nodeLink
    node_to_test.nodeLink = target_node


def load_company_data(file):
    """
    Load company data
    :param file: The path of company json
    :return: Return company count json file as a list of tuples
    """
    try:
        with open(file) as json_file:
            data_ = json.load(json_file)
    except FileNotFoundError:
        file = os.path.join(os.environ["HOME"], "Downloads", "data", "company_count.json")
        with open(file) as json_file:
            data_ = json.load(json_file)
    sorted_x = sorted(data_.items(), key=operator.itemgetter(1), reverse=True)
    data = []

    checker = LocTrie().is_loc

    # Splitting regex : split hyphens but keep hyphens between English words
    re_split = re.compile('(?<=[\u4e00-\u9fa5,0-9])-(?=[\u4e00-\u9fa50-9A-Z])')
    for o_line, _ in sorted_x:
        line = o_line.replace('（', '(')
        line = line.replace('）', ')')
        line = line.replace('，', ',')
        pars = re.findall('\\(([^())]+)\\)', line)
        for p in pars:
            '''
            For law firms
            '''
            if '特殊普通合伙' == p:
                continue
            line = line.replace('(' + p + ')', '')
            if not (checker(p) or len(p) < 2 or re.match(r'^集团', p) or 'China' == p or '亚洲' == p or '国际' == p
                    or '实习' == p):
                line = line + '-' + p
        lst = re_split.split(line)
        name_set = set()
        if len(lst) > 1:
            data.append((lst, data_[o_line]))
            name_set.update(lst)

    return data


def create_initset(dataset):
    """
    Create initial dataset list and return a dictionary
    :param dataset: The original dataset, a list of tuples
    :return: A dictionary containing frozenset and count pairs
    """
    ret_dict = collections.defaultdict(int)

    for trans in dataset:
        ret_dict[frozenset(trans[0])] = trans[1]
    return ret_dict


def ascend_tree(leaf, prefix_path):  # ascends from leaf node to root
    """
    Traceback a prefix root recursively, concatenate prefix_path and traced path in prefix_path
    :param leaf: The leaf node to start with
    :param prefix_path: the prefix path to append to
    :return: None
    """
    if leaf.parent:
        prefix_path.append(leaf.name)
        ascend_tree(leaf.parent, prefix_path)


def find_prefix_path(node):  # treeNode comes from header table
    """
    :param node treeNode comes from header table
    :return conditions prefix paths dictionary with counts as its values
    """
    cond_pats = {}
    while node:
        prefix_path = []
        ascend_tree(node, prefix_path)
        if len(prefix_path) > 1:
            cond_pats[frozenset(prefix_path[1:])] = node.count
        # Move onto the next node Link
        node = node.nodeLink
    return cond_pats


def mine_tree(header_table, min_sup, prefix, freq_item_list):
    """
    Mine a established FP-tree or conditional FP-tree
    :param header_table: Header table for link lists
    :param min_sup: the minimum supporting number
    :param prefix: the prefix path / patterns, the frequency pattern mining process conditioned on
    :param freq_item_list: result
    :return: None
    """
    sorted_header = [v for v in sorted(header_table, key=lambda itm: header_table[itm][0])]  # (sort header table)
    # Start from bottom of header table
    for basePat in sorted_header:
        # Create a hard copy of prefix path
        new_freq_set = prefix.copy()
        new_freq_set.add(basePat)
        freq_item_list.append(new_freq_set)
        cond_patt_bases = find_prefix_path(header_table[basePat][1])
        # 2. construct cond FP-tree from cond. pattern base
        my_cond_tree, my_head = create_tree(cond_patt_bases, min_sup)
        # print 'head from conditional tree: ', myHead
        if my_head:  # 3. mine cond. FP-tree
            mine_tree(my_head, min_sup, new_freq_set, freq_item_list)


if __name__ == '__main__':
    default_path = os.path.join("/tmp", "company_count.json")
    parser = argparse.ArgumentParser(description="Populate company_aliases collection in ai_service")
    parser.add_argument("--path",
                        default=default_path,
                        help="Path to a json file")
    parser.add_argument("--min",
                        default=10,
                        help="Minimum supporting number for FP-growth")

    args = parser.parse_args()

    print(20 * '=', 'Loading data', 20 * '=')
    data = load_company_data(args.path)
    min_sup = args.min
    print(15 * '=', 'Start data association', 15 * '=')
    start = timeit.default_timer()
    init_set = create_initset(data)
    fptree, my_header_tab = create_tree(init_set, min_sup)

    freq_item = []
    mine_tree(my_header_tab, min_sup, set([]), freq_item)

    uf = UnionFind(list(my_header_tab.keys()))

    for edge in freq_item:
        if len(edge) > 1:
            tmp = list(edge)
            for i in range(len(tmp) - 1):
                uf.union(tmp[i], tmp[i + 1])
    dd = uf.component_mapping()

    """
    Dump company aliases to mongodb
    """
    myclient = pymongo.MongoClient("mongodb://10.0.0.21:27017/")
    mydb = myclient["ai_search_engine"]
    mycol = mydb["company_aliases"]

    dictlist = []
    for key, value in dd.items():
        if len(value) > 1:
            '''try:
                temp = {'name': key, 'candidates': [{'name': v, 'score': float(support_dict[frozenset({v, key})]) /
                                                                         float(support_dict[frozenset([v])])} for v in
                                                    value]}
            except (ValueError, NameError):'''
            temp = {'name': key, 'candidates': [{'name': v, 'score': 0.0} for v in value]}
            dictlist.append(temp)
    mycol.insert_many(dictlist)
    print('Total Merging time:', timeit.default_timer() - start, 'seconds')
