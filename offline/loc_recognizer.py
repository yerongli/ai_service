"""
A normalizer for locations.
"""

from __future__ import division
from __future__ import print_function
from __future__ import absolute_import

__author__ = 'Hypnosis.Yuan'
__all__ = ['loc_recognizer']

import json
import time
#print(time.time_ns()/10**9-time.time())
from typing import List, Tuple, Iterable, Hashable
from os.path import dirname

loc_dir = f'{dirname(__file__)}/location.json'
SKIP_WORD = {'特别行政区直辖行政区',
             '省直辖县级行政区划',
             '准省辖市',
             '市辖区',
             '新城区',
             '郊区',
             '矿区',
             '城区', }
LOC_DICT = json.load(open(loc_dir, 'r'))


class Trie:
    def __init__(self, word_seq: 'seq(word, leaf)' = None):
        self.root = dict()
        if word_seq:
            for _ in word_seq:
                self.insert(*_)

    def insert(self, word: Iterable[Hashable], leaf: any = None):
        node, temp = self.root, []
        for char in word:
            if char not in node:
                node[char] = dict()
            node = node[char]
        if 1 in node:
            node[1]['leaf'].append(leaf)
        else:
            node[1] = {'word': word, 'leaf': [leaf]}

    def start_with(self, word: str, if_rest: bool = False) \
            -> Tuple[dict, str]:
        res, node = None, self.root
        for char in word:
            if char in node:
                node = node[char]
            else:
                break
            if 1 in node:
                res = node[1]
        if if_rest:
            return res, word[len(res['word']) if res else 1:]
        else:
            return res

    def search(self, word):
        if word:
            res, rest = self.start_with(word, if_rest=True)
            if res:
                return res['word']
                # return [res, *self.search(rest)]
            else:
                return self.search(rest)
        return []


class RelTrie(Trie):
    def __iter__(self):
        def _recur(node: dict, pre_leaves: list = None):
            if list(node) == [1]:
                if pre_leaves:
                    yield node[1]['word'], pre_leaves + node[1]['leaf']
                else:
                    yield node[1]['word'], node[1]['leaf']
            else:
                if 1 in node:
                    if pre_leaves:
                        pre_leaves = pre_leaves + node[1]['leaf']
                    else:
                        pre_leaves = node[1]['leaf']
                for _k, _v in node.items():
                    if _k != 1:
                        for res in _recur(_v, pre_leaves):
                            yield res

        return _recur(self.root)


class LocTrie(Trie):
    def __init__(self):
        self.loc_dict = LOC_DICT
        super().__init__((_n, tuple(_k[i:i + 2] for i in range(0, len(_k), 2)))
                         for _k, _v in self.loc_dict.items()
                         for _n in _v['namesake'])

    @staticmethod
    def __combine_rel(relations: Iterable[List[tuple]]) \
            -> List[Tuple[str]]:
        rel_trie = RelTrie()
        for i, rel_lst in enumerate(relations):
            for rel in rel_lst:
                rel_trie.insert(word=rel, leaf=i)
        res, max_count = [], 0
        for name, leaf in rel_trie:
            count = len(set(leaf))
            if count > max_count:
                res, max_count = [name], count
            elif count == max_count:
                res.append(name)
        return res
    def is_loc(self, word):
        tmp = self.search(word)
        #print(len(tmp), 'search length')
        #print(tmp != None, 'tmp != None')
        #print(len(tmp) > 0, 'len(tmp) > 0')
        return tmp != None and len(tmp) > 0

    def normalize_location(self, word):
        print(word, 'normalize_location')
        leaf_iter = ([_l for _l in _n['leaf']]
                     for _n in self.search(word))
        print(self.search(word))
        return 
        '''
        return [tuple(self.loc_dict[''.join(_k[:i + 1])]['name']
                      for i in range(len(_k)))
                for _k in self.__combine_rel(leaf_iter) if _k]
        '''

loc_recognizer = LocTrie().normalize_location

if __name__ == '__main__':
    print(loc_recognizer('BUND18(外滩18号) 精品店'))