from grpc import insecure_channel

from ai_service_protos.feedback_pb2_grpc import feedbackStub
from config import ai_service_config

resume_recommend_engine_channel = insecure_channel(
    target=ai_service_config['FEEDBACK_ENGINE_ADDRESS'])

feedback_stub: feedbackStub = feedbackStub(resume_recommend_engine_channel)
