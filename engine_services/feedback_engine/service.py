from grpcalchemy.blueprint import Blueprint, Context

from common.feedback.manage import FeedbackManage
from config import ai_service_config
from grpc_models import NormalStatusResponse
from grpc_models.ai_feedback_model import JdResumeFeedbackMessage
from utils.ai_server import ai_server

feedback_manage = FeedbackManage()
app = ai_server("feedback_engine", config=ai_service_config)

feedback = Blueprint("feedback")


# 用户反馈，jd_index-resume_index 匹配
@feedback.register
def FeedbackJdResumeMatch(request: JdResumeFeedbackMessage,
                          context: Context) -> NormalStatusResponse:
    feedback_manage.feedback_jd_resume_match(
        tenant=request.tenant,
        jd_aid=request.jd_aid,
        resume_aid_list=request.resume_aid_list)
    return NormalStatusResponse(status=True, result="用户反馈成功")


# 用户反馈，jd_index-resume_index 不匹配
@feedback.register
def FeedbackJdResumeMismatch(request: JdResumeFeedbackMessage,
                             context: Context) -> NormalStatusResponse:
    feedback_manage.feedback_jd_resume_mismatch(
        tenant=request.tenant,
        jd_aid=request.jd_aid,
        resume_aid_list=request.resume_aid_list)
    return NormalStatusResponse(status=True, result="用户反馈成功")
