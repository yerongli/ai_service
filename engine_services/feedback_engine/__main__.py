from service_checker.contrib import grpc as grpc_check

from config import ai_service_config
from engine_services.feedback_engine.service import app, feedback
from utils.health_checker import snooper

grpc_check.install(
    snooper=snooper,
    name='gllue_data',
    address=
    f'{ai_service_config["GLLUE_DATA_HOST"]}:{ai_service_config["GLLUE_DATA_PORT"]}'
)
grpc_check.server_install(snooper=snooper, server=app)
if __name__ == '__main__':

    app.register_blueprint(feedback)
    app.run(port=app.config["FEEDBACK_ENGINE_PORT"])
