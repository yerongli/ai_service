from grpcalchemy.blueprint import Blueprint, Context

from grpc_models import NormalTextRequest, NormTextIntMappingMessage
from grpc_models.ai_major_model import CellPredictRequest, \
    CellPredictResponse, LowestDegreeMajorPredictRequest, \
    LowestDegreeMajorPredictResponse

from .service import search_engine

major_classify_engine = Blueprint("major_classify_engine")
major_multi_classify = search_engine.load_major_classify()
major_multi_classify.all_tag = list()
major_multi_classify.extract_tags()


@major_classify_engine.register
def CellPredict(request: CellPredictRequest,
                context: Context) -> CellPredictResponse:
    result = major_multi_classify.sub_classify[request.level].predict(
        request.major)
    return CellPredictResponse(result=result)


@major_classify_engine.register
def LowestDegreeMajorPredict(
        request: LowestDegreeMajorPredictRequest,
        context: Context) -> LowestDegreeMajorPredictResponse:
    # 根据 最低 学历要求，major 归类major
    major_list = request.major_list
    lowest_degree = request.lowest_degree

    result = set()
    query_major = ['专科', '本科', '硕士']
    if lowest_degree == '':
        major_index = 0
    elif lowest_degree == '博士':
        major_index = 2
    else:
        major_index = query_major.index(lowest_degree)
    for i in range(major_index, 3):
        m = query_major[i]
        for j in major_list:
            try:
                r = major_multi_classify.sub_classify[m].predict(j)
                result.add(r)
            except ValueError:
                continue
    else:
        result = [i for i in result if i]
        result = LowestDegreeMajorPredictResponse(result=result)
        return result


@major_classify_engine.register
def GetMajor2Id(request: NormalTextRequest,
                context: Context) -> NormTextIntMappingMessage:
    result = major_multi_classify.all_tag2id
    return NormTextIntMappingMessage(result=result)


@major_classify_engine.register
def GetMajorParent2Id(request: NormalTextRequest,
                      context: Context) -> NormTextIntMappingMessage:
    result = major_multi_classify.all_parent2id
    return NormTextIntMappingMessage(result=result)
