from grpc import insecure_channel

from ai_service_protos import extract_poi_engine_pb2_grpc, \
    lda_predict_engine_pb2_grpc, major_classify_engine_pb2_grpc, \
    title_multi_classify_engine_pb2_grpc
from config import ai_service_config

description_engine_channel = insecure_channel(
    target=ai_service_config['DESCRIPTION_ENGINE_ADDRESS'])

extract_poi_engine_stub = extract_poi_engine_pb2_grpc.extract_poi_engineStub(
    description_engine_channel)
lda_predict_engine_stub = lda_predict_engine_pb2_grpc.lda_predict_engineStub(
    description_engine_channel)

title_multi_classify_engine_stub = title_multi_classify_engine_pb2_grpc.title_multi_classify_engineStub(
    description_engine_channel)
major_classify_engine_stub = major_classify_engine_pb2_grpc.major_classify_engineStub(
    description_engine_channel)
