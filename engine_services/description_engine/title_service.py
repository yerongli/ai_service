from grpcalchemy.blueprint import Blueprint, Context

from grpc_models import (NormalTextRequest, NormalTextResponse,
                         NormTextIntMappingMessage, NormTextListMessage,
                         NormTextListResponse, NormTextTextMappingMessage)
from grpc_models.ai_title_model import (TitleMultiplePredictMeta,
                                        TitleMultiplePredictResponse)

from .service import search_engine

title_multi_classify_engine = Blueprint("title_multi_classify_engine")
# title_multi_classify = search_engine.load_title_multi_classify()
# title_multi_classify.all_tag = list()
# title_multi_classify.extract_tags()

title_textcnn_classify = search_engine.load_title_textcnn_classify()
title_textcnn_classify.predict(['python开发工程师'])  # for multiprocessing fork


@title_multi_classify_engine.register
def MultiplePredict(request: NormTextListMessage,
                    context: Context) -> TitleMultiplePredictResponse:
    result = title_textcnn_classify.predict(request.text_list)
    items = [
        TitleMultiplePredictMeta(pred=i['pred'], pred_prob=i['pred_prob'])
        for i in result
    ]
    result = TitleMultiplePredictResponse(items=items)
    return result


@title_multi_classify_engine.register
def Predict(request: NormalTextRequest,
            context: Context) -> NormalTextResponse:
    result = title_textcnn_classify.predict([request.text])
    return NormalTextResponse(text=result[0]['pred'])


@title_multi_classify_engine.register
def PredictProbTop(request: NormalTextRequest,
                   context: Context) -> NormTextListResponse:
    result = title_textcnn_classify.predict([request.text])
    return NormTextListResponse(result=list(result[0]['pred_prob'].keys()))


@title_multi_classify_engine.register
def GetTitle2Id(request: NormalTextRequest,
                context: Context) -> NormTextIntMappingMessage:
    result = title_textcnn_classify.title2id
    return NormTextIntMappingMessage(result=result)


@title_multi_classify_engine.register
def GetTitleParent2Id(request: NormalTextRequest,
                      context: Context) -> NormTextIntMappingMessage:
    title2id = title_textcnn_classify.title2id
    title_parent_list = list(
        {'-'.join(i.split('-')[:-1])
         for i in title2id.keys()})
    title_parent_list.sort()
    result = {v: i for i, v in enumerate(title_parent_list)}
    return NormTextIntMappingMessage(result=result)


@title_multi_classify_engine.register
def GetTitle2Parent(request: NormalTextRequest,
                    context: Context) -> NormTextTextMappingMessage:
    parent_dict = {
        i: '-'.join(i.split('-')[:-1])
        for i in title_textcnn_classify.title2id.keys()
    }
    return NormTextTextMappingMessage(result=parent_dict)
