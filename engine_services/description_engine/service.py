from gllue_ai_libs import search_engine
from grpcalchemy.blueprint import Blueprint, Context

from config import ai_service_config
from grpc_models import NormalTextRequest, NormalTitleDescriptionRequest, \
    NormIntFloatDictListResponse, NormTextFloatDictListResponse, \
    NormTextIntMappingMessage
from grpc_models.ai_meta_model import IntFloatDictMeta, TextFloatDictMeta
from utils.ai_server import ai_server

search_engine.set_model_dir_path(ai_service_config['MODEL_PATH'])
search_engine.set_mongo_config(
    db=ai_service_config['MONGO_DB'],
    host=ai_service_config['MONGO_HOST'],
    port=ai_service_config['MONGO_PORT'],
    username=ai_service_config['MONGO_USER'],
    password=ai_service_config['MONGO_PASSWORD'])

extract_poi = search_engine.load_extract_poi()
lda_predict = search_engine.load_lda_predict()

app = ai_server("description_engine", config=ai_service_config)

extract_poi_engine = Blueprint("extract_poi_engine")
lda_predict_engine = Blueprint("lda_predict_engine")


@extract_poi_engine.register
def DescriptionPoiSortWithTitle(
        request: NormalTitleDescriptionRequest,
        context: Context) -> NormTextFloatDictListResponse:
    poi_weight_list = extract_poi.description_poi_sort_with_title(
        title=request.title, desc=request.description)
    result = list()
    for poi_weight in poi_weight_list:
        result.append(
            TextFloatDictMeta(index=poi_weight[0], value=poi_weight[1]))
    response = NormTextFloatDictListResponse(result=result)
    return response


@extract_poi_engine.register
def GetPoi2Id(request: NormalTextRequest,
              context: Context) -> NormTextIntMappingMessage:
    poi2id = {poi: i + 1 for i, poi in enumerate(extract_poi.poi_set)}
    return NormTextIntMappingMessage(result=poi2id)


@lda_predict_engine.register
def GetTextLda(request: NormalTitleDescriptionRequest,
               context: Context) -> NormIntFloatDictListResponse:
    topic_weight_list = lda_predict.get_text_lda(
        title=request.title, content=request.description)
    result = list()
    for topic_weight in topic_weight_list:
        result.append(
            IntFloatDictMeta(index=topic_weight[0], value=topic_weight[1]))
    response = NormIntFloatDictListResponse(result=result)
    return response
