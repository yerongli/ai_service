from service_checker.contrib import grpc as grpc_check

from engine_services.description_engine.major_service import \
    major_classify_engine
from engine_services.description_engine.service import (
    app, extract_poi_engine, lda_predict_engine)
from engine_services.description_engine.title_service import \
    title_multi_classify_engine
from utils.health_checker import snooper

grpc_check.server_install(snooper=snooper, server=app)
if __name__ == '__main__':
    app.register_blueprint(extract_poi_engine)
    app.register_blueprint(lda_predict_engine)
    app.register_blueprint(title_multi_classify_engine)
    app.register_blueprint(major_classify_engine)

    app.run(port=app.config["DESCRIPTION_ENGINE_PORT"])
