from grpc import insecure_channel

from ai_service_protos.duplicate_checking_pb2_grpc import \
    duplicate_checkingStub
from ai_service_protos.resume_features_pb2_grpc import resume_featuresStub
from ai_service_protos.resume_recommend_pb2_grpc import resume_recommendStub
from config import ai_service_config

resume_recommend_engine_channel = insecure_channel(
    target=ai_service_config['RESUME_RECOMMEND_ENGINE_ADDRESS'])

resume_recommend_stub: resume_recommendStub = resume_recommendStub(
    resume_recommend_engine_channel)
resume_features_stub = resume_featuresStub(resume_recommend_engine_channel)
duplicate_checking_stub = duplicate_checkingStub(
    resume_recommend_engine_channel)
