from service_checker.contrib import elasticsearch
from service_checker.contrib import grpc as grpc_check

from config import ai_service_config
from engine_services.resume_recommend_engine.service import (
    app, duplicate_checking, resume_features, resume_recommend)
from utils.health_checker import snooper

grpc_check.server_install(snooper=snooper, server=app)

grpc_check.install(
    snooper=snooper,
    name='gllue_data',
    address=
    f'{ai_service_config["GLLUE_DATA_HOST"]}:{ai_service_config["GLLUE_DATA_PORT"]}'
)
grpc_check.install(snooper=snooper,
                   name='COMPANY_ENGINE'.lower(),
                   address=ai_service_config['COMPANY_ENGINE_ADDRESS'])
grpc_check.install(snooper=snooper,
                   name='DESCRIPTION_ENGINE'.lower(),
                   address=ai_service_config['DESCRIPTION_ENGINE_ADDRESS'])
grpc_check.install(snooper=snooper,
                   name='JD_ENGINE'.lower(),
                   address=ai_service_config['JD_ENGINE_ADDRESS'])
grpc_check.install(snooper=snooper,
                   name='JD_INDEX_ENGINE'.lower(),
                   address=ai_service_config['JD_INDEX_ENGINE_ADDRESS'])
grpc_check.install(snooper=snooper,
                   name='RESUME_INDEX_ENGINE'.lower(),
                   address=ai_service_config['RESUME_INDEX_ENGINE_ADDRESS'])
# grpc_check.install(
#     snooper=snooper,
#     name='RESUME_RECOMMEND_ENGINE'.lower(),
#     address=ai_service_config['RESUME_RECOMMEND_ENGINE_ADDRESS'])
grpc_check.install(snooper=snooper,
                   name='FEEDBACK_ENGINE'.lower(),
                   address=ai_service_config['FEEDBACK_ENGINE_ADDRESS'])
grpc_check.install(snooper=snooper,
                   name='SCHOOL_ENGINE'.lower(),
                   address=ai_service_config['SCHOOL_ENGINE_ADDRESS'])
elasticsearch.install(snooper=snooper,
                      hosts=[{
                          "host": ai_service_config['ES_HOST'],
                          "port": ai_service_config['ES_PORT']
                      }])

if __name__ == '__main__':
    app.register_blueprint(resume_recommend)
    app.register_blueprint(resume_features)
    app.register_blueprint(duplicate_checking)
    app.run(port=app.config["RESUME_RECOMMEND_ENGINE_PORT"])
