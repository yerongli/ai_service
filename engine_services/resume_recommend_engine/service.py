import datetime

from grpcalchemy.blueprint import Blueprint, Context

from common.resume_duplicate_check import manage as resume_duplicate_check_util
from common.resume_recommend import rule_filter
from common.resume_recommend.manage import batch_score_jd_cv
from common.resume_similar import manage as resume_similar_util
from config import ai_service_config
from ctr_recommend.ml import manage as resume_recommend_util
from database.elasticsearch.models import ResumeFeatures
from database.mongodb import joborder_find, jobsubmission_find
from database.mongodb.models import (DuplicateResumeResult,
                                     RecommendResumeFromJdInfoResult,
                                     SimilarResumeResult)
from grpc_models import NormAidTenantRequest
from grpc_models.ai_meta_model import BaseAidTenantMessage, TextFloatDictMeta
from grpc_models.ai_resume_recommend_model import (
    BatchScoreJdCVResponse, DuplicateResumeCheckingFromResumeIdMessage,
    DuplicateResumeCheckingFromResumeMessage,
    DuplicateResumeCheckingPreciselyMessage, DuplicateResumeCheckingResponse,
    RecommendResumeFromJdInfoRequest, RecommendResumeHomePageRequest,
    RecommendResumeHomePageResponse, RecommendResumeResponse, ScoreJdCVMessage,
    ScoreJdCVResponse, SimilarResumeFromResumeIdMessage,
    SimilarResumeFromResumeMessage, SimilarResumeFromResumeResponse,
    TextFloatDictListResponse)
from utils.ai_server import ai_server
from utils.api_utils import paging_items

app = ai_server("resume_recommend_engine", config=ai_service_config)

resume_recommend = Blueprint("resume_recommend")
duplicate_checking = Blueprint("duplicate_checking")
resume_features = Blueprint("resume_features")


@resume_recommend.register
def GetRecommendResumeHomePage(request: RecommendResumeHomePageRequest,
                               context: Context
                               ) -> RecommendResumeHomePageResponse:
    """
    首页推荐 api，根据当前租户和用户，获取用户首页的推荐列表。
    获取当前用户所有未被删除且状态在 Live 的 joborder。根据joborder从 RecommendResumeFromJdInfoResult 表中获取最新的推荐缓存。
    聚合所有joborder的推荐结果，按分数排序。返回结果
    :param request:
    :param context:
    :return:
    """
    tenant = request.tenant
    topn = request.topn
    user = request.user
    now = datetime.datetime.now()

    # mongodb 的 pipeline，取出create_time 最新的items
    pipeline = [{
        "$group": {
            "_id": "$jd_aid",
            "ct": {
                "$max": "$create_time"
            },
            "items": {
                "$last": "$items"
            }
        }
    }, {
        "$project": {
            "_id": 1,
            "ct": 1,
            "items": 1
        }
    }]

    # 找到所有符合条件的 joborders
    joborders = joborder_find(query={
        'client_key': tenant,
        'users.user': user,
        'is_deleted': False,
        'jobStatus': 'Live'
    },
                              projection={'id': 1})
    joborder_id = list({i['id'] for i in joborders})

    data = list(
        RecommendResumeFromJdInfoResult.objects(
            tenant=tenant, jd_aid__in=joborder_id).aggregate(*pipeline))

    dont_see_aid_set = rule_filter.get_jds_dont_see_resume_aid_set(
        tenant=tenant, jd_aids=[rr['_id'] for rr in data])

    # 聚合所有 推荐结果
    result = list()
    for rr in data:
        if not rr['items']:
            continue
        items = [
            i for i in rr['items']
            if i['aid'] not in dont_see_aid_set.get(rr['_id'], [])
        ]
        if items:
            for item in items:
                result.append({
                    'jd_aid': rr['_id'],
                    'resume_aid': item['aid'],
                    'score': item['score'],
                    'reason': item['reason'],
                    'tenant': tenant
                })

    # 按分数排序
    result.sort(key=lambda x: x['score'], reverse=True)

    result = RecommendResumeHomePageResponse(items=result[:topn])
    return result


# 根据 jd_info 推荐 简历
@resume_recommend.register
def RecommendResumeFromJdInfo(request: RecommendResumeFromJdInfoRequest,
                              context: Context) -> RecommendResumeResponse:
    """
    根据jd信息推荐简历。根据 refresh 参数判断是否使用缓存数据。
    :param request:
    :param context:
    :return:
    """

    # 重新推荐
    def refresh():
        """
        重新推荐。然后缓存到 RecommendResumeFromJdInfoResult 表。并执行分页
        :return:
        """
        # TODO
        recommend_result = resume_recommend_util.recommend_resume_from_jd_info(
            jd_info=ri['jd_info'], topn=ri['topn'], min_score=ri['min_score'])

        items = recommend_result['items']
        rr = RecommendResumeFromJdInfoResult()
        rr.link_uuid = recommend_result['link_uuid']
        rr.jd_aid = jd_info['aid']
        rr.tenant = jd_info['tenant']
        rr.jd_version = jd_info['version']
        rr.items = items
        rr.save()

        items = rr.filter_items(
            rule_filter.get_jd_dont_see_resume_aid_set(tenant=rr.tenant,
                                                       jd_aid=rr.jd_aid))

        items, total, size = paging_items(items,
                                          size=ri['size'],
                                          offset=ri['offset'])
        rst = {
            "recommend_id": str(rr.id),
            "items": items,
            "total": total,
            "size": size,
        }
        return rst

    # 调用缓存数据
    def paging():
        rr = RecommendResumeFromJdInfoResult.objects(
            jd_aid=jd_info['aid'],
            tenant=jd_info['tenant'],
            jd_version=jd_info['version']).order_by("-create_time").limit(
                1).first()
        if rr:
            items = rr.filter_items(
                rule_filter.get_jd_dont_see_resume_aid_set(tenant=rr.tenant,
                                                           jd_aid=rr.jd_aid))
            items, total, size = paging_items(items,
                                              size=ri['size'],
                                              offset=ri['offset'])
            rst = {
                "recommend_id": str(rr.id),
                "items": items,
                "total": total,
                "size": size,
            }
            return rst
        else:
            # 如果当前 jd 没有缓存数据，则重新推荐
            return refresh()

    ri = request.message_to_dict(preserving_proto_field_name=True,
                                 including_default_value_fields=True)
    jd_info = ri['jd_info']

    if ri['refresh']:
        result = refresh()
    else:
        result = paging()

    response = RecommendResumeResponse(**result)
    return response


# jd resume匹配打分
@resume_recommend.register
def ScoreJdCV(request: ScoreJdCVMessage,
              context: Context) -> ScoreJdCVResponse:
    ri = request.message_to_dict(preserving_proto_field_name=True,
                                 including_default_value_fields=True)
    rst = resume_recommend_util.score_jd_cv(jd_info=ri['jd_info'],
                                            resume_info=ri['resume_info'])
    result = ScoreJdCVResponse(score=rst)
    return result


@resume_recommend.register
def BatchScoreJdCV(request: BaseAidTenantMessage,
                   context: Context) -> BatchScoreJdCVResponse:
    data = jobsubmission_find(query={
        'client_key': request.tenant,
        'joborder': request.aid
    },
                              projection={
                                  'candidate': 1,
                                  '_id': 0,
                              })
    resume_id_list = []
    for job_submission in data:
        if job_submission['candidate']:
            resume_id_list.append(job_submission['candidate'])
    return BatchScoreJdCVResponse(
        result=batch_score_jd_cv(tenant=request.tenant,
                                 jd_aid=request.aid,
                                 resume_id_list=resume_id_list))


# 根据resume_id推荐相似简历
@resume_recommend.register
def SimilarResumeFromResumeId(request: SimilarResumeFromResumeIdMessage,
                              context: Context
                              ) -> SimilarResumeFromResumeResponse:
    ri = request.message_to_dict(preserving_proto_field_name=True,
                                 including_default_value_fields=True)
    recommend_result = resume_similar_util.similar_resume_from_resume_id(
        resume_info=ri['resume_info'],
        topn=ri['topn'],
        min_score=ri['min_score'])

    sr = SimilarResumeResult()
    sr._id = recommend_result['link_uuid']
    sr.resume_info = ri['resume_info']
    sr.tenant = ri['resume_info']['tenant']
    sr.items = recommend_result['items']
    sr.save()

    result = SimilarResumeFromResumeResponse(items=recommend_result['items'],
                                             recommend_id=str(sr.id))
    return result


# 根据简历信息推荐相似简历
@resume_recommend.register
def SimilarResumeFromResume(request: SimilarResumeFromResumeMessage,
                            context: Context
                            ) -> SimilarResumeFromResumeResponse:
    ri = request.message_to_dict(preserving_proto_field_name=True,
                                 including_default_value_fields=True)
    recommend_result = resume_similar_util.similar_resume_from_resume(
        resume_info=ri['resume_info'],
        topn=ri['topn'],
        min_score=ri['min_score'])

    sr = SimilarResumeResult()
    sr._id = recommend_result['link_uuid']
    sr.resume_info = ri['resume_info']
    sr.tenant = ri['resume_info']['tenant']
    sr.items = recommend_result['items']
    sr.save()

    result = SimilarResumeFromResumeResponse(items=recommend_result['items'],
                                             recommend_id=str(sr.id))
    return result


# 根据简历id查重
@duplicate_checking.register
def FromResumeId(request: DuplicateResumeCheckingFromResumeIdMessage,
                 context: Context) -> DuplicateResumeCheckingResponse:
    ri = request.message_to_dict(preserving_proto_field_name=True,
                                 including_default_value_fields=True)
    recommend_result = resume_duplicate_check_util.resume_duplicate_checking_from_resume_id(
        resume_info=ri['resume_info'],
        topn=ri['topn'],
        min_score=ri['min_score'])

    # 记录
    dr = DuplicateResumeResult()
    dr.link_uuid = recommend_result['link_uuid']
    dr.resume_info = ri['resume_info']
    dr.tenant = ri['resume_info']['tenant']
    dr.items = recommend_result['items']
    dr.save()

    result = DuplicateResumeCheckingResponse(items=recommend_result['items'],
                                             recommend_id=str(dr.id))
    return result


# 根据简历信息查重
@duplicate_checking.register
def FromResume(request: DuplicateResumeCheckingFromResumeMessage,
               context: Context) -> DuplicateResumeCheckingResponse:
    ri = request.message_to_dict(preserving_proto_field_name=True,
                                 including_default_value_fields=True)
    recommend_result = resume_duplicate_check_util.resume_duplicate_checking_from_resume(
        resume_info=ri['resume_info'],
        topn=ri['topn'],
        min_score=ri['min_score'])

    # 记录
    dr = DuplicateResumeResult()
    dr.link_uuid = recommend_result['link_uuid']
    dr.resume_info = ri['resume_info']
    dr.tenant = ri['resume_info']['tenant']
    dr.items = recommend_result['items']
    dr.save()

    result = DuplicateResumeCheckingResponse(items=recommend_result['items'],
                                             recommend_id=str(dr.id))
    return result


# 精准查重
@duplicate_checking.register
def PreciselyFromResume(request: DuplicateResumeCheckingPreciselyMessage,
                        context: Context) -> DuplicateResumeCheckingResponse:
    ri = request.message_to_dict(preserving_proto_field_name=True,
                                 including_default_value_fields=True)
    recommend_result = resume_duplicate_check_util.precision_duplicate_checking(
        ri)

    # 记录
    dr = DuplicateResumeResult()
    dr.link_uuid = recommend_result['link_uuid']
    dr.resume_info = {
        'email': ri['email'],
        'mobile': ri['mobile'],
    }
    dr.tenant = ri['tenant']
    dr.items = recommend_result['items']
    dr.save()

    result = DuplicateResumeCheckingResponse(items=recommend_result['items'],
                                             recommend_id=str(dr.id))
    return result


# 获取POI
@resume_features.register
def GetResumePoiList(request: NormAidTenantRequest,
                     context: Context) -> TextFloatDictListResponse:
    rst = ResumeFeatures.get_poi_list_with_tenant_and_aid(
        request.tenant, request.aid)
    if not rst:
        return TextFloatDictListResponse()
    items = list()
    for poi_weight in rst:
        items.append(
            TextFloatDictMeta(index=poi_weight['index'],
                              value=poi_weight['value']))
    response = TextFloatDictListResponse(items=items)
    return response
