from service_checker.contrib import grpc as grpc_check

from config import ai_service_config
from engine_services.school_engine.service import app
from utils.health_checker import snooper

grpc_check.install(
    snooper=snooper,
    name='gllue_data',
    address=
    f'{ai_service_config["GLLUE_DATA_HOST"]}:{ai_service_config["GLLUE_DATA_PORT"]}'
)
grpc_check.server_install(snooper=snooper, server=app)
if __name__ == '__main__':
    app.run(port=app.config["SCHOOL_ENGINE_PORT"])
