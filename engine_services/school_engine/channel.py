from grpc import insecure_channel

from ai_service_protos import school_engine_pb2_grpc
from config import ai_service_config

school_engine_channel = insecure_channel(
    target=ai_service_config['SCHOOL_ENGINE_ADDRESS'])

school_engine_stub = school_engine_pb2_grpc.school_engineStub(
    school_engine_channel)
