from gllue_ai_libs import search_engine
from grpcalchemy.blueprint import Context

from config import ai_service_config
from grpc_models import NormalTextRequest, NormIntIntMappingMessage, NormTextListMessage
from grpc_models.ai_school_model import (
    BatchSchoolSearchResponse,
    SchoolAidMessage,
    SchoolInfoDictResponse,
    SchoolSearchResponse,
)
from utils.ai_server import ai_server

search_engine.set_model_dir_path(ai_service_config["MODEL_PATH"])
search_engine.set_mongo_config(
    db=ai_service_config["MONGO_DB"],
    host=ai_service_config["MONGO_HOST"],
    port=ai_service_config["MONGO_PORT"],
    username=ai_service_config["MONGO_USER"],
    password=ai_service_config["MONGO_PASSWORD"],
)

# school_hinter = search_engine.load_school_remind()
school_searcher = search_engine.load_school_search_engine()
school_info_dict = search_engine.load_school_info_dict()

app = ai_server("school_engine", config=ai_service_config)


@app.register
def SchoolSearch(request: NormalTextRequest, context: Context) -> SchoolSearchResponse:
    from database.elasticsearch.models.school import SchoolFeatures

    try:
        rst = SchoolFeatures.search_list(request.text, k=2.0, n=1)[0]
        response = SchoolSearchResponse(
            aid=rst["aid"], ix=rst["ix"], score=rst["score"]
        )
    except IndexError:
        response = SchoolSearchResponse()

    return response


@app.register
def BatchSchoolSearch(
    request: NormTextListMessage, context: Context
) -> BatchSchoolSearchResponse:
    request = request.message_to_dict(
        preserving_proto_field_name=True, including_default_value_fields=True
    )
    result = list()
    for text in request["text_list"]:
        rst = school_searcher.search(text)
        if rst:
            res = SchoolSearchResponse(aid=rst["aid"], ix=rst["ix"], score=rst["score"])
        else:
            res = SchoolSearchResponse()
        result.append(res)
    response = BatchSchoolSearchResponse(items=result)
    return response


@app.register
def GetSchoolInfoDict(
    request: SchoolAidMessage, context: Context
) -> SchoolInfoDictResponse:
    result = school_info_dict.get(request.aid)
    if result:
        return SchoolInfoDictResponse(
            is_985=result.is_985,
            is_211=result.is_211,
            is_abroad=result.is_abroad,
            is_qs200=result.is_qs200,
        )
    else:
        return SchoolInfoDictResponse()


@app.register
def GetSchool2Id(
    request: NormalTextRequest, context: Context
) -> NormIntIntMappingMessage:
    school_ids = list(school_info_dict.data.keys())
    school_ids.sort()

    school2id = {s: i + 1 for i, s in enumerate(school_ids)}
    return NormIntIntMappingMessage(result=school2id)
