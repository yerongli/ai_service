import json

from gllue_ai_libs import search_engine
from grpcalchemy.blueprint import Context

from config import ai_service_config
from grpc_models import NormalTextRequest, NormalTextResponse
from utils.ai_server import ai_server

search_engine.set_model_dir_path(ai_service_config['MODEL_PATH'])
search_engine.set_mongo_config(
    db=ai_service_config['MONGO_DB'],
    host=ai_service_config['MONGO_HOST'],
    port=ai_service_config['MONGO_PORT'],
    username=ai_service_config['MONGO_USER'],
    password=ai_service_config['MONGO_PASSWORD'])

splitter = search_engine.load_query_split()

app = ai_server("splitter_engine", config=ai_service_config)


@app.register
def Split(request: NormalTextRequest, context: Context) -> NormalTextResponse:
    return NormalTextResponse(
        text=json.dumps(splitter.query2query(request.text)))
