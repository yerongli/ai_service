from grpc import insecure_channel

from ai_service_protos import splitter_engine_pb2_grpc
from config import ai_service_config

splitter_engine_channel = insecure_channel(
    target=ai_service_config['SPLITTER_ENGINE_ADDRESS'])

splitter_engine_stub = splitter_engine_pb2_grpc.splitter_engineStub(
    splitter_engine_channel)
