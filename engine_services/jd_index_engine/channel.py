from grpc import insecure_channel

from ai_service_protos import jd_process_engine_pb2_grpc
from config import ai_service_config

jd_index_engine_channel = insecure_channel(
    target=ai_service_config['JD_INDEX_ENGINE_ADDRESS'])

jd_process_engine_stub = jd_process_engine_pb2_grpc.jd_process_engineStub(
    jd_index_engine_channel)
