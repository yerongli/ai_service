from grpcalchemy.blueprint import Blueprint, Context

from config import ai_service_config
from database.elasticsearch.models import JobOrderFeatures
from grpc_models import (NormAidTenantRequest, NormalIdentRequest,
                         NormalStatusResponse, NormTenantRequest)
from utils.ai_server import ai_server

app = ai_server("jd_index_engine", config=ai_service_config)

jd_process_engine = Blueprint("jd_process_engine")


@jd_process_engine.register
def IdentUpdateJdInfo(request: NormalIdentRequest,
                      context: Context) -> NormalStatusResponse:
    JobOrderFeatures.ident_update(ident=request.ident)
    result = NormalStatusResponse(status=True, result="更新jd成功")
    return result


@jd_process_engine.register
def DeleteSingleJdInfo(request: NormAidTenantRequest,
                       context: Context) -> NormalStatusResponse:
    jd_info = request.message_to_dict(
        preserving_proto_field_name=True, including_default_value_fields=True)
    JobOrderFeatures.delete_with_tenant_and_aid(jd_info['tenant'],
                                                jd_info['aid'])
    response = NormalStatusResponse(status=True, result="删除jd成功")
    return response


@jd_process_engine.register
def ReindexAllJd(request: NormTenantRequest,
                 context: Context) -> NormalStatusResponse:
    JobOrderFeatures.all_update()
    result = NormalStatusResponse(status=True, result="更新所有 jd_index Index成功")
    return result


@jd_process_engine.register
def ReindexJdUseTenant(request: NormTenantRequest,
                       context: Context) -> NormalStatusResponse:
    JobOrderFeatures.tenant_update(tenant=request.tenant)
    result = NormalStatusResponse(
        status=True, result=f"更新租户[{request.tenant}] jd_index Index成功")
    return result
