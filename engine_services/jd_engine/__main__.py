from engine_services.jd_engine.service import app
from service_checker.contrib import grpc as grpc_check
from utils.health_checker import snooper

grpc_check.server_install(snooper=snooper, server=app)
if __name__ == '__main__':
    app.run(port=app.config["JD_ENGINE_PORT"])
