from grpc import insecure_channel

from ai_service_protos import jd_engine_pb2_grpc
from config import ai_service_config

jd_engine_channel = insecure_channel(
    target=ai_service_config['JD_ENGINE_ADDRESS'])

jd_engine_stub = jd_engine_pb2_grpc.jd_engineStub(jd_engine_channel)
