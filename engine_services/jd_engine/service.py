from gllue_ai_libs import search_engine
from grpcalchemy import Context

from config import ai_service_config
from grpc_models import NormalTextRequest
from grpc_models.ai_jd_model import ExtractContentInfoResponse
from utils.ai_server import ai_server

search_engine.set_model_dir_path(ai_service_config['MODEL_PATH'])
search_engine.set_mongo_config(
    db=ai_service_config['MONGO_DB'],
    host=ai_service_config['MONGO_HOST'],
    port=ai_service_config['MONGO_PORT'],
    username=ai_service_config['MONGO_USER'],
    password=ai_service_config['MONGO_PASSWORD'])

jd_process = search_engine.load_jd_process()

app = ai_server("jd_engine", config=ai_service_config)


@app.register
def ExtractContentInfo(request: NormalTextRequest,
                       context: Context) -> ExtractContentInfoResponse:
    result = jd_process.extract_content_info(jd_content=request.text)
    age_range = result['age_range']
    result['age_range'] = {'gte': age_range[0], 'lte': age_range[1]}

    working_year_range = result['working_year_range']
    result['working_year_range'] = {
        'gte': working_year_range[0],
        'lte': working_year_range[1]
    }

    result = ExtractContentInfoResponse(**result)
    return result
