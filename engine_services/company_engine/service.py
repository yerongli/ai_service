from gllue_ai_libs import search_engine
from grpcalchemy import Blueprint, Context

from config import ai_service_config
from grpc_models import NormalTextRequest, NormalTextResponse
from grpc_models.ai_company_model import CompanyFetchRequest, CompanyMessage, \
    CompanyQueryRequest, CompanyQueryResponse, \
    CompanySearchDuplicateCountRequest, CompanySearchDuplicateCountResponse, \
    CompanySearchResponse, CompanySearchRquest
from utils.ai_server import ai_server

search_engine.set_model_dir_path(ai_service_config['MODEL_PATH'])
search_engine.set_mongo_config(
    db=ai_service_config['MONGO_DB'],
    host=ai_service_config['MONGO_HOST'],
    port=ai_service_config['MONGO_PORT'],
    username=ai_service_config['MONGO_USER'],
    password=ai_service_config['MONGO_PASSWORD'])

tenant_searcher = search_engine.load_company_tenant_search_engine()

app = ai_server("company_engine", config=ai_service_config)

tenant_search_engine = Blueprint("tenant_search_engine")


@tenant_search_engine.register
def Search(request: CompanySearchRquest,
           context: Context) -> CompanySearchResponse:
    result = tenant_searcher.search_remind(
        tenant=request.tenant, name=request.name, cid=request.cid)
    return CompanySearchResponse(
        result=list(map(lambda x: CompanyMessage(**x), result)))


@tenant_search_engine.register
def SearchDuplicateCountWithCache(
        request: CompanySearchDuplicateCountRequest,
        context: Context) -> CompanySearchDuplicateCountResponse:
    result = tenant_searcher.batch_fetch_duplicate_count(
        tenant=request.tenant, ids=list(request.id_name_pairs.keys()))
    return CompanySearchDuplicateCountResponse(result=result)


@tenant_search_engine.register
def SearchDuplicateCountWithoutCache(
        request: CompanySearchDuplicateCountRequest,
        context: Context) -> CompanySearchDuplicateCountResponse:
    result = {
        cid: len(
            tenant_searcher.search_remind(
                tenant=request.tenant, name=name, cid=cid))
        for cid, name in request.id_name_pairs
    }
    return CompanySearchDuplicateCountResponse(result=result)


@tenant_search_engine.register
def FetchExistingIds(request: CompanyFetchRequest,
                     context: Context) -> CompanyQueryResponse:
    return CompanyQueryResponse(
        ids=tenant_searcher.fetch_existing_ids(
            tenant=request.tenant,
            min_id=request.min_id,
            max_id=request.max_id))


@tenant_search_engine.register
def QueryIds(request: CompanyQueryRequest,
             context: Context) -> CompanyQueryResponse:
    return CompanyQueryResponse(
        ids=tenant_searcher.query_ids_with_dup_count(
            tenant=request.tenant,
            count=request.count,
            operator=request.operator))


#TODO 整合 (add_company batch_add_company)
#TODO 整合 (delete_company batch_delete_company)

keyword_model = search_engine.load_company_keyword_model()
keyword_model.predict('')  # for multiprocessing fork

company_keyword_engine = Blueprint("company_keyword_engine")


@company_keyword_engine.register
def ExtractKeyword(request: NormalTextRequest,
                   context: Context) -> NormalTextResponse:
    result = keyword_model.predict(request.text)
    return NormalTextResponse(text=result)
