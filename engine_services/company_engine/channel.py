from grpc import insecure_channel

from ai_service_protos import (company_keyword_engine_pb2_grpc,
                               tenant_search_engine_pb2_grpc)
from config import ai_service_config

company_engine_channel = insecure_channel(
    target=ai_service_config['COMPANY_ENGINE_ADDRESS'])

company_keyword_engine_stub = company_keyword_engine_pb2_grpc.company_keyword_engineStub(
    company_engine_channel)

tenant_search_engine_stub = tenant_search_engine_pb2_grpc.tenant_search_engineStub(
    company_engine_channel)
