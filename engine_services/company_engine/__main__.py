from engine_services.company_engine.service import app, \
    company_keyword_engine, tenant_search_engine
from service_checker.contrib import grpc as grpc_check
from utils.health_checker import snooper

grpc_check.server_install(snooper=snooper, server=app)
if __name__ == '__main__':
    app.register_blueprint(tenant_search_engine)
    app.register_blueprint(company_keyword_engine)
    app.run(port=app.config["COMPANY_ENGINE_PORT"])
