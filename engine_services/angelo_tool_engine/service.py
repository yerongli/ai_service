from gllue_ai_libs.angelo_tool.nlp.base_func import NlpBase
from grpcalchemy import Context

from config import ai_service_config
from grpc_models import NormalTextRequest, NormalTextResponse
from utils.ai_server import ai_server

app = ai_server("angelo_tool_engine", config=ai_service_config)


@app.register
def Traditional2Simplified(request: NormalTextRequest,
                           context: Context) -> NormalTextResponse:
    result = NlpBase.traditional2simplified(request.text)
    return NormalTextResponse(text=result)
