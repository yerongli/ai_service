from grpc import insecure_channel

from config import ai_service_config

angelo_tool_engine_channel = insecure_channel(
    target=ai_service_config['ANGELO_TOOL_ENGINE_ADDRESS'])
