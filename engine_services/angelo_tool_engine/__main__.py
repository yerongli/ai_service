from service_checker.contrib import grpc as grpc_check

from engine_services.angelo_tool_engine.service import app
from utils.health_checker import snooper

grpc_check.server_install(snooper=snooper, server=app)
if __name__ == '__main__':
    app.run(port=app.config["ANGELO_TOOL_ENGINE_PORT"])
