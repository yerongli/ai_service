from grpcalchemy.blueprint import Blueprint, Context

from common.resume_index import *
from config import ai_service_config
from database.elasticsearch.models import (BasicInfoDoc, EducationListDoc,
                                           ExperienceListDoc, ResumeFeatures)
from grpc_models import (NormAidTenantRequest, NormalIdentRequest,
                         NormalStatusResponse, NormalTextRequest,
                         NormTenantRequest)
from grpc_models.ai_resume_index_model import (
    ResumeIndexResponse, ResumeMessage, UpdateSingleResumeCustomTagsMessage)
from utils.ai_server import ai_server

app = ai_server("resume_index_engine", config=ai_service_config)
resume_process_engine = Blueprint("resume_process_engine")


@resume_process_engine.register
def IdentUpdateResume(request: NormalIdentRequest,
                      context: Context) -> NormalStatusResponse:
    ResumeFeatures.ident_update(ident=request.ident)
    result = NormalStatusResponse(status=True, result="更新简历成功")
    return result


@resume_process_engine.register
def IndexSingleResume(request: ResumeMessage,
                      context: Context) -> ResumeIndexResponse:
    resume = request.message_to_dict(
        preserving_proto_field_name=True, including_default_value_fields=True)
    resume_index = index_single_resume(resume)

    result = ResumeIndexResponse(**resume_index)
    return result


@resume_process_engine.register
def DeleteSingleResume(request: NormAidTenantRequest,
                       context: Context) -> NormalStatusResponse:
    resume_info = request.message_to_dict(
        preserving_proto_field_name=True, including_default_value_fields=True)
    ResumeFeatures.delete_with_tenant_and_aid(
        tenant=resume_info['tenant'], aid=resume_info['aid'])
    result = NormalStatusResponse(status=True, result="删除简历成功")
    return result


@resume_process_engine.register
def UpdateSingleResumeCustomTags(request: UpdateSingleResumeCustomTagsMessage,
                                 context: Context) -> NormalStatusResponse:
    resume_info = request.message_to_dict(
        preserving_proto_field_name=True, including_default_value_fields=True)
    rdm = ResumeFeatures.get_with_tenant_and_aid(resume_info['tenant'],
                                                 resume_info['aid'])
    rdm.custom_tags = resume_info['custom_tags']
    rdm.save()
    result = NormalStatusResponse(status=True, result="更新简历custom_tags成功")
    return result


@resume_process_engine.register
def ReindexAllResume(request: NormalTextRequest,
                     context: Context) -> NormalStatusResponse:
    type_ = request.text
    if type_ in {'basic_info'}:
        BasicInfoDoc.all_update()
    elif type_ in {'education', 'educations'}:
        EducationListDoc.all_update()
    elif type_ in {'experience', 'experiences'}:
        ExperienceListDoc.all_update()
    else:
        ResumeFeatures.all_update()
    result = NormalStatusResponse(
        status=True, result="更新所有 resume_index Index成功")
    return result


@resume_process_engine.register
def ReindexResumeUseTenant(request: NormTenantRequest,
                           context: Context) -> NormalStatusResponse:
    ResumeFeatures.tenant_update(tenant=request.tenant)
    result = NormalStatusResponse(
        status=True, result=f"更新租户[{request.tenant}] resume_index Index成功")
    return result
