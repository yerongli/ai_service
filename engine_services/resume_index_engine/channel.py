from grpc import insecure_channel

from ai_service_protos import resume_process_engine_pb2_grpc
from config import ai_service_config

resume_index_engine_channel = insecure_channel(
    target=ai_service_config['RESUME_INDEX_ENGINE_ADDRESS'])

resume_process_engine_stub = resume_process_engine_pb2_grpc.resume_process_engineStub(
    resume_index_engine_channel)
