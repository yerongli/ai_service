from grpcalchemy.orm import Message, StringField, ListField, MapField, FloatField


class TitleMultiplePredictMeta(Message):
    pred = StringField()
    pred_prob = MapField(StringField, FloatField)


class TitleMultiplePredictResponse(Message):
    """通用响应，用于通用返回值的构建"""
    __filename__ = "title"

    items = ListField(TitleMultiplePredictMeta)
