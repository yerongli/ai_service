# 此文件中的 model 都不能直接调用， 只能继承或者ReferenceField
from grpcalchemy.orm import BooleanField, FloatField, Int32Field, ListField, \
    Message, StringField


class IntRangeMeta(Message):
    """int 范围对象"""
    __filename__ = "meta"

    gte = Int32Field()
    lte = Int32Field()


class FloatRangeMeta(Message):
    """float 范围对象"""
    __filename__ = "meta"

    gte = FloatField()
    lte = FloatField()


class TextFloatDictMeta(Message):
    __filename__ = "meta"

    index = StringField()
    value = FloatField()


class IntFloatDictMeta(Message):
    __filename__ = "meta"

    index = Int32Field()
    value = FloatField()


class StatusBaseResponse(Message):
    __filename__ = "meta"

    status = BooleanField()  #: Bool: 该操作是否成功
    msg = StringField()  #: String: 对应消息


class BaseAidTenantMessage(Message):
    """tenant 和 aid"""
    __filename__ = "meta"

    aid = Int32Field()
    tenant = StringField()


class BaseRecommendItemsMeta(Message):
    """基础的推荐返回结果中的items元素， 包含 aid，tenant，score"""
    __filename__ = "meta"

    aid = Int32Field()
    tenant = StringField()
    score = FloatField()
