from typing import Dict, List

from grpcalchemy.orm import FloatField, Int32Field, ListField, MapField, \
    Message, StringField


class CompanyMessage(Message):
    """公司检索基础结构对象"""

    __filename__ = "company"

    id = Int32Field()  #: Int: 公司ID
    name = StringField()  #: String: 公司名称
    score = FloatField()  #: Float:


class CompanySearchRquest(Message):
    """公司检索请求"""

    __filename__ = "company"

    tenant = StringField()  #: StringField: tenant
    cid = Int32Field()  #: String: 检索ID
    name = StringField()  #: String: 检索内如


class CompanySearchResponse(Message):
    """公司检索搜索结果"""

    __filename__ = "company"

    result = ListField(CompanyMessage)


class CompanySearchDuplicateCountRequest(Message):
    """查重"""

    __filename__ = "company"

    id_name_pairs: Dict[int, str] = MapField(Int32Field, StringField)
    tenant = StringField()  #: StringField: tenant


class CompanySearchDuplicateCountResponse(Message):
    """查重"""

    __filename__ = "company"

    result: Dict[int, int] = MapField(Int32Field, Int32Field)


class CompanyFetchRequest(Message):
    __filename__ = "company"

    tenant = StringField()  #: StringField: tenant
    min_id = Int32Field()  #: Int32:
    max_id = Int32Field()  #: Int32:


class CompanyQueryRequest(Message):
    __filename__ = "company"

    tenant = StringField()  #: StringField: tenant
    operator = StringField()  #: Sting: Union["gte","lte","eq"]
    count = Int32Field()  #: Int32:


class CompanyQueryResponse(Message):
    __filename__ = "company"

    ids: List[int] = ListField(Int32Field)  #: List[int]: 公司ID
