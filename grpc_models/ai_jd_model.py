from grpcalchemy.orm import (BooleanField, ListField, Message, ReferenceField,
                             StringField)

from grpc_models.ai_meta_model import IntRangeMeta


class ExtractContentInfoRequest(Message):
    __filename__ = "jd_index"

    content = StringField()


class ExtractContentInfoResponse(Message):
    __filename__ = "jd_index"

    content_sentence = ListField(StringField)
    content_clean = StringField()
    need_985_211 = BooleanField()
    lowest_degree = StringField()
    need_majors = ListField(StringField)
    age_range = ReferenceField(IntRangeMeta)
    working_year_range = ReferenceField(IntRangeMeta)
