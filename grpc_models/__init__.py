from grpcalchemy.orm import (BooleanField, Message, StringField, ListField,
                             MapField, Int32Field)

from .ai_meta_model import TextFloatDictMeta, IntFloatDictMeta, BaseAidTenantMessage


class NormalStatusResponse(Message):
    """通用响应，用于通用返回值的构建"""
    __filename__ = "base"

    status = BooleanField()  #: Bool: 该操作是否成功
    result = StringField()  #: String: 操作返回内容


class NormalTextRequest(Message):
    """通用text请求对象"""
    __filename__ = "base"

    text = StringField()  #: String: 文本内容


class NormalIdentRequest(Message):
    """通用ident请求对象"""
    __filename__ = "base"

    ident = StringField()


class NormalTextResponse(Message):
    """通用text返回对象"""
    __filename__ = "base"

    text = StringField()  #: String: 文本内容


class NormalTitleDescriptionRequest(Message):
    """通用text返回对象"""
    __filename__ = "base"

    title = StringField()
    description = StringField()


class NormTextFloatDictListResponse(Message):
    """通用index(text)-value(float)返回对象"""
    __filename__ = "base"

    result = ListField(TextFloatDictMeta)


class NormIntFloatDictListResponse(Message):
    """通用index(int)-value(float)返回对象"""
    __filename__ = "base"

    result = ListField(IntFloatDictMeta)


class NormTextListMessage(Message):
    """通用的字符串list请求对象"""
    __filename__ = "base"

    text_list = ListField(StringField)


class NormTextListResponse(Message):
    """通用的字符串list返回对象"""
    __filename__ = "base"

    result = ListField(StringField)


class NormAidTenantRequest(BaseAidTenantMessage):
    """通用的带 aid 和 tenant的标识"""
    __filename__ = "base"


class NormTenantRequest(Message):
    """通用的带 tenant"""

    __filename__ = "base"

    tenant = StringField()


class NormTextIntMappingMessage(Message):
    """dict[str, int] 类型"""

    __filename__ = "base"

    result = MapField(StringField, Int32Field)


class NormIntIntMappingMessage(Message):
    """dict[str, int] 类型"""

    __filename__ = "base"

    result = MapField(Int32Field, Int32Field)


class NormTextTextMappingMessage(Message):
    """dict[str, int] 类型"""

    __filename__ = "base"

    result = MapField(StringField, StringField)
