from grpcalchemy.orm import Int32Field, ListField, Message, StringField


# 反馈
class JdResumeFeedbackMessage(Message):
    __filename__ = "feedback"

    jd_aid = Int32Field()
    tenant = StringField()

    resume_aid_list = ListField(Int32Field)
