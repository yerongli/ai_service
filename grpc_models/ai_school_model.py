from grpcalchemy.orm import BooleanField, FloatField, Int32Field, ListField, \
    Message, StringField


class SchoolAidMessage(Message):
    __filename__ = "school"
    aid = Int32Field()


class SchoolInfoDictResponse(Message):
    __filename__ = "school"

    is_985 = BooleanField()
    is_211 = BooleanField()
    is_abroad = BooleanField()
    is_qs200 = BooleanField()


class SchoolSearchResponse(Message):
    __filename__ = "school"

    aid = Int32Field()
    ix = StringField()
    score = FloatField()


class BatchSchoolSearchResponse(Message):
    __filename__ = "school"

    items = ListField(SchoolSearchResponse)
