from grpcalchemy.orm import ListField, Message, StringField


class CellPredictRequest(Message):
    __filename__ = "major"

    level = StringField()  #: String: 学位等级 Union["专科","本科","硕士"]
    major = StringField()  #: String: 专业名称


class CellPredictResponse(Message):
    __filename__ = "major"

    result = StringField()


class LowestDegreeMajorPredictRequest(Message):
    __filename__ = "major"

    major_list = ListField(StringField)
    lowest_degree = StringField()  # String: "", '专科', '本科', '硕士', '博士'


class LowestDegreeMajorPredictResponse(Message):
    __filename__ = "major"

    result = ListField(StringField)
