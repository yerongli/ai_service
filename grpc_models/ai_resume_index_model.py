from grpcalchemy.orm import (BooleanField, FloatField, Int32Field, ListField,
                             MapField, Message, ReferenceField, StringField)

from grpc_models.ai_meta_model import BaseAidTenantMessage


class ResumeBasicInfoMessage(Message):
    """简历基础信息"""
    __filename__ = "resume_index"

    name = StringField()
    gender = StringField()
    mobile = StringField()
    email = StringField()
    linkedin = StringField()
    birth_day = StringField()
    age = Int32Field()
    current_salary = FloatField()
    locations = ListField(Int32Field)


class EducationExperienceMessage(Message):
    """教育经历信息"""
    __filename__ = "resume_index"

    school = StringField()
    major = StringField()
    degree = StringField()
    start = StringField()
    end = StringField()
    is_recent = BooleanField()
    is_current = BooleanField()


class WorkExperienceMessage(Message):
    """工作经历信息"""
    __filename__ = "resume_index"

    company = StringField()
    title = StringField()
    description = StringField()
    start = StringField()
    end = StringField()
    is_recent = BooleanField()
    is_current = BooleanField()


class ResumeMessage(BaseAidTenantMessage):
    """简历信息请求"""
    __filename__ = "resume_index"

    is_deleted = BooleanField()
    basic_info = ReferenceField(ResumeBasicInfoMessage)
    educations = ListField(EducationExperienceMessage)
    experiences = ListField(WorkExperienceMessage)
    custom_tags = ListField(StringField)


class BasicInfoIndexMessage(Message):
    __filename__ = "resume_index"

    name = StringField()
    gender = StringField()
    mobile = StringField()
    email = StringField()
    linkedin = StringField()
    birth_day = StringField()
    age = Int32Field()
    current_salary = FloatField()
    locations = ListField(Int32Field)


class EducationExperienceIndexMessage(Message):
    __filename__ = "resume_index"

    is_recent = BooleanField()
    is_current = BooleanField()
    weight = FloatField()

    school = StringField()
    school_stand_id = Int32Field()

    major = StringField()
    major_stand = StringField()

    degree = StringField()
    degree_stand = StringField()

    start = StringField()
    end = StringField()

    duration_month = Int32Field()

    is_major_legal = BooleanField()
    is_time_legal = BooleanField()
    is_edu_legal = BooleanField()

    is_985 = BooleanField()
    is_211 = BooleanField()
    is_985_211 = BooleanField()
    is_abroad = BooleanField()
    is_qs200 = BooleanField()


class EducationExperienceListIndexMessage(Message):
    __filename__ = "resume_index"

    # school_weight = MapField(Int32Field, FloatField)
    # major_weight = MapField(StringField, FloatField)
    # degree_weight = MapField(StringField, FloatField)
    total_duration_month = Int32Field()

    last_school = Int32Field()
    last_major = StringField()
    last_degree = StringField()
    last_graduation_time = StringField()

    is_education_interrupt = BooleanField()
    is_education_illegal = BooleanField()
    is_985 = BooleanField()
    is_211 = BooleanField()
    is_985_211 = BooleanField()
    is_qs200 = BooleanField()
    is_abroad = BooleanField()

    first_is_985 = BooleanField()
    first_is_211 = BooleanField()
    first_is_985_211 = BooleanField()
    first_is_qs200 = BooleanField()
    first_is_abroad = BooleanField()

    edu_list = ListField(EducationExperienceIndexMessage)


class WorkExperienceIndexMessage(Message):
    __filename__ = "resume_index"

    is_recent = BooleanField()
    is_current = BooleanField()
    weight = FloatField()

    company = StringField()
    company_keyword = StringField()

    title = StringField()
    title_stand = StringField()
    title_parent = StringField()
    title_level = Int32Field()

    start = StringField()
    end = StringField()

    duration_month = Int32Field()

    company_industry_list = ListField(StringField)
    is_industry_famous = BooleanField()

    description_industry_list = ListField(StringField)
    description = StringField()

    poi_weight = MapField(StringField, FloatField)
    lda_weight = MapField(Int32Field, FloatField)


class WorkExperienceListIndexMessage(Message):
    __filename__ = "resume_index"

    title_level_highest = Int32Field()
    title_weight = MapField(StringField, FloatField)
    title_duration_month = MapField(StringField, Int32Field)
    total_duration_month = Int32Field()

    company_industry_duration_month = MapField(StringField, Int32Field)
    description_industry_duration_month = MapField(StringField, Int32Field)

    is_work_exp_across = BooleanField()
    is_work_exp_severed = BooleanField()

    poi_weight = MapField(StringField, FloatField)
    poi_duration_month = MapField(StringField, Int32Field)

    lda_weight = MapField(Int32Field, FloatField)

    work_list = ListField(WorkExperienceIndexMessage)


class ResumeIndexResponse(BaseAidTenantMessage):
    __filename__ = "resume_index"

    is_deleted = BooleanField()
    basic_info = ReferenceField(BasicInfoIndexMessage)
    educations = ReferenceField(EducationExperienceListIndexMessage)
    experiences = ReferenceField(WorkExperienceListIndexMessage)
    custom_tags = ListField(StringField)


class UpdateSingleResumeCustomTagsMessage(BaseAidTenantMessage):
    __filename__ = "resume_index"

    custom_tags = ListField(StringField)
