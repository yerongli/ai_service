from grpcalchemy.orm import BooleanField, FloatField, Int32Field, ListField, \
    Message, ReferenceField, StringField

from grpc_models.ai_meta_model import BaseAidTenantMessage, \
    BaseRecommendItemsMeta, TextFloatDictMeta
from grpc_models.ai_resume_index_model import EducationExperienceMessage, \
    ResumeBasicInfoMessage, WorkExperienceMessage


class CustomPreferMeta(Message):
    """用户自定义标签"""
    __filename__ = "resume_recommend"

    field = StringField()
    type = StringField()  # target_company, target_school, custom_tags, 985/211
    boost = FloatField()


class JdInfoOfRecommendMessage(BaseAidTenantMessage):
    """人岗匹配使用的jd_info"""
    __filename__ = "resume_recommend"

    version = Int32Field()
    custom_prefer_list = ListField(CustomPreferMeta)


class RecommendReasonItemsMeta(BaseRecommendItemsMeta):
    """简历推荐人才接口返回结果的meta"""
    __filename__ = "resume_recommend"

    reason = ListField(StringField)


class BaseResumeInfoMessage(BaseAidTenantMessage):
    """推荐使用的基础 resume_info"""
    __filename__ = "resume_recommend"


class ResumeInfoMessage(Message):
    """不包含 aid 和tenant的resume"""
    __filename__ = "resume_recommend"

    tenant = StringField()

    basic_info = ReferenceField(ResumeBasicInfoMessage)
    educations = ListField(EducationExperienceMessage)
    experiences = ListField(WorkExperienceMessage)


# 简历推荐
class RecommendResumeFromJdInfoRequest(Message):
    __filename__ = "resume_recommend"

    refresh = BooleanField()  # 控制是否重新推荐

    topn = Int32Field()  # 请求最多返回数量
    min_score = FloatField()  # 最低分数阈值

    size = Int32Field()  # 请求推荐你的数量
    offset = Int32Field()  # 偏移量，用于翻页

    jd_info = ReferenceField(JdInfoOfRecommendMessage)


class RecommendResumeResponse(Message):
    __filename__ = "resume_recommend"

    recommend_id = StringField()  # 推荐的唯一 id 用于追踪这次推荐
    total = Int32Field()  # 此次推荐数量
    size = Int32Field()  # 分页数量

    items = ListField(RecommendReasonItemsMeta)


# 人人匹配
class SimilarResumeFromResumeIdMessage(Message):
    """人人匹配, 基于已存在的简历"""
    __filename__ = "resume_recommend"

    topn = Int32Field()  # 请求最多返回数量
    min_score = FloatField()  # 最低分数阈值

    resume_info = ReferenceField(BaseResumeInfoMessage)


class SimilarResumeFromResumeMessage(Message):
    """人人匹配，基于新简历"""
    __filename__ = "resume_recommend"

    topn = Int32Field()  # 请求最多返回数量
    min_score = FloatField()  # 最低分数阈值

    resume_info = ReferenceField(ResumeInfoMessage)


class SimilarResumeFromResumeResponse(Message):
    """人人匹配接口返回"""
    __filename__ = "resume_recommend"

    recommend_id = StringField()
    items = ListField(BaseRecommendItemsMeta)


# 简历查重
class DuplicateResumeCheckingFromResumeIdMessage(Message):
    """简历查重，基于已存在简历"""
    __filename__ = "resume_recommend"

    topn = Int32Field()  # 请求最多返回数量
    min_score = FloatField()  # 最低分数阈值

    resume_info = ReferenceField(BaseResumeInfoMessage)


class DuplicateResumeCheckingFromResumeMessage(Message):
    """简历查重，基于独立简历信息"""
    __filename__ = "resume_recommend"

    topn = Int32Field()  # 请求最多返回数量
    min_score = FloatField()  # 最低分数阈值

    resume_info = ReferenceField(ResumeInfoMessage)


class DuplicateResumeCheckingPreciselyMessage(Message):
    """简历精准查重，refer: https://confluence.gllue.com/pages/viewpage.action?pageId=23761314"""
    __filename__ = "resume_recommend"

    tenant = StringField()
    mobile = StringField()
    email = StringField()
    linkedin = StringField()


class DuplicateResumeCheckingResponse(Message):
    """简历查重返回"""
    __filename__ = "resume_recommend"

    recommend_id = StringField()
    items = ListField(BaseRecommendItemsMeta)


# 打分
class ScoreJdCVMessage(Message):
    __filename__ = "resume_recommend"

    jd_info = ReferenceField(JdInfoOfRecommendMessage)
    resume_info = ReferenceField(BaseResumeInfoMessage)


class ScoreJdCVResponse(Message):
    __filename__ = "resume_recommend"

    score = FloatField()


class BatchScoreJdCVResponse(Message):
    __filename__ = "resume_recommend"

    result = ListField(BaseRecommendItemsMeta)


class TextFloatDictListResponse(Message):
    """index-value List"""
    __filename__ = "resume_recommend"

    items = ListField(TextFloatDictMeta)


class RecommendResumeHomePageRequest(Message):
    __filename__ = "resume_recommend"

    tenant = StringField()
    topn = Int32Field()
    user = Int32Field()


class RecommendResumeHomePageMeta(Message):
    __filename__ = "resume_recommend"

    jd_aid = Int32Field()
    resume_aid = Int32Field()
    score = FloatField()
    tenant = StringField()
    reason = ListField(StringField)


class RecommendResumeHomePageResponse(Message):
    __filename__ = "resume_recommend"

    items = ListField(RecommendResumeHomePageMeta)
