.. highlight:: shell

============
贡献代码
============

注意事项
---------------

- **所有新增项目依赖需** 需要添加至 `environment.yml` 以及 `deploy/Dockerfile.final`

贡献代码应遵循以下步骤:

Commit 格式
----------------------

**:tag: [#id] 内容**

- *id* 为 **client** 协作系统 上对应的 **issue id**。

修复 Bugs
~~~~~~~~~

tag 可为

- **bug** 处理 Bug
- **ambulance** 处理线上 Bug


优化以及增加功能
~~~~~~~~~~~~~~~~~~

tag 可为

- **star2** 增加新功能新模块或者新添测试代码
- **hammer** 包括文件代码结构以及代码风格的重构
- **rocket** 重大改进，提升性能如修改某个方法或算法

文档更新
~~~~~~~~~~~~~~~~~~~

tag 可为

- **lipstick** 文档内容更新


Get Started!
------------

以下步骤基于已经通过 `README.rst` 配置好基础开发环境。

1. Create a branch for local development::

    $ git checkout -b name-of-your-bugfix-or-feature

   Now you can make your changes locally.

2. When you're done making changes, check that your changes pass the tests.::

    $ make test


3. Commit your changes and push your branch to GitHub::

    $ git add .
    $ git commit -m ":tag: [#id] Your detailed description of your changes."
    $ git push origin name-of-your-bugfix-or-feature

4. Submit a pull request through the GitHub website.

