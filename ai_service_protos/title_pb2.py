# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: ai_service_protos/title.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


from ai_service_protos import titlemultiplepredictmeta_pb2 as ai__service__protos_dot_titlemultiplepredictmeta__pb2


DESCRIPTOR = _descriptor.FileDescriptor(
  name='ai_service_protos/title.proto',
  package='',
  syntax='proto3',
  serialized_options=None,
  serialized_pb=_b('\n\x1d\x61i_service_protos/title.proto\x1a\x30\x61i_service_protos/titlemultiplepredictmeta.proto\"H\n\x1cTitleMultiplePredictResponse\x12(\n\x05items\x18\x01 \x03(\x0b\x32\x19.TitleMultiplePredictMetab\x06proto3')
  ,
  dependencies=[ai__service__protos_dot_titlemultiplepredictmeta__pb2.DESCRIPTOR,])




_TITLEMULTIPLEPREDICTRESPONSE = _descriptor.Descriptor(
  name='TitleMultiplePredictResponse',
  full_name='TitleMultiplePredictResponse',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='items', full_name='TitleMultiplePredictResponse.items', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=83,
  serialized_end=155,
)

_TITLEMULTIPLEPREDICTRESPONSE.fields_by_name['items'].message_type = ai__service__protos_dot_titlemultiplepredictmeta__pb2._TITLEMULTIPLEPREDICTMETA
DESCRIPTOR.message_types_by_name['TitleMultiplePredictResponse'] = _TITLEMULTIPLEPREDICTRESPONSE
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

TitleMultiplePredictResponse = _reflection.GeneratedProtocolMessageType('TitleMultiplePredictResponse', (_message.Message,), dict(
  DESCRIPTOR = _TITLEMULTIPLEPREDICTRESPONSE,
  __module__ = 'ai_service_protos.title_pb2'
  # @@protoc_insertion_point(class_scope:TitleMultiplePredictResponse)
  ))
_sym_db.RegisterMessage(TitleMultiplePredictResponse)


# @@protoc_insertion_point(module_scope)
