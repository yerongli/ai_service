# Generated by the gRPC Python protocol compiler plugin. DO NOT EDIT!
import grpc

from ai_service_protos import base_pb2 as ai__service__protos_dot_base__pb2
from ai_service_protos import title_pb2 as ai__service__protos_dot_title__pb2


class title_multi_classify_engineStub(object):
  # missing associated documentation comment in .proto file
  pass

  def __init__(self, channel):
    """Constructor.

    Args:
      channel: A grpc.Channel.
    """
    self.MultiplePredict = channel.unary_unary(
        '/title_multi_classify_engine/MultiplePredict',
        request_serializer=ai__service__protos_dot_base__pb2.NormTextListMessage.SerializeToString,
        response_deserializer=ai__service__protos_dot_title__pb2.TitleMultiplePredictResponse.FromString,
        )
    self.Predict = channel.unary_unary(
        '/title_multi_classify_engine/Predict',
        request_serializer=ai__service__protos_dot_base__pb2.NormalTextRequest.SerializeToString,
        response_deserializer=ai__service__protos_dot_base__pb2.NormalTextResponse.FromString,
        )
    self.PredictProbTop = channel.unary_unary(
        '/title_multi_classify_engine/PredictProbTop',
        request_serializer=ai__service__protos_dot_base__pb2.NormalTextRequest.SerializeToString,
        response_deserializer=ai__service__protos_dot_base__pb2.NormTextListResponse.FromString,
        )
    self.GetTitle2Id = channel.unary_unary(
        '/title_multi_classify_engine/GetTitle2Id',
        request_serializer=ai__service__protos_dot_base__pb2.NormalTextRequest.SerializeToString,
        response_deserializer=ai__service__protos_dot_base__pb2.NormTextIntMappingMessage.FromString,
        )
    self.GetTitleParent2Id = channel.unary_unary(
        '/title_multi_classify_engine/GetTitleParent2Id',
        request_serializer=ai__service__protos_dot_base__pb2.NormalTextRequest.SerializeToString,
        response_deserializer=ai__service__protos_dot_base__pb2.NormTextIntMappingMessage.FromString,
        )
    self.GetTitle2Parent = channel.unary_unary(
        '/title_multi_classify_engine/GetTitle2Parent',
        request_serializer=ai__service__protos_dot_base__pb2.NormalTextRequest.SerializeToString,
        response_deserializer=ai__service__protos_dot_base__pb2.NormTextTextMappingMessage.FromString,
        )


class title_multi_classify_engineServicer(object):
  # missing associated documentation comment in .proto file
  pass

  def MultiplePredict(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def Predict(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def PredictProbTop(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def GetTitle2Id(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def GetTitleParent2Id(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def GetTitle2Parent(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')


def add_title_multi_classify_engineServicer_to_server(servicer, server):
  rpc_method_handlers = {
      'MultiplePredict': grpc.unary_unary_rpc_method_handler(
          servicer.MultiplePredict,
          request_deserializer=ai__service__protos_dot_base__pb2.NormTextListMessage.FromString,
          response_serializer=ai__service__protos_dot_title__pb2.TitleMultiplePredictResponse.SerializeToString,
      ),
      'Predict': grpc.unary_unary_rpc_method_handler(
          servicer.Predict,
          request_deserializer=ai__service__protos_dot_base__pb2.NormalTextRequest.FromString,
          response_serializer=ai__service__protos_dot_base__pb2.NormalTextResponse.SerializeToString,
      ),
      'PredictProbTop': grpc.unary_unary_rpc_method_handler(
          servicer.PredictProbTop,
          request_deserializer=ai__service__protos_dot_base__pb2.NormalTextRequest.FromString,
          response_serializer=ai__service__protos_dot_base__pb2.NormTextListResponse.SerializeToString,
      ),
      'GetTitle2Id': grpc.unary_unary_rpc_method_handler(
          servicer.GetTitle2Id,
          request_deserializer=ai__service__protos_dot_base__pb2.NormalTextRequest.FromString,
          response_serializer=ai__service__protos_dot_base__pb2.NormTextIntMappingMessage.SerializeToString,
      ),
      'GetTitleParent2Id': grpc.unary_unary_rpc_method_handler(
          servicer.GetTitleParent2Id,
          request_deserializer=ai__service__protos_dot_base__pb2.NormalTextRequest.FromString,
          response_serializer=ai__service__protos_dot_base__pb2.NormTextIntMappingMessage.SerializeToString,
      ),
      'GetTitle2Parent': grpc.unary_unary_rpc_method_handler(
          servicer.GetTitle2Parent,
          request_deserializer=ai__service__protos_dot_base__pb2.NormalTextRequest.FromString,
          response_serializer=ai__service__protos_dot_base__pb2.NormTextTextMappingMessage.SerializeToString,
      ),
  }
  generic_handler = grpc.method_handlers_generic_handler(
      'title_multi_classify_engine', rpc_method_handlers)
  server.add_generic_rpc_handlers((generic_handler,))
