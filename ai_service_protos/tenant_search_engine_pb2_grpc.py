# Generated by the gRPC Python protocol compiler plugin. DO NOT EDIT!
import grpc

from ai_service_protos import company_pb2 as ai__service__protos_dot_company__pb2


class tenant_search_engineStub(object):
  # missing associated documentation comment in .proto file
  pass

  def __init__(self, channel):
    """Constructor.

    Args:
      channel: A grpc.Channel.
    """
    self.Search = channel.unary_unary(
        '/tenant_search_engine/Search',
        request_serializer=ai__service__protos_dot_company__pb2.CompanySearchRquest.SerializeToString,
        response_deserializer=ai__service__protos_dot_company__pb2.CompanySearchResponse.FromString,
        )
    self.SearchDuplicateCountWithCache = channel.unary_unary(
        '/tenant_search_engine/SearchDuplicateCountWithCache',
        request_serializer=ai__service__protos_dot_company__pb2.CompanySearchDuplicateCountRequest.SerializeToString,
        response_deserializer=ai__service__protos_dot_company__pb2.CompanySearchDuplicateCountResponse.FromString,
        )
    self.SearchDuplicateCountWithoutCache = channel.unary_unary(
        '/tenant_search_engine/SearchDuplicateCountWithoutCache',
        request_serializer=ai__service__protos_dot_company__pb2.CompanySearchDuplicateCountRequest.SerializeToString,
        response_deserializer=ai__service__protos_dot_company__pb2.CompanySearchDuplicateCountResponse.FromString,
        )
    self.FetchExistingIds = channel.unary_unary(
        '/tenant_search_engine/FetchExistingIds',
        request_serializer=ai__service__protos_dot_company__pb2.CompanyFetchRequest.SerializeToString,
        response_deserializer=ai__service__protos_dot_company__pb2.CompanyQueryResponse.FromString,
        )
    self.QueryIds = channel.unary_unary(
        '/tenant_search_engine/QueryIds',
        request_serializer=ai__service__protos_dot_company__pb2.CompanyQueryRequest.SerializeToString,
        response_deserializer=ai__service__protos_dot_company__pb2.CompanyQueryResponse.FromString,
        )


class tenant_search_engineServicer(object):
  # missing associated documentation comment in .proto file
  pass

  def Search(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def SearchDuplicateCountWithCache(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def SearchDuplicateCountWithoutCache(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def FetchExistingIds(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def QueryIds(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')


def add_tenant_search_engineServicer_to_server(servicer, server):
  rpc_method_handlers = {
      'Search': grpc.unary_unary_rpc_method_handler(
          servicer.Search,
          request_deserializer=ai__service__protos_dot_company__pb2.CompanySearchRquest.FromString,
          response_serializer=ai__service__protos_dot_company__pb2.CompanySearchResponse.SerializeToString,
      ),
      'SearchDuplicateCountWithCache': grpc.unary_unary_rpc_method_handler(
          servicer.SearchDuplicateCountWithCache,
          request_deserializer=ai__service__protos_dot_company__pb2.CompanySearchDuplicateCountRequest.FromString,
          response_serializer=ai__service__protos_dot_company__pb2.CompanySearchDuplicateCountResponse.SerializeToString,
      ),
      'SearchDuplicateCountWithoutCache': grpc.unary_unary_rpc_method_handler(
          servicer.SearchDuplicateCountWithoutCache,
          request_deserializer=ai__service__protos_dot_company__pb2.CompanySearchDuplicateCountRequest.FromString,
          response_serializer=ai__service__protos_dot_company__pb2.CompanySearchDuplicateCountResponse.SerializeToString,
      ),
      'FetchExistingIds': grpc.unary_unary_rpc_method_handler(
          servicer.FetchExistingIds,
          request_deserializer=ai__service__protos_dot_company__pb2.CompanyFetchRequest.FromString,
          response_serializer=ai__service__protos_dot_company__pb2.CompanyQueryResponse.SerializeToString,
      ),
      'QueryIds': grpc.unary_unary_rpc_method_handler(
          servicer.QueryIds,
          request_deserializer=ai__service__protos_dot_company__pb2.CompanyQueryRequest.FromString,
          response_serializer=ai__service__protos_dot_company__pb2.CompanyQueryResponse.SerializeToString,
      ),
  }
  generic_handler = grpc.method_handlers_generic_handler(
      'tenant_search_engine', rpc_method_handlers)
  server.add_generic_rpc_handlers((generic_handler,))
