# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: ai_service_protos/splitter_engine.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


from ai_service_protos import base_pb2 as ai__service__protos_dot_base__pb2


DESCRIPTOR = _descriptor.FileDescriptor(
  name='ai_service_protos/splitter_engine.proto',
  package='',
  syntax='proto3',
  serialized_options=None,
  serialized_pb=_b('\n\'ai_service_protos/splitter_engine.proto\x1a\x1c\x61i_service_protos/base.proto2E\n\x0fsplitter_engine\x12\x32\n\x05Split\x12\x12.NormalTextRequest\x1a\x13.NormalTextResponse\"\x00\x62\x06proto3')
  ,
  dependencies=[ai__service__protos_dot_base__pb2.DESCRIPTOR,])



_sym_db.RegisterFileDescriptor(DESCRIPTOR)



_SPLITTER_ENGINE = _descriptor.ServiceDescriptor(
  name='splitter_engine',
  full_name='splitter_engine',
  file=DESCRIPTOR,
  index=0,
  serialized_options=None,
  serialized_start=73,
  serialized_end=142,
  methods=[
  _descriptor.MethodDescriptor(
    name='Split',
    full_name='splitter_engine.Split',
    index=0,
    containing_service=None,
    input_type=ai__service__protos_dot_base__pb2._NORMALTEXTREQUEST,
    output_type=ai__service__protos_dot_base__pb2._NORMALTEXTRESPONSE,
    serialized_options=None,
  ),
])
_sym_db.RegisterServiceDescriptor(_SPLITTER_ENGINE)

DESCRIPTOR.services_by_name['splitter_engine'] = _SPLITTER_ENGINE

# @@protoc_insertion_point(module_scope)
