# Generated by the gRPC Python protocol compiler plugin. DO NOT EDIT!
import grpc

from ai_service_protos import base_pb2 as ai__service__protos_dot_base__pb2
from ai_service_protos import school_pb2 as ai__service__protos_dot_school__pb2


class school_engineStub(object):
  # missing associated documentation comment in .proto file
  pass

  def __init__(self, channel):
    """Constructor.

    Args:
      channel: A grpc.Channel.
    """
    self.SchoolSearch = channel.unary_unary(
        '/school_engine/SchoolSearch',
        request_serializer=ai__service__protos_dot_base__pb2.NormalTextRequest.SerializeToString,
        response_deserializer=ai__service__protos_dot_school__pb2.SchoolSearchResponse.FromString,
        )
    self.BatchSchoolSearch = channel.unary_unary(
        '/school_engine/BatchSchoolSearch',
        request_serializer=ai__service__protos_dot_base__pb2.NormTextListMessage.SerializeToString,
        response_deserializer=ai__service__protos_dot_school__pb2.BatchSchoolSearchResponse.FromString,
        )
    self.GetSchoolInfoDict = channel.unary_unary(
        '/school_engine/GetSchoolInfoDict',
        request_serializer=ai__service__protos_dot_school__pb2.SchoolAidMessage.SerializeToString,
        response_deserializer=ai__service__protos_dot_school__pb2.SchoolInfoDictResponse.FromString,
        )
    self.GetSchool2Id = channel.unary_unary(
        '/school_engine/GetSchool2Id',
        request_serializer=ai__service__protos_dot_base__pb2.NormalTextRequest.SerializeToString,
        response_deserializer=ai__service__protos_dot_base__pb2.NormIntIntMappingMessage.FromString,
        )


class school_engineServicer(object):
  # missing associated documentation comment in .proto file
  pass

  def SchoolSearch(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def BatchSchoolSearch(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def GetSchoolInfoDict(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def GetSchool2Id(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')


def add_school_engineServicer_to_server(servicer, server):
  rpc_method_handlers = {
      'SchoolSearch': grpc.unary_unary_rpc_method_handler(
          servicer.SchoolSearch,
          request_deserializer=ai__service__protos_dot_base__pb2.NormalTextRequest.FromString,
          response_serializer=ai__service__protos_dot_school__pb2.SchoolSearchResponse.SerializeToString,
      ),
      'BatchSchoolSearch': grpc.unary_unary_rpc_method_handler(
          servicer.BatchSchoolSearch,
          request_deserializer=ai__service__protos_dot_base__pb2.NormTextListMessage.FromString,
          response_serializer=ai__service__protos_dot_school__pb2.BatchSchoolSearchResponse.SerializeToString,
      ),
      'GetSchoolInfoDict': grpc.unary_unary_rpc_method_handler(
          servicer.GetSchoolInfoDict,
          request_deserializer=ai__service__protos_dot_school__pb2.SchoolAidMessage.FromString,
          response_serializer=ai__service__protos_dot_school__pb2.SchoolInfoDictResponse.SerializeToString,
      ),
      'GetSchool2Id': grpc.unary_unary_rpc_method_handler(
          servicer.GetSchool2Id,
          request_deserializer=ai__service__protos_dot_base__pb2.NormalTextRequest.FromString,
          response_serializer=ai__service__protos_dot_base__pb2.NormIntIntMappingMessage.SerializeToString,
      ),
  }
  generic_handler = grpc.method_handlers_generic_handler(
      'school_engine', rpc_method_handlers)
  server.add_generic_rpc_handlers((generic_handler,))
