import sentry_sdk
from celery import Celery
from celery.schedules import crontab
from sentry_sdk.integrations.celery import CeleryIntegration

from config import ai_service_config
from sync_server.celery_task.candidate import candidate_insert, \
    candidate_update
from sync_server.celery_task.client import client_insert, client_update
from sync_server.celery_task.command import all_reindex_resume_task, all_reindex_jd_task, tenant_reindex_resume_task, \
    tenant_reindex_jd_task, tenant_re_recommend_resume_from_jd_task, ident_index_resume_task, ident_index_jd_task
from sync_server.celery_task.cron_task import \
    all_re_recommend_resume_from_jd_task
from sync_server.celery_task.joborder import joborder_insert, joborder_update
from sync_server.celery_task.jobsubmission import jobsubmission_insert, \
    jobsubmission_update

celery_app = Celery('ai_celery')
celery_app.config_from_object(ai_service_config, namespace='CELERY')
celery_app.conf.accept_content = ['json']
celery_app.conf.broker_transport_options = {'max_retries': 5}


@celery_app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    sender.add_periodic_task(crontab(hour=0, minute=3),
                             all_re_recommend_resume_from_jd_task.s())


sentry_sdk.init(dsn=ai_service_config["SENTRY_DSN"],
                integrations=[CeleryIntegration()])

celery_app.register_task(candidate_insert)
celery_app.register_task(candidate_update)
celery_app.register_task(client_insert)
celery_app.register_task(client_update)
celery_app.register_task(joborder_insert)
celery_app.register_task(joborder_update)
celery_app.register_task(jobsubmission_insert)
celery_app.register_task(jobsubmission_update)

celery_app.register_task(all_reindex_resume_task)
celery_app.register_task(all_reindex_jd_task)
celery_app.register_task(tenant_reindex_resume_task)
celery_app.register_task(tenant_reindex_jd_task)
celery_app.register_task(tenant_re_recommend_resume_from_jd_task)
celery_app.register_task(ident_index_resume_task)
celery_app.register_task(ident_index_jd_task)

celery_app.register_task(all_re_recommend_resume_from_jd_task)
