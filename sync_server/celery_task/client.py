from typing import Any, Dict

from celery.task import task


@task(name='client.insert', ignore_result=True)
def client_insert(document: Dict[str, Any]):
    pass


@task(name='client.update', ignore_result=True)
def client_update(document: Dict[str, Any]):
    pass
