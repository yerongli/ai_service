from typing import Any, Dict

from celery.task import task


@task(name='jobsubmission.insert', ignore_result=True)
def jobsubmission_insert(document: str):
    pass


@task(name='jobsubmission.update', ignore_result=True)
def jobsubmission_update(document: str):
    pass
