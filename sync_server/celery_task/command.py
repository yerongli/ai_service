from celery.task import task

from preprocess.jd_index import (ident_update_jd_info, reindex_all_jd,
                                 reindex_jd_use_tenant)
from preprocess.recommend import (all_re_recommend_resume_from_jd,
                                  tenant_re_recommend_resume_from_jd)
from preprocess.resume_index import (all_reindex_resume, ident_update_resume,
                                     tenant_reindex_resume)

__all__ = [
    'all_reindex_resume_task', 'all_reindex_jd_task',
    'tenant_reindex_resume_task', 'tenant_reindex_jd_task',
    'tenant_re_recommend_resume_from_jd_task', 'ident_index_resume_task',
    'ident_index_jd_task'
]


@task(name='command.all_reindex_resume', ignore_result=True)
def all_reindex_resume_task(type_):
    all_reindex_resume(type_)


@task(name='command.all_reindex_jd', ignore_result=True)
def all_reindex_jd_task():
    reindex_all_jd()


@task(name="command.all_re_recommend_resume_from_jd", ignore_result=True)
def all_re_recommend_resume_from_jd_task():
    all_re_recommend_resume_from_jd()


@task(name='command.tenant_reindex_resume', ignore_result=True)
def tenant_reindex_resume_task(tenant: str):
    tenant_reindex_resume(tenant)


@task(name='command.tenant_reindex_jd', ignore_result=True)
def tenant_reindex_jd_task(tenant: str):
    reindex_jd_use_tenant(tenant)


@task(name="command.tenant_re_recommend_resume_from_jd", ignore_result=True)
def tenant_re_recommend_resume_from_jd_task(tenant):
    # 离线为单个租户的所有jd重新推荐
    tenant_re_recommend_resume_from_jd(tenant=tenant)


# 用于修复某些 resume_index 的数据
@task(name='command.ident_index_resume', ignore_result=True)
def ident_index_resume_task(ident: str):
    ident_update_resume(ident)


# 用于修复某些 jd_index 的数据
@task(name='command.ident_index_jd', ignore_result=True)
def ident_index_jd_task(ident: str):
    ident_update_jd_info(ident)
