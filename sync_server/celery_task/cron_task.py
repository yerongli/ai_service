from celery.task import task

from preprocess.recommend import all_re_recommend_resume_from_jd


@task(name="cron_task.all_re_recommend_resume_from_jd", ignore_result=True)
def all_re_recommend_resume_from_jd_task():
    # 离线为所有租户的所有jd重新推荐
    # 每日凌晨更新一次
    all_re_recommend_resume_from_jd()
