import json
from typing import Any, Dict

from bson import json_util
from celery.task import task

from config import logger
from database.mongodb.models import SyncLog
from preprocess.jd_index import ident_update_jd_info


@task(name='joborder.insert', ignore_result=True)
def joborder_insert(document: str):
    document: Dict[str, Any] = json.loads(
        document, object_hook=json_util.object_hook)
    ident: str = document['documentKey']['_id']
    sync_log = SyncLog.objects(ident=str(ident)).modify(
        upsert=True, new=True, status='in progress')
    logger.info(f'{ident} inserting')
    ident_update_jd_info(ident)
    sync_log.status = 'success'
    sync_log.save()


@task(name='joborder.update', ignore_result=True)
def joborder_update(document: str):
    document: Dict[str, Any] = json.loads(
        document, object_hook=json_util.object_hook)
    ident: str = document['documentKey']['_id']
    sync_log = SyncLog.objects(ident=str(ident)).modify(
        upsert=True, new=True, status='in progress')
    logger.info(f'{ident} updating')
    ident_update_jd_info(ident)
    sync_log.status = 'success'
    sync_log.save()
