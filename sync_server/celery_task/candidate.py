import json
from typing import Any, Dict

from bson import json_util
from celery.task import task

from config import logger
from database.mongodb.models import SyncLog
from preprocess.resume_index import ident_update_resume


@task(name='candidate.insert', ignore_result=True)
def candidate_insert(document: str):
    document: Dict[str, Any] = json.loads(
        document, object_hook=json_util.object_hook)
    ident = document['documentKey']['_id']
    sync_log = SyncLog.objects(ident=str(ident)).modify(
        upsert=True, new=True, status='in progress')
    logger.info(f'{ident} inserting')
    ident_update_resume(ident)
    sync_log.status = 'success'
    sync_log.save()


@task(name='candidate.update', ignore_result=True)
def candidate_update(document: str):
    document: Dict[str, Any] = json.loads(
        document, object_hook=json_util.object_hook)
    ident = document['documentKey']['_id']
    sync_log = SyncLog.objects(ident=str(ident)).modify(
        upsert=True, new=True, status='in progress')
    logger.info(f'{ident} updating')
    ident_update_resume(ident)
    sync_log.status = 'success'
    sync_log.save()
