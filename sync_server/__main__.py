import json
import logging
import time
from concurrent.futures import ThreadPoolExecutor

import gllue_data_client
import grpc
from bson import json_util
from grpc._channel import _Rendezvous

from database.mongodb.models import SyncLog
from config import ai_service_config, logger
from sync_server.worker import celery_app

gllue_data_client.init_channel(host=ai_service_config['GLLUE_DATA_HOST'],
                               port=ai_service_config['GLLUE_DATA_PORT'])

client = gllue_data_client.client.Client()


class MaxRetryError(Exception):
    pass


def process_event(event: dict):
    '''event schema::
    {
       _id : str,
       "operationType" : "<operation>",
       "fullDocument" : { <document> },
       "ns" : {
          "db" : "<database>",
          "coll" : "<collection>"
       },
       "to" : {
          "db" : "<database>",
          "coll" : "<collection>"
       },
       "documentKey" : { "_id" : str },
       "updateDescription" : {
          "updatedFields" : { <document> },
          "removedFields" : [ "<field>", ... ]
       }
       "clusterTime" : <Timestamp>,
       "txnNumber" : <NumberLong>,
       "lsid" : {
          "id" : <UUID>,
          "uid" : <BinData>
       }
    }
    :param event:
    :return:
    '''

    try:
        SyncLog.objects(ident=str(event['documentKey']['_id'])).modify(
            upsert=True, new=True, status='before task call')
        document = json.dumps(event, default=json_util.default)
        celery_app.tasks[
            f'{event["ns"]["coll"]}.{event["operationType"]}'].delay(
                document=document)
    except Exception:
        logging.exception('call celery_app.tasks error')


if __name__ == '__main__':
    logger.info(
        f"listen to {ai_service_config['GLLUE_DATA_HOST']}:{ai_service_config['GLLUE_DATA_PORT']}"
    )
    thread_worker = ThreadPoolExecutor(max_workers=10)
    try:
        retries = 5
        while retries:
            try:
                for event in client.subscribe_events():
                    result = thread_worker.submit(process_event, event=event)
            except _Rendezvous as error:
                if error.code() == grpc.StatusCode.UNAVAILABLE:
                    time.sleep(1)
                    retries -= 1
                    continue
                logging.exception('gPRC 异常')
        else:
            raise MaxRetryError()
    finally:
        thread_worker.shutdown()
