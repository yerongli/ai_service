==============
AI Service
==============

AI 服务器

* 文档: https://docs.gllue.net/docs/ai-service/en/latest/index.html

Development
---------------

MacOS
========

前置依赖
:::::::::

* `Conda` - `文档 <https://conda.io/docs/user-guide/install/index.html>`_
* `Docker` - `docker-for-mac <https://mirrors.aliyun.com/docker-toolbox/mac/docker-for-mac/stable/>`_
* `Git`
* `ai_models` - `ai_models <https://gitlab.gllue.com/angelo.li/ai_model_file>`_

拉取代码
:::::::::

.. code-block:: shell

    git clone git@gitlab.gllue.com:gllue/ai_service.git ~/ai_service
    cd ~/ai_service

命令行初始化
::::::::::::

初始化Python虚拟化环境
'''''''''''''''''''''''

.. code-block:: shell

    conda-env create -f environment.yml
    source activate ai-service

初始化数据库
''''''''''''

.. code-block:: shell

    docker volume create mongodata
    docker run --name mongodb -p 27017:27017 -v mongodata:/data/db -d mongo
    brew install redis
    brew services start redis

配置ai_models地址
'''''''''''''''''''

.. code-block:: shell

    vim /opt/web/ai_service_config.json

    {
        "MODEL_PATH": "your_ai_models_path/"
    }

增添git hook
'''''''''''''

.. code-block:: shell

    pre-commit install


Go
--------

.. code-block:: shell

    source activate ai-service
    python -m engine_service.your_engine


更新protobuff文件
----------------------

.. code-block:: shell

    source activate ai-service
    export AI_LIBS_TEST=TEST && make run-all
