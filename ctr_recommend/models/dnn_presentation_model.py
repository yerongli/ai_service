from keras.layers import *
from keras.models import Model

from ctr_recommend.config import *


class IndexValueEmbedding(Embedding):
    '''
    Example:
        model = Sequential()
        model.add(InputLayer(input_shape=(100,2)))
        model.add(IndexValueEmbedding(1000, 200))

    input_data: List[[Index, Weight]] = [[1, 0.1], [2, 0.5]]
    '''

    def __init__(self, *args, **kwargs):
        self.multiply = Multiply()
        super().__init__(*args, **kwargs)

    def call(self, inputs):
        if isinstance(inputs, list):
            index_list, weight_list = inputs
            input_len = len(K.get_variable_shape(index_list))
            weight_list = K.expand_dims(weight_list, axis=input_len)
            if K.dtype(index_list) != 'int32':
                index_list = K.cast(index_list, 'int32')
            index_list = K.gather(self.embeddings, index_list)
            out = self.multiply([index_list, weight_list])
            return out

        else:
            return super().call(inputs)

    def compute_output_shape(self, input_shape):
        if isinstance(input_shape[0], tuple):
            return input_shape[0] + (self.output_dim, )
        else:
            return super().compute_output_shape(input_shape)


class AttentionLayer(Layer):
    def __init__(self, **kwargs):
        super(AttentionLayer, self).__init__(**kwargs)

    def build(self, input_shape):
        assert len(input_shape) == 3
        # W.shape = (time_steps, time_steps)
        self.W = self.add_weight(
            name='att_weight',
            shape=(input_shape[1], input_shape[1]),
            initializer='uniform',
            trainable=True)
        super(AttentionLayer, self).build(input_shape)

    def call(self, inputs, mask=None):
        # inputs.shape = (batch_size, time_steps, seq_len)
        x = K.permute_dimensions(inputs, (0, 2, 1))
        # x.shape = (batch_size, seq_len, time_steps)
        # general
        a = K.softmax(K.tanh(K.dot(x, self.W)))
        a = K.permute_dimensions(a, (0, 2, 1))
        outputs = a * inputs
        outputs = K.sum(outputs, axis=1)
        return outputs

    def compute_output_shape(self, input_shape):
        return input_shape[0], input_shape[2]


class BatchDotLayer(Layer):
    def __init__(self,
                 units,
                 use_bias=True,
                 bias_initializer='zeros',
                 bias_regularizer=None,
                 **kwargs):
        self.units = units
        self.bias = None
        self.use_bias = use_bias
        self.bias_initializer = initializers.get(bias_initializer)
        self.bias_regularizer = regularizers.get(bias_regularizer)

        super().__init__(**kwargs)

    def build(self, input_shape):
        if self.use_bias:
            self.bias = self.add_weight(
                shape=(self.units, ),
                name='bias',
                initializer=self.bias_initializer,
                regularizer=self.bias_regularizer)

    def call(self, inputs, **kwargs):
        jd_features, cv_samples = inputs
        outputs = K.softmax(K.batch_dot(jd_features, cv_samples) + self.bias)
        return outputs

    def compute_output_shape(self, input_shape):
        return (input_shape[0][0], ) + (input_shape[1][-1], )


school_embed = IndexValueEmbedding(SCHOOL_NUM_LEN, 100, name='school_embed')
major_embed = IndexValueEmbedding(MAJOR_NUM_LEN, 100, name='major_embed')
title_embed = IndexValueEmbedding(TITLE_NUM_LEN, 200, name='title_embed')

desc_lda_embed = IndexValueEmbedding(LDA_NUM_LEN, 300, name='desc_lda_embed')
desc_poi_embed = IndexValueEmbedding(POI_NUM_LEN, 200, name='desc_poi_embed')


def dense_dropout_batch_norm(layer,
                             units,
                             name,
                             activation='relu',
                             drop_rate=None):
    x = Dense(
        units, activation=activation,
        name=name + '_' + 'dense_' + str(units))(layer)
    if drop_rate:
        x = Dropout(drop_rate, name=name + '_' + 'dropout_' + str(units))(x)
    x = BatchNormalization(name=name + '_' + 'batch_norm_' + str(units))(x)
    return x


def get_cv_basic_info_model():
    '''
    简历的基础信息表征
    input: [age, edu_year, exp_year, current_salary]
    :return:
    '''
    cv_basic_info_input = Input(shape=(4, ), name='cv_basic_info_input')
    cv_basic_info_features = dense_dropout_batch_norm(
        cv_basic_info_input, 32, 'cv_basic_info_features')
    return [cv_basic_info_input], cv_basic_info_features


def get_cv_edu_model():
    '''
    简历的教育信息表征
    input: [degree, start, end, total_year, 985_211, abroad, qs200]
    :return:
    '''

    cv_edu_other_input = Input(
        shape=(CV_MAX_EDU_LEN, 7), name='cv_edu_other_input')
    cv_edu_school_input = Input(
        shape=(CV_MAX_EDU_LEN, ), name='cv_edu_school_input')
    cv_edu_major_input = Input(
        shape=(CV_MAX_EDU_LEN, ), name='cv_edu_major_input')

    school_features = school_embed(cv_edu_school_input)
    major_features = major_embed(cv_edu_major_input)

    cv_edu_other_features = Dense(
        56, name="cv_edu_other_features")(cv_edu_other_input)

    cv_edu_features_concat = Concatenate(
        axis=2, name="cv_edu_features_concat")(
            [school_features, major_features, cv_edu_other_features])

    cv_edu_rnn = GRU(
        128, return_sequences=True, name="cv_edu_rnn")(cv_edu_features_concat)
    cv_edu_att = AttentionLayer(name="cv_edu_att")(cv_edu_rnn)

    inputs = [cv_edu_other_input, cv_edu_school_input, cv_edu_major_input]

    return inputs, cv_edu_att


def get_cv_exp_model():
    """
    简历的工作信息表征
    input: [start, end, total_year, title_level]
    :return:
    """
    cv_exp_other_input = Input(
        shape=(CV_MAX_EXP_LEN, 4), name='cv_exp_other_input')

    cv_exp_title_input = Input(
        shape=(CV_MAX_EXP_LEN, ), name='cv_exp_title_input')
    # cv_exp_title_weight_input = Input(shape=(CV_MAX_EXP_LEN, CV_MAX_TITLE_LEN,), name='cv_exp_title_weight_input')

    cv_exp_desc_lda_input = Input(
        shape=(
            CV_MAX_EXP_LEN,
            CV_MAX_LDA_LEN,
        ), name='cv_exp_desc_lda_input')
    cv_exp_desc_lda_weight_input = Input(
        shape=(
            CV_MAX_EXP_LEN,
            CV_MAX_LDA_LEN,
        ),
        name='cv_exp_desc_lda_weight_input')

    cv_exp_desc_poi_input = Input(
        shape=(
            CV_MAX_EXP_LEN,
            CV_MAX_POI_LEN,
        ), name='cv_exp_desc_poi_input')
    cv_exp_desc_poi_weight_input = Input(
        shape=(
            CV_MAX_EXP_LEN,
            CV_MAX_POI_LEN,
        ),
        name='cv_exp_desc_poi_weight_input')

    title_features = title_embed(cv_exp_title_input)

    desc_lda_features = desc_lda_embed(
        [cv_exp_desc_lda_input, cv_exp_desc_lda_weight_input])
    desc_poi_features = desc_poi_embed(
        [cv_exp_desc_poi_input, cv_exp_desc_poi_weight_input])

    # title_features = Lambda(
    #     lambda x: K.max(x, axis=2), name="lambda_title_max")(title_features)
    desc_lda_features = Lambda(
        lambda x: K.max(x, axis=2),
        name="lambda_desc_lda_max")(desc_lda_features)
    desc_poi_features = Lambda(
        lambda x: K.max(x, axis=2),
        name="lambda_desc_poi_max")(desc_poi_features)

    cv_exp_other_features = Dense(
        32, name="cv_exp_other_features_dense")(cv_exp_other_input)

    cv_exp_features_concat = Concatenate(
        axis=2, name="cv_exp_features_concat")([
            cv_exp_other_features, title_features, desc_lda_features,
            desc_poi_features
        ])

    cv_exp_rnn = GRU(
        128, return_sequences=True, name="cv_exp_rnn")(cv_exp_features_concat)
    cv_exp_att = AttentionLayer(name="cv_exp_att")(cv_exp_rnn)

    inputs = [
        cv_exp_other_input,
        cv_exp_title_input,
        # cv_exp_title_weight_input,
        cv_exp_desc_lda_input,
        cv_exp_desc_lda_weight_input,
        cv_exp_desc_poi_input,
        cv_exp_desc_poi_weight_input
    ]
    return inputs, cv_exp_att


def get_jd_basic_info_model():
    """
    jd的基础信息表征
    input: [lowest_degree, need_985_211, title_level, working_years_range, salary_range, #age_range]
    :return:
    """
    jd_basic_other_input = Input(shape=(7, ), name='jd_basic_other_input')
    jd_major_input = Input(shape=(JD_MAX_MAJOR_LEN, ), name='jd_major_input')
    jd_title_input = Input(shape=(JD_MAX_TITLE_LEN, ), name='jd_title_input')

    jd_desc_lda_input = Input(
        shape=(JD_MAX_LDA_LEN, ), name='jd_desc_lda_input')
    jd_desc_lda_weight_input = Input(
        shape=(JD_MAX_LDA_LEN, ), name='jd_desc_lda_weight_input')

    jd_desc_poi_input = Input(
        shape=(JD_MAX_POI_LEN, ), name='jd_desc_poi_input')
    jd_desc_poi_weight_input = Input(
        shape=(JD_MAX_POI_LEN, ), name='jd_desc_poi_weight_input')

    jd_basic_other_features = Dense(
        124, name="jd_basic_other_features_dense")(jd_basic_other_input)

    jd_major_features = Lambda(
        lambda x: K.mean(x, axis=1),
        name="lambda_jd_major_mean")(major_embed(jd_major_input))
    jd_title_features = Lambda(
        lambda x: K.max(x, axis=1),
        name="lambda_jd_title_max")(title_embed(jd_title_input))

    jd_desc_lda_embed_features_multiply = desc_lda_embed(
        [jd_desc_lda_input, jd_desc_lda_weight_input])
    jd_desc_lda_features = GlobalMaxPool1D(
        name='jd_desc_lda_max_pool')(jd_desc_lda_embed_features_multiply)

    jd_desc_poi_embed_features_multiply = desc_poi_embed(
        [jd_desc_poi_input, jd_desc_poi_weight_input])
    jd_desc_poi_features = GlobalMaxPool1D(
        name='jd_desc_poi_max_pool')(jd_desc_poi_embed_features_multiply)

    jd_basic_info_features_concat = Concatenate(
        name="jd_basic_info_features_concat")([
            jd_basic_other_features, jd_major_features, jd_title_features,
            jd_desc_lda_features, jd_desc_poi_features
        ])

    jd_basic_info_features = dense_dropout_batch_norm(
        jd_basic_info_features_concat, 256, 'jd_basic_info_features')

    inputs = [
        jd_basic_other_input, jd_major_input, jd_title_input,
        jd_desc_lda_input, jd_desc_lda_weight_input, jd_desc_poi_input,
        jd_desc_poi_weight_input
    ]
    return inputs, jd_basic_info_features


def get_jd_preference_model():
    """
    jd的人才偏好信息表征
    input: [degree, level, working_year, salary, # age]
    :return:
    """
    jd_pref_other_input = Input(shape=(5, ), name='jd_pref_other_input')

    jd_pref_major_input = Input(
        shape=(JD_MAX_PREF_MAJOR_LEN, ), name='jd_pref_major_input')
    jd_pref_major_weight_input = Input(
        shape=(JD_MAX_PREF_MAJOR_LEN, ), name='jd_pref_major_weight_input')

    jd_pref_title_input = Input(
        shape=(JD_MAX_PREF_TITLE_LEN, ), name='jd_pref_title_input')
    jd_pref_title_weight_input = Input(
        shape=(JD_MAX_PREF_TITLE_LEN, ), name='jd_pref_title_weight_input')

    jd_pref_desc_poi_input = Input(
        shape=(JD_MAX_PREF_POI_LEN, ), name='jd_pref_desc_poi_input')
    jd_pref_desc_poi_weight_input = Input(
        shape=(JD_MAX_PREF_POI_LEN, ), name='jd_pref_desc_poi_weight_input')

    jd_pref_desc_lda_input = Input(
        shape=(JD_MAX_PREF_LDA_LEN, ), name='jd_pref_desc_lda_input')
    jd_pref_desc_lda_weight_input = Input(
        shape=(JD_MAX_PREF_LDA_LEN, ), name='jd_pref_desc_lda_weight_input')

    jd_pref_other_features = Dense(
        128, name="jd_pref_other_features_dense")(jd_pref_other_input)

    jd_pref_major_features = major_embed(
        [jd_pref_major_input, jd_pref_major_weight_input])
    jd_pref_title_features = title_embed(
        [jd_pref_title_input, jd_pref_title_weight_input])
    jd_pref_desc_lda_features = desc_lda_embed(
        [jd_pref_desc_lda_input, jd_pref_desc_lda_weight_input])
    jd_pref_desc_poi_features = desc_poi_embed(
        [jd_pref_desc_poi_input, jd_pref_desc_poi_weight_input])

    jd_pref_major_features = Lambda(lambda x: K.mean(x, axis=1))(
        jd_pref_major_features)
    jd_pref_title_features = Lambda(lambda x: K.mean(x, axis=1))(
        jd_pref_title_features)
    jd_pref_desc_lda_features = Lambda(lambda x: K.mean(x, axis=1))(
        jd_pref_desc_lda_features)
    jd_pref_desc_poi_features = Lambda(lambda x: K.mean(x, axis=1))(
        jd_pref_desc_poi_features)

    jd_pref_features_concat = Concatenate(name="jd_pref_features_concat")([
        jd_pref_major_features, jd_pref_title_features,
        jd_pref_desc_lda_features, jd_pref_desc_poi_features,
        jd_pref_other_features
    ])

    jd_pref_features = dense_dropout_batch_norm(jd_pref_features_concat, 256,
                                                'jd_pref_features')

    inputs = [
        jd_pref_other_input, jd_pref_major_input, jd_pref_major_weight_input,
        jd_pref_title_input, jd_pref_title_weight_input,
        jd_pref_desc_poi_input, jd_pref_desc_poi_weight_input,
        jd_pref_desc_lda_input, jd_pref_desc_lda_weight_input
    ]

    return inputs, jd_pref_features


def get_jd_model():
    jd_basic_info_inputs, jd_basic_info_features = get_jd_basic_info_model()
    jd_pref_inputs, jd_pref_features = get_jd_preference_model()

    jd_features = Concatenate(name="jd_features_concat")(
        [jd_basic_info_features, jd_pref_features])

    jd_features = dense_dropout_batch_norm(
        jd_features, 256, 'jd_presentation', drop_rate=0.3)
    jd_features = dense_dropout_batch_norm(
        jd_features, 128, 'jd_presentation', drop_rate=0.3)

    model = Model(
        inputs=jd_basic_info_inputs + jd_pref_inputs, outputs=jd_features)
    return model


def get_cv_model():
    cv_basic_info_inputs, cv_basic_info_out = get_cv_basic_info_model()
    cv_edu_inputs, cv_edu_out = get_cv_edu_model()
    cv_exp_inputs, cv_exp_out = get_cv_exp_model()

    cv_features_concat = Concatenate(name="cv_features_concat")(
        [cv_basic_info_out, cv_edu_out, cv_exp_out])

    cv_features = dense_dropout_batch_norm(
        cv_features_concat, 256, 'cv_presentation', drop_rate=0.3)
    cv_features = dense_dropout_batch_norm(
        cv_features, 128, 'cv_presentation', drop_rate=0.3)

    model = Model(
        inputs=cv_basic_info_inputs + cv_edu_inputs + cv_exp_inputs,
        outputs=cv_features)
    return model


def get_jd_cv_match_model():
    jd_model = get_jd_model()
    cv_model = get_cv_model()

    jd_inputs = jd_model.inputs
    cv_inputs = cv_model.inputs

    jd_features = jd_model.outputs
    cv_features = cv_model.outputs

    match_features_concat = Concatenate(
        name="match_features_concat")(jd_features + cv_features)

    features = dense_dropout_batch_norm(match_features_concat, 64,
                                        'match_features')
    out = dense_dropout_batch_norm(
        features, 1, "match_out", activation='sigmoid')

    model = Model(inputs=jd_inputs + cv_inputs, outputs=out)
    return model


def get_jd_cv_sample_match_model(nb_negative=16):
    jd_model = get_jd_model()
    cv_model = get_cv_model()

    jd_inputs = jd_model.inputs
    cv_inputs = cv_model.inputs

    jd_features = jd_model.output
    cv_features = cv_model.output

    cv_negatives = Lambda(
        lambda x: K.random_normal((K.shape(x)[0], nb_negative, K.shape(x)[1])),
        name='negatives_random_cv')(cv_features)
    cv_features = Lambda(lambda x: K.expand_dims(x, 1))(cv_features)
    cv_samples = Lambda(
        lambda x: K.concatenate(x, axis=1),
        name='title_concatenate')([cv_features, cv_negatives])

    cv_samples = Permute((2, 1))(cv_samples)

    out = BatchDotLayer(1, name='batch_dot_jd_cv')([jd_features, cv_samples])
    model = Model(inputs=jd_inputs + cv_inputs, outputs=out)
    model.compile(
        loss='sparse_categorical_crossentropy',
        optimizer='sgd',
        metrics=['accuracy', 'sparse_categorical_accuracy'])
    return model
