import random
from collections import defaultdict
from typing import Dict, List
from uuid import uuid4

import numpy as np

from common.utils import link_tracking_time
from common.jd_index.jd_util import JobOrderMatchUtil
from common.recall import resume_recall as resume_recall
from common.resume_recommend import (ranker, rule_filter, schema_transformer,
                                     source_fields)
from ctr_recommend.utils import (get_jd_dnn_presentation_features,
                                 get_resume_dnn_presentation_features)
from database.elasticsearch.config import elastic_resume_feature_searcher
from database.elasticsearch.models import JobOrderFeatures, ResumeFeatures
from database.mongodb.operate import jobsubmission_find
from preprocess.resume_index import index_single_resume


class BaseGeneratorUuid(object):
    def __init__(self):
        self.link_uuid = uuid4()


class SimilarResume(BaseGeneratorUuid):
    def __init__(self, tenant):
        self.tenant = tenant
        super().__init__()

    # 人人匹配-库外建立，需要做画像
    @link_tracking_time
    def similar_resume_from_resume(self,
                                   resume_info: dict,
                                   topn: int = 10,
                                   min_score: float = 0.) -> dict:
        resume_index = self.__index_single_resume(resume_info)
        resume_index = ResumeFeatures.from_json(resume_index)
        return self._similar_resume_from_resume_index(
            resume_index, topn=topn, min_score=min_score)

    # 人人匹配
    @link_tracking_time
    def similar_resume_from_resume_id(self,
                                      resume_info: dict,
                                      topn: int = 10,
                                      min_score: float = 0.) -> dict:
        resume_index = ResumeFeatures.get_with_tenant_and_aid(
            resume_info['tenant'],
            resume_info['aid'],
            source=source_fields.cv_cv_match_field_list)

        return self._similar_resume_from_resume_index(
            resume_index, topn=topn, min_score=min_score)

    # 人人匹配-从resume_index
    @link_tracking_time
    def _similar_resume_from_resume_index(self,
                                          resume_index: ResumeFeatures,
                                          topn: int = 10,
                                          min_score: float = 0.) -> dict:
        score_list = self.__similar_resume_from_resume_index_use_description(
            resume_index, topn, min_score)
        company_title_score_list = self.__similar_resume_from_resume_index_use_company_title(
            resume_index)
        items = score_list[:7] + company_title_score_list[:3]
        result = {"link_uuid": self.link_uuid, "items": items}
        return result

    @link_tracking_time
    def __index_single_resume(self, resume_info: dict) -> dict:
        return index_single_resume(resume_info)

    @link_tracking_time
    def __recall_for_use_description(self, resume_index: ResumeFeatures):
        # 召回
        lda_aid_set = resume_recall.use_description_lda_weight(
            resume_index.experiences.description_lda_weight,
            self.tenant,
            topn=1000,
            min_score=0.35)

        title_aid_set = resume_recall.use_title_weight(
            resume_index.experiences.title_weight,
            self.tenant,
            topn=500,
            min_score=0.5)

        aid_set = lda_aid_set | title_aid_set
        return aid_set

    @link_tracking_time
    def __get_data_for_similar_resume(self, aid_list: list):
        query = elastic_resume_feature_searcher.filter(
            "term", tenant=self.tenant).filter(
                "terms",
                aid=aid_list).source(source_fields.cv_cv_match_field_list)

        data: List[dict] = [
            schema_transformer.resume_index_to_cv_cv_match_dict(i.to_dict())
            for i in query.scan()
        ]
        return data

    @link_tracking_time
    def __batch_similar_resume_resume(
            self, resume_index, candidate_resume_list: list) -> List[float]:
        return ranker.batch_similar_resume_resume(resume_index,
                                                  candidate_resume_list)

    # 人人匹配， 使用 lda, title 等召回
    @link_tracking_time
    def __similar_resume_from_resume_index_use_description(
            self,
            resume_index: ResumeFeatures,
            topn: int = 1000,
            min_score: float = 0.35) -> List[Dict]:
        aid_set = self.__recall_for_use_description(resume_index)

        resume_recall.record_recall(
            self.link_uuid, self.tenant, dtype='SRD', aid_set=aid_set)

        # 过滤
        aid_set = list(rule_filter.filter_id(aid_set, resume_index['aid']))

        # 获取数据
        data = self.__get_data_for_similar_resume(aid_set)

        resume_index = schema_transformer.resume_index_to_cv_cv_match_dict(
            resume_index.to_dict())

        # 计算分数
        score_list = self.__batch_similar_resume_resume(resume_index, data)

        result = list()
        for candidate_resume, score in zip(data, score_list):
            if score >= min_score:
                result.append({
                    'aid': candidate_resume['aid'],
                    'score': score,
                    "tenant": self.tenant
                })
        else:
            result.sort(key=lambda x: x['score'], reverse=True)
        return result[:topn]

    @link_tracking_time
    def __recall_for_use_company_title(self,
                                       resume_index: ResumeFeatures) -> set:
        company_list = {
            i.company_stand
            for i in resume_index.experiences.work_list
        }
        title_list = {
            i.title_stand
            for i in resume_index.experiences.work_list
        }

        aid_set = resume_recall.use_target_company_title(
            company_list, title_list, tenant=self.tenant)
        return aid_set

    # 人人匹配， 使用company-title联合召回.目的是找到相同公司相同职位的人
    @link_tracking_time
    def __similar_resume_from_resume_index_use_company_title(
            self,
            resume_index: ResumeFeatures,
            topn: int = 1000,
            min_score: float = 0.35) -> List[Dict]:

        # 召回
        aid_set = self.__recall_for_use_company_title(resume_index)

        resume_recall.record_recall(self.link_uuid, self.tenant, "SRCT",
                                    aid_set)

        # 过滤
        aid_set = list(rule_filter.filter_id(aid_set, resume_index['aid']))

        # 获取data
        data = self.__get_data_for_similar_resume(aid_set)

        resume_index = schema_transformer.resume_index_to_cv_cv_match_dict(
            resume_index.to_dict())

        score_list = self.__batch_similar_resume_resume(resume_index, data)

        result = list()
        for candidate_resume, score in zip(data, score_list):
            if score >= min_score:
                result.append({
                    'aid': candidate_resume['aid'],
                    'score': score,
                    "tenant": self.tenant
                })
        else:
            result.sort(key=lambda x: x['score'], reverse=True)
        return result[:topn]


class RecommendResume(BaseGeneratorUuid):
    def __init__(self, tenant):
        self.tenant = tenant
        super().__init__()

    # 简岗匹配
    @link_tracking_time
    def recommend_resume_from_jd_info(self,
                                      jd_info: dict,
                                      topn: int = 2000,
                                      min_score: float = 0.1) -> dict:
        # jd_info: aid, tenant, version, custom_prefer_list
        # 返回字段包含 aid, score, reason

        jd = self.__get_and_upgrade_jd(jd_info)
        return self._recommend_resume_from_jd(jd, topn, min_score)

    # jd_index 和cv 的分数
    @link_tracking_time
    def score_jd_cv(self, jd_info: dict, resume_info: dict) -> float:
        assert jd_info['tenant'] == resume_info['tenant'], "tenant 不匹配"

        jd = self.__get_and_upgrade_jd(jd_info)

        # 检查 resume_index 信息
        resume = ResumeFeatures.get_with_tenant_and_aid(
            resume_info['tenant'],
            resume_info['aid'],
            source=source_fields.jd_cv_match_field_list)

        if not resume:
            raise Exception('不存在的简历 [{aid}]'.format(aid=resume_info['aid']))
        resume_info = schema_transformer.resume_index_to_jd_cv_match_dict(
            resume.to_dict())

        score = ranker.distance_jd_resume(jd, resume_info)
        return score

    def recommend_use_ctr(self, jd_info):
        jd = self.__get_and_upgrade_jd(jd_info)

        recommend_reason_dict = defaultdict(list)
        aid_set = self.__recall_for_recommend_resume_from_jd(
            jd, recommend_reason_dict)

        resume_recall.record_recall(self.link_uuid, self.tenant, "RRJ",
                                    aid_set)

        # mismatch_aid_set = rule_filter.get_jd_resume_mismatch_aid_set(
        #     tenant=self.tenant, jd_aid=jd_index.aid)
        # aid_set = list(
        #     rule_filter.filter_ids(aid_set, mismatch_aid_set))

        aid_set = list(aid_set)

        # 获取获选简历
        data = self.__get_data_for_recommend_resume_from_jd(aid_set)
        data = get_resume_dnn_presentation_features(data)
        jd_data = get_jd_dnn_presentation_features(jd)
        jd_data = [np.repeat(i, data[0].shape[0], axis=0) for i in jd_data]
        return jd_data, data, aid_set

    def get_jd(self, tenant, aid):
        jd_info = {'tenant': tenant, 'aid': aid}
        jd = self.__get_and_upgrade_jd(jd_info)
        return jd

    def score_ctr_data(self, tenant, jd_aid, resume_id_list):
        jd_info = {'tenant': tenant, 'aid': jd_aid}
        jd = self.__get_and_upgrade_jd(jd_info)
        data = self.__get_data_for_recommend_resume_from_jd(resume_id_list)
        data = get_mixed_features(jd, data)
        data = np.concatenate(data, axis=1)
        jd_data = get_jd_dnn_presentation_features(jd)
        jd_data = np.repeat(jd_data, data.shape[0], axis=0)
        data = np.concatenate([jd_data, data], axis=1)
        return data

    def get_all_ctr_data(self, tenant):
        import tqdm
        jd_list = []
        resume_list = []
        result_list = []
        for i in tqdm.tqdm(range(1, 226)):
            try:
                jdm = JobOrderFeatures.get_with_tenant_and_aid(tenant, i)
                if len(jdm.content_clean) >= 20:
                    jd_info = {'tenant': tenant, 'aid': i}
                    x = self.get_ctr_data(jd_info)
                    if x is None:
                        continue
                    jd_data, data, Y = x
                    jd_list.append(jd_data)
                    resume_list.append(data)
                    result_list.append(Y)
            except Exception as e:
                print(i, e)

        jd_result = []
        resume_result = []

        for i in range(len(jd_list[0])):
            jd_result.append(np.concatenate([j[i] for j in jd_list]))

        for i in range(len(resume_list[0])):
            resume_result.append(np.concatenate([j[i] for j in resume_list]))

        Y_result = np.concatenate(result_list)

        return jd_result, resume_result, Y_result

    def get_ctr_predict_data(self, jd_info):
        jobsubmission_list = jobsubmission_find(
            query={
                'client_key': jd_info['tenant'],
                'joborder': jd_info['aid']
            },
            projection={
                'candidate': 1,
                'joborder': 1,
                'max_status': 1
            })
        candidate = [
            i['candidate'] for i in jobsubmission_list if i['candidate']
        ]
        jd = self.__get_and_upgrade_jd(jd_info)
        data = self.__get_data_for_recommend_resume_from_jd(candidate)

        data = get_resume_dnn_presentation_features(data)
        jd_data = get_jd_dnn_presentation_features(jd)
        jd_data = [np.repeat(i, data[0].shape[0], axis=0) for i in jd_data]
        return jd_data, data, candidate

    def get_all_ctr_sample_data(self):
        while True:
            for jdm in JobOrderFeatures.search().scan():
                try:
                    if len(jdm.content_clean) >= 20:
                        (jd_half_data, half_data,
                         Y_half_data), (jd_one_data, one_data,
                                        Y_one_data) = self.get_ctr_sample_data(
                                            jdm.tenant, jdm.aid)

                        if jd_half_data[0].shape[0] != 0:
                            yield jd_half_data + half_data, Y_half_data
                        if jd_one_data[0].shape[0] != 0:
                            yield jd_one_data + one_data, Y_one_data
                except Exception:
                    continue

    def get_all_ctr_sample_fusion_data(self):
        import tqdm
        all_data = []
        all_y = []
        data = list(JobOrderFeatures.search().scan())
        for jdm in tqdm.tqdm(data):
            try:
                if len(jdm.content_clean) >= 20:
                    jd_data, data, y = self.get_ctr_sample_fusion_data(
                        jdm.tenant, jdm.aid)
                    all_data.append(jd_data + data)
                    all_y.append(y)
                    # yield jd_data + data, y
            except KeyboardInterrupt:
                raise KeyboardInterrupt
            except Exception as e:
                print(e)
                continue
        else:
            rst = []
            for i in range(len(all_data[0])):
                rst.append(np.concatenate([d[i] for d in all_data]))
            else:
                all_data = rst
            all_y = np.concatenate(all_y)

        return all_data, all_y

    def get_ctr_sample_fusion_data(self, tenant, aid):
        jobsubmission_list = jobsubmission_find(
            query={
                'client_key': tenant,
                'joborder': aid
            },
            projection={
                'candidate': 1,
                'joborder': 1,
                'max_status': 1
            })

        half_aid_set = {
            i['candidate']
            for i in jobsubmission_list
            if not i['max_status'] and i['candidate'] is not None
        }
        one_aid_set = {
            i['candidate']
            for i in jobsubmission_list
            if i['max_status'] is not None and i['candidate'] is not None
        }
        if not half_aid_set or not one_aid_set:
            raise Exception("nononono")
        zero_aid_set = list(
            random.sample(
                list(range(1, 200000)), len(one_aid_set | half_aid_set)))
        zero_aid_set = [
            i for i in zero_aid_set if i not in (one_aid_set | half_aid_set)
        ]

        half_data = self.__get_data_for_recommend_resume_from_jd(
            list(half_aid_set))
        one_data = self.__get_data_for_recommend_resume_from_jd(
            list(one_aid_set))
        zero_data = self.__get_data_for_recommend_resume_from_jd(zero_aid_set)

        jd = JobOrderFeatures.get_with_tenant_and_aid(tenant=tenant, aid=aid)

        data = get_resume_dnn_presentation_features(zero_data + half_data +
                                                    one_data)
        y = []

        y += [1] * len(half_data)
        half_data = get_resume_dnn_presentation_features(half_data)

        y += [1] * len(one_data)
        one_data = get_resume_dnn_presentation_features(one_data)

        y += [0] * len(zero_data)
        zero_data = get_resume_dnn_presentation_features(zero_data)

        jd_data = get_jd_dnn_presentation_features(
            jd,
            lengths=half_data[0].shape[0] + one_data[0].shape[0] +
            zero_data[0].shape[0])
        # jd_one_data = get_jd_dnn_presentation_features(jd_index, lengths=one_data[0].shape[0])
        # jd_zero_data = get_jd_dnn_presentation_features(jd_index, lengths=zero_data[0].shape[0])
        y = np.array(y).reshape(-1, 1)
        return jd_data, data, y

    def get_ctr_sample_data(self, tenant, aid):
        jobsubmission_list = jobsubmission_find(
            query={
                'client_key': tenant,
                'joborder': aid
            },
            projection={
                'candidate': 1,
                'joborder': 1,
                'max_status': 1
            })

        half_aid_set = {
            i['candidate']
            for i in jobsubmission_list
            if not i['max_status'] and i['candidate'] is not None
        }
        one_aid_set = {
            i['candidate']
            for i in jobsubmission_list
            if i['max_status'] is not None and i['candidate'] is not None
        }

        half_data = self.__get_data_for_recommend_resume_from_jd(
            list(half_aid_set))
        one_data = self.__get_data_for_recommend_resume_from_jd(
            list(one_aid_set))

        jd = JobOrderFeatures.get_with_tenant_and_aid(tenant=tenant, aid=aid)
        # jd_data = get_jd_dnn_presentation_features(jd_index, lengths=1)

        half_data = get_resume_dnn_presentation_features(half_data)
        one_data = get_resume_dnn_presentation_features(one_data)

        jd_half_data = get_jd_dnn_presentation_features(
            jd, lengths=half_data[0].shape[0])
        jd_one_data = get_jd_dnn_presentation_features(
            jd, lengths=one_data[0].shape[0])

        Y_half_data = np.zeros((half_data[0].shape[0], 1))
        Y_half_data[:, 0] = 0.5
        Y_one_data = np.zeros((one_data[0].shape[0], 1))
        Y_one_data[:, 0] = 1
        return (jd_half_data, half_data, Y_half_data), (jd_one_data, one_data,
                                                        Y_one_data)

    def get_ctr_data(self, jd_info):
        jobsubmission_list = jobsubmission_find(
            query={
                'client_key': jd_info['tenant'],
                'joborder': jd_info['aid']
            },
            projection={
                'candidate': 1,
                'joborder': 1,
                'max_status': 1
            })
        jobsubmission_list = [i for i in jobsubmission_list if i['candidate']]

        half_aid_set = {
            i['candidate']
            for i in jobsubmission_list if not i['max_status']
        }
        one_aid_set = {
            i['candidate']
            for i in jobsubmission_list if i['max_status'] is not None
        }

        jjd = JobOrderFeatures.get_with_tenant_and_aid(
            tenant=self.tenant, aid=jd_info['aid'])
        jd = self.__get_and_upgrade_jd(jd_info)
        recommend_reason_dict = defaultdict(list)
        aid_set = self.__recall_for_recommend_resume_from_jd(
            jd, recommend_reason_dict)
        aid_set = aid_set - half_aid_set - one_aid_set

        if not (aid_set and half_aid_set and one_aid_set):
            return None
        # 获取获选简历
        zero_data = self.__get_data_for_recommend_resume_from_jd(list(aid_set))
        half_data = self.__get_data_for_recommend_resume_from_jd(
            list(half_aid_set))
        one_data = self.__get_data_for_recommend_resume_from_jd(
            list(one_aid_set))

        jd_data = get_jd_dnn_presentation_features(jjd, lengths=1)
        Y = []

        zero_data = get_resume_dnn_presentation_features(zero_data)
        [Y.append(0) for _ in range(zero_data[0].shape[0])]

        half_data = get_resume_dnn_presentation_features(half_data)
        half_data = [np.repeat(i, 5, axis=0) for i in half_data]
        [Y.append(0.5) for _ in range(half_data[0].shape[0])]

        one_data = get_resume_dnn_presentation_features(one_data)
        one_data = [np.repeat(i, 50, axis=0) for i in one_data]
        [Y.append(1) for _ in range(one_data[0].shape[0])]

        # jd_data = [np.repeat(i, len(Y), axis=0) for i in jd_data]
        data = [
            np.concatenate([x, y, z])
            for x, y, z in zip(zero_data, half_data, one_data)
        ]
        Y = np.array(Y).reshape(-1, 1)
        return jd_data, one_data, half_data, zero_data

    # 批量jd 和cv 的分数
    @link_tracking_time
    def batch_score_jd_cv(self, jd_aid: int,
                          resume_id_list: list) -> List[Dict]:
        jd_info = {'tenant': self.tenant, 'aid': jd_aid}
        jd = self.__get_and_upgrade_jd(jd_info)

        # 获取获选简历
        data = self.__get_data_for_recommend_resume_from_jd(resume_id_list)
        # 距离计算
        score_list = self.__batch_distance_jd_resume(jd, data)

        result = []
        for candidate_resume, score in zip(data, score_list):
            result.append({
                'aid': candidate_resume['aid'],
                "score": score,
                "tenant": self.tenant,
            })
        return result

    # 简岗匹配， 给出 jd_index 对象，做简岗匹配
    @link_tracking_time
    def _recommend_resume_from_jd(self,
                                  jd: JD,
                                  topn: int = 2000,
                                  min_score: float = 0.2) -> dict:

        recommend_reason_dict = defaultdict(list)
        aid_set = self.__recall_for_recommend_resume_from_jd(
            jd, recommend_reason_dict)

        resume_recall.record_recall(self.link_uuid, self.tenant, "RRJ",
                                    aid_set)

        # mismatch_aid_set = rule_filter.get_jd_resume_mismatch_aid_set(
        #     tenant=self.tenant, jd_aid=jd_index.aid)
        # aid_set = list(
        #     rule_filter.filter_ids(aid_set, mismatch_aid_set))

        aid_set = list(aid_set)

        # 获取获选简历
        data = self.__get_data_for_recommend_resume_from_jd(aid_set)
        # 距离计算
        score_list = self.__batch_distance_jd_resume(jd, data)

        result = []
        for candidate_resume, score in zip(data, score_list):
            if score >= min_score:
                result.append({
                    'aid':
                    candidate_resume['aid'],
                    "score":
                    score,
                    "tenant":
                    self.tenant,
                    "reason":
                    recommend_reason_dict[candidate_resume['aid']]
                })

        result.sort(key=lambda x: x['score'], reverse=True)
        return {"link_uuid": self.link_uuid, "items": result[:topn]}

    @link_tracking_time
    def __recall_for_recommend_resume_from_jd(
            self, jd_match_util: JobOrderMatchUtil, recommend_reason_dict):
        # 工作内容召回
        lda_aid_set = resume_recall.use_description_lda_weight(
            jd_match_util.description_lda_weight,
            self.tenant,
            topn=1500,
            min_score=0.35)
        self.__set_recommend_reason(recommend_reason_dict, lda_aid_set,
                                    "工作内容相似")

        if jd_match_util.cp_description_lda_weight:
            _lda_aid_set = resume_recall.use_description_lda_weight(
                jd_match_util.cp_description_lda_weight,
                self.tenant,
                topn=1500,
                min_score=0.35)
            self.__set_recommend_reason(recommend_reason_dict, _lda_aid_set,
                                        "工作内容相似")
            lda_aid_set |= _lda_aid_set

        # poi召回
        poi_aid_set = resume_recall.use_poi_weight(
            jd_match_util.description_poi_weight,
            self.tenant,
            topn=1500,
            min_score=0.35)
        self.__set_recommend_reason(recommend_reason_dict, poi_aid_set,
                                    "poi匹配")

        if jd_match_util.cp_description_poi_weight:
            _poi_aid_set = resume_recall.use_description_lda_weight(
                jd_match_util.cp_description_poi_weight,
                self.tenant,
                topn=1500,
                min_score=0.35)
            self.__set_recommend_reason(recommend_reason_dict, _poi_aid_set,
                                        "工作内容相似")
            poi_aid_set |= _poi_aid_set

        # 目标公司+职位召回
        target_company_set = jd_match_util.target_company_list | jd_match_util.recommend_company
        company_title_aid_set = resume_recall.use_target_company_title(
            target_company_set, jd_match_util.title_prob_list, self.tenant)
        self.__set_recommend_reason(recommend_reason_dict,
                                    company_title_aid_set, "公司+职位")

        # 自定义标签召回
        custom_tag_aid_set = resume_recall.use_custom_tags(
            custom_tags=jd_match_util.custom_tags, tenant=self.tenant)
        self.__set_recommend_reason(recommend_reason_dict, custom_tag_aid_set,
                                    "自定义标签")

        aid_set = lda_aid_set | company_title_aid_set | custom_tag_aid_set | poi_aid_set

        return aid_set

    @link_tracking_time
    def __get_and_upgrade_jd(self, jd_info) -> JobOrderMatchUtil:
        exist_jd_info = JobOrderFeatures.get_with_tenant_and_aid(
            self.tenant, jd_info['aid'])
        exist_jd_info = exist_jd_info.to_dict(skip_empty=False)
        jd = schema_transformer.check_and_fill_jd_info(jd_info, exist_jd_info)
        return jd

    @link_tracking_time
    def __batch_distance_jd_resume(self, jd_match_util: JobOrderMatchUtil,
                                   data: list) -> List[float]:
        score_list = ranker.batch_distance_jd_resume(jd_match_util, data)
        return score_list

    @link_tracking_time
    def __get_data_for_recommend_resume_from_jd(self, aid_list: list):
        # ResumeFeatures.search()
        query = ResumeFeatures.search().filter(
            'term', tenant=self.tenant).filter(
                "terms", aid=aid_list)
        data = [i for i in query.scan()]
        # query = elastic_resume_feature_searcher.filter(
        #     "term", tenant=self.tenant).filter(
        #         "terms",
        #         aid=aid_list).source(source_fields.jd_cv_match_field_list)
        #
        # data = [
        #     schema_transformer.resume_index_to_jd_cv_match_dict(i.to_dict())
        #     for i in query.scan()
        # ]
        return data

    # 批量添加推荐理由
    @classmethod
    def __set_recommend_reason(cls, rd: dict, aid_set: set,
                               reason: str) -> None:
        for aid in aid_set:
            rd[aid].append(reason)


class DuplicateResume(BaseGeneratorUuid):
    def __init__(self, tenant):
        self.tenant = tenant
        super().__init__()

    # 简历查重 从已存在resume
    @link_tracking_time
    def resume_duplicate_checking_from_resume_id(
            self, resume_info, topn: int = 5, min_score: float = 0.5) -> dict:
        resume_index = ResumeFeatures.get_with_tenant_and_aid(
            resume_info['tenant'],
            resume_info['aid'],
            source=source_fields.duplicate_resume_field_list)
        if resume_index:
            return self._resume_duplicate_checking(resume_index, topn,
                                                   min_score)
        else:
            result = {"link_uuid": self.link_uuid, "items": []}
            return result

    # 简历查重 从新resume
    @link_tracking_time
    def resume_duplicate_checking_from_resume(self,
                                              resume_info,
                                              topn: int = 5,
                                              min_score: float = 0.5) -> dict:
        resume_index = self.__index_single_resume(resume_info)
        resume_index = ResumeFeatures.from_json(resume_index)
        return self._resume_duplicate_checking(resume_index, topn, min_score)

    @link_tracking_time
    def __recall_for_resume_duplicate_checking(
            self, resume_index: ResumeFeatures) -> set:

        company_set = set(
            [i.company_stand for i in resume_index.experiences.work_list])
        title_set = set(
            [i.title_stand for i in resume_index.experiences.work_list])
        company_title_aid_set = resume_recall.use_target_company_title(
            company_set, title_set, tenant=self.tenant)

        school_set = set(
            [i.school_stand_id for i in resume_index.educations.edu_list])
        major_set = set(
            [i.major_stand for i in resume_index.educations.edu_list])
        school_major_aid_set = resume_recall.use_target_company_title(
            school_set, major_set, tenant=self.tenant)
        aid_set = company_title_aid_set | school_major_aid_set
        return aid_set

    @link_tracking_time
    def __get_data_for_resume_duplicate_checking(self, aid_list: list):
        query = elastic_resume_feature_searcher.filter(
            "term", tenant=self.tenant).filter(
                "terms",
                aid=aid_list).source(source_fields.duplicate_resume_field_list)
        data = [
            schema_transformer.resume_index_to_resume_duplicate_dict(
                i.to_dict()) for i in query.scan()
        ]
        return data

    @link_tracking_time
    def __batch_distance_duplicate_resume(self, resume_index,
                                          data: list) -> List[float]:
        return ranker.batch_duplicate_distance_resume_resume(
            resume_index, data)

    # 简历查重
    @link_tracking_time
    def _resume_duplicate_checking(self,
                                   resume_index: ResumeFeatures,
                                   topn: int = 5,
                                   min_score: float = 0.5) -> dict:

        aid_set = self.__recall_for_resume_duplicate_checking(resume_index)

        resume_recall.record_recall(self.link_uuid, self.tenant, "RDC",
                                    aid_set)

        aid_set = list(rule_filter.filter_id(aid_set, resume_index['aid']))

        data = self.__get_data_for_resume_duplicate_checking(aid_list=aid_set)

        resume_index = schema_transformer.resume_index_to_resume_duplicate_dict(
            resume_index.to_dict())

        score_list = self.__batch_distance_duplicate_resume(resume_index, data)

        items = list()
        for candidate_resume, score in zip(data, score_list):
            if score >= min_score:
                items.append({
                    'aid': candidate_resume['aid'],
                    'score': score,
                    "tenant": self.tenant
                })
        else:
            items.sort(key=lambda x: x['score'], reverse=True)
        result = {"link_uuid": self.link_uuid, "items": items[:topn]}
        return result

    @link_tracking_time
    def __index_single_resume(self, resume_info):
        return index_single_resume(resume_info)


# 人人匹配-库外建立，需要做画像
def similar_resume_from_resume(resume_info: dict,
                               topn: int = 10,
                               min_score: float = 0.) -> dict:
    return SimilarResume(
        tenant=resume_info['tenant']).similar_resume_from_resume(
            resume_info, topn, min_score)


# 人人匹配
def similar_resume_from_resume_id(resume_info: dict,
                                  topn: int = 10,
                                  min_score: float = 0.) -> dict:
    return SimilarResume(
        tenant=resume_info['tenant']).similar_resume_from_resume_id(
            resume_info, topn, min_score)


# 简岗匹配
def recommend_resume_from_jd_info(jd_info: dict,
                                  topn: int = 2000,
                                  min_score: float = 0.4) -> dict:
    return RecommendResume(
        tenant=jd_info['tenant']).recommend_resume_from_jd_info(
            jd_info, topn, min_score)


# jd_index 和cv 的分数
def score_jd_cv(jd_info: dict, resume_info: dict) -> float:
    return RecommendResume(tenant=jd_info['tenant']).score_jd_cv(
        jd_info, resume_info)


# 批量计算 jd和cv的分数
def batch_score_jd_cv(tenant: str, jd_aid: int,
                      resume_id_list: List[int]) -> List[Dict]:
    return RecommendResume(tenant=tenant).batch_score_jd_cv(
        jd_aid, resume_id_list)


# 简历查重 从已存在resume
def resume_duplicate_checking_from_resume_id(resume_info,
                                             topn: int = 5,
                                             min_score: float = 0.5) -> dict:
    return DuplicateResume(
        tenant=resume_info['tenant']).resume_duplicate_checking_from_resume_id(
            resume_info, topn, min_score)


# 简历查重 从新resume
def resume_duplicate_checking_from_resume(resume_info,
                                          topn: int = 5,
                                          min_score: float = 0.5) -> dict:
    return DuplicateResume(
        tenant=resume_info['tenant']).resume_duplicate_checking_from_resume(
            resume_info, topn, min_score)
