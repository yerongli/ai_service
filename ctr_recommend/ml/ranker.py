from typing import Dict, List, Tuple, Union

import numpy as np
from gllue_ai_libs.search_engine.utils import abs_model_path, load_model
from xgboost import XGBRegressor

from common.jd_index.jd_util import JobOrderMatchUtil
from common.utils import XGBRegressorModelLoader
from database.elasticsearch.meta import VectorFieldMeta
from preprocess.title import get_title2parent
from database.elasticsearch.models import ResumeFeatures

from .model import get_dnn_model

# model = get_dnn_model(85)
# model.load_weights(abs_model_path('./recommend/resume_ctr_dnn.model'))

xgbr = load_model(
    loader=XGBRegressorModelLoader(
        abs_model_path('./recommend/resume_ctr_xgbr.model')),
    default_factory=XGBRegressor)
xgbr.n_jobs = 1


def cosine_dict(d1: Union[Dict, Tuple], d2: Union[Dict, Tuple]) -> float:
    # 计算d1 d2的余弦距离， d1,d2可以为dict 或 tuple
    if not isinstance(d1, dict):
        d1 = dict(d1)
    if not isinstance(d2, dict):
        d2 = dict(d2)
    intersection_keys = d1.keys() & d2.keys()
    v = 0
    for i in intersection_keys:
        v += d1.get(i, 0) * d2.get(i, 0)
    return v


def deal_batch_resume(jd_match_util: JobOrderMatchUtil,
                      resume_info_list: list) -> List[list]:
    features = []
    for resume_info in resume_info_list:
        r = deal_single_resume(jd_match_util, resume_info)
        features.append(r)

    return features


def explain_deal_single_resume(jd_match_util: JobOrderMatchUtil,
                               resume_info: dict):
    x = deal_single_resume(jd_match_util, resume_info)
    print("age: ", x[0:5])
    print("salary: ", x[5:10])
    print("location: ", x[10:14])
    print("degree: ", x[14:18])
    print("major: ", x[18:22])
    print("985 211: ", x[22:25])
    print("first 985 211: ", x[25:28])
    print("other info: ", x[28:30])
    print("title: ", x[30:34])
    print("last title: ", x[34:38])
    print("level: ", x[38:41])
    print("work year: ", x[41:47])
    print("poi: ", x[47:51])
    print("lda: ", x[51:55])
    print("age: ", x[55:59])
    print("salary: ", x[59:61])
    print("degree: ", x[61:65])
    print("major: ", x[65:69])
    print("title: ", x[69:73])
    print("title level: ", x[73:77])
    print("work duration: ", x[77:81])
    print("poi: ", x[81:85])
    print("lda: ", x[85:89])


def deal_single_resume(jd_match_util: JobOrderMatchUtil, resume_info: dict):
    rst = []
    basic_info: dict = resume_info['basic_info']
    educations: dict = resume_info['educations']
    experiences: dict = resume_info['experiences']
    edu_list: list = educations.get('edu_list') or []
    work_list: list = experiences.get('work_list') or []

    age = basic_info.get('age')
    current_salary = basic_info.get('current_salary', 0)
    locations: list = basic_info.get('locations')

    degree_list: list = [i.get('degree_stand') for i in edu_list]
    title_list: list = [i.get('title_stand') for i in work_list]
    parent_title_list: list = [
        get_title2parent(i.get('title_stand')) for i in work_list
    ]
    total_duration_year: float = experiences.get('total_duration_month',
                                                 0) / 12
    major_list: list = [i.get('major_stand') for i in edu_list]
    poi_weight_dict: dict = VectorFieldMeta.index_value_to_dict(
        experiences.get('poi_weight'))
    lda_weight_dict: dict = VectorFieldMeta.index_value_to_dict(
        experiences.get('lda_weight'))

    title_level_highest = experiences.get('title_level_highest')
    total_duration_month = experiences.get('total_duration_month')

    # basic info match features
    # age   *5
    rst.extend(jd_match_util.age_match(age))
    # gender pass

    # salary *5
    rst.extend(jd_match_util.salary_match(current_salary))
    # location *4
    rst.extend(jd_match_util.location_match(locations))

    # education match features
    # degree *4
    rst.extend(jd_match_util.degree_list_match(degree_list))
    # major *4
    rst.extend(jd_match_util.major_list_match(major_list))
    # 985 211 *3
    rst.extend(jd_match_util.match_985_211(educations.get('is_985_211')))
    # first 985 211 *3
    rst.extend(jd_match_util.match_985_211(educations.get('first_is_985_211')))
    # other info *2
    rst.extend([
        int(educations.get('is_education_interrupt')),
        int(educations.get('is_education_illegal'))
    ])

    # work experience match features
    # title *4
    rst.extend(jd_match_util.title_list_match(title_list))
    # parent_title *4
    rst.extend(jd_match_util.parent_title_list_match(parent_title_list))
    # last title *4
    rst.extend(
        jd_match_util.title_list_match([title_list[0]] if title_list else []))
    # level *3
    rst.extend(jd_match_util.title_level_match(title_level_highest))
    # work year *6
    rst.extend(jd_match_util.working_year_match(total_duration_year))

    # poi   *4
    rst.extend(jd_match_util.poi_match(poi_weight_dict))
    # lda   *4
    rst.extend(jd_match_util.lda_match(lda_weight_dict))

    # cp basic info features
    # age *4
    rst.extend(jd_match_util.cp_age_match(age))
    # salary * 4
    rst.extend(jd_match_util.cp_salary_match(current_salary))

    # cp education features
    # degree *4
    rst.extend(jd_match_util.cp_degree_list_match(degree_list))
    # major *4
    rst.extend(jd_match_util.cp_major_list_match(major_list))

    # cp work experience features
    # title *4
    rst.extend(jd_match_util.cp_title_list_match(title_list))
    # title_parent * 4
    rst.extend(jd_match_util.cp_parent_title_list_match(parent_title_list))
    # title level *3
    rst.extend(jd_match_util.cp_title_level_match(title_level_highest))
    # work duration month *3
    rst.extend(jd_match_util.cp_working_year_match(total_duration_month))
    # poi *4
    rst.extend(jd_match_util.cp_poi_match(poi_weight_dict))
    # lda *4
    rst.extend(jd_match_util.cp_lda_match(lda_weight_dict))
    return rst


def deal_single_resume_sub(jd_match_util: JobOrderMatchUtil,
                           resume_info: dict):
    rst = []
    basic_info: dict = resume_info['basic_info']
    educations: dict = resume_info['educations']
    experiences: dict = resume_info['experiences']
    edu_list: list = educations.get('edu_list') or []
    work_list: list = experiences.get('work_list') or []

    age = basic_info.get('age')
    current_salary = basic_info.get('current_salary', 0)
    locations: list = basic_info.get('locations')

    degree_list: list = [i.get('degree_stand') for i in edu_list]
    title_list: list = [i.get('title_stand') for i in work_list]
    parent_title_list: list = [
        get_title2parent(i.get('title_stand')) for i in work_list
    ]
    total_duration_month = experiences.get('total_duration_month', 0)

    total_duration_year: float = total_duration_month / 12
    major_list: list = [i.get('major_stand') for i in edu_list]
    poi_weight_dict: dict = VectorFieldMeta.index_value_to_dict(
        experiences.get('poi_weight'))
    lda_weight_dict: dict = VectorFieldMeta.index_value_to_dict(
        experiences.get('lda_weight'))

    title_level_highest = experiences.get('title_level_highest')

    # basic info match features
    # age   *5
    rst.extend(jd_match_util.age_match(age))
    # gender pass

    # salary *5
    rst.extend(jd_match_util.salary_match(current_salary))
    # location *4
    rst.extend(jd_match_util.location_match(locations))

    # education match features
    # degree *4
    rst.extend(jd_match_util.degree_list_match(degree_list))
    # major *4
    rst.extend(jd_match_util.major_list_match(major_list))
    # 985 211 *3
    rst.extend(jd_match_util.match_985_211(educations.get('is_985_211')))
    # first 985 211 *3
    rst.extend(jd_match_util.match_985_211(educations.get('first_is_985_211')))
    # other info *2
    rst.extend([
        educations.get('is_education_interrupt'),
        educations.get('is_education_illegal')
    ])

    # work experience match features
    # title *4
    rst.extend(jd_match_util.title_list_match(title_list))
    # last title *4
    rst.extend(
        jd_match_util.title_list_match([title_list[0]] if title_list else []))
    # parent_title *4
    rst.extend(jd_match_util.parent_title_list_match(parent_title_list))
    # level *3
    rst.extend(jd_match_util.title_level_match(title_level_highest))
    # work year *6
    rst.extend(jd_match_util.working_year_match(total_duration_year))

    # poi   *4
    rst.extend(jd_match_util.poi_match(poi_weight_dict))
    # lda   *4
    rst.extend(jd_match_util.lda_match(lda_weight_dict))

    # cp basic info features
    # age *4
    rst.extend([1, 0, 0, 0])
    # salary * 4
    rst.extend([1, 0, 0, 0])

    # cp education features
    # degree *4
    rst.extend([1, 0, 0, 0])
    # major *4
    rst.extend([1, 0, 0, 0])

    # cp work experience features
    # title *4
    rst.extend([1, 0, 0, 0])
    # parent_title *4
    rst.extend([1, 0, 0, 0])
    # title level *3
    rst.extend([1, 0, 0])
    # work duration month *3
    rst.extend([1, 0, 0])
    # poi *4
    rst.extend([0, len(poi_weight_dict), 0, 0])
    # lda *4
    rst.extend([0, len(lda_weight_dict), 0, 0])
    return rst


# def deal_single_resume(jd_match_util: JobOrderMatchUtil,
#                        resume_info: ResumeFeatures):
#     rst = []
#
#     degree_list: list = [i.degree_stand for i in resume_info.educations.edu_list]
#     title_list: list = [i.title_stand for i in resume_info.experiences.work_list]
#     total_duration_year: float = resume_info.experiences.total_duration_month / 12
#     major_list: list = [i.major_stand for i in resume_info.educations.edu_list]
#     poi_weight_dict: dict = VectorFieldMeta.index_value_to_dict(resume_info.experiences.poi_weight)
#     lda_weight_dict: dict = VectorFieldMeta.index_value_to_dict(resume_info.experiences.lda_weight)
#
#     # basic info match features
#     # age   *5
#     rst.extend(jd_match_util.age_match(resume_info.basic_info.age))
#     # gender pass
#
#     # salary *5
#     rst.extend(
#         jd_match_util.salary_match(resume_info.basic_info.current_salary))
#     # location *4
#     rst.extend(jd_match_util.location_match(resume_info.basic_info.locations))
#
#     # education match features
#     # degree *4
#     rst.extend(jd_match_util.degree_list_match(degree_list))
#     # major *4
#     rst.extend(jd_match_util.major_list_match(major_list))
#     # 985 211 *3
#     rst.extend(jd_match_util.match_985_211(resume_info.educations.is_985_211))
#     # first 985 211 *3
#     rst.extend(
#         jd_match_util.match_985_211(resume_info.educations.first_is_985_211))
#     # other info *2
#     rst.extend([
#         resume_info.educations.is_education_interrupt,
#         resume_info.educations.is_education_illegal
#     ])
#
#     # work experience match features
#     # title *4
#     rst.extend(jd_match_util.title_list_match(title_list))
#     # last title *4
#     rst.extend(
#         jd_match_util.title_list_match([title_list[0]] if title_list else []))
#     # level *3
#     rst.extend(
#         jd_match_util.title_level_match(
#             resume_info.experiences.title_level_highest))
#     # work year *6
#     rst.extend(jd_match_util.working_year_match(total_duration_year))
#     # poi   *4
#     rst.extend(jd_match_util.poi_match(poi_weight_dict))
#     # lda   *4
#     rst.extend(jd_match_util.lda_match(lda_weight_dict))
#
#     # cp basic info features
#     # age *4
#     rst.extend(jd_match_util.cp_age_match(resume_info.basic_info.age))
#     # salary * 4
#     rst.extend(
#         jd_match_util.cp_salary_match(resume_info.basic_info.current_salary))
#
#     # cp education features
#     # degree *4
#     rst.extend(jd_match_util.cp_degree_list_match(degree_list))
#     # major *4
#     rst.extend(jd_match_util.cp_major_list_match(major_list))
#
#     # cp work experience features
#     # title *4
#     rst.extend(jd_match_util.cp_title_list_match(title_list))
#     # title level *3
#     rst.extend(
#         jd_match_util.cp_title_level_match(
#             resume_info.experiences.title_level_highest))
#     # work duration month *3
#     rst.extend(
#         jd_match_util.cp_working_year_match(
#             resume_info.experiences.total_duration_month))
#     # poi *4
#     rst.extend(jd_match_util.cp_poi_match(poi_weight_dict))
#     # lda *4
#     rst.extend(jd_match_util.cp_lda_match(lda_weight_dict))
#
#     return rst
