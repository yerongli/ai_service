from keras.models import Model
from keras.layers import Dense, BatchNormalization, Concatenate, Input


def block_layer(in_layer, last_layer, dense_unit):
    dense = Dense(dense_unit, activation='relu')(last_layer)
    bn = BatchNormalization()(dense)
    ct = Concatenate()([in_layer, bn])
    return ct


def get_dnn_model(input_unit):
    input_layer = Input(shape=(input_unit, ))
    x = block_layer(input_layer, input_layer, input_unit * 4)
    x = block_layer(input_layer, x, input_unit * 2)
    x = block_layer(input_layer, x, input_unit)
    x = Dense(1, activation='sigmoid')(x)

    model = Model(inputs=input_layer, outputs=x)
    model.compile(loss='mse', optimizer='adam', metrics=['mse', 'accuracy'])
    return model
