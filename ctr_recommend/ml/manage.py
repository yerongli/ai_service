from collections import defaultdict
from typing import Dict, List

import numpy as np

from common.jd_index.jd_util import JobOrderMatchUtil
from common.recall import resume_recall
from common.resume_recommend import ranker, schema_transformer, source_fields
from common.utils import BaseGeneratorUuid, link_tracking_time
from database.elasticsearch.config import elastic_resume_feature_searcher
from database.elasticsearch.models import JobOrderFeatures, ResumeFeatures

from .ranker import deal_batch_resume, xgbr


class RecommendResume(BaseGeneratorUuid):
    def __init__(self, tenant):
        self.tenant = tenant
        super().__init__()

    # 简岗匹配
    # TODO
    @link_tracking_time
    def recommend_resume_from_jd_info(self,
                                      jd_info: dict,
                                      topn: int = 2000,
                                      min_score: float = 0.1) -> dict:
        # jd_info: aid, tenant, version, custom_prefer_list
        # 返回字段包含 aid, score, reason
        jd_util = self._get_and_upgrade_jd(jd_info)
        return self._recommend_resume_from_jd(jd_util, topn, min_score)

    # jd 和cv 的分数
    @link_tracking_time
    def score_jd_cv(self, jd_info: dict, resume_info: dict) -> float:
        assert jd_info['tenant'] == resume_info['tenant'], "tenant 不匹配"

        jd_match_util = self._get_and_upgrade_jd(jd_info)

        # 检查 resume 信息
        resume = ResumeFeatures.get_with_tenant_and_aid(
            resume_info['tenant'],
            resume_info['aid'],
            source=source_fields.jd_cv_match_field_list)

        resume_info = resume.to_dict(skip_empty=False)
        features = self._deal_batch_resume(jd_match_util, [resume_info])
        score = self.batch_distance_jd_resume(features)
        score = float(score[0])
        return score

    # 批量jd 和cv 的分数
    @link_tracking_time
    def batch_score_jd_cv(self, jd_aid: int,
                          resume_id_list: list) -> List[Dict]:
        jd_info = {'tenant': self.tenant, 'aid': jd_aid}
        jd_util = self._get_and_upgrade_jd(jd_info)

        # 获取获选简历
        data = self._get_data_for_recommend_resume_from_jd(resume_id_list)
        # 距离计算
        score_list = self._batch_distance_jd_resume(jd_util, data)

        result = []
        for candidate_resume, score in zip(data, score_list):
            result.append({
                'aid': candidate_resume['aid'],
                "score": score,
                "tenant": self.tenant,
            })
        return result

    @link_tracking_time
    def _deal_batch_resume(self, jd_match_util: JobOrderMatchUtil, data):
        features = deal_batch_resume(jd_match_util, data)
        return features

    @link_tracking_time
    def batch_distance_jd_resume(self, features):
        if not features:
            return []
        data = np.array(features)
        rst = xgbr.predict(data)
        rst = [float(i) for i in rst]
        return rst

    # 简岗匹配， 给出 jd 对象，做简岗匹配
    @link_tracking_time
    def _recommend_resume_from_jd(self,
                                  jd_match_util: JobOrderMatchUtil,
                                  topn: int = 2000,
                                  min_score: float = 0.2) -> dict:

        recommend_reason_dict = defaultdict(list)
        aid_set = self._recall_for_recommend_resume_from_jd_util(
            jd_match_util, recommend_reason_dict)

        resume_recall.record_recall(self.link_uuid, self.tenant, "RRJ",
                                    aid_set)

        # mismatch_aid_set = rule_filter.get_jd_resume_mismatch_aid_set(
        #     tenant=self.tenant, jd_aid=jd.aid)
        # aid_set = list(
        #     rule_filter.filter_ids(aid_set, mismatch_aid_set))

        aid_set = list(aid_set)

        # 获取获选简历
        data = self._get_data_for_recommend_resume_from_jd(aid_set)
        features = self._deal_batch_resume(jd_match_util, data)
        score_list = self.batch_distance_jd_resume(features)
        # 距离计算
        # score_list = self._deal_batch_resume(jd_match_util, data)

        result = []
        _v = 0
        for candidate_resume, score in zip(data, score_list):
            if score >= min_score:
                _v += 1
            result.append({
                'aid':
                candidate_resume['aid'],
                "score":
                min(0.95, float(score)),
                "tenant":
                self.tenant,
                "reason":
                recommend_reason_dict[candidate_resume['aid']]
            })

        result.sort(key=lambda x: x['score'], reverse=True)
        if _v < 10:
            result = result[:10]
        else:
            result = [i for i in result if i['score'] >= min_score][:topn]
        return {"link_uuid": self.link_uuid, "items": result}

    @link_tracking_time
    def _recall_for_recommend_resume_from_jd_util(
            self, jd_util: JobOrderMatchUtil, recommend_reason_dict):
        # 工作内容召回
        lda_aid_set = resume_recall.use_lda_weight(
            jd_util.jd.lda_weight, self.tenant, topn=1000, min_score=0.35)
        self._set_recommend_reason(recommend_reason_dict, lda_aid_set,
                                   "工作内容相似")

        # poi召回
        poi_aid_set = resume_recall.use_poi_weight(
            jd_util.jd.poi_weight, self.tenant, topn=1000, min_score=0.35)
        self._set_recommend_reason(recommend_reason_dict, poi_aid_set, "poi匹配")

        # 目标公司+职位召回
        company_title_aid_set = resume_recall.use_target_company_title(
            jd_util.need_companies,
            set([i['index'] for i in jd_util.jd.title_prob_weight]),
            self.tenant)
        self._set_recommend_reason(recommend_reason_dict,
                                   company_title_aid_set, "公司+职位")

        # 自定义标签召回
        custom_tag_aid_set = resume_recall.use_custom_tags(
            custom_tags=jd_util.need_custom_tags, tenant=self.tenant)
        self._set_recommend_reason(recommend_reason_dict, custom_tag_aid_set,
                                   "自定义标签")

        aid_set = lda_aid_set | company_title_aid_set | custom_tag_aid_set | poi_aid_set

        return aid_set

    @link_tracking_time
    def _get_and_upgrade_jd(self, jd_info) -> JobOrderMatchUtil:
        jdf = JobOrderFeatures.get_with_tenant_and_aid(jd_info['tenant'],
                                                       jd_info['aid'])

        jd_util = schema_transformer.check_and_fill_jd_info(jdf, jd_info)
        return jd_util

    @link_tracking_time
    def _batch_distance_jd_resume(self, jd_match_util: JobOrderMatchUtil,
                                  data: list) -> List[float]:
        score_list = ranker.batch_distance_jd_resume(jd_match_util, data)
        return score_list

    @link_tracking_time
    def _get_data_for_recommend_resume_from_jd(self, aid_list: list):
        query = elastic_resume_feature_searcher.filter(
            "term", tenant=self.tenant).filter(
                "terms",
                aid=aid_list).source(source_fields.jd_cv_match_field_list)

        data = [i.to_dict() for i in query.scan()]
        return data

    # 批量添加推荐理由
    @classmethod
    def _set_recommend_reason(cls, rd: dict, aid_set: set,
                              reason: str) -> None:
        for aid in aid_set:
            rd[aid].append(reason)

# TODO
# 简岗匹配
def recommend_resume_from_jd_info(jd_info: dict,
                                  topn: int = 2000,
                                  min_score: float = 0.4) -> dict:
    """
    :param jd_info:
    :param topn:
    :param min_score:
    :return:
    """
    return RecommendResume(
        tenant=jd_info['tenant']).recommend_resume_from_jd_info(
            jd_info, topn, min_score)


# jd 和cv 的分数
def score_jd_cv(jd_info: dict, resume_info: dict) -> float:
    return RecommendResume(tenant=jd_info['tenant']).score_jd_cv(
        jd_info, resume_info)


# 批量计算 jd和cv的分数
def batch_score_jd_cv(tenant: str, jd_aid: int,
                      resume_id_list: List[int]) -> List[Dict]:
    return RecommendResume(tenant=tenant).batch_score_jd_cv(
        jd_aid, resume_id_list)
