from keras.preprocessing.sequence import pad_sequences

from ctr_recommend.models.dnn_presentation_model import *
from preprocess.degree import get_degree_id
from preprocess.description import get_poi2id
from preprocess.major import get_major2id
from preprocess.school import get_school2id
from preprocess.title import get_title_id


def one_zero_transform(x):
    return 1 if x else 0


def year_of_date_date(x):
    if not x:
        x = datetime.date.today()
    if isinstance(x, datetime.datetime):
        t = (x - SDTT).days // 365
    else:
        t = (x - SDT).days // 365
    return t


def pad_sequences_list(sequences,
                       seq_len,
                       element_len,
                       dtype='float32',
                       padding='post',
                       truncating='post',
                       value=0.):
    """
    padding 多段的 sequence
    :param sequences: [batch, seq, element]
    :param seq_len:
    :param element_len:
    :param dtype:
    :param padding:
    :param truncating:
    :param value:
    :return:
    """
    num_sequences = len(sequences)

    x = np.full((num_sequences, seq_len, element_len), value, dtype=dtype)
    for seq_idx, ss in enumerate(sequences):
        for idx, s in enumerate(ss):
            if idx < seq_len:
                if not len(s):
                    continue
                if truncating == 'pre':
                    trunc = s[-element_len:]
                elif truncating == 'post':
                    trunc = s[:element_len]
                else:
                    raise ValueError(
                        f'Truncating type not {truncating} understood')

                trunc = np.asarray(trunc, dtype=dtype)
                if padding == 'post':
                    x[seq_idx, idx, :len(trunc)] = trunc
                elif padding == 'pre':
                    x[seq_idx, idx, -len(trunc):] = trunc
                else:
                    raise ValueError(f'Padding type {padding} not understood')
    return x


def get_jd_dnn_presentation_features(jd, lengths=2):
    degree_features = get_degree_id(jd['lowest_degree'])
    need_985_211_features = 1 if jd.need_985_211 else 0
    level_features = jd['title_level']
    work_year_range_features = [
        jd.working_years_range.gte, jd.working_years_range.lte
    ]
    salary_range_features = [
        jd.salary_range.gte or 0, jd.salary_range.lte or 0
    ]

    basic_other_features = [
        degree_features,
        need_985_211_features,
        level_features,
    ] + work_year_range_features + salary_range_features
    major_features = [get_major2id(i) for i in (jd.need_majors or [])]
    title_features = [get_title_id(i) for i in jd.title_prob_list]
    desc_lda_features = [i['index'] for i in jd['description_lda_weight']]
    desc_lda_weight_features = [
        i['value'] for i in jd['description_lda_weight']
    ]

    desc_poi_features = [
        get_poi2id(i['index']) for i in jd['description_poi_weight']
    ]
    desc_poi_weight_features = [
        i['value'] for i in jd['description_poi_weight']
    ]

    cp = jd['candidate_preference']

    pref_degree_features = cp['degree_avg']
    pref_title_level_features = cp['title_level_avg']
    pref_work_duration_month_features = cp['work_duration_month_avg']
    pref_salary_features = cp['salary_avg']
    pref_age_features = cp['age_avg']

    pref_other_features = [
        pref_degree_features, pref_title_level_features,
        pref_work_duration_month_features, pref_salary_features,
        pref_age_features
    ]

    pref_major_features = [get_major2id(i['index']) for i in cp.major_weight]
    pref_major_weight_features = [i['value'] for i in cp.major_weight]

    pref_title_features = [get_title_id(i['index']) for i in cp.title_weight]
    pref_title_weight_features = [i['value'] for i in cp.title_weight]

    pref_desc_lda_features = [i['index'] for i in cp.description_lda_weight]
    pref_desc_lda_weight_features = [
        i['value'] for i in cp.description_lda_weight
    ]

    pref_desc_poi_features = [
        get_poi2id(i['index']) for i in cp.description_poi_weight
    ]
    pref_desc_poi_weight_features = [
        i['value'] for i in cp.description_poi_weight
    ]

    result = [
        np.array([basic_other_features] * lengths),  # 0
        pad_sequences(  # 1
            np.array([major_features] * lengths),
            maxlen=JD_MAX_MAJOR_LEN,
            padding='post',
            truncating='post'),
        pad_sequences(  # 2
            np.array([title_features] * lengths),
            maxlen=JD_MAX_TITLE_LEN,
            padding='post',
            truncating='post'),
        pad_sequences(  # 3
            np.array([desc_lda_features] * lengths),
            maxlen=JD_MAX_LDA_LEN,
            padding='post',
            truncating='post'),
        pad_sequences(  # 4
            np.array([desc_lda_weight_features] * lengths),
            maxlen=JD_MAX_LDA_LEN,
            padding='post',
            truncating='post',
            dtype='float32'),
        pad_sequences(  # 5
            np.array([desc_poi_features] * lengths),
            maxlen=JD_MAX_POI_LEN,
            padding='post',
            truncating='post'),
        pad_sequences(  # 6
            np.array([desc_poi_weight_features] * lengths),
            maxlen=JD_MAX_POI_LEN,
            padding='post',
            truncating='post',
            dtype='float32'),
        np.array([pref_other_features] * lengths),  # 7
        pad_sequences(  # 8
            np.array([pref_major_features] * lengths),
            maxlen=JD_MAX_PREF_MAJOR_LEN,
            padding='post',
            truncating='post'),
        pad_sequences(  # 9
            np.array([pref_major_weight_features] * lengths),
            maxlen=JD_MAX_PREF_MAJOR_LEN,
            padding='post',
            truncating='post',
            dtype='float32'),
        pad_sequences(  # 10
            np.array([pref_title_features] * lengths),
            maxlen=JD_MAX_PREF_TITLE_LEN,
            padding='post',
            truncating='post'),
        pad_sequences(  # 11
            np.array([pref_title_weight_features] * lengths),
            maxlen=JD_MAX_PREF_TITLE_LEN,
            padding='post',
            truncating='post',
            dtype='float32'),
        pad_sequences(  # 12
            np.array([pref_desc_poi_features] * lengths),
            maxlen=JD_MAX_PREF_POI_LEN,
            padding='post',
            truncating='post'),
        pad_sequences(  # 13
            np.array([pref_desc_poi_weight_features] * lengths),
            maxlen=JD_MAX_PREF_POI_LEN,
            padding='post',
            truncating='post',
            dtype='float32'),
        pad_sequences(  # 14
            np.array([pref_desc_lda_features] * lengths),
            maxlen=JD_MAX_PREF_LDA_LEN,
            padding='post',
            truncating='post'),
        pad_sequences(  # 15
            np.array([pref_desc_lda_weight_features] * lengths),
            maxlen=JD_MAX_PREF_LDA_LEN,
            padding='post',
            truncating='post',
            dtype='float32'),
    ]
    return result


def get_resume_dnn_presentation_features(resume_list):
    basic_info_features_list = []

    edu_other_features_list = []
    school_features_list = []
    major_features_list = []

    exp_other_features_list = []
    title_features_list = []

    desc_lda_features_list = []
    desc_lda_weight_features_list = []

    desc_poi_features_list = []
    desc_poi_weight_features_list = []

    for resume in resume_list:

        basic_info_features_list.append([
            resume.basic_info.age or 0,
            resume.educations.total_duration_month / 12,
            resume.experiences.total_work_duration_month / 12,
            resume.basic_info.current_salary or 0
        ])

        edu_other_features = []
        school_features = []
        major_features = []

        for edu in resume.educations.edu_list:
            edu_other_features.append([
                get_degree_id(edu.degree_stand),
                year_of_date_date(edu.start),
                year_of_date_date(edu.end), (edu.duration_month or 0) / 12,
                one_zero_transform(edu.is_985_211),
                one_zero_transform(edu.is_abroad),
                one_zero_transform(edu.is_qs200)
            ])
            school_features.append(get_school2id(edu.school_stand_id))
            major_features.append(get_major2id(edu.major_stand))

        exp_other_features = []
        title_features = []

        desc_lda_features = []
        desc_lda_weight_features = []
        desc_poi_features = []
        desc_poi_weight_features = []

        for exp in resume.experiences.work_list:
            exp_other_features.append([
                year_of_date_date(exp.start),
                year_of_date_date(exp.end), exp.duration_month / 12,
                exp.title_level
            ])
            title_features.append(get_title_id(exp.title_stand))

            desc_lda_features.append(
                [i['index'] for i in exp.description_lda_weight])
            desc_lda_weight_features.append(
                [i['value'] for i in exp.description_lda_weight])

            desc_poi_features.append(
                [get_poi2id(i['index']) for i in exp.description_poi_weight])
            desc_poi_weight_features.append(
                [i['value'] for i in exp.description_poi_weight])

        edu_other_features_list.append(edu_other_features)
        school_features_list.append(school_features)
        major_features_list.append(major_features)

        exp_other_features_list.append(exp_other_features)
        title_features_list.append(title_features)

        desc_lda_features_list.append(desc_lda_features)
        desc_lda_weight_features_list.append(desc_lda_weight_features)

        desc_poi_features_list.append(desc_poi_features)
        desc_poi_weight_features_list.append(desc_poi_weight_features)

    result = [
        np.array(basic_info_features_list),
        pad_sequences_list(
            edu_other_features_list, seq_len=CV_MAX_EDU_LEN, element_len=7),
        pad_sequences(
            school_features_list,
            maxlen=CV_MAX_EDU_LEN,
            padding='post',
            truncating='post'),
        pad_sequences(
            major_features_list,
            maxlen=CV_MAX_EDU_LEN,
            padding='post',
            truncating='post'),
        pad_sequences_list(
            exp_other_features_list, seq_len=CV_MAX_EXP_LEN, element_len=4),
        pad_sequences(
            title_features_list,
            maxlen=CV_MAX_EXP_LEN,
            padding='post',
            truncating='post'),
        pad_sequences_list(
            desc_lda_features_list,
            seq_len=CV_MAX_EXP_LEN,
            element_len=CV_MAX_LDA_LEN),
        pad_sequences_list(
            desc_lda_weight_features_list,
            seq_len=CV_MAX_EXP_LEN,
            element_len=CV_MAX_LDA_LEN),
        pad_sequences_list(
            desc_poi_features_list,
            seq_len=CV_MAX_EXP_LEN,
            element_len=CV_MAX_POI_LEN),
        pad_sequences_list(
            desc_poi_weight_features_list,
            seq_len=CV_MAX_EXP_LEN,
            element_len=CV_MAX_POI_LEN),
    ]
    return result
