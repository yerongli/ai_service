import functools
import json

from config import ai_service_config
from database.redis.config import RedisPrefixOf, redis_cache


def get_cache(name, redis_prefix):
    rst = redis_cache.get(redis_prefix + name)
    if rst:
        return json.loads(rst)
    else:
        return None


get_company_cache = functools.partial(
    get_cache, redis_prefix=RedisPrefixOf.COMPANY)
get_title_cache = functools.partial(
    get_cache, redis_prefix=RedisPrefixOf.TITLE)
get_school_cache = functools.partial(
    get_cache, redis_prefix=RedisPrefixOf.SCHOOL)
get_major_cache = functools.partial(
    get_cache, redis_prefix=RedisPrefixOf.MAJOR)


def set_cache(name, value, redis_prefix):
    value = json.dumps(value)
    redis_cache.set(redis_prefix + name, value, ex=ai_service_config['TTL'])


set_company_cache = functools.partial(
    set_cache, redis_prefix=RedisPrefixOf.COMPANY)
set_title_cache = functools.partial(
    set_cache, redis_prefix=RedisPrefixOf.TITLE)
set_school_cache = functools.partial(
    set_cache, redis_prefix=RedisPrefixOf.SCHOOL)
set_major_cache = functools.partial(
    set_cache, redis_prefix=RedisPrefixOf.MAJOR)


def clear_cache(type_):
    x = list(redis_cache.scan_iter(type_ + '*'))
    redis_cache.delete(*x)


clear_company_cache = functools.partial(
    clear_cache, type_=RedisPrefixOf.COMPANY)
clear_title_cache = functools.partial(clear_cache, type_=RedisPrefixOf.TITLE)
clear_school_cache = functools.partial(clear_cache, type_=RedisPrefixOf.SCHOOL)
clear_major_cache = functools.partial(clear_cache, type_=RedisPrefixOf.MAJOR)
clear_all_ai_cache = functools.partial(clear_cache, type_='ai:cache:')
