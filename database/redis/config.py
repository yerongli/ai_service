import redis

from config import ai_service_config

redis_cache = redis.StrictRedis.from_url(ai_service_config['REDIS_URL'])


class RedisPrefixOf:
    COMPANY = 'ai:cache:company:'
    TITLE = 'ai:cache:title:'
    SCHOOL = 'ai:cache:school:'
    MAJOR = 'ai:cache:major:'
