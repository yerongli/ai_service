数据库的各种操作放在这边。

note: 首次部署需要给gllue_data_center创建索引。

```python
from database.mongodb.jobsubmission_operate import jobsubmission_create_index
jobsubmission_create_index()
```
