import datetime
import tqdm
from typing import List
from mongoengine import (
    BooleanField,
    DateTimeField,
    DictField,
    Document,
    EmbeddedDocument,
    EmbeddedDocumentField,
    EmbeddedDocumentListField,
    IntField,
    ListField,
    StringField,
    UUIDField,
)
from mongoengine.errors import DoesNotExist
from xpinyin import Pinyin
from database.mongodb.config import mongo_school_collection
from googletrans import Translator

py = Pinyin()
translator = Translator(service_urls=["translate.google.cn"])


class RecommendResumeFromJdInfoResult(Document):
    """
    link_uuid : LUUID for individual document
    tenant : Tenant ID, string field
    jd_aid : Job description ID
    jd_version : Job description version ID
    """

    # 记录每次简历推荐的结果
    link_uuid = UUIDField(primary_key=True)

    tenant = StringField(required=True)

    jd_aid = IntField(required=True)
    jd_version = IntField(required=True)

    # aid, tenant, score, reason
    items = ListField()

    create_time = DateTimeField(default=datetime.datetime.now)

    meta = {
        "indexes": ["#tenant", "#jd_aid", "jd_version", "create_time"],
        "collection": "ai_result_recommend_resume_from_jd",
    }

    def filter_items(self, aid_set):
        items = [i for i in self.items if i["aid"] not in aid_set]
        return items


RECALL_RESULT_TYPE_CHOICES = (
    ("RRJ", "recommend_resume_from_jd"),
    ("SRD", "similar_resume_from_resume_index_use_description"),
    ("RDC", "resume_duplicate_checking"),
    ("SRCT", "similar_resume_from_resume_index_use_company_title"),
)


class RecallResumeResult(Document):
    link_uuid = UUIDField(required=True)
    tenant = StringField(required=True)
    dtype = StringField(choices=RECALL_RESULT_TYPE_CHOICES)
    aid_list = ListField()
    create_time = DateTimeField(default=datetime.datetime.now)

    meta = {
        "indexes": ["#tenant", "#link_uuid"],
        "collection": "ai_result_recall_resume",
    }


class SimilarResumeResult(Document):
    _id = UUIDField(primary_key=True)
    tenant = StringField(required=True)
    resume_info = DictField()

    # aid, tenant, score
    items = ListField()

    create_time = DateTimeField(default=datetime.datetime.now)

    meta = {
        "indexes": ["#tenant", "create_time"],
        "collection": "ai_result_similar_resume",
    }


class DuplicateResumeResult(Document):
    link_uuid = UUIDField(primary_key=True)

    tenant = StringField(required=True)
    resume_info = DictField()

    # aid, tenant, score
    items = ListField()

    create_time = DateTimeField(default=datetime.datetime.now)

    meta = {
        "indexes": ["#tenant", "create_time"],
        "collection": "ai_result_duplicate_resume",
    }


class FeedBackJdResume(Document):
    tenant = StringField(required=True)
    jd_aid = IntField()

    match_aid_list = ListField()
    mismatch_aid_list = ListField()

    meta = {"indexes": ["#tenant", "#jd_aid"], "collection": "ai_feedback_jd_resume"}


class SyncLog(Document):
    ident = StringField(primary_key=True)
    status = StringField()

    meta = {"collection": "sync_log"}


class SchoolTags(EmbeddedDocument):
    # 985院校
    is_985 = BooleanField(default=False)
    # 211院校
    is_211 = BooleanField(default=False)
    # 一流大学
    is_first_class_university = BooleanField(default=False)
    # 一流学科
    is_first_class_major = BooleanField(default=False)
    # 双一流
    is_double_first = BooleanField(default=False)
    # 教育部直属
    is_education_directly = BooleanField(default=False)
    # 中央部委
    is_central_ministry_directly = BooleanField(default=False)
    # 自主招生
    is_independent_recruitment = BooleanField(default=False)
    # QS200排名
    qs200 = IntField(null=True)


class SchoolOtherInfo(EmbeddedDocument):
    index = StringField()
    value = StringField()

    @classmethod
    def init_from_dict(cls, data: dict):
        rst = []
        for k, v in data.items():
            rst.append(cls(index=k, value=v))
        return rst


class School(Document):
    aid = IntField(primary_key=True)
    is_deleted = BooleanField(default=False)
    # 学校名
    name = StringField()
    # 学校拼音
    name_pinyin = StringField()
    name_pinyin_tone = StringField()
    # 显示名称
    display_name = StringField()
    # 英文名
    english_name = StringField()
    # 别名
    alias = ListField()

    # 百度的词条id
    lemma_id = StringField()
    # 简介
    summary = StringField()
    # 百科地址
    url = StringField()
    # 校徽
    school_badge_url = StringField(null=True)

    # tags
    tags = EmbeddedDocumentField(SchoolTags, default=SchoolTags)
    tag_list = ListField()

    # other_info
    other_info = EmbeddedDocumentListField(SchoolOtherInfo)

    # 人数
    popularity = IntField(default=0)

    def stand_name_pinyin(self):
        self.name_pinyin = py.get_pinyin(self.name, splitter=" ")

    def stand_name_pinyin_tone(self):
        self.name_pinyin_tone = py.get_pinyin(
            self.name, splitter=" ", tone_marks="marks"
        )

    def stand_english_name(self):
        self.english_name = translator.translate(self.name, src="zh-cn", dest="en").text

    def stand_alias(self):
        alias = list(self.alias)
        alias.append(self.name)
        alias.append(self.name_pinyin)
        alias.append(self.display_name)
        alias.append(self.english_name)
        rst = []
        for i in alias:
            if not i:
                continue
            i = i.strip()
            if i:
                if " " in i:
                    i = i.title()
                rst.append(i)
        rst = list(set(rst))

        return rst

    meta = {"indexes": ["#aid", "#name"], "collection": mongo_school_collection}

    @classmethod
    def init_single_from_school_info(cls, school_info):
        try:
            school = cls.objects.get(aid=int(school_info["lemma_id"]))
        except DoesNotExist:
            school = cls()

        school.aid = int(school_info["lemma_id"])
        school.name = school_info["school_name"]
        school.display_name = school_info["school_name"]
        school.alias = school_info["alias"]
        school.lemma_id = school_info["lemma_id"]
        school.summary = school_info["summary"]
        school.url = school_info["url"]
        school.stand_name_pinyin()
        school.stand_name_pinyin_tone()
        school.other_info = SchoolOtherInfo.init_from_dict(school_info["other_info"])
        school.save()

    @classmethod
    def init_all_data(cls, data: List[dict]):
        for school in tqdm.tqdm(data):
            cls.init_single_from_school_info(school)

    def __str__(self):
        return self.display_name


class CompanyAlias(Document):
    """
    Query company aliases from collection company_aliases
    company alias BO result from the mongodb query, used by searching service only
    """

    name = StringField(required=True)
    candidates = ListField()

    meta = {"collection": "company_aliases"}
