from functools import partial
from typing import Union

import pymongo

from .config import data_center_client


def collection_find(collection_name: str,
                    query: dict = None,
                    projection: dict = None,
                    timeout=5,
                    *args,
                    **kwargs) -> list:
    # 封装 data center的find，所有data center的数据获取入口
    return data_center_client.find(
        collection_name,
        filter=query,
        projection=projection,
        timeout=timeout,
        *args,
        **kwargs)


def get_single_object(collection_name,
                      ident: str,
                      projection: dict = None,
                      timeout=5,
                      *args,
                      **kwargs) -> Union[None, dict]:
    # 获取对应 collection 的单个文档
    object_list = collection_find(
        collection_name=collection_name,
        query={'_id': ident},
        projection=projection,
        timeout=timeout,
        *args,
        **kwargs)
    if object_list:
        return object_list[0]
    else:
        return None


candidate_find = partial(collection_find, collection_name="candidate")
get_single_candidate = partial(get_single_object, collection_name="candidate")

client_find = partial(collection_find, collection_name="client")
get_single_client = partial(get_single_object, collection_name="client")

joborder_find = partial(collection_find, collection_name="joborder")
get_single_joborder = partial(get_single_object, collection_name="joborder")

jobsubmission_find = partial(collection_find, collection_name="jobsubmission")
get_single_jobsubmission = partial(
    get_single_object, collection_name="jobsubmission")


def jobsubmission_create_index():
    data_center_client.create_index(
        type_='jobsubmission', keys=[('joborder', pymongo.HASHED)], timeout=5)

    data_center_client.create_index(
        type_='jobsubmission',
        keys=[('client_key', pymongo.HASHED)],
        timeout=5)

    data_center_client.create_index(
        type_='jobsubmission', keys=[('candidate', pymongo.HASHED)], timeout=5)
