from preprocess.work_year import WORK_YEAR_MAPPING


def candidate_convert(candidate: dict):
    # 将data center的 candidate 数据转为 resume_index 标准格式
    resume_info = dict()

    resume_info['aid'] = candidate['id']
    resume_info['tenant'] = candidate['client_key']
    resume_info['is_deleted'] = candidate['is_deleted']

    basic_info = dict()
    basic_info['name'] = candidate['chineseName']
    basic_info['english_name'] = candidate['englishName']
    basic_info['gender'] = candidate['gender']
    basic_info['mobile'] = candidate['mobile']
    basic_info['email'] = candidate['email']
    basic_info['linkedin'] = candidate['linkedin']
    basic_info['birth_day'] = candidate['dateOfBirth']
    basic_info['locations'] = candidate.get('location_ids', [])
    basic_info['city_ids'] = candidate.get('city_ids', [])
    basic_info['current_salary'] = candidate.get('current_salary', 0)

    exp_list = candidate.get('experiences', [])
    experiences = list()
    if isinstance(exp_list, list):
        for exp in exp_list:
            if 'company' not in exp:
                if 'client_name' in exp:
                    if exp['client_name']:
                        exp['company'] = exp['client_name'][0]
                    else:
                        exp['company'] = ''
                else:
                    exp['company'] = ''
            experiences.append(exp)

    educations = candidate.get('educations', [])
    projects = candidate.get('projects', [])
    custom_tags = candidate.get('tag_ids', [])

    resume_info['basic_info'] = basic_info
    resume_info['experiences'] = experiences
    resume_info['educations'] = educations
    resume_info['projects'] = projects
    resume_info['custom_tags'] = custom_tags
    return resume_info


def job_order_convert(job_order: dict):
    # 将data center的 job_order 数据转为 resume_index 标准格式
    jd_info = dict()

    jd_info['aid'] = job_order['id']
    jd_info['tenant'] = job_order['client_key']
    jd_info['version'] = job_order['current_version'] or 1
    jd_info['is_deleted'] = job_order['is_deleted']

    jd_info['cities'] = job_order.get('city_ids', [])
    jd_info['lowest_degree'] = job_order['degree_name']
    jd_info['working_years_range'] = WORK_YEAR_MAPPING.get(
        job_order['work_year'])

    jd_info['title'] = job_order['jobTitle'] or ""
    jd_info['content'] = job_order['description'] or ""
    jd_info['company'] = ''
    jd_info['salary_range'] = {
        "gte": job_order['monthlySalary'],
        "lte": job_order['maxMonthlySalary']
    }
    return jd_info
