import gllue_data_client
import mongoengine

from config import ai_service_config
from utils import LazyLoad

gllue_data_client.init_channel(
    host=ai_service_config["GLLUE_DATA_HOST"], port=ai_service_config["GLLUE_DATA_PORT"]
)

data_center_client: gllue_data_client.client.Client = LazyLoad(
    gllue_data_client.client.Client
)

mongodb_client = mongoengine.connect(
    db=ai_service_config["MONGO_DB"],
    host=ai_service_config["MONGO_HOST"],
    port=ai_service_config["MONGO_PORT"],
    username=ai_service_config["MONGO_USER"],
    password=ai_service_config["MONGO_PASSWORD"],
    authentication_source=ai_service_config["MONGO_AUTH_DB"],
    connect=False,
    connectTimeoutMS=1000,
)

mongo_school_collection = ai_service_config["MONGO_SCHOOL_COL"]
