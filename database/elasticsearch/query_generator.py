"""
生成es查询需要的部分字段，所有函数返回 查询对象
"""

from elasticsearch_dsl.query import Q


def gen_title_weight_nested_pairs(title_weight: dict) -> Q:
    query = Q(
        "term", **{
            "experiences.work_list.title_stand": {
                "value": title_weight['index'],
                "boost": title_weight['value']
            }
        })
    return query


def gen_poi_weight_nested_pairs(poi_weight: dict) -> Q:
    query = Q(
        "term", **{
            "experiences.poi_weight.index": {
                "value": poi_weight['index'],
                "boost": poi_weight['value']
            }
        })
    return query


def gen_lda_weight_nested_pairs(lda_vector_pair: dict) -> Q:
    query = Q(
        "term", **{
            "experiences.lda_weight.index": {
                "value": lda_vector_pair['index'],
                "boost": lda_vector_pair['value']
            }
        })

    return query


def gen_company_title_nested_pairs(company: str, title: str) -> Q:
    query = Q(
        "bool",
        must=[
            Q("match_phrase", **{"experiences.work_list.company": company}),
            Q("term", **{"experiences.work_list.title_stand": title})
        ])
    return query


def gen_school_major_nested_pairs(school_id: str, major: str) -> Q:
    query = Q(
        "bool",
        must=[
            Q("term", **{"educations.edu_list.school_stand_id": school_id}),
            Q("term", **{"educations.edu_list.major_stand": major})
        ])
    return query
