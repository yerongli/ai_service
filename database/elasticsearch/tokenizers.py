from elasticsearch_dsl import tokenizer

basic_pinyin_tokenizer = tokenizer(
    "basic_pinyin_tokenizer",
    type="pinyin",
    keep_first_letter=True,
    keep_separate_first_letter=True,
    keep_full_pinyin=True,
    keep_original=False,
    limit_first_letter_length=16,
    lowercase=True,
)
