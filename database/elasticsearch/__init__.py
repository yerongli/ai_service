from elasticsearch.exceptions import NotFoundError
from .config import elastic_search, es_jd_feature_index, es_resume_feature_index, es_company_feature_index, \
    es_school_feature_index


def es_jd_index_init():
    from .models import JobOrderFeatures
    try:
        elastic_search.indices.close(es_jd_feature_index)
    except NotFoundError:
        pass
    JobOrderFeatures.init()
    elastic_search.indices.open(es_jd_feature_index)


def es_resume_index_init():
    from .models import ResumeFeatures
    try:
        elastic_search.indices.close(es_resume_feature_index)
    except NotFoundError:
        pass
    ResumeFeatures.init()
    elastic_search.indices.open(es_resume_feature_index)


def es_company_index_init():
    from .models import CompanyFeatures
    try:
        elastic_search.indices.close(es_company_feature_index)
    except NotFoundError:
        pass
    CompanyFeatures.init()
    elastic_search.indices.open(es_company_feature_index)


def es_school_index_init():
    from .models import SchoolFeatures
    try:
        elastic_search.indices.close(es_school_feature_index)
    except NotFoundError:
        pass
    SchoolFeatures.init()
    elastic_search.indices.open(es_school_feature_index)
