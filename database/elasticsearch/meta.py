import logging

from elasticsearch.exceptions import NotFoundError
from elasticsearch_dsl import document as e_document
from elasticsearch_dsl import field as e_field

from database import get_ident


class TenantDocumentMeta(e_document.Document):
    """
    所有document需要继承。
    """
    tenant = e_field.Keyword(required=True)  # 用以区分租户
    aid = e_field.Keyword(required=True)  # id
    is_deleted = e_field.Boolean(required=True)  # 软删除

    def save(self,
             using=None,
             index=None,
             validate=True,
             skip_empty=True,
             **kwargs):
        self.meta.id = get_ident(self.tenant, self.aid)
        super().save(using, index, validate, skip_empty, **kwargs)

    @classmethod
    def get_with_tenant_and_aid(cls, tenant, aid, source: list = None):
        # 根据 aid tenant 得到对应的jd
        ident = get_ident(tenant=tenant, aid=aid)
        try:
            rdm = cls.get(id=ident, _source=source)
            return rdm
        except NotFoundError:
            logging.warning(
                f"[NotFoundError], object [{cls.__name__}], tenant [{tenant}], aid [{aid}]"
            )
            return None

    @classmethod
    def delete_with_tenant_and_aid(cls, tenant, aid) -> bool:
        rdm = cls.get_with_tenant_and_aid(tenant, aid)
        if rdm:
            rdm.is_deleted = True
            rdm.save()
            return True
        else:
            return False


class VectorFieldMeta:
    def __init__(self):
        self.index = None
        self.value = None

    @classmethod
    def from_json(cls, data):
        rst = []
        if isinstance(data, dict):
            data = data.items()
        for k, v in data:
            o = cls()
            o.index = k
            o.value = v
            rst.append(o)
        return rst

    @classmethod
    def index_value_to_dict(cls, data_list: list):
        if not data_list:
            return {}
        rst = {}
        for i in data_list:
            rst[i['index']] = i['value']
        return rst

    @classmethod
    def extract_index(cls, data_list: list) -> list:
        if not data_list:
            return []
        rst = []
        for i in data_list:
            v = i.get('index')
            if v:
                rst.append(v)
        return rst

    def to_dict(self):
        d = {"index": self.index, "value": self.value}
        return d


class KeywordFloatVectorField(e_document.InnerDoc, VectorFieldMeta):
    index = e_field.Keyword()
    value = e_field.Float()


class KeywordIntVectorField(e_document.InnerDoc, VectorFieldMeta):
    index = e_field.Keyword()
    value = e_field.Integer()


class TextTextVectorField(e_document.InnerDoc, VectorFieldMeta):
    index = e_field.Text()
    value = e_field.Text()
