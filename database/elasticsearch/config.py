from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search
from elasticsearch_dsl import connections as es_connections

from config import ai_service_config

es_jd_feature_index = ai_service_config["ES_JD_FEATURE_INDEX"]
es_resume_feature_index = ai_service_config["ES_RESUME_FEATURE_INDEX"]
es_company_feature_index = ai_service_config["ES_COMPANY_FEATURE_INDEX"]
es_school_feature_index = ai_service_config["ES_SCHOOL_FEATURES_INDEX"]

elastic_search = Elasticsearch(
    hosts=ai_service_config["ES_HOST"], port=ai_service_config["ES_PORT"]
)
es_connections.create_connection(
    hosts=ai_service_config["ES_HOST"], port=ai_service_config["ES_PORT"]
)

elastic_jd_feature_searcher = Search(using=elastic_search, index=es_jd_feature_index)
elastic_resume_feature_searcher = Search(
    using=elastic_search, index=es_resume_feature_index
)
elastic_company_feature_searcher = Search(
    using=elastic_search, index=es_company_feature_index
)
elastic_school_feature_searcher = Search(
    using=elastic_search, index=es_school_feature_index
)
