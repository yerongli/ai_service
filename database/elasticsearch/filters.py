from elasticsearch_dsl import token_filter
from gllue_ai_libs import search_engine
from gllue_ai_libs.search_engine.utils import abs_model_path

from config import ai_service_config

search_engine.set_model_dir_path(ai_service_config["MODEL_PATH"])


# area_synonyms = [
#     '四川,四川省'
# ]

with open(abs_model_path("./es/area_synonym.txt"), "r") as f:
    area_synonyms = f.read().split("\n")

area_synonym_filter = token_filter(
    "area_synonym_filter", type="synonym", synonyms=area_synonyms
)
