from elasticsearch_dsl import analyzer, tokenizer

from .filters import area_synonym_filter
from .tokenizers import basic_pinyin_tokenizer

text_english_analyzer = analyzer("english")
text_chinese_analyzer = analyzer("ik_smart")

lower_keyword_analyzer = analyzer(
    "lowercase_keyword", tokenizer="keyword", filter=["standard", "lowercase"]
)

long_ngram_analyzer = analyzer(
    "long_ngram",
    tokenizer=tokenizer("long_trigram", "nGram", min_gram=1, max_gram=50),
    filter=["standard", "lowercase"],
)

text_chinese_area_synonyms_analyzer = analyzer(
    "text_chinese_area_synonyms_analyzer",
    tokenizer="ik_smart",
    filter=[area_synonym_filter],
)

basic_pinyin_analyzer = analyzer(
    "basic_pinyin_analyzer", tokenizer=basic_pinyin_tokenizer
)

completion_analyzer = analyzer(
    "completion_analyzer",
    tokenizer="standard",
    filter=["standard", "lowercase", "shingle"],
)
