import tqdm
from typing import Dict, List

from elasticsearch_dsl import field as e_field
from elasticsearch_dsl import document
from elasticsearch_dsl.query import Q
from database.elasticsearch.analyzer import (
    lower_keyword_analyzer,
    text_chinese_area_synonyms_analyzer,
    basic_pinyin_analyzer,
    completion_analyzer,
)
from database.elasticsearch.config import es_school_feature_index, elastic_search
from elasticsearch_dsl.utils import AttrList
from elasticsearch.exceptions import NotFoundError
from gllue_ai_libs.angelo_tool.nlp.base_func import sub_brackets
from database.mongodb.models import School


class SchoolFeatures(document.Document):
    """
    学校信息主页
    """

    aid = e_field.Keyword(required=True)
    is_deleted = e_field.Boolean(required=True)

    automatic_completion = e_field.Completion(analyzer=completion_analyzer)

    # 学校名(如：东北大学)
    name = e_field.Text(
        required=True,
        analyzer=text_chinese_area_synonyms_analyzer,
        copy_to=["automatic_completion"],
        fields={
            "raw": e_field.Text(analyzer=lower_keyword_analyzer),
            "pinyin": e_field.Text(analyzer=basic_pinyin_analyzer, boost=1.2),
        },
    )

    # 别名（如：东北工学院）
    alias = e_field.Text(
        required=True,
        analyzer=text_chinese_area_synonyms_analyzer,
        copy_to="automatic_completion",
        fields={
            "raw": e_field.Text(analyzer=lower_keyword_analyzer),
            "pinyin": e_field.Text(analyzer=basic_pinyin_analyzer, boost=0.8),
        },
    )

    # 985院校
    is_985 = e_field.Boolean()
    # 211院校
    is_211 = e_field.Boolean()
    # 一流大学
    is_first_class_university = e_field.Boolean()
    # 一流学科
    is_first_class_major = e_field.Boolean()
    # 双一流
    is_double_first = e_field.Boolean()
    # 教育部直属
    is_education_directly = e_field.Boolean()
    # 中央部委
    is_central_ministry_directly = e_field.Boolean()
    # 自主招生
    is_independent_recruitment = e_field.Boolean()
    # QS200排名
    qs200 = e_field.Integer()

    # 人数
    popularity = e_field.Integer()

    @classmethod
    def init_all_data(cls, data: List[School]):
        for school in tqdm.tqdm(data):
            sf = cls.init_single_from_school_info(school)
            sf.save()

    @classmethod
    def init_single_from_school_info(cls, school: School) -> "SchoolFeatures":
        try:
            sf = cls.get(id=school.aid)
        except NotFoundError:
            sf = cls()
        sf.aid = int(school.aid)
        sf.is_deleted = school.is_deleted
        sf.name = school.name
        sf.alias = school.alias
        sf.popularity = school.popularity

        sf.is_985 = school.tags.is_985
        sf.is_211 = school.tags.is_211
        sf.is_first_class_university = school.tags.is_first_class_university
        sf.is_first_class_major = school.tags.is_first_class_major
        sf.is_double_first = school.tags.is_double_first
        sf.is_education_directly = school.tags.is_education_directly
        sf.is_central_ministry_directly = school.tags.is_central_ministry_directly
        sf.is_independent_recruitment = school.tags.is_independent_recruitment
        sf.qs200 = school.tags.qs200
        return sf

    def save(self, *args, **kwargs):
        if self.aid is not None:
            self.meta.id = self.aid
            super().save(*args, **kwargs)

    @classmethod
    def suggest_name_to_alias(cls, name, size=10):
        """
        学校的搜索补全。输入名称，返回对饮名称。比如 北大 -> [北大,北大光华]。只做补全用
        :param name: 输入的字符串
        :param size: 返回数量
        :return:
            {
                "took": int,        # 耗时
                "timed_out": bool,  # 是否超时
                "items": [item],        # 值
            }

            item = {
                "text": str,        # 返回值
                "_index": str,      # 对应表
                "_id": str,         # id
                "_score": _score    # 分数
            }
        """
        query = {
            "suggest": {
                "school-suggest": {
                    "prefix": name,
                    "completion": {
                        "field": "automatic_completion",
                        "skip_duplicates": True,
                        "size": size,
                    },
                }
            },
            "_source": ["suggest"],
        }

        query_rst = elastic_search.search(
            index=es_school_feature_index, doc_type="doc", body=query
        )

        result = dict()
        result["took"] = query_rst["took"]
        result["timed_out"] = query_rst["timed_out"]
        result["items"] = [
            {
                "text": i["text"],
                "_index": i["_index"],
                "_id": i["_id"],
                "_score": i["_score"],
            }
            for i in query_rst["suggest"]["school-suggest"][0]["options"]
        ]

        return result

    @classmethod
    def suggest_name_to_name(cls, name, size=10):
        query = {
            "suggest": {
                "school-suggest": {
                    "prefix": name,
                    "completion": {
                        "field": "automatic_completion",
                        "skip_duplicates": False,
                        "size": size,
                    },
                }
            },
            "_source": ["suggest", "name"],
        }

        query_rst = elastic_search.search(
            index=es_school_feature_index, doc_type="doc", body=query
        )

        result = dict()
        result["took"] = query_rst["took"]
        result["timed_out"] = query_rst["timed_out"]
        result["items"] = [
            {
                "text": i["_source"]["name"],
                "_index": i["_index"],
                "_id": i["_id"],
                "_score": i["_score"],
            }
            for i in query_rst["suggest"]["school-suggest"][0]["options"]
        ]

        return result

    @classmethod
    def search_list(cls, name: str, k: float = 5.0, n: int = 10) -> List[Dict]:
        name = sub_brackets(name)
        x = (
            cls.search()
            .query(
                "bool",
                should=[
                    Q(
                        "multi_match",
                        query=name,
                        fields=["name^1.2", "alias^0.8"],
                        boost=0.6,
                    ),
                    Q("term", **{"name.raw": {"value": name, "boost": 1.2}}),
                    Q("term", **{"alias.raw": {"value": name, "boost": 0.8}}),
                ],
            )
            .source(["name", "aid"])[:n]
        )

        r = x.execute()
        if len(r) > 0:
            rst = [
                {"aid": i.aid, "name": i.name, "ix": i.name, "score": i.meta["score"]}
                for i in r.hits
                if i.meta["score"] >= k
            ]
        else:
            rst = []
        return rst

    @classmethod
    def add_alias(cls, sf: "SchoolFeatures", alias: list):
        # 增加别名
        x_alias = list(sf.alias) if isinstance(sf.alias, AttrList) else [sf.alias]
        x_alias.extend(alias)
        x_alias = list(set([i for i in x_alias]))
        sf.alias = x_alias
        sf.save()

    @classmethod
    def restore_from_es_json(cls, data_list):
        fields = [
            "aid",
            "is_deleted",
            "name",
            "alias",
            "lemma_id",
            "summary",
            "url",
            "school_badge_url",
            "is_985",
            "is_211",
            "is_first_class_university",
            "is_first_class_discipline",
            "is_education_directly",
            "is_central_ministry_directly",
            "is_independent_recruitment",
            "is_directly",
            "qs200",
            "double_first",
            "other_info",
            "popularity",
        ]
        for d in tqdm.tqdm(data_list):
            sf = cls()
            for f in fields:
                setattr(sf, f, d.get(f))
            sf.save()

    class Index:
        name = es_school_feature_index
