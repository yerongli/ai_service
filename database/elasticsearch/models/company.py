from typing import Dict, List, Union

import tqdm
from elasticsearch_dsl import field as e_field
from elasticsearch_dsl.query import Q
from elasticsearch_dsl.utils import AttrList

from database import get_ident
from database.elasticsearch.analyzer import (
    lower_keyword_analyzer,
    text_chinese_area_synonyms_analyzer,
    basic_pinyin_analyzer,
)
from database.elasticsearch.config import es_company_feature_index
from database.elasticsearch.meta import TenantDocumentMeta
from database.mongodb import (client_find, data_center_client, get_single_client, CompanyAlias)


class CompanyFeatures(TenantDocumentMeta):
    """
    公司。对应 gllue web 的client
    """

    name = e_field.Text(
        required=True,
        analyzer=text_chinese_area_synonyms_analyzer,
        fields={
            "raw": e_field.Text(analyzer=lower_keyword_analyzer),
            "pinyin": e_field.Text(analyzer=basic_pinyin_analyzer, boost=1.2),
        },
    )

    alias = e_field.Text(
        required=True,
        analyzer=text_chinese_area_synonyms_analyzer,
        fields={
            "raw": e_field.Text(analyzer=lower_keyword_analyzer),
            "pinyin": e_field.Text(analyzer=basic_pinyin_analyzer, boost=0.8),
        },
    )

    def save(self, *args, **kwargs):
        if self.aid is not None and self.tenant is not None:
            self.meta.id = get_ident(tenant=self.tenant, aid=self.aid)
            super().save(*args, **kwargs)

    @classmethod
    def need_fields(cls):
        fs = {
            "_id": 1,
            "client_key": 1,
            "id": 1,
            "name": 1,
            "alias": 1,
            "is_deleted": 1,
        }
        return fs

    @classmethod
    def all_init_data(cls):
        # 从数据中心初始化所有数据
        client_keys = data_center_client.get_client_keys(timeout=5)
        for tenant in client_keys:
            cls.tenant_init_data(tenant)

    @classmethod
    def tenant_init_data(cls, tenant):
        # 从数据中心初始化租户的数据
        data = client_find(
            query={"client_key": tenant}, projection=cls.need_fields(), timeout=None
        )
        for i in tqdm.tqdm(data):
            cls.ident_update_company_info(company=i)

    @classmethod
    def ident_update_company_info(
        cls, company: dict = None, ident: str = None
    ) -> "CompanyFeatures":
        # 单个更新
        if not company:
            company = get_single_client(ident=ident, projection=cls.need_fields())
        cf = cls()
        cf.meta.id = company["_id"]
        cf.tenant = company["client_key"]
        cf.aid = company["id"]
        cf.is_deleted = company["is_deleted"]
        cf.name = company["name"]
        cf.alias = company.get("alias") or company["name"]
        cf.save()
        return cf

    @classmethod
    def search_list(
        cls, name: str, k: float = 6.0, n: int = 10
    ) -> Union[List[Dict], None]:
        # 通用的名称搜索
        x = (
            cls.search()
            .query(
                "bool",
                should=[
                    Q(
                        "multi_match",
                        query=name,
                        fields=["name^1.2", "alias^0.8"],
                        boost=0.6,
                    ),
                    Q("term", **{"name.raw": name}),
                    Q("term", **{"alias.raw": name}),
                ],
            )
            .source(["name", "aid"])[:n]
        )
        r = x.execute()

        rst = list()
        for i in r.hits:
            if i.meta["score"] >= k:
                rst.append({"aid": i.aid, "name": i.name, "score": i.meta["score"]})
        return rst

    @classmethod
    def search_list_(cls, name: str, k: float = 6.,
                    n: int = 10) -> List[Dict]:
        """
        Search company query aliases with collection company_aliaes
        :param name: Company name input
        :param k: The cutoff threshold the similarity confidence from index company_features
        :param n: Maximum number of entries in query result
        :return: A list of projected entries from index company_features
                Return [] if no aliases exists
        """
        # 通用的名称搜索
        candidates = CompanyAlias.objects(name=name)
        if len(candidates) > 0:
            candidates = [sibling_['name'] for sibling_ in candidates.first().candidates]
        else:
            '''
            Collection company_aliases stores companies with aliases, if a specific name if not in 
            alias collection, do the normal query in elastic search, so the candidates list is [name]
            '''
            candidates = [name]
            
        """
        Fetch a document if a candidate name appears matches name or alias directly, or 
        the name appears in the name alias list
        """
        should_list = [Q("simple_query_string", query='|'.join(candidates),
                         fields=["name^1.5", "alias^0.5"], boost=0.3)] + \
                      [Q("term", **{"name.raw": {"value": sibling_, "boost": 1.4}}) for sibling_ in candidates] + \
                      [Q("term", **{"alias.raw": {"value": sibling_, "boost": 1.0}}) for sibling_ in candidates]

        x = cls.search().query("bool", should=should_list).source(['name', 'aid'])[:n]
        r = x.execute()

        # Return ID, company name, ES score only
        if len(r.hits) > 0:
            rst = [{"aid": i.aid, "name": i.name, "score": i.meta['score']} for i in r.hits if i.meta['score'] >= k]
        else:
            rst = list()
        return rst

    @classmethod
    def add_alias(cls, sf: "CompanyFeatures", alias: list):
        # 增加别名
        x_alias = list(sf.alias) if isinstance(sf.alias, AttrList) else [sf.alias]
        x_alias.extend(alias)
        x_alias = list(set([i for i in x_alias]))
        sf.alias = x_alias
        sf.save()

    class Index:
        name = es_company_feature_index
