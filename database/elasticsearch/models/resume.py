import logging
from typing import Dict, List

import pendulum
import tqdm
from elasticsearch.exceptions import NotFoundError
from elasticsearch_dsl import document as e_document
from elasticsearch_dsl import field as e_field
from elasticsearch_dsl.query import Q

from common.resume_index.index import (BasicInfo, EducationExperienceList,
                                       WorkExperienceList, index_single_resume)
from database.elasticsearch.analyzer import (
    long_ngram_analyzer, lower_keyword_analyzer, text_chinese_analyzer)
from database.elasticsearch.config import (elastic_search,
                                           es_resume_feature_index)
from database.elasticsearch.meta import (
    KeywordFloatVectorField, KeywordIntVectorField, TenantDocumentMeta)
from database.mongodb import (candidate_convert, candidate_find,
                              data_center_client, get_single_candidate)


class BasicInfoDoc(e_document.InnerDoc):
    name = e_field.Text(
        analyzer=long_ngram_analyzer,
        fields={"raw": e_field.Text(analyzer=lower_keyword_analyzer)})
    gender = e_field.Keyword()
    mobile = e_field.Text(
        analyzer=long_ngram_analyzer,
        fields={"raw": e_field.Text(analyzer=lower_keyword_analyzer)})
    email = e_field.Text(
        analyzer=long_ngram_analyzer,
        fields={"raw": e_field.Text(analyzer=lower_keyword_analyzer)})
    linkedin = e_field.Text(
        analyzer=long_ngram_analyzer,
        fields={"raw": e_field.Text(analyzer=lower_keyword_analyzer)})
    birth_day = e_field.Date()
    age = e_field.Integer()
    current_salary = e_field.Float()
    locations = e_field.Keyword()

    @classmethod
    def from_json(cls, data):
        o = cls()
        o.name = data['name']
        o.gender = data['gender']
        o.mobile = data['mobile']
        o.email = data['email']
        o.linkedin = data['linkedin']
        o.birth_day = data['birth_day'] if data['birth_day'] else None
        o.age = data['age']
        o.current_salary = data['current_salary']
        o.locations = data['locations']
        return o

    @classmethod
    def ident_update(cls, ident):
        resume_info = get_single_candidate(ident=ident)
        if resume_info:
            resume_info = candidate_convert(resume_info)
            basic_info = BasicInfo.init_from_basic_info(
                resume_info['basic_info'])
            try:
                rdm = ResumeFeatures.get(id=ident)
                rdm.basic_info = BasicInfoDoc.from_json(basic_info.to_json())
                rdm.save()
            except NotFoundError:
                return

    @classmethod
    def tenant_update(cls, tenant):
        # 按租户更新 resume_index basic_info
        data = candidate_find(
            query={"client_key": tenant},
            projection={"ident": 1},
            timeout=None)
        ident_list = [i['ident'] for i in data]
        for ident in tqdm.tqdm(ident_list):
            try:
                cls.tenant_update(ident)
            except Exception as e:
                logging.exception("tenant_reindex_resume_basic_info")

    @classmethod
    def all_update(cls):
        # 更新所有 resume_index basic_info
        client_key_list = data_center_client.get_client_keys(timeout=5)
        for client_key in client_key_list:
            cls.tenant_update(tenant=client_key)


class EducationDoc(e_document.InnerDoc):
    # 最近
    is_recent = e_field.Boolean()
    # 在读
    is_current = e_field.Boolean()
    # 权重
    weight = e_field.Float()

    # 学校名
    school = e_field.Text(
        analyzer=text_chinese_analyzer,
        fields={"raw": e_field.Text(analyzer=lower_keyword_analyzer)})
    # 学校归一化
    school_stand_id = e_field.Keyword()

    # 专业
    major = e_field.Text(
        analyzer=text_chinese_analyzer,
        fields={"raw": e_field.Text(analyzer=lower_keyword_analyzer)})

    # 专业归一化
    major_stand = e_field.Keyword()

    # 学历
    degree = e_field.Text()
    # 学历归一化
    degree_stand = e_field.Keyword()

    # 开始时间
    start = e_field.Date()
    # 结束时间
    end = e_field.Date()

    # 持续时间（月）
    duration_month = e_field.Integer()

    # 专业是否合法
    is_major_legal = e_field.Boolean()
    # 时间是否合法
    is_time_legal = e_field.Boolean()
    # 教育经历是否合法
    is_edu_legal = e_field.Boolean()

    # 是否985
    is_985 = e_field.Boolean()
    # 是否211
    is_211 = e_field.Boolean()
    # 是否985、211
    is_985_211 = e_field.Boolean()
    # 是否海外
    is_abroad = e_field.Boolean()
    # 是否QS200
    is_qs200 = e_field.Boolean()

    @classmethod
    def from_json(cls, data):
        o = cls()
        o.is_recent = data['is_recent']
        o.is_current = data['is_current']
        o.weight = data['weight']

        o.school = data['school']
        o.school_stand_id = data['school_stand_id']

        o.major = data['major']
        o.major_stand = data['major_stand']

        o.degree = data['degree']
        o.degree_stand = data['degree_stand']

        o.start = data['start'] if data['start'] else None
        o.end = data['end'] if data['end'] else None

        o.duration_month = data['duration_month'] or None

        o.is_major_legal = bool(data['is_major_legal'])
        o.is_time_legal = bool(data['is_time_legal'])
        o.is_edu_legal = bool(data['is_edu_legal'])
        o.is_985 = bool(data['is_985'])
        o.is_211 = bool(data['is_211'])
        o.is_985_211 = bool(data['is_985_211'])
        o.is_abroad = bool(data['is_abroad'])
        o.is_qs200 = bool(data['is_qs200'])
        return o


class EducationListDoc(e_document.InnerDoc):
    # 最近学校
    last_school = e_field.Keyword()
    # 最近专业
    last_major = e_field.Keyword()
    # 最近学历
    last_degree = e_field.Keyword()
    # 最近毕业时间
    last_graduation_time = e_field.Date()

    # 教育经历总时长（月）
    total_duration_month = e_field.Integer()

    # 教育经历是否中断
    is_education_interrupt = e_field.Boolean()
    # 教育经历是否合法
    is_education_illegal = e_field.Boolean()
    # 是否985
    is_985 = e_field.Boolean()
    # 是否211
    is_211 = e_field.Boolean()
    # 是否985、211
    is_985_211 = e_field.Boolean()
    # 是否QS200
    is_qs200 = e_field.Boolean()
    # 是否海外
    is_abroad = e_field.Boolean()

    # 第一学历985
    first_is_985 = e_field.Boolean()
    # 第一学历211
    first_is_211 = e_field.Boolean()
    # 第一学历985、211
    first_is_985_211 = e_field.Boolean()
    # 第一学历QS200
    first_is_qs200 = e_field.Boolean()
    # 第一学历是否海外
    first_is_abroad = e_field.Boolean()

    # 教育经历
    edu_list = e_field.Nested(EducationDoc)

    def add_edu(self, data):
        edu = EducationDoc.from_json(data)
        self.edu_list.append(edu)

    @classmethod
    def from_json(cls, data):
        o = cls()
        o.last_school = data['last_school']
        o.last_major = data['last_major']
        o.last_degree = data['last_degree']
        o.last_graduation_time = data['last_graduation_time']
        o.total_duration_month = data['total_duration_month']

        o.is_education_interrupt = data['is_education_interrupt']
        o.is_education_illegal = data['is_education_illegal']
        o.is_985 = data['is_985']
        o.is_211 = data['is_211']
        o.is_985_211 = data['is_985_211']
        o.is_qs200 = data['is_qs200']
        o.is_abroad = data['is_abroad']

        o.first_is_985 = data['first_is_985']
        o.first_is_211 = data['first_is_211']
        o.first_is_985_211 = data['first_is_985_211']
        o.first_is_qs200 = data['first_is_qs200']
        o.first_is_abroad = data['first_is_abroad']

        for edu in data['edu_list']:
            o.add_edu(edu)
        return o

    @classmethod
    def ident_update(cls, ident):
        resume_info = get_single_candidate(ident=ident)
        if resume_info:
            resume_info = candidate_convert(resume_info)
            educations = EducationExperienceList.init_from_educations(
                resume_info['educations'])
            try:
                rdm = ResumeFeatures.get(id=ident)
                rdm.educations = EducationListDoc.from_json(
                    educations.to_json())
                rdm.save()
            except NotFoundError:
                return

    @classmethod
    def tenant_update(cls, tenant):
        # 按租户更新 resume_index educations
        data = candidate_find(
            query={"client_key": tenant},
            projection={"ident": 1},
            timeout=None)
        ident_list = [i['ident'] for i in data]
        for ident in tqdm.tqdm(ident_list):
            try:
                cls.ident_update(ident)
            except Exception as e:
                logging.exception("tenant_reindex_resume_educations")

    @classmethod
    def all_update(cls):
        # 更新所有 resume_index educations
        client_key_list = data_center_client.get_client_keys(timeout=5)
        for client_key in client_key_list:
            cls.tenant_update(tenant=client_key)


class ExperienceDoc(e_document.InnerDoc):
    # 是否最近
    is_recent = e_field.Boolean()
    # 是否在职
    is_current = e_field.Boolean()
    # 权重
    weight = e_field.Float()
    # 公司名
    company = e_field.Text(
        analyzer=text_chinese_analyzer,
        fields={"raw": e_field.Text(analyzer=lower_keyword_analyzer)})
    # 公司名关键词
    company_keyword = e_field.Text()

    # 职位
    title = e_field.Text(
        analyzer=text_chinese_analyzer,
        fields={"raw": e_field.Text(analyzer=lower_keyword_analyzer)})
    # 职位归一
    title_stand = e_field.Keyword()
    # 母职位
    title_parent = e_field.Keyword()
    # 职级
    title_level = e_field.Integer()

    # 开始时间
    start = e_field.Date()
    # 结束时间
    end = e_field.Date()

    # 持续时间（月）
    duration_month = e_field.Integer()

    # 公司行业
    company_industry_list = e_field.Keyword()
    # 是否行业知名
    is_industry_famous = e_field.Boolean()

    # 描述
    description = e_field.Text(analyzer=text_chinese_analyzer)
    # POI
    poi_weight = e_field.Nested(KeywordFloatVectorField)
    # LDA
    lda_weight = e_field.Nested(KeywordFloatVectorField)

    @classmethod
    def from_json(cls, data):
        o: ExperienceDoc = cls()
        o.is_recent = data['is_recent']
        o.is_current = data['is_current']
        o.weight = data['weight']

        o.company = data['company']
        o.company_keyword = data['company_keyword']

        o.title = data['title']
        o.title_stand = data['title_stand']
        o.title_parent = data['title_parent']
        o.title_level = data['title_level']

        o.start = data['start'] if data['start'] else None
        o.end = data['end'] if data['end'] else None

        o.duration_month = data['duration_month']
        o.company_industry_list = data['company_industry_list']
        o.is_industry_famous = bool(data['is_industry_famous'])

        o.description = data['description']
        o.poi_weight = KeywordFloatVectorField.from_json(data['poi_weight'])
        o.lda_weight = KeywordFloatVectorField.from_json(data['lda_weight'])
        return o


class ExperienceListDoc(e_document.InnerDoc):
    # 最高职级
    title_level_highest = e_field.Integer()
    title_weight = e_field.Nested(KeywordFloatVectorField)
    # 职位持续时间（月）
    title_duration_month = e_field.Nested(KeywordIntVectorField)
    # 总工作时长（月）
    total_duration_month = e_field.Integer()
    # 行业时长（月）
    company_industry_duration_month = e_field.Nested(KeywordIntVectorField)
    # 是否工作交叉
    is_work_exp_across = e_field.Boolean()
    # 是否工作断档
    is_work_exp_severed = e_field.Boolean()

    # POI
    poi_weight = e_field.Nested(KeywordFloatVectorField)
    # POI月
    poi_duration_month = e_field.Nested(KeywordIntVectorField)
    # LDA
    lda_weight = e_field.Nested(KeywordFloatVectorField)

    work_list = e_field.Nested(ExperienceDoc)

    def add_work(self, data):
        work = ExperienceDoc.from_json(data)
        self.work_list.append(work)

    @classmethod
    def from_json(cls, data):
        o: ExperienceListDoc = cls()

        o.title_level_highest = data['title_level_highest']
        o.title_weight = KeywordFloatVectorField.from_json(
            data['title_weight'])
        o.title_duration_month = KeywordIntVectorField.from_json(
            data['title_duration_month'])

        o.total_duration_month = data['total_duration_month']

        o.company_industry_duration_month = KeywordIntVectorField.from_json(
            data['company_industry_duration_month'])

        o.is_work_exp_across = data['is_work_exp_across']
        o.is_work_exp_severed = data['is_work_exp_severed']

        o.poi_weight = KeywordFloatVectorField.from_json(data['poi_weight'])
        o.poi_duration_month = KeywordIntVectorField.from_json(
            data['poi_duration_month'])
        o.lda_weight = KeywordFloatVectorField.from_json(data['lda_weight'])

        for work in data['work_list']:
            o.add_work(work)

        return o

    @classmethod
    def ident_update(cls, ident):
        resume_info = get_single_candidate(ident=ident)
        if resume_info:
            resume_info = candidate_convert(resume_info)
            experiences = WorkExperienceList.init_from_experiences(
                resume_info['experiences'])
            try:
                rdm = ResumeFeatures.get(id=ident)
                rdm.experiences = ExperienceListDoc.from_json(
                    experiences.to_json())
                rdm.save()
            except NotFoundError:
                return

    @classmethod
    def tenant_update(cls, tenant):
        # 按租户更新 resume_index experiences
        data = candidate_find(
            query={"client_key": tenant},
            projection={"ident": 1},
            timeout=None)
        ident_list = [i['ident'] for i in data]
        for ident in tqdm.tqdm(ident_list):
            try:
                cls.ident_update(ident)
            except Exception as e:
                logging.exception("tenant_reindex_resume_experiences")

    @classmethod
    def all_update(cls):
        # 更新所有 resume_index experiences
        client_key_list = data_center_client.get_client_keys(timeout=5)
        for client_key in client_key_list:
            cls.tenant_update(tenant=client_key)


class ResumeFeatures(TenantDocumentMeta):
    basic_info = e_field.Object(BasicInfoDoc)
    educations = e_field.Object(EducationListDoc)
    experiences = e_field.Object(ExperienceListDoc)

    custom_tags = e_field.Keyword()

    @classmethod
    def get_poi_list_with_tenant_and_aid(cls, tenant, aid) -> List[Dict]:
        rdm = cls.get_with_tenant_and_aid(
            tenant, aid, source=["experiences.poi_weight"])
        if rdm:
            result = rdm.experiences.poi_weight
            return result

    @classmethod
    def from_json(cls, data):
        resume = cls()
        resume.aid = data['aid']
        resume.tenant = data['tenant']
        resume.is_deleted = data['is_deleted']

        resume.basic_info = BasicInfoDoc.from_json(data['basic_info'])
        resume.custom_tags = data['custom_tags']

        resume.educations = EducationListDoc.from_json(data['educations'])
        resume.experiences = ExperienceListDoc.from_json(data['experiences'])

        return resume

    @classmethod
    def ident_update(cls, ident) -> "ResumeFeatures":
        # 根据 ident 索引 resume_index 并保存
        resume_info = get_single_candidate(ident=ident)
        if resume_info:
            try:
                resume_info = candidate_convert(resume_info)
                resume_index = index_single_resume(resume_info)
                rdm = ResumeFeatures.from_json(resume_index)
                rdm.save()
                return rdm
            except Exception as e:
                logging.exception("ident_update_resume")

    @classmethod
    def tenant_update(cls, tenant):
        # 按租户更新 resume_index index
        data = candidate_find(
            query={"client_key": tenant},
            projection={"ident": 1},
            timeout=None)
        ident_list = [i['ident'] for i in data]
        for ident in tqdm.tqdm(ident_list):
            try:
                cls.ident_update(ident)
            except Exception as e:
                logging.exception("tenant_reindex_resume")

    @classmethod
    def all_update(cls):
        # 更新所有 resume_index index
        client_key_list = data_center_client.get_client_keys(timeout=5)
        for client_key in client_key_list:
            cls.tenant_update(tenant=client_key)

    @classmethod
    def organization_chart(cls, company_list: list) -> dict:
        """
        组织架构图聚合
        :param company_list: 目标公司列表
        :return:
        """
        query = {
            "size": 0,
            "query": {
                "nested": {
                    "path": "experiences.work_list",
                    "query": {
                        "terms": {
                            "experiences.work_list.company.raw": company_list
                        }
                    }
                }
            },
            "aggs": {
                "title_count": {
                    "nested": {
                        "path": "experiences.work_list"
                    },
                    "aggs": {
                        "title_activate_count": {
                            "filter": {
                                "bool": {
                                    "must":
                                    [{
                                        "terms": {
                                            "experiences.work_list.company.raw":
                                            company_list
                                        }
                                    },
                                     {
                                         "term": {
                                             "experiences.work_list.is_recent":
                                             True
                                         }
                                     }]
                                }
                            },
                            "aggs": {
                                "title_count": {
                                    "terms": {
                                        "field":
                                        "experiences.work_list.title_stand"
                                    }
                                }
                            }
                        },
                        "title_inactive_count": {
                            "filter": {
                                "bool": {
                                    "must": [
                                        {
                                            "terms": {
                                                "experiences.work_list.company.raw":
                                                company_list
                                            }
                                        },
                                        {
                                            "term": {
                                                "experiences.work_list.is_recent":
                                                False
                                            }
                                        }
                                    ]
                                }
                            },
                            "aggs": {
                                "title_count": {
                                    "terms": {
                                        "field":
                                        "experiences.work_list.title_stand"
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        data = elastic_search.search(es_resume_feature_index, body=query)

        took = data['took']
        total = data['hits']['total']

        title_inactive_count = data['aggregations']['title_count'][
            'title_inactive_count']
        title_activate_count = data['aggregations']['title_count'][
            'title_activate_count']
        rst = {
            # 耗时
            "took": took,
            # 共有员工
            "total": total,
            # 离职员工
            "title_inactive_count": {
                "total": title_inactive_count['doc_count'],
                "buckets": title_inactive_count['title_count']['buckets']
            },
            # 在职员工
            "title_activate_count": {
                "total": title_activate_count['doc_count'],
                "buckets": title_activate_count['title_count']['buckets']
            }
        }
        return rst

    @classmethod
    def organization_chart_candidate_list(cls, tenant, company_list, title):
        """
        组织架构图，最后获取候选人列表
        :param tenant:
        :param company_list:
        :param title:
        :return:
        """
        rp = cls.search().filter(
            'term', tenant=tenant).query(
                'nested',
                path="experiences.work_list",
                query=Q(
                    'bool',
                    must=[
                        Q(
                            'terms', **{
                                "experiences.work_list.company.raw":
                                company_list
                            }),
                        Q('term', **{"experiences.work_list.is_recent": True}),
                        Q('prefix',
                          **{"experiences.work_list.title_stand": title})
                    ]))
        rst = rp.execute()
        return rst

    class Index:
        name = es_resume_feature_index
