import logging

import tqdm
from elasticsearch_dsl import document as e_document
from elasticsearch_dsl import field as e_field

from database.elasticsearch.analyzer import (lower_keyword_analyzer,
                                             text_chinese_analyzer)
from database.elasticsearch.config import es_jd_feature_index
from database.elasticsearch.meta import (KeywordFloatVectorField,
                                         TenantDocumentMeta)
from database.mongodb import (data_center_client, get_single_joborder,
                              job_order_convert, joborder_find)


class CandidatePreferenceDoc(e_document.InnerDoc):
    degree_avg = e_field.Float()
    title_level_avg = e_field.Float()
    work_duration_month_avg = e_field.Float()
    salary_avg = e_field.Float()
    age_avg = e_field.Float()

    major_weight = e_field.Nested(KeywordFloatVectorField)
    title_weight = e_field.Nested(KeywordFloatVectorField)
    parent_title_weight = e_field.Nested(KeywordFloatVectorField)
    lda_weight = e_field.Nested(KeywordFloatVectorField)
    poi_weight = e_field.Nested(KeywordFloatVectorField)

    @classmethod
    def from_json(cls, data):
        o = cls()
        o.degree_avg = data['degree_avg']
        o.title_level_avg = data['title_level_avg']
        o.work_duration_month_avg = data['work_duration_month_avg']
        o.salary_avg = data['salary_avg']
        o.age_avg = data['age_avg']

        o.major_weight = KeywordFloatVectorField.from_json(
            data['major_weight'])
        o.title_weight = KeywordFloatVectorField.from_json(
            data['title_weight'])
        o.parent_title_weight = KeywordFloatVectorField.from_json(
            data['parent_title_weight'])
        o.lda_weight = KeywordFloatVectorField.from_json(data['lda_weight'])
        o.poi_weight = KeywordFloatVectorField.from_json(data['poi_weight'])

        return o


class JobOrderFeatures(TenantDocumentMeta):
    version = e_field.Integer(required=True)

    # 职位名
    title = e_field.Text(
        analyzer=text_chinese_analyzer,
        fields={"raw": e_field.Text(analyzer=lower_keyword_analyzer)})
    # 职位名清洗
    title_clean = e_field.Text(
        analyzer=text_chinese_analyzer,
        fields={"raw": e_field.Text(analyzer=lower_keyword_analyzer)})
    # 职位名标准化
    title_stand = e_field.Keyword()
    # 职位prob
    title_prob_weight = e_field.Nested(KeywordFloatVectorField)
    # 职级
    title_level = e_field.Integer()
    # 职位描述
    content = e_field.Text()
    # 职位描述清洗
    content_clean = e_field.Text(analyzer=text_chinese_analyzer)

    cities = e_field.Keyword()
    company = e_field.Text(
        analyzer=text_chinese_analyzer,
        fields={"raw": e_field.Text(analyzer=lower_keyword_analyzer)})
    company_keyword = e_field.Text(
        analyzer=text_chinese_analyzer,
        fields={"raw": e_field.Text(analyzer=lower_keyword_analyzer)})
    recommend_company = e_field.Text(
        analyzer=text_chinese_analyzer,
        fields={"raw": e_field.Text(analyzer=lower_keyword_analyzer)})

    lowest_degree = e_field.Keyword()
    need_majors = e_field.Keyword()
    need_985 = e_field.Boolean()
    need_211 = e_field.Boolean()
    need_985_211 = e_field.Boolean()
    need_industries = e_field.Keyword()

    salary_range = e_field.FloatRange()
    working_years_range = e_field.IntegerRange()
    age_range = e_field.IntegerRange()

    lda_weight = e_field.Nested(KeywordFloatVectorField)
    poi_weight = e_field.Nested(KeywordFloatVectorField)

    candidate_preference = e_field.Object(CandidatePreferenceDoc)

    @classmethod
    def from_json(cls, data):
        o = cls()

        o.tenant = data['tenant']
        o.aid = data['aid']

        o.is_deleted = data['is_deleted']
        o.version = data['version']
        o.title = data['title']
        o.title_clean = data['title_clean']
        o.title_stand = data['title_stand']
        o.title_prob_weight = KeywordFloatVectorField.from_json(
            data['title_prob_weight'])
        o.title_level = data['title_level']
        o.content = data['content']
        o.content_clean = data['content_clean']
        o.cities = data['cities']
        o.company = data['company']
        o.company_keyword = data['company_keyword']
        o.recommend_company = data['recommend_company']
        o.lowest_degree = data['lowest_degree']
        o.need_majors = data['need_majors']
        o.need_985_211 = data['need_985_211']
        o.need_industries = data['need_industries']
        o.salary_range = data['salary_range']
        o.working_years_range = data['working_years_range']
        o.age_range = data['age_range']

        o.lda_weight = KeywordFloatVectorField.from_json(data['lda_weight'])
        o.poi_weight = KeywordFloatVectorField.from_json(data['poi_weight'])
        o.candidate_preference = data['candidate_preference']

        candidate_preference = data['candidate_preference']
        if candidate_preference:
            o.candidate_preference = CandidatePreferenceDoc.from_json(
                candidate_preference)
        return o

    @classmethod
    def ident_update(cls, ident: str) -> "JobOrderFeatures":
        from common.jd_index import JobDescription
        jd_info = get_single_joborder(ident=ident)
        if jd_info:
            jd_info = job_order_convert(jd_info)
            try:
                jd = JobDescription.init_from_jd_info(jd_info)
                jdm = JobOrderFeatures.from_json(jd.to_json())
                jdm.save()
                return jdm
            except Exception as e:
                logging.exception("ident_update_single_jd_info")

    @classmethod
    def tenant_update(cls, tenant: str):
        # 按租户更新 jd_index index
        data = joborder_find(
            query={"client_key": tenant}, projection={"ident": 1})
        ident_list = [i['ident'] for i in data]
        for ident in tqdm.tqdm(ident_list):
            try:
                cls.ident_update(ident)
            except Exception as e:
                logging.exception("tenant_reindex_jd")

    @classmethod
    def all_update(cls):
        # 更新所有 resume_index index
        client_key_list = data_center_client.get_client_keys(timeout=5)
        for client_key in client_key_list:
            cls.tenant_update(tenant=client_key)

    class Index:
        name = es_jd_feature_index
