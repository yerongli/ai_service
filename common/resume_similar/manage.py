from typing import Dict, List

from common.recall import resume_recall
from common.resume_recommend import schema_transformer
from common.resume_similar import ranker, source_fields
from common.utils import (BaseGeneratorUuid, link_tracking_time,
                          remove_set_element)
from database.elasticsearch.config import elastic_resume_feature_searcher
from database.elasticsearch.models import ResumeFeatures
from preprocess.resume_index import index_single_resume


class SimilarResume(BaseGeneratorUuid):
    def __init__(self, tenant):
        self.tenant = tenant
        super().__init__()

    # 人人匹配-库外建立，需要做画像
    @link_tracking_time
    def similar_resume_from_resume(self,
                                   resume_info: dict,
                                   topn: int = 10,
                                   min_score: float = 0.) -> dict:
        resume_index = self.__index_single_resume(resume_info)
        resume_index = ResumeFeatures.from_json(resume_index)
        return self._similar_resume_from_resume_index(
            resume_index, topn=topn, min_score=min_score)

    # 人人匹配
    @link_tracking_time
    def similar_resume_from_resume_id(self,
                                      resume_info: dict,
                                      topn: int = 10,
                                      min_score: float = 0.) -> dict:
        resume_index = ResumeFeatures.get_with_tenant_and_aid(
            resume_info['tenant'],
            resume_info['aid'],
            source=source_fields.cv_cv_match_field_list)

        return self._similar_resume_from_resume_index(
            resume_index, topn=topn, min_score=min_score)

    # 人人匹配-从resume_index
    @link_tracking_time
    def _similar_resume_from_resume_index(self,
                                          resume_index: ResumeFeatures,
                                          topn: int = 10,
                                          min_score: float = 0.) -> dict:
        score_list = self.__similar_resume_from_resume_index_use_description(
            resume_index, topn, min_score)
        company_title_score_list = self.__similar_resume_from_resume_index_use_company_title(
            resume_index)
        items = score_list[:7] + company_title_score_list[:3]
        result = {"link_uuid": self.link_uuid, "items": items}
        return result

    @link_tracking_time
    def __index_single_resume(self, resume_info: dict) -> dict:
        return index_single_resume(resume_info)

    @link_tracking_time
    def __recall_for_use_description(self, resume_index: ResumeFeatures):
        # 召回
        lda_aid_set = resume_recall.use_lda_weight(
            resume_index.experiences.lda_weight,
            self.tenant,
            topn=1000,
            min_score=0.35)

        title_aid_set = resume_recall.use_title_weight(
            [{
                'index': i['title_stand'],
                'value': i['weight']
            } for i in resume_index.experiences.work_list],
            self.tenant,
            topn=500,
            min_score=0.5)

        aid_set = lda_aid_set | title_aid_set
        return aid_set

    @link_tracking_time
    def __get_data_for_similar_resume(self, aid_list: list):
        query = elastic_resume_feature_searcher.filter(
            "term", tenant=self.tenant).filter(
                "terms",
                aid=aid_list).source(source_fields.cv_cv_match_field_list)
        data: List[dict] = [
            schema_transformer.resume_index_to_cv_cv_match_dict(i.to_dict())
            for i in query.scan()
        ]
        return data

    @link_tracking_time
    def __batch_similar_resume_resume(
            self, resume_index, candidate_resume_list: list) -> List[float]:
        return ranker.batch_similar_resume_resume(resume_index,
                                                  candidate_resume_list)

    # 人人匹配， 使用 lda, title 等召回
    @link_tracking_time
    def __similar_resume_from_resume_index_use_description(
            self,
            resume_index: ResumeFeatures,
            topn: int = 1000,
            min_score: float = 0.35) -> List[Dict]:
        aid_set = self.__recall_for_use_description(resume_index)

        resume_recall.record_recall(
            self.link_uuid, self.tenant, dtype='SRD', aid_set=aid_set)

        # 过滤
        aid_set = list(remove_set_element(aid_set, resume_index['aid']))

        # 获取数据
        resume_list = self.__get_data_for_similar_resume(aid_set)

        resume_index = schema_transformer.resume_index_to_cv_cv_match_dict(
            resume_index.to_dict())

        # 计算分数
        score_list = self.__batch_similar_resume_resume(
            resume_index, resume_list)

        result = list()
        for candidate_resume, score in zip(resume_list, score_list):
            if score >= min_score:
                result.append({
                    'aid': candidate_resume['aid'],
                    'score': score,
                    "tenant": self.tenant
                })
        else:
            result.sort(key=lambda x: x['score'], reverse=True)
        return result[:topn]

    @link_tracking_time
    def __recall_for_use_company_title(self,
                                       resume_index: ResumeFeatures) -> set:
        company_list = {
            i.company_keyword
            for i in resume_index.experiences.work_list
        }
        title_list = {
            i.title_stand
            for i in resume_index.experiences.work_list
        }

        aid_set = resume_recall.use_target_company_title(
            company_list, title_list, tenant=self.tenant)
        return aid_set

    # 人人匹配， 使用company-title联合召回.目的是找到相同公司相同职位的人
    @link_tracking_time
    def __similar_resume_from_resume_index_use_company_title(
            self,
            resume_index: ResumeFeatures,
            topn: int = 1000,
            min_score: float = 0.35) -> List[Dict]:

        # 召回
        aid_set = self.__recall_for_use_company_title(resume_index)

        resume_recall.record_recall(self.link_uuid, self.tenant, "SRCT",
                                    aid_set)

        # 过滤
        aid_set = list(remove_set_element(aid_set, resume_index['aid']))

        # 获取data
        data = self.__get_data_for_similar_resume(aid_set)

        resume_index = schema_transformer.resume_index_to_cv_cv_match_dict(
            resume_index.to_dict())

        score_list = self.__batch_similar_resume_resume(resume_index, data)

        result = list()
        for candidate_resume, score in zip(data, score_list):
            if score >= min_score:
                result.append({
                    'aid': candidate_resume['aid'],
                    'score': score,
                    "tenant": self.tenant
                })
        else:
            result.sort(key=lambda x: x['score'], reverse=True)
        return result[:topn]


# 人人匹配-库外建立，需要做画像
def similar_resume_from_resume(resume_info: dict,
                               topn: int = 10,
                               min_score: float = 0.) -> dict:
    return SimilarResume(
        tenant=resume_info['tenant']).similar_resume_from_resume(
            resume_info, topn, min_score)


# 人人匹配
def similar_resume_from_resume_id(resume_info: dict,
                                  topn: int = 10,
                                  min_score: float = 0.) -> dict:
    return SimilarResume(
        tenant=resume_info['tenant']).similar_resume_from_resume_id(
            resume_info, topn, min_score)
