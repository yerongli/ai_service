import functools
import math
from typing import List

from typing import Dict, Tuple, Union

from gllue_ai_libs.angelo_tool.analyze.algorithm import get_newton_law_of_cooling_a, newton_law_of_cooling


def cosine_dict(d1: Union[Dict, Tuple], d2: Union[Dict, Tuple]) -> float:
    # 计算d1 d2的余弦距离， d1,d2可以为dict 或 tuple
    if not isinstance(d1, dict):
        d1 = dict(d1)
    if not isinstance(d2, dict):
        d2 = dict(d2)
    intersection_keys = d1.keys() & d2.keys()
    v = 0
    for i in intersection_keys:
        v += d1.get(i, 0) * d2.get(i, 0)
    return v


class SimilarDistanceUtil:
    # 工作经验的系数
    work_exp_weight_cooling_a = get_newton_law_of_cooling_a(100, 1, 0, 10)
    work_exp_weight_cooling = functools.partial(newton_law_of_cooling, 100, 0,
                                                work_exp_weight_cooling_a)

    # 教育经验自身系数
    school_weight_cooling_a = get_newton_law_of_cooling_a(100, 1, 0, 10)
    school_weight_cooling = functools.partial(newton_law_of_cooling, 100, 0,
                                              school_weight_cooling_a)

    # 教育经验较工作经验的系数
    school_exp_weight_cooling_a = get_newton_law_of_cooling_a(100, 1, 0, 50)
    school_exp_weight_cooling = functools.partial(
        newton_law_of_cooling, 100, 0, school_exp_weight_cooling_a)

    # 工作经验时间差系数
    work_duration_cooling_a = get_newton_law_of_cooling_a(1, 0.5, 0,
                                                          5)  # 1-0.5,差5年则为0.5
    work_duration_cooling = functools.partial(newton_law_of_cooling, 1, 0.5,
                                              work_duration_cooling_a)

    @classmethod
    def similar_exp_list(cls, exp_list1, exp_list2):
        '''
        计算两段工作经历的相似分数，cosine_dict，越大越相似
        '''

        title_score = cosine_dict(exp_list1['title_weight'],
                                  exp_list1['title_weight']) * 3

        desc_poi_score = cosine_dict(exp_list1['poi_weight'],
                                     exp_list2['poi_weight']) * 3

        desc_lda_score = cosine_dict(exp_list1['lda_weight'],
                                     exp_list2['lda_weight']) * 3

        score = sum([title_score, desc_poi_score, desc_lda_score])
        score = score / 9
        return score

    @classmethod
    def similar_edu_list(cls, edu_list1, edu_list2):
        """
        计算两段教育经历的相似分数, cosine_dict，越大越相似
        """
        score = 0

        # school_weight_1 = edu_list1['school_weight']
        # school_weight_2 = edu_list2['school_weight']
        # score += rmse_dict(school_weight_1, school_weight_2) * 1
        score += cosine_dict(edu_list1['school_weight'],
                             edu_list2['school_weight']) * 2

        score += cosine_dict(edu_list1['major_weight'],
                             edu_list2['major_weight']) * 2

        score += cosine_dict(edu_list1['degree_weight'],
                             edu_list2['degree_weight']) * 1

        score = score / 5
        return score

    @classmethod
    def similar_resume_step2(cls, resume1, resume2):
        # todo: 待补全
        """
        resume_index 的相似比较, 第二次召回使用
        Parameters
        ----------
        resume1 搜索resume
        resume2 候选resume

        Returns
        -------

        """
        work_year = math.ceil(
            resume1['experiences']['total_duration_month'] / 12)
        school_exp_a = cls.school_exp_weight_cooling(work_year)
        work_duration_abs = abs(
            resume1['experiences']['total_duration_month'] -
            resume2['experiences']['total_duration_month']) / 12
        work_duration_a = cls.work_duration_cooling(work_duration_abs)
        # experiences1 = resume1['experiences']
        # experiences2 = resume2['experiences']
        exp_score = cls.similar_exp_list(
            resume1['experiences'],
            resume2['experiences']) * 2 * work_duration_a

        # educations1 = resume1['educations']
        # educations2 = resume2['educations']
        edu_score = cls.similar_edu_list(
            resume1['educations'],
            resume2['educations']) * (school_exp_a / 100) * 1

        score = exp_score + edu_score

        return score


distance_util = SimilarDistanceUtil()


def similar_resume_resume(stand_resume_index: dict,
                          resume_index: dict) -> float:
    return distance_util.similar_resume_step2(stand_resume_index, resume_index)


def batch_similar_resume_resume(stand_resume_index: dict,
                                resume_index_list: List[dict]) -> List[float]:
    score_list = []

    for resume_index in resume_index_list:
        score_list.append(
            similar_resume_resume(stand_resume_index, resume_index))
    return score_list
