# 人人匹配需要用到的字段
cv_cv_match_field_list = [
    'aid', 'experiences.total_duration_month', 'experiences.poi_weight',
    'experiences.lda_weight', 'experiences.work_list.company_keyword',
    'experiences.work_list.title_stand', 'experiences.work_list.weight',
    'educations.edu_list.school_stand_id', 'educations.edu_list.major_stand',
    'educations.edu_list.degree_stand', 'educations.edu_list.weight'
]
