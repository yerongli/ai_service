from database.mongodb.models import FeedBackJdResume


class FeedbackManage:
    # 用户反馈，jd_index-resume_index 匹配
    @classmethod
    def feedback_jd_resume_match(cls, tenant, jd_aid, resume_aid_list: list):
        fbjr = FeedBackJdResume.objects(tenant=tenant, jd_aid=jd_aid).first()
        if fbjr:
            # 加入匹配列表
            match_aid_list = set(fbjr.match_aid_list)
            resume_aid_list = set(resume_aid_list)
            fbjr.match_aid_list = list(match_aid_list | resume_aid_list)

            # 从不匹配列表移除
            mismatch_aid_list = set(fbjr.mismatch_aid_list)
            fbjr.mismatch_aid_list = list(mismatch_aid_list - resume_aid_list)
            fbjr.save()
        else:
            fbjr = FeedBackJdResume(
                tenant=tenant, jd_aid=jd_aid, match_aid_list=resume_aid_list)
            fbjr.save()

    # 用户反馈，jd_index-resume_index 不匹配
    @classmethod
    def feedback_jd_resume_mismatch(cls, tenant, jd_aid,
                                    resume_aid_list: list):
        fbjr = FeedBackJdResume.objects(tenant=tenant, jd_aid=jd_aid).first()
        if fbjr:
            # 加入不匹配列表
            mismatch_aid_list = set(fbjr.mismatch_aid_list)
            resume_aid_list = set(resume_aid_list)
            fbjr.mismatch_aid_list = list(mismatch_aid_list | resume_aid_list)

            # 从匹配列表移除
            match_aid_list = set(fbjr.match_aid_list)
            fbjr.match_aid_list = list(match_aid_list - resume_aid_list)
            fbjr.save()
        else:
            fbjr = FeedBackJdResume(
                tenant=tenant,
                jd_aid=jd_aid,
                mismatch_aid_list=resume_aid_list)
            fbjr.save()
