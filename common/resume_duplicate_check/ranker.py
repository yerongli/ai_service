import datetime
import re
from collections import defaultdict
from typing import Dict, List, Tuple, Union

import numpy as np
from gllue_ai_libs.search_engine.utils import abs_model_path, load_model
from xgboost import XGBRegressor

from common.utils import XGBRegressorModelLoader
from database.elasticsearch.meta import VectorFieldMeta
from database.elasticsearch.models import (BasicInfoDoc, EducationDoc,
                                           EducationListDoc, ExperienceDoc,
                                           ExperienceListDoc, ResumeFeatures)

xgbr = load_model(
    loader=XGBRegressorModelLoader(
        abs_model_path('./duplicate/duplicate_xgbr.model')),
    default_factory=XGBRegressor)


def cosine_dict(d1: Union[Dict, Tuple], d2: Union[Dict, Tuple]) -> float:
    # 计算d1 d2的余弦距离， d1,d2可以为dict 或 tuple
    if not isinstance(d1, dict):
        d1 = dict(d1)
    if not isinstance(d2, dict):
        d2 = dict(d2)
    intersection_keys = d1.keys() & d2.keys()
    v = 0
    for i in intersection_keys:
        v += d1.get(i, 0) * d2.get(i, 0)
    return v


def explain_compare(target: dict, resume: dict):
    rst = compare_resume(target, resume)
    print("name: ", rst[:4])
    print("mobile: ", rst[4:7])
    print("gender: ", rst[7:10])
    print("age: ", rst[10:13])
    print("locations: ", rst[13:16])
    print("长度: ", rst[16:19])
    print("学校: ", rst[19:22])
    print("专业: ", rst[22:25])
    print("学历: ", rst[25:28])
    print("学校属性: ", rst[28:32])
    print("开始时间: ", rst[32:35])
    print("结束时间: ", rst[35:38])
    print("教育时间: ", rst[38:39])
    print("edu: ", rst[39:43])
    print("长度: ", rst[43:46])
    print("title: ", rst[46:49])
    print("company: ", rst[49:52])
    print("level: ", rst[52:53])
    print("start: ", rst[53:56])
    print("end: ", rst[56:59])
    print("duration: ", rst[59:60])
    print("poi: ", rst[60:64])
    print("lda: ", rst[64:68])
    print("edu: ", rst[68:72])
    return rst


def compare_resume(target: dict, resume: dict) -> list:
    rst = []
    rst.extend(compare_basic_info(target['basic_info'], resume['basic_info']))
    rst.extend(compare_educations(target['educations'], resume['educations']))
    rst.extend(
        compare_experiences(target['experiences'], resume['experiences']))
    return rst


def compare_basic_info(target: dict, basic_info: dict) -> list:
    rst = []
    rst.extend(
        distance_chinese_name(target.get('name'), basic_info.get('name')))  # 4
    rst.extend(
        distance_mobile(target.get('mobile'), basic_info.get('mobile')))  # 3
    rst.extend(
        distance_gender(target.get('gender'), basic_info.get('gender')))  # 3
    rst.extend(distance_age(target.get('age'), basic_info.get('age')))  # 3
    rst.extend(
        distance_locations(
            target.get('locations'), basic_info.get('locations')))  # 3
    return rst


def compare_educations(target: dict, educations: dict) -> list:
    l1, l2 = len(target.get('edu_list', [])), len(
        educations.get('edu_list', []))
    # 长度 *3
    rst = [l1, l2, abs(l1 - l2)]
    # 学校 *3
    rst.extend(
        distance_set(
            set([i.get('school_stand_id')
                 for i in target.get('edu_list', [])]),
            set([
                i.get('school_stand_id')
                for i in educations.get('edu_list', [])
            ])))
    # 专业 *3
    rst.extend(
        distance_set(
            set([i.get('major_stand') for i in target.get('edu_list', [])]),
            set([i.get('major_stand')
                 for i in educations.get('edu_list', [])])))
    # 学历 *3
    rst.extend(
        distance_list(
            [i.get('degree_stand') for i in target.get('edu_list', [])],
            [i.get('degree_stand') for i in educations.get('edu_list', [])]))
    # 学校属性 *4   # todo: 去掉
    rst.extend(
        distance_school_property([
            target.get('is_985_211'),
            target.get('is_abroad'),
            target.get('is_education_illegal'),
            target.get('is_education_interrupt')
        ], [
            educations.get('is_985_211'),
            educations.get('is_abroad'),
            educations.get('is_education_illegal'),
            educations.get('is_education_interrupt')
        ]))

    # 开始时间 *3
    try:
        rst.extend(
            distance_date(
                target.get('edu_list', [])[-1]['start'],
                educations.get('edu_list', [])[-1]['start']))
    except (IndexError, KeyError):
        rst.extend([1, 0, 0])

    # 结束时间 *3
    try:
        rst.extend(
            distance_date(
                target.get('edu_list', [])[-1]['end'],
                educations.get('edu_list', [])[-1]['end']))
    except (IndexError, KeyError):
        rst.extend([1, 0, 0])

    # 教育时间 *1
    rst.append(
        target.get('total_duration_month', 0) // 12 -
        educations.get('total_duration_month', 0) // 12)

    # edu *4
    rst.extend(
        distance_edu_list(
            target.get('edu_list', []), educations.get('edu_list', [])))
    return rst


def compare_experiences(target: dict, experiences: dict) -> list:
    l1, l2 = len(target.get('work_list', [])), len(
        experiences.get('work_list', []))

    # 长度 *3
    rst = [l1, l2, abs(l1 - l2)]

    # title *3
    rst.extend(
        distance_list(
            [i.get('title_stand') for i in target.get('work_list', [])],
            [i.get('title_stand') for i in experiences.get('work_list', [])]))
    # company *3
    rst.extend(
        distance_list(
            [i.get('company_keyword') for i in target.get('work_list', [])], [
                i.get('company_keyword')
                for i in experiences.get('work_list', [])
            ]))

    # level *3
    rst.append(
        target.get('title_level_highest') -
        experiences.get('title_level_highest'))

    # start *3
    try:
        rst.extend(
            distance_date(
                target.get('work_list', [])[-1]['start'],
                experiences.get('work_list', [])[-1]['start']))
    except (IndexError, KeyError):
        rst.extend([1, 0, 0])

    # end *3
    try:
        rst.extend(
            distance_date(
                target.get('work_list', [])[-1]['end'],
                experiences.get('work_list', [])[-1]['end']))
    except (IndexError, KeyError):
        rst.extend([1, 0, 0])

    # duration *1
    rst.append(
        target.get('total_duration_month') // 12 -
        experiences.get('total_duration_month') // 12)

    # poi *4
    rst.extend([
        len(target.get('poi_weight', [])),
        len(experiences.get('poi_weight', [])),
        len(target.get('poi_weight', [])) - len(
            experiences.get('poi_weight', [])),
        cosine_dict(
            VectorFieldMeta.index_value_to_dict(target.get('poi_weight', [])),
            VectorFieldMeta.index_value_to_dict(
                experiences.get('poi_weight', [])))
    ])

    # lda *4
    rst.extend([
        len(target.get('lda_weight', [])),
        len(experiences.get('lda_weight', [])),
        len(target.get('lda_weight', [])) - len(
            experiences.get('lda_weight', [])),
        cosine_dict(
            VectorFieldMeta.index_value_to_dict(target.get('lda_weight', [])),
            VectorFieldMeta.index_value_to_dict(
                experiences.get('lda_weight', [])))
    ])

    # edu *4
    rst.extend(
        distance_exp_list(
            target.get('work_list', []), experiences.get('work_list', [])))
    return rst


def distance_set(s1: set, s2: set) -> List[int]:
    s1 = {i for i in s1 if i}
    s2 = {i for i in s2 if i}
    return [len(s1), len(s2), len(s1 & s2)]


def distance_list(l1: list, l2: list) -> List[int]:
    l1 = [i for i in l1 if i]
    l2 = [i for i in l2 if i]
    inter = [i for i in l2 if i in l1]
    return [len(l1), len(l2), len(inter)]


def distance_chinese_name(name1, name2) -> List[int]:
    # 姓名缺失 [1, 0, 0, 0]
    # 姓名不等 [0, 1, 0, 0]
    # 姓名同名 [0, 0, 1, 0]
    # 姓名相等 [0, 0, 0, 1]
    if name1 and name2:
        if name1 == name2:
            return [0, 0, 0, 1]
        else:
            if re.findall('先生|女士', name1):
                pass
            elif re.findall('先生|女士', name2):
                name1, name2 = name2, name1
            else:
                return [0, 1, 0, 0]
            last_name1 = re.sub('先生|女士', '', name1)
            if name2.startswith(last_name1):
                return [0, 0, 1, 0]
            else:
                return [0, 1, 0, 0]
    else:
        return [1, 0, 0, 0]


def distance_mobile(mobile1, mobile2) -> List[int]:
    # 相等 [0, 0, 1]
    # 不等 [0, 1, 0]
    # 缺失 [1, 0, 0]
    if mobile1 and mobile2:
        if mobile1 == mobile2:
            return [0, 0, 1]
        else:
            return [0, 1, 0]
    else:
        return [1, 0, 0]


def distance_gender(g1, g2) -> List[int]:
    # 相等 [0, 0, 1]
    # 不等 [0, 1, 0]
    # 缺失 [1, 0, 0]
    if g1 is not None and g2 is not None:
        if g1 == g2:
            return [0, 0, 1]
        else:
            return [0, 1, 0]
    else:
        return [1, 0, 0]


def distance_age(age1: int, age2: int) -> List[int]:
    # 缺失 [1, 0, 0]
    # 存在 [0, 1, year]
    if age1 and age2:
        return [0, 1, abs(age1 - age2)]
    else:
        return [1, 0, 0]


def distance_locations(location1: list, location2: list) -> List[int]:
    # [len1, len2, inter_len]
    if not isinstance(location1, list):
        location1 = [location1]
    if not isinstance(location2, list):
        location2 = [location2]
    return distance_list(location1, location2)


def distance_school_property(sp1: list, sp2: list) -> List[int]:
    return [int(i == j) for i, j in zip(sp1, sp2)]


def distance_date(d1: datetime.datetime, d2: datetime.datetime) -> List[int]:
    if d1 and d2:
        return [0, 1, abs((d1 - d2).days / 365)]
    else:
        return [1, 0, 0]


def distance_edu_list(edu_list1: List[dict],
                      edu_list2: List[dict]) -> List[int]:
    fet = [0, 0, 0, 0]
    for edu1 in edu_list1:
        for edu2 in edu_list2:
            if edu1['school_stand_id'] == edu2['school_stand_id']:
                fet[0] += 1
                if edu1['degree_stand'] == edu2['degree_stand']:
                    fet[1] += 1
                if edu1['major_stand'] == edu2['major_stand']:
                    fet[2] += 1
                if edu1.get('start') and edu2.get('start'):
                    if edu1['start'].year == edu2['start'].year:
                        fet[3] += 1
    else:
        return fet


def distance_exp_list(exp_list1: List[dict],
                      exp_list: List[dict]) -> List[int]:
    fet = [0, 0, 0, 0]
    for exp1 in exp_list1:
        for exp2 in exp_list:
            if exp1['company_keyword'] == exp2['company_keyword']:
                fet[0] += 1
                if exp1.get('title_stand') == exp2.get('title_stand'):
                    fet[1] += 1
                if exp1['title_level'] == exp2['title_level']:
                    fet[2] += 1
                if exp1.get('start') and exp2.get('start'):
                    if exp1['start'].year == exp2['start'].year:
                        fet[3] += 1
    else:
        return fet


class SeqDistance(object):
    def __init__(self, dif_func=None):
        self.instance = defaultdict(lambda: defaultdict(list))
        self.dis_func = dif_func if dif_func else self.scard_distance

    def add(self, name, attr, value):
        self.instance[name][attr].append(value)

    @classmethod
    def scard_distance(cls, a, b):
        s = len(set(a) & set(b)) / len(set(a) | set(b))
        return s

    @classmethod
    def time_list_distance(cls, a, b):
        a, b = (a, b) if len(a) < len(b) else (b, a)
        score = 0
        stand_score = len(a)
        for i in a:
            max_score = 0
            for j in b:
                s = cls._time_distance(i, j)
                if s > max_score:
                    max_score = s
            else:
                score += max_score
        else:
            return score / stand_score

    @classmethod
    def _time_distance(cls, a, b):
        if isinstance(a, int) and isinstance(b, int):
            x = 1 - abs(a - b) * 0.2
            if x < 0.5:
                return 0
            else:
                return min(x, 1)
        else:
            return 0

    def distance(self, sd):
        assert isinstance(sd, type(self))
        stand_instance, instance = (self.instance, sd.instance) \
            if len(self.instance) < len(sd.instance) else (sd.instance, self.instance)
        stand_score = len(stand_instance)
        if stand_score == 0:
            return 0
        score = 0
        for name, av in stand_instance.items():
            stand_av = instance[name]
            stand_av, av = (stand_av, av) \
                if len(stand_av) < len(av) else (av, stand_av)
            if stand_av:
                score2 = 0
                stand_score2 = len(av)
                for attr, value in av.items():
                    stand_value = stand_av[attr]
                    if stand_value:
                        c = self.dis_func(stand_value, value)
                        score2 += c
                    else:
                        score2 += 0.5
                else:
                    score += (score2 / stand_score2)
            else:
                score += 0
        else:
            return score / stand_score


class SeqDistanceResumeSorter:
    # 姓氏相同分数
    name_half_sim_score = 0.25
    # 姓名全相同分数
    name_all_sim_score = 1
    # 姓名权重
    name_weight = 20

    # 工作经验残差相同分数
    exp_half_sim_score = 0.5
    # 工作经验全相同分数
    exp_all_sim_score = 1
    # 工作经验权重
    exp_weight = 60

    # 教育经验残差相同分数
    edu_half_sim_score = 0.5
    # 教育经验全相同分数
    edu_all_sim_score = 1
    # 教育经验权重
    edu_weight = 60

    def sort(self, resume_to_match, resumes, k=5, threshold=0.3):
        resume_score_pairs = [(resume,
                               self.distance_between_resume(
                                   resume_to_match, resume))
                              for resume in resumes]
        resume_score_pairs = [
            pair for pair in resume_score_pairs if pair[1] > threshold
        ]
        resume_score_pairs.sort(key=lambda pair: pair[1], reverse=True)
        return resume_score_pairs[:k]

    def distance_between_resume(self, r1: dict, r2: dict) -> float:
        """
        带关联的简历距离函数
        :param r1: 简历1
        :param r2: 简历2
        :return:
        """
        score = 0

        score += self.distance_chinese_name(r1.get('name'),
                                            r2.get('name')) * self.name_weight
        score += self.distance_education(
            r1.get('edu_list'), r2.get('edu_list')) * self.edu_weight
        score += self.distance_experiences(
            r1.get('work_list'), r2.get('work_list')) * self.exp_weight

        if score >= 100:
            score = 99
        return score / 100

    @classmethod
    def distance_chinese_name(cls, name1, name2) -> float:
        if name1 and name2:
            if name1 == name2:
                return cls.name_all_sim_score
            else:
                if re.findall('先生|女士', name1):
                    pass
                elif re.findall('先生|女士', name2):
                    name1, name2 = name2, name1
                else:
                    return 0
                last_name1 = re.sub('先生|女士', '', name1)
                if name2.startswith(last_name1):
                    return cls.name_half_sim_score
                else:
                    return 0
        else:
            return 0

    @staticmethod
    def distance_mobile(mobile1, mobile2):
        if mobile1 and mobile2:
            if mobile1 == mobile2:
                return 1
            else:
                return 0
        else:
            return 0

    @staticmethod
    def distance_gender(g1, g2):
        if g1 is not None and g2 is not None:
            if g1 == g2:
                return 1
            else:
                return 0
        else:
            return 0.5

    @classmethod
    def distance_experiences(cls, exp_list1, exp_list2):
        if not exp_list1 or not exp_list2:
            return 0
        l1 = len(exp_list1)
        l2 = len(exp_list2)
        max_len = max(l1, l2)
        if l1 > l2:
            exp_list1, exp_list2 = exp_list2, exp_list1
        single_weight = 1 / max_len

        score = 0

        half_score = cls.exp_half_sim_score * single_weight
        all_score = cls.exp_all_sim_score * single_weight

        for exp in exp_list1:
            score_cache = []
            for exp2 in exp_list2:
                _score = 0
                c1 = exp.get("company_keyword")
                c2 = exp2.get("company_keyword")
                if c1 and c2 and c1 == c2:
                    if exp.get('title') == exp2.get('title') and exp.get(
                            'start') == exp2.get('start'):
                        score += all_score
                    else:
                        score += half_score
                score_cache.append(_score)
            else:
                score += max(score_cache, default=0)
        else:
            return score

    @classmethod
    def distance_education(cls, edu_list1, edu_list2):
        if not edu_list1 or not edu_list2:
            return 0
        l1 = len(edu_list1)
        l2 = len(edu_list2)
        max_len = max(l1, l2)
        if l1 > l2:
            edu_list1, edu_list2 = edu_list2, edu_list1
        single_weight = 1 / max_len

        score = 0

        half_score = cls.edu_half_sim_score * single_weight
        all_score = cls.edu_all_sim_score * single_weight

        for edu in edu_list1:
            score_cache = []
            for edu2 in edu_list2:
                _score = 0
                if edu.get('degree_stand') == '高中' or edu2.get(
                        'degree_stand') == '高中':
                    continue
                if edu.get('school_stand_id') == edu2.get('school_stand_id'):
                    if edu.get('major_stand') == edu2.get(
                            'major_stand') and edu.get('start') == edu2.get(
                                'start'):
                        _score += all_score
                    else:
                        _score += half_score
                score_cache.append(_score)
            else:
                # 取分数最高的一段
                score += max(score_cache, default=0)
        else:
            return score


duplicate_resume_util = SeqDistanceResumeSorter()


def duplicate_distance_resume_resume(stand_resume_index: dict,
                                     resume_index: dict) -> float:
    return duplicate_resume_util.distance_between_resume(
        stand_resume_index, resume_index)


# def batch_duplicate_distance_resume_resume(
#     stand_resume_index: dict,
#     resume_index_list: List[dict]) -> List[float]:
#     score_list = []
#     for resume_index in resume_index_list:
#         score_list.append(
#             duplicate_distance_resume_resume(stand_resume_index, resume_index))
#     return score_list
# xgbc = XGBClassifier()
# from xgboost.compat import XGBLabelEncoder
#
# xgbc.load_model('./duplicate_xbgc.model')
# xgbc._le = XGBLabelEncoder().fit(np.array([0, 1]))


def batch_duplicate_distance_resume_resume(
        stand_resume_index: dict,
        resume_index_list: List[dict]) -> List[float]:
    features = list()
    for resume_index in resume_index_list:
        features.append(compare_resume(stand_resume_index, resume_index))
    data = np.array(features)
    rst = xgbr.predict(data)
    rst = [float(i) for i in rst]
    return rst
