# 简历查重需要用到的字段
# duplicate_resume_field_list = [
#     "aid",
#     "basic_info.name",
#     "educations.edu_list.degree_stand",
#     "educations.edu_list.school_stand_id",
#     "educations.edu_list.major_stand",
#     "educations.edu_list.start",
#     "experiences.work_list.company_keyword",
#     "experiences.work_list.title_stand",
#     "experiences.work_list.start",
# ]

duplicate_resume_field_list = [
    "aid", "basic_info.name", "basic_info.mobile", "basic_info.email",
    "basic_info.gender", "basic_info.age", "basic_info.locations",
    "educations.is_985_211", "educations.is_abroad",
    "educations.is_education_illegal", "educations.is_education_interrupt",
    "educations.edu_list.start", "educations.edu_list.end",
    "educations.edu_list.total_duration_month",
    "educations.edu_list.school_stand_id", "educations.edu_list.major_stand",
    "educations.edu_list.degree_stand", "experiences.work_list.title_stand",
    "experiences.work_list.company_keyword", "experiences.work_list.start",
    "experiences.work_list.end", "experiences.work_list",
    "experiences.lda_weight", "experiences.total_duration_month",
    "experiences.title_level_highest"
]
