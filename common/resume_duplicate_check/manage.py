from typing import List

from common.recall import resume_recall as custom_recall
from common.resume_duplicate_check import ranker, recall
from common.utils import (BaseGeneratorUuid, link_tracking_time,
                          remove_set_element)
from database.elasticsearch.models import ResumeFeatures
from preprocess.resume_index import index_single_resume

from .source_fields import duplicate_resume_field_list


class DuplicateResume(BaseGeneratorUuid):
    def __init__(self, tenant):
        self.tenant = tenant
        super().__init__()

    # 简历精准查重
    @link_tracking_time
    def precision_duplicate_checking(self, resume_info) -> dict:
        return self._precision_duplicate_checking(resume_info)

    # 简历查重 从已存在resume
    @link_tracking_time
    def resume_duplicate_checking_from_resume_id(
            self, resume_info, topn: int = 5, min_score: float = 0.5) -> dict:
        resume_index: ResumeFeatures = ResumeFeatures.get_with_tenant_and_aid(
            resume_info['tenant'],
            resume_info['aid'],
            source=duplicate_resume_field_list)
        if resume_index:
            return self._resume_duplicate_checking(resume_index.to_dict(),
                                                   topn, min_score)
        else:
            result = {"link_uuid": self.link_uuid, "items": []}
            return result

    # 简历查重 从新resume
    @link_tracking_time
    def resume_duplicate_checking_from_resume(self,
                                              resume_info: dict,
                                              topn: int = 5,
                                              min_score: float = 0.5) -> dict:
        resume_index = self.__index_single_resume(resume_info)
        resume_index = ResumeFeatures.from_json(resume_index)
        return self._resume_duplicate_checking(resume_index, topn, min_score)

    @link_tracking_time
    def recall_for_precision_duplicate_checking(self,
                                                resume_info: dict) -> set:
        mobile = resume_info['mobile']
        email = resume_info['email']
        linkedin = resume_info['linkedin']
        aid_set = recall.use_precision_info(
            tenant=self.tenant, mobile=mobile, email=email, linkedin=linkedin)
        return aid_set

    @link_tracking_time
    def recall_for_resume_duplicate_checking(self, resume_index: dict) -> set:

        company_set = set([
            i.get('company_keyword')
            for i in resume_index['experiences'].get('work_list', [])
        ])
        title_set = set([
            i.get('title_stand')
            for i in resume_index['experiences'].get('work_list', [])
        ])
        company_title_aid_set = recall.use_target_company_title(
            company_set, title_set, tenant=self.tenant)

        school_set = set([
            i.get('school_stand_id')
            for i in resume_index['educations'].get('edu_list', [])
        ])
        major_set = set([
            i.get('major_stand')
            for i in resume_index['educations'].get('edu_list', [])
        ])
        school_major_aid_set = recall.use_target_school_major(
            school_set, major_set, tenant=self.tenant)

        aid_set = company_title_aid_set | school_major_aid_set
        return aid_set

    @link_tracking_time
    def get_data_for_resume_duplicate_checking(self, aid_list: list):
        query = ResumeFeatures.search().filter(
            "term", tenant=self.tenant).filter(
                "terms", aid=aid_list).source(duplicate_resume_field_list)
        # query = elastic_resume_feature_searcher.filter(
        #     "term", tenant=self.tenant).filter(
        #     "terms",
        #     aid=aid_list)  # .source(source_fields.duplicate_resume_field_list)
        # data = [
        #     schema_transformer.resume_index_to_resume_duplicate_dict(
        #         i.to_dict()) for i in query.scan()
        # ]
        data = [i.to_dict() for i in query.scan()]
        return data

    @link_tracking_time
    def batch_distance_duplicate_resume(self, resume_index: dict,
                                        data: List[dict]) -> List[float]:
        return ranker.batch_duplicate_distance_resume_resume(
            resume_index, data)

    # 简历精准查重
    @link_tracking_time
    def _precision_duplicate_checking(self, resume_info: dict) -> dict:
        aid_set = self.recall_for_precision_duplicate_checking(
            resume_info=resume_info)
        items = []
        for aid in aid_set:
            items.append({"aid": aid, "tenant": self.tenant})
        result = {"link_uuid": self.link_uuid, "items": items}
        return result

    # 简历查重
    @link_tracking_time
    def _resume_duplicate_checking(self,
                                   resume_index: dict,
                                   topn: int = 5,
                                   min_score: float = 0.6) -> dict:

        aid_set = self.recall_for_resume_duplicate_checking(resume_index)

        custom_recall.record_recall(self.link_uuid, self.tenant, "RDC",
                                    aid_set)

        aid_set = list(remove_set_element(aid_set, resume_index['aid']))

        data = self.get_data_for_resume_duplicate_checking(aid_list=aid_set)
        if not data:
            return {"link_uuid": self.link_uuid, "items": []}
        # resume_index = schema_transformer.resume_index_to_resume_duplicate_dict(
        #     resume_index.to_dict())

        score_list = self.batch_distance_duplicate_resume(resume_index, data)

        items = list()
        for candidate_resume, score in zip(data, score_list):
            if score >= min_score:
                items.append({
                    'aid': candidate_resume['aid'],
                    'score': score,
                    "tenant": self.tenant
                })
        else:
            items.sort(key=lambda x: x['score'], reverse=True)
        result = {"link_uuid": self.link_uuid, "items": items[:topn]}
        return result

    @link_tracking_time
    def __index_single_resume(self, resume_info):
        return index_single_resume(resume_info)


# 简历查重 从已存在resume
def resume_duplicate_checking_from_resume_id(resume_info: dict,
                                             topn: int = 5,
                                             min_score: float = 0.5) -> dict:
    """
    :param resume_info:
        {
            "tenant": str,
            "aid": int
        }
    :param topn:
    :param min_score:
    :return:
        {
            "link_uuid": str,
            "items": [
                {
                    "aid": int,
                    "tenant": str,
                    "score": float
                }
            ]
        }
    """
    return DuplicateResume(
        tenant=resume_info['tenant']).resume_duplicate_checking_from_resume_id(
            resume_info, topn, min_score)


# 简历查重 从新resume
def resume_duplicate_checking_from_resume(resume_info: dict,
                                          topn: int = 5,
                                          min_score: float = 0.5) -> dict:
    """
    :param resume_info:
        - refer @grpc_models.ai_resume_recommend_model.ResumeInfoMessage
    :param topn:
    :param min_score:
    :return:
        {
            "link_uuid": str,
            "items": [
                {
                    "aid": int,
                    "tenant": str,
                    "score": float
                }
            ]
        }
    """

    return DuplicateResume(
        tenant=resume_info['tenant']).resume_duplicate_checking_from_resume(
            resume_info, topn, min_score)


def precision_duplicate_checking(resume_info):
    """
    精准查重
    refer@ https://confluence.gllue.com/pages/viewpage.action?pageId=23761314
    :param resume_info:
        {
            "tenant": str,
            "mobile": str,
            "email": str,
            "linkedin": str
        }
    :return:
        {
            "link_uuid": str,
            "items": [
                {
                    "aid": int,
                    "tenant": str
                }
            ]
        }
    """
    return DuplicateResume(
        tenant=resume_info['tenant']).precision_duplicate_checking(resume_info)
