# 将 数据库 中的简历index信息转为 resume_duplicate 计算用的格式
def resume_index_to_resume_duplicate_dict(d: dict) -> dict:
    resume_info = dict()
    resume_info['aid'] = d['aid']
    resume_info['name'] = d.get('basic_info', {}).get('name')
    resume_info['edu_list'] = d.get('educations', {}).get('edu_list')
    resume_info['work_list'] = d.get('experiences', {}).get('work_list')
    return resume_info
