__all__ = ["use_target_company_title", "use_target_school_major"]

import itertools
from typing import Set

from elasticsearch_dsl.query import Q

from database.elasticsearch.models import ResumeFeatures


def gen_company_title_nested_pairs(company: str, title: str) -> Q:
    query = Q(
        "bool",
        must=[
            Q("match_phrase",
              **{"experiences.work_list.company_keyword": company}),
            Q("term", **{"experiences.work_list.title_stand": title})
        ])
    return query


def gen_school_major_nested_pairs(school_id: str, major: str) -> Q:
    query = Q(
        "bool",
        must=[
            Q("term", **{"educations.edu_list.school_stand_id": school_id}),
            Q("term", **{"educations.edu_list.major_stand": major})
        ])
    return query


# 根据 目标公司和职位 召回相关简历
def use_target_company_title(company_list: Set[str],
                             title_list: Set[str],
                             tenant: str,
                             topn: int = 200,
                             min_score: float = 1.) -> Set[int]:
    company_list = {i for i in company_list if i}
    title_list = {i for i in title_list if i}

    if not (company_list and title_list):
        return set()
    # 生成查询
    company_title_pairs = itertools.product(company_list, title_list)
    should_query_list = []
    for company, title in company_title_pairs:
        should_query_list.append(
            gen_company_title_nested_pairs(company, title))

    query = ResumeFeatures.search().filter(
        "term", tenant=tenant).filter(
            'term', is_deleted=False).query(
                "nested",
                path="experiences.work_list",
                query=Q("bool",
                        should=should_query_list)).source(["aid"])[0:topn]

    # 获取数据
    data = query.execute()
    aid_set = set()
    for r in data:
        if r.meta.score >= min_score:
            aid_set.add(r.aid)

    return aid_set


# 根据 目标公司和职位 召回相关简历
def use_target_school_major(school_list: Set[str],
                            major_list: Set[str],
                            tenant: str,
                            topn: int = 200,
                            min_score: float = 1.) -> Set[int]:
    school_list = {i for i in school_list if i}
    major_list = {i for i in major_list if i}

    if not (school_list and major_list):
        return set()

    # 生成查询
    school_title_pairs = itertools.product(school_list, major_list)

    should_query_list = []
    for school, major in school_title_pairs:
        should_query_list.append(gen_school_major_nested_pairs(school, major))

    query = ResumeFeatures.search().filter(
        "term", tenant=tenant).filter(
            'term', is_deleted=False).query(
                "nested",
                path="educations.edu_list",
                query=Q("bool",
                        should=should_query_list)).source(['aid'])[0:topn]

    # 获取数据
    data = query.execute()
    aid_set = set()
    for r in data:
        if r.meta.score >= min_score:
            aid_set.add(r.aid)

    return aid_set


def use_precision_info(tenant, mobile=None, email=None,
                       linkedin=None) -> Set[int]:
    # 使用 mobile email linkedin 等字段召回。预判断字段是否存在，不做合规判断。可能会有脏数据的问题。
    should_query_list = []
    if mobile:
        should_query_list.append(Q("term", **{'basic_info.mobile': mobile}))
    if email:
        should_query_list.append(Q("term", **{'basic_info.email': email}))
    if linkedin:
        should_query_list.append(
            Q("term", **{'basic_info.linkedin': linkedin}))

    query = ResumeFeatures.search().filter(
        "term", tenant=tenant).filter(
            "term", is_deleted=False).filter(
                "bool", should=should_query_list).source(['aid'])

    data = query.execute()
    aid_set = set([i.aid for i in data])

    return aid_set
