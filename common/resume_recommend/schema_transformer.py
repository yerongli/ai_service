import math
from collections import defaultdict

from common.jd_index import JobDescription
from common.jd_index.jd_util import JobOrderMatchUtil
from database.elasticsearch.meta import VectorFieldMeta
from database.elasticsearch.models import JobOrderFeatures


# 将 数据库 中的简历index信息转为 jd_cv_match 计算用的格式
def resume_index_to_jd_cv_match_dict(d: dict) -> dict:
    resume_info = {}

    educations = d.get('educations', {})
    experiences = d.get('experiences', {})

    resume_info['aid'] = d.get('aid')
    resume_info['custom_tags'] = d.get('custom_tags')

    resume_info['major_list'] = VectorFieldMeta.extract_index(
        educations.get('major_weight'))
    resume_info['major_weight'] = educations.get('major_weight', [])
    resume_info['last_degree'] = educations.get('last_degree')
    resume_info['degree_list'] = VectorFieldMeta.extract_index(
        educations.get('degree_weight'))
    resume_info['school_list'] = VectorFieldMeta.extract_index(
        educations.get('school_weight'))
    resume_info['school_weight'] = educations.get('school_weight', [])

    resume_info['is_985_211'] = educations.get('is_985') or educations.get(
        'is_211')
    resume_info['is_education_interrupt'] = educations.get(
        'is_education_interrupt')
    resume_info['is_abroad'] = educations.get('is_abroad')

    resume_info['description_poi_weight'] = experiences.get(
        'description_poi_weight', [])

    resume_info['total_work_duration_year'] = math.ceil(
        experiences.get('total_work_duration_month', 0) / 12)
    resume_info['title_weight'] = VectorFieldMeta.index_value_to_dict(
        experiences.get('title_weight', []))

    resume_info['title_level_highest'] = experiences.get('title_level_highest')

    resume_info['company_list'] = VectorFieldMeta.extract_index(
        educations.get('company_weight'))
    resume_info['description_lda_weight'] = experiences.get(
        'description_lda_weight', [])
    resume_info['current_salary'] = 0
    return resume_info


# 将 数据库 中的简历index信息转为 cv_cv_match 计算用的格式
def resume_index_to_cv_cv_match_dict(d: dict) -> dict:
    resume_info = dict()

    educations = d.get('educations', {})
    experiences = d.get('experiences', {})

    tdc = defaultdict(float)
    for i in experiences.get('work_list', []):
        tdc[i['title_stand']] += i['weight']
    experiences['title_weight'] = tdc
    experiences['poi_weight'] = VectorFieldMeta.index_value_to_dict(
        experiences.get('poi_weight', []))
    experiences['lda_weight'] = VectorFieldMeta.index_value_to_dict(
        experiences.get('lda_weight', []))

    sdc = defaultdict(float)
    mdc = defaultdict(float)
    ddc = defaultdict(float)
    for i in educations.get('edu_list', []):
        sdc[i['school_stand_id']] += i['weight']
        mdc[i['major_stand']] += i['weight']
        ddc[i['degree_stand']] += i['weight']

    educations['school_weight'] = sdc
    educations['major_weight'] = mdc
    educations['degree_weight'] = ddc

    resume_info['aid'] = d['aid']
    resume_info['experiences'] = experiences
    resume_info['educations'] = educations
    # resume_info['total_work_duration_year'] = math.ceil(
    #     experiences.get('total_work_duration_month', 0) / 12)
    #
    # resume_info['title_weight'] = VectorFieldMeta.index_value_to_dict(
    #     experiences.get('title_weight', []))
    # resume_info[
    #     'description_poi_weight'] = VectorFieldMeta.index_value_to_dict(
    #         experiences.get('description_poi_weight', []))
    # resume_info[
    #     'description_lda_weight'] = VectorFieldMeta.index_value_to_dict(
    #         experiences.get('description_lda_weight', []))
    #
    # resume_info['school_weight'] = VectorFieldMeta.index_value_to_dict(
    #     educations.get('school_weight', []))
    # resume_info['major_weight'] = VectorFieldMeta.index_value_to_dict(
    #     educations.get('major_weight', []))
    # resume_info['degree_weight'] = VectorFieldMeta.index_value_to_dict(
    #     educations.get('degree_weight', []))

    return resume_info


# 检查简历信息，并加入新的查询信息，返回 JD 对象
def check_and_fill_jd_info(jdf: JobOrderFeatures,
                           query_info: dict) -> JobOrderMatchUtil:
    """
    检查简历信息，并加入新的查询信息，返回 JD 对象
    :param jdf: 原始 jd_index
    :param query_info: 拓展查询信息
    :return:
    """
    # 检测 jd是否存在
    # if not exist_jd:
    #     raise Exception('不存在的jd [{aid}]'.format(aid=query_jd['aid']))
    query_info = _repair_jd_info(query_info)
    target_company_list = JobDescription.fill_target_company_list(
        query_info.get('target_company_list', []))
    target_school_list = JobDescription.fill_target_school_list(
        query_info.get('target_school_list', []))
    custom_tags = query_info.get('custom_tags', [])
    need_985_211 = query_info.get('need_985_211', False)
    jd = JobOrderMatchUtil(
        jdf,
        need_companies=target_company_list,
        need_school=target_school_list,
        custom_tags=custom_tags,
        need_985_211=need_985_211)
    return jd


# 外部传入的jd_info的小处理
def _repair_jd_info(jd_info: dict):
    custom_prefer_list = jd_info.get('custom_prefer_list', [])
    target_company_list = []
    target_school_list = []
    custom_tags = []
    need_985_211 = 0

    for tag in custom_prefer_list:
        tt = tag['type']
        tc = tag['field']
        if tt == "target_company":
            target_company_list.append(tc)
        elif tt == "target_school":
            target_school_list.append(tc)
        elif tt == "custom_tags":
            custom_tags.append(tc)
        elif tt == "985/211":
            if tc:
                need_985_211 = True
    jd_info['target_company_list'] = target_company_list
    jd_info['target_school_list'] = target_school_list
    jd_info['custom_tags'] = custom_tags
    jd_info['need_985_211'] = need_985_211
    return jd_info
