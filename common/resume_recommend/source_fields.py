# 人岗匹配需要用到的字段
jd_cv_match_field_list = [
    'aid', 'basic_info.age', 'basic_info.current_salary',
    'basic_info.locations', 'educations.edu_list.degree_stand',
    'educations.edu_list.major_stand', 'educations.is_985_211',
    'educations.first_is_985_211', 'educations.is_education_interrupt',
    'educations.is_education_illegal', 'experiences.work_list.title_stand',
    'experiences.total_duration_month', 'experiences.poi_weight',
    'experiences.lda_weight', 'experiences.title_level_highest'
]
