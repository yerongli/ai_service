from collections import defaultdict
from typing import Dict

from database.mongodb import jobsubmission_find
from database.mongodb.models import FeedBackJdResume


# 获取jd 不匹配 resume 列表
def get_jd_resume_mismatch_aid_set(tenant: str,
                                   jd_aids: list) -> Dict[int, set]:
    # 获取
    fbjrs = FeedBackJdResume.objects(tenant=tenant, jd_aid__in=jd_aids)
    result = {fbjr.jd_aid: set(fbjr.mismatch_aid_list) for fbjr in fbjrs}

    return result


# 获取已经被挂在 jd下的resume集合
def get_jd_resume_include_aid_set(tenant: str,
                                  jd_aids: list) -> Dict[int, set]:
    data = jobsubmission_find(
        query={
            'client_key': tenant,
            'joborder': {
                "$in": jd_aids
            }
        },
        projection={
            'candidate': 1,
            'joborder': 1
        })
    result = defaultdict(set)
    for i in data:
        if i['candidate']:
            result[i['joborder']].add(i['candidate'])

    return result


def get_jd_dont_see_resume_aid_set(tenant: str, jd_aid: int) -> set:
    # 根据提供的jd，返回会被过滤掉的resume

    aid_set1 = get_jd_resume_mismatch_aid_set(tenant, [jd_aid])
    aid_set2 = get_jd_resume_include_aid_set(tenant, [jd_aid])
    result = aid_set1.get(jd_aid, set()) | aid_set2.get(jd_aid, set())
    return result


def get_jds_dont_see_resume_aid_set(tenant: str,
                                    jd_aids: list) -> Dict[int, set]:
    # 传入jd list,返回dict。key: jd_index, value: resume_index
    aid_set1 = get_jd_resume_mismatch_aid_set(tenant, jd_aids)
    aid_set2 = get_jd_resume_include_aid_set(tenant, jd_aids)
    result = dict()
    for jd_aid in set(aid_set1.keys() | aid_set2.keys()):
        result[jd_aid] = aid_set1.get(jd_aid, set()) | aid_set2.get(
            jd_aid, set())
    return result
