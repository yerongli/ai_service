import functools
import math
from typing import List

from typing import Dict, Tuple, Union

from gllue_ai_libs.angelo_tool.analyze.algorithm import (
    get_newton_law_of_cooling_a, newton_law_of_cooling)

from common.jd_index.jd_util import JobOrderMatchUtil
from database.elasticsearch.meta import VectorFieldMeta


def cosine_dict(d1: Union[Dict, Tuple], d2: Union[Dict, Tuple]) -> float:
    # 计算d1 d2的余弦距离， d1,d2可以为dict 或 tuple
    if not isinstance(d1, dict):
        d1 = dict(d1)
    if not isinstance(d2, dict):
        d2 = dict(d2)
    intersection_keys = d1.keys() & d2.keys()
    v = 0
    for i in intersection_keys:
        v += d1.get(i, 0) * d2.get(i, 0)
    return v


class SimilarDistanceUtil:
    TITLE_WEIGHT = 1.2
    LEVEL_WEIGHT = 1.5
    MAJOR_WEIGHT = 1.1
    LDA_WEIGHT = 1.3
    POI_WEIGHT = 1.2
    COMPANY_WEIGHT = 1.5
    WORK_YEAR_WEIGHT = 1.2
    _985211_WEIGHT = 1.2

    TARGET_COMPANY_WEIGHT = 0.1
    TARGET_SCHOOL_WEIGHT = 0.05
    CUSTOM_TAGS_WEIGHT = 0.1
    WEIGHT_SUM = functools.reduce(lambda x, y: x * y, [
        TITLE_WEIGHT, LEVEL_WEIGHT, MAJOR_WEIGHT, LDA_WEIGHT, POI_WEIGHT,
        COMPANY_WEIGHT, WORK_YEAR_WEIGHT, _985211_WEIGHT
    ]) * 0.5

    # 工作经验的系数
    work_exp_weight_cooling_a = get_newton_law_of_cooling_a(100, 1, 0, 10)
    work_exp_weight_cooling = functools.partial(newton_law_of_cooling, 100, 0,
                                                work_exp_weight_cooling_a)

    # 教育经验自身系数
    school_weight_cooling_a = get_newton_law_of_cooling_a(100, 1, 0, 10)
    school_weight_cooling = functools.partial(newton_law_of_cooling, 100, 0,
                                              school_weight_cooling_a)

    # 教育经验较工作经验的系数
    school_exp_weight_cooling_a = get_newton_law_of_cooling_a(100, 1, 0, 50)
    school_exp_weight_cooling = functools.partial(
        newton_law_of_cooling, 100, 0, school_exp_weight_cooling_a)

    # 工作经验时间差系数
    work_duration_cooling_a = get_newton_law_of_cooling_a(1, 0.5, 0,
                                                          5)  # 1-0.5,差5年则为0.5
    work_duration_cooling = functools.partial(newton_law_of_cooling, 1, 0.5,
                                              work_duration_cooling_a)

    @classmethod
    def distance_jd_resume(cls,
                           jd_match_util: JobOrderMatchUtil,
                           resume_info: dict,
                           explain=False) -> float:
        educations = resume_info['educations']
        experiences = resume_info['experiences']

        edu_list = educations.get('edu_list', [])
        work_list = educations.get('work_list', [])

        # JD和resume的匹配分数
        base_score = 1
        work_year_score = 1
        level_score = 1
        title_score = 1
        major_score = 1
        _985211_score = 1
        company_score = 1
        poi_score = 1
        desc_lda_score = 1

        custom_company_score = 0
        custom_school_score = 0
        custom_tags_score = 0

        # 学历 过滤
        if not jd_match_util.degree_list_match(
            [i['degree_stand'] for i in edu_list]):
            return 0

        # 工作年限 过滤
        if not jd_match_util.working_year_match(
                experiences['total_duration_month'] / 12):
            return 0

        # 工作年限 加分
        # todo: 年限加分曲线
        if experiences[
                'total_duration_month'] / 12 - jd_match_util.jd.working_years_range.gte <= 2:
            work_year_score = cls.WORK_YEAR_WEIGHT

        # 职位，职级 加分
        if jd_match_util.title_list_match(
            [i['title_stand'] for i in work_list]):
            title_score = cls.TITLE_WEIGHT
        level_score += (
            cls.LEVEL_WEIGHT - 1) * jd_match_util.title_level_match(
                experiences['title_level_highest'])

        # 专业加分
        if jd_match_util.major_list_match(
            [i['major_stand'] for i in edu_list]):
            major_score = cls.MAJOR_WEIGHT

        # 985/211加分
        if jd_match_util.need_985_211 and educations['is_985_211']:
            _985211_score = cls._985211_WEIGHT

        # 推荐公司 加分
        if jd_match_util.company_list_match(
            [i['company_keyword'] for i in work_list]):
            company_score = cls.COMPANY_WEIGHT

        # poi 匹配 加分
        poi_score = cosine_dict(
            jd_match_util.poi_weight_dict,
            VectorFieldMeta.index_value_to_dict(experiences.get('poi_weight')))
        poi_score = min(poi_score * 1.5, 1) * cls.POI_WEIGHT

        # 工作描述 加分
        #: 加上最低分避免描述的主题错误导致分数为0
        desc_lda_score = max(
            0.1,
            cosine_dict(
                jd_match_util.lda_weight_dict,
                VectorFieldMeta.index_value_to_dict(
                    experiences.get('lda_weight'))))
        desc_lda_score = min(desc_lda_score * 1.5, 1) * cls.LDA_WEIGHT

        # 自定义标签部分
        # 公司
        if jd_match_util.company_list_match(
            [i['company_keyword'] for i in work_list]):
            custom_company_score = cls.TARGET_COMPANY_WEIGHT

        # 学校
        if jd_match_util.school_list_match(
            [i['school_stand_id'] for i in edu_list]):
            custom_school_score = cls.TARGET_SCHOOL_WEIGHT

        # 标签
        if jd_match_util.need_custom_tags:
            if resume_info['custom_tags']:
                x = len(
                    set(jd_match_util.need_custom_tags) & set(
                        resume_info['custom_tags']))
            else:
                x = 0
            s = math.log(1 + (9 / len(jd_match_util.need_custom_tags)) * x)
            custom_tags_score = cls.CUSTOM_TAGS_WEIGHT * s

        mul_score = functools.reduce(lambda x, y: x * y, [
            base_score, work_year_score, level_score, title_score, major_score,
            _985211_score, company_score, poi_score, desc_lda_score
        ])
        score = sum([
            mul_score / cls.WEIGHT_SUM, custom_company_score,
            custom_school_score, custom_tags_score
        ])

        if explain:
            explain_info = {
                "base_score": base_score,
                "work_year_score": work_year_score,
                "level_score": level_score,
                "title_score": title_score,
                "major_score": major_score,
                "_985211_score": _985211_score,
                "company_score": company_score,
                "poi_score": poi_score,
                "desc_lda_score": desc_lda_score,
                "custom_company_score": custom_company_score,
                "custom_school_score": custom_school_score,
                "custom_tags_score": custom_tags_score,
                'mul_score': mul_score,
                "WEIGHT_SUM": cls.WEIGHT_SUM
            }
            print(explain_info)
        score = min(score, 0.95)
        return score

    @classmethod
    def similar_exp_list(cls, exp_list1, exp_list2):
        '''
        计算两段工作经历的相似分数，cosine_dict，越大越相似
        '''

        title_score = cosine_dict(exp_list1['title_weight'],
                                  exp_list1['title_weight']) * 3

        desc_poi_score = cosine_dict(exp_list1['poi_weight'],
                                     exp_list2['poi_weight']) * 3

        desc_lda_score = cosine_dict(exp_list1['lda_weight'],
                                     exp_list2['lda_weight']) * 3

        score = sum([title_score, desc_poi_score, desc_lda_score])
        score = score / 9
        return score

    @classmethod
    def similar_edu_list(cls, edu_list1, edu_list2):
        """
        计算两段教育经历的相似分数, cosine_dict，越大越相似
        """
        score = 0

        # school_weight_1 = edu_list1['school_weight']
        # school_weight_2 = edu_list2['school_weight']
        # score += rmse_dict(school_weight_1, school_weight_2) * 1
        score += cosine_dict(edu_list1['school_weight'],
                             edu_list2['school_weight']) * 2

        score += cosine_dict(edu_list1['major_weight'],
                             edu_list2['major_weight']) * 2

        score += cosine_dict(edu_list1['degree_weight'],
                             edu_list2['degree_weight']) * 1

        score = score / 5
        return score

    @classmethod
    def similar_resume_step2(cls, resume1, resume2):
        # todo: 待补全
        """
        resume_index 的相似比较, 第二次召回使用
        Parameters
        ----------
        resume1 搜索resume
        resume2 候选resume

        Returns
        -------

        """
        work_year = math.ceil(
            resume1['experiences']['total_duration_month'] / 12)
        school_exp_a = cls.school_exp_weight_cooling(work_year)
        work_duration_abs = abs(
            resume1['experiences']['total_duration_month'] -
            resume2['experiences']['total_duration_month']) / 12
        work_duration_a = cls.work_duration_cooling(work_duration_abs)
        # experiences1 = resume1['experiences']
        # experiences2 = resume2['experiences']
        exp_score = cls.similar_exp_list(
            resume1['experiences'],
            resume2['experiences']) * 2 * work_duration_a

        # educations1 = resume1['educations']
        # educations2 = resume2['educations']
        edu_score = cls.similar_edu_list(
            resume1['educations'],
            resume2['educations']) * (school_exp_a / 100) * 1

        score = exp_score + edu_score

        return score


distance_util = SimilarDistanceUtil()


def distance_jd_resume(jd_match_util: JobOrderMatchUtil,
                       resume_index: dict) -> float:
    return distance_util.distance_jd_resume(jd_match_util, resume_index)


def batch_distance_jd_resume(jd_match_util: JobOrderMatchUtil,
                             resume_index_list: List) -> List[float]:
    score_list = list()
    for resume_index in resume_index_list:
        score_list.append(distance_jd_resume(jd_match_util, resume_index))
    return score_list
