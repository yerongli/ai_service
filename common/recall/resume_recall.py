"""
简历召回类的功能函数, 所有函数返回 对应id
"""

import itertools
from typing import Dict, List, Set

from elasticsearch_dsl.query import Q

from database.elasticsearch import query_generator
from database.elasticsearch.models import ResumeFeatures
from database.mongodb.models import RecallResumeResult


# 记录此次召回结果
def record_recall(link_uuid, tenant, dtype: str, aid_set):
    rrr = RecallResumeResult()
    rrr.tenant = tenant
    rrr.link_uuid = link_uuid
    rrr.dtype = dtype
    rrr.aid_list = list(aid_set)
    rrr.save()


# 根据 工作描述 召回相关简历
def use_lda_weight(lda_vector_pairs: List[Dict],
                   tenant: str,
                   topn: int = 2000,
                   min_score: float = 0.35) -> Set[int]:
    if not lda_vector_pairs:
        return set()

    should_query_list = []
    for pair in lda_vector_pairs:
        should_query_list.append(
            query_generator.gen_lda_weight_nested_pairs(pair))

    query = ResumeFeatures.search().filter(
        "term", tenant=tenant).filter(
            'term', is_deleted=False).query(
                "nested",
                path="experiences.lda_weight",
                score_mode="sum",
                query=Q(
                    "function_score",
                    query=Q("bool", should=should_query_list),
                    field_value_factor={
                        "field": "experiences.lda_weight.value"
                    })).source(["aid"])[0:topn]

    data = query.execute()
    aid_set = set()
    for r in data:
        if r.meta.score >= min_score:
            aid_set.add(r.aid)

    return aid_set


# 根据poi_weight召回相关简历
def use_poi_weight(poi_weight_pairs: List[Dict],
                   tenant: str,
                   topn: int = 1000,
                   min_score: float = 0.35) -> Set[int]:
    if not poi_weight_pairs:
        return set()
    should_query_list = []
    poi_weight_pairs.sort(key=lambda x: x['value'], reverse=True)

    for pair in poi_weight_pairs[:20]:
        should_query_list.append(
            query_generator.gen_poi_weight_nested_pairs(pair))

    query = ResumeFeatures.search().filter(
        "term", tenant=tenant).filter(
            'term', is_deleted=False).query(
                "nested",
                path="experiences.poi_weight",
                score_mode="sum",
                query=Q(
                    "function_score",
                    query=Q("bool", should=should_query_list),
                    field_value_factor={
                        "field": "experiences.poi_weight.value"
                    })).source(["aid"])[0:topn]

    data = query.execute()
    aid_set = set()
    for r in data:
        if r.meta.score >= min_score:
            aid_set.add(r.aid)

    return aid_set


# 根据 title_weight 召回相关简历
def use_title_weight(title_weight_pairs: List[Dict],
                     tenant: str,
                     topn: int = 1000,
                     min_score: float = 0.5) -> Set[int]:
    if not title_weight_pairs:
        return set()
    should_query_list = []
    title_weight_pairs.sort(key=lambda x: x['value'], reverse=True)

    for pair in title_weight_pairs[:20]:
        should_query_list.append(
            query_generator.gen_title_weight_nested_pairs(pair))

    query = ResumeFeatures.search().filter(
        "term", tenant=tenant).filter(
            'term', is_deleted=False).query(
                "nested",
                path="experiences.work_list",
                score_mode="sum",
                query=Q(
                    "function_score",
                    query=Q("bool", should=should_query_list),
                    field_value_factor={
                        "field": "experiences.work_list.weight"
                    })).source(["aid"])[0:topn]

    data = query.execute()
    aid_set = set()
    for r in data:
        if r.meta.score >= min_score:
            aid_set.add(r.aid)

    return aid_set


# 根据 目标公司和职位 召回相关简历
def use_target_company_title(company_list: Set[str],
                             title_list: Set[str],
                             tenant: str,
                             topn: int = 200,
                             min_score: float = 1.) -> Set[int]:
    company_list = {i for i in company_list if i}
    title_list = {i for i in title_list if i}

    if not (company_list and title_list):
        return set()
    # 生成查询
    company_title_pairs = itertools.product(company_list, title_list)
    should_query_list = []
    for company, title in company_title_pairs:
        should_query_list.append(
            query_generator.gen_company_title_nested_pairs(company, title))

    query = ResumeFeatures.search().filter(
        "term", tenant=tenant).filter(
            'term', is_deleted=False).query(
                "nested",
                path="experiences.work_list",
                query=Q("bool",
                        should=should_query_list)).source(["aid"])[0:topn]

    # 获取数据
    data = query.execute()
    aid_set = set()
    for r in data:
        if r.meta.score >= min_score:
            aid_set.add(r.aid)

    return aid_set


# 根据 目标公司和职位 召回相关简历
def use_target_school_major(school_list: Set[str],
                            major_list: Set[str],
                            tenant: str,
                            topn: int = 200,
                            min_score: float = 1.) -> Set[int]:
    school_list = {i for i in school_list if i}
    major_list = {i for i in major_list if i}

    if not (school_list and major_list):
        return set()

    # 生成查询
    school_title_pairs = itertools.product(school_list, major_list)

    should_query_list = []
    for school, major in school_title_pairs:
        should_query_list.append(
            query_generator.gen_school_major_nested_pairs(school, major))

    query = ResumeFeatures.search().filter(
        "term", tenant=tenant).filter(
            'term', is_deleted=False).query(
                "nested",
                path="educations.edu_list",
                query=Q("bool",
                        should=should_query_list)).source(['aid'])[0:topn]

    # 获取数据
    data = query.execute()
    aid_set = set()
    for r in data:
        if r.meta.score >= min_score:
            aid_set.add(r.aid)

    return aid_set


# 根据 custom_tags 召回相关简历
def use_custom_tags(custom_tags: Set[str],
                    tenant: str,
                    topn: int = 2000,
                    min_score: float = 0.01) -> Set[int]:
    # 生成查询
    if not custom_tags:
        return set()
    should_query_list = []

    for tag in custom_tags:
        should_query_list.append(Q("term", custom_tags=tag))

    query = ResumeFeatures.search().filter(
        "term", tenant=tenant).filter(
            'term', is_deleted=False).query(
                "bool", should=should_query_list).source(["aid"])[0:topn]

    # 获取数据
    data = query.execute()
    aid_set = set()
    for r in data:
        if r.meta.score >= min_score:
            aid_set.add(r.aid)

    return aid_set
