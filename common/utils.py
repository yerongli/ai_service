import logging
import time
from uuid import uuid4

from gllue_ai_libs import search_engine
from gllue_ai_libs.search_engine.utils.model_loader import BaseModelLoader
from xgboost import XGBClassifier, XGBRegressor

from config import ai_service_config

search_engine.set_model_dir_path(ai_service_config['MODEL_PATH'])


class XGBRegressorModelLoader(BaseModelLoader):
    def load(self) -> XGBRegressor:
        model = XGBRegressor()
        model.load_model(self.model_path)
        return model


class XGBClassifierModelLoader(BaseModelLoader):
    def load(self) -> XGBClassifier:
        model = XGBClassifier()
        model.load_model(self.model_path)
        return model


def print_time(f):
    def fi(*args, **kwargs):
        s = time.time()
        res = f(*args, **kwargs)
        logging.info('--> RUN TIME: <%s> : %s' % (f.__name__, time.time() - s))
        return res

    return fi


def link_tracking_time(func):
    def wrapper(self, *args, **kwargs):
        s = time.time()
        res = func(self, *args, **kwargs)
        logging.info(
            f"--> LINK TRACKING TIME [{self.link_uuid}]: <{func.__name__}> : {time.time() - s}"
        )
        return res

    return wrapper


def remove_set_element(origin_set: set, element) -> set:
    if element in origin_set:
        origin_set.remove(element)
    return origin_set


def remove_set_elements(origin_set: set, elements: set) -> set:
    return origin_set - elements


class BaseGeneratorUuid(object):
    def __init__(self):
        self.link_uuid = uuid4()
