from typing import Dict

import tqdm
from gllue_ai_libs.angelo_tool.analyze.metrics import *

from database import get_ident
from database.mongodb import (
    RecallResumeResult, RecommendResumeFromJdInfoResult, get_single_joborder,
    joborder_find, jobsubmission_find)


def get_recommend_resume_pred(tenant: str, aid: int, force: bool = True
                              ) -> RecommendResumeFromJdInfoResult:
    if force:
        ident = get_ident(tenant, aid)
        jd = get_single_joborder(
            ident=ident, projection={"current_version": 1})
        jd_version = jd['current_version']

        rrfj = RecommendResumeFromJdInfoResult.objects(
            tenant=tenant, jd_aid=aid,
            jd_version=jd_version).order_by('-create_time')
        if not rrfj:
            raise Exception("计算 verify_recommend_resume 失败，不存在推荐结果，请重新推荐后计算")

        rrfj = rrfj[0]
    else:
        rrfj = RecommendResumeFromJdInfoResult.objects(
            tenant=tenant,
            jd_aid=aid,
        ).order_by('-create_time')
        if not rrfj:
            raise Exception("计算 verify_recommend_resume 失败，不存在推荐结果，请重新推荐后计算")

        rrfj = rrfj[0]
    return rrfj


def get_recommend_resume_true(tenant: str, aid: int):
    data = jobsubmission_find(
        query={
            "client_key": tenant,
            "joborder": aid
        },
        projection={"candidate": 1})
    y_true_candidate = set([i['candidate'] for i in data])
    return y_true_candidate


def aid_verify_recommend_resume(tenant: str, aid: int, force: bool = True
                                ) -> Dict[str, Dict[str, float]]:
    """
    验证简历推荐结果的指标
    :param tenant: 租户
    :param aid: 简历id
    :param force: 如果为True，则定位当前版本的简历。
    :return: 精确率，召回率，f1值
    """
    result = dict()

    y_true_candidate = get_recommend_resume_true(tenant, aid)

    rrfj = get_recommend_resume_pred(tenant, aid, force)
    y_predict_candidate = set([i['aid'] for i in rrfj.items])

    precision = precision_score(y_true_candidate, y_predict_candidate)
    recall = recall_score(y_true_candidate, y_predict_candidate)
    f1 = fa_score(precision, recall)

    _recommend_score = {
        "precision_score": precision,
        "recall_score": recall,
        "f1_score": f1,
        "true_num": len(y_true_candidate),
        "pred_num": len(y_predict_candidate)
    }
    result["resume_recommend"] = _recommend_score

    rrr = RecallResumeResult.objects(
        link_uuid=rrfj.id, tenant=tenant,
        dtype='RRJ').order_by('-create_time').first()
    if rrr:
        y_recall_candidate = set(rrr.aid_list)

        recall_precision = precision_score(y_true_candidate,
                                           y_recall_candidate)
        recall_recall = recall_score(y_true_candidate, y_recall_candidate)
        recall_f1 = fa_score(recall_precision, recall_recall)

        _recall_score = {
            "precision_score": recall_precision,
            "recall_score": recall_recall,
            "f1_score": recall_f1,
            "true_num": len(y_true_candidate),
            "pred_num": len(y_recall_candidate)
        }
        result["recall"] = _recall_score

    return result


def tenant_verify_recommend_resume(tenant: str):
    # 计算单个租户所有推荐的指标
    data = joborder_find(query={"client_key": tenant}, projection={"id": 1})
    result = dict()
    for joborder in tqdm.tqdm(data):
        aid = joborder['id']
        _rst = dict()
        _r = aid_verify_recommend_resume(tenant=tenant, aid=aid)
        _rst['推荐精准率'] = _r.get('resume_recommend', {}).get("precision_score")
        _rst['推荐召回率'] = _r.get('resume_recommend', {}).get("recall_score")
        _rst['推荐f1值'] = _r.get('resume_recommend', {}).get("f1_score")

        _rst['召回精准率'] = _r.get('recall', {}).get("precision_score")
        _rst['召回召回率'] = _r.get('recall', {}).get("recall_score")
        _rst['召回f1值'] = _r.get('recall', {}).get("f1_score")

        _rst['_真实数量'] = _r.get('resume_recommend', {}).get("true_num")
        _rst['_召回数量'] = _r.get('recall', {}).get("pred_num")
        _rst['_推荐数量'] = _r.get('resume_recommend', {}).get("pred_num")
        result[aid] = _rst
    return result
