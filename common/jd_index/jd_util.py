from typing import Dict, List, Tuple, Union

from database.elasticsearch.meta import VectorFieldMeta
from database.elasticsearch.models import (CandidatePreferenceDoc,
                                           JobOrderFeatures)
from preprocess.degree import get_degree_id
from preprocess.title import get_title2parent


def cosine_dict(d1: Union[Dict, Tuple], d2: Union[Dict, Tuple]) -> float:
    # 计算d1 d2的余弦距离， d1,d2可以为dict 或 tuple
    if not isinstance(d1, dict):
        d1 = dict(d1)
    if not isinstance(d2, dict):
        d2 = dict(d2)
    intersection_keys = d1.keys() & d2.keys()
    v = 0
    for i in intersection_keys:
        v += d1.get(i, 0) * d2.get(i, 0)
    # d1 = sorted(d1.items(), key=lambda x: x[1], reverse=True)[:30]
    # d2 = sorted(d2.items(), key=lambda x: x[1], reverse=True)[:30]
    # d1 = set([i[0] for i in d1])
    # d2 = set([i[0] for i in d2])
    # v = len(d1 & d2)
    return v


def distance_list(l1: list, l2: list) -> List[int]:
    l1 = [i for i in l1 if i]
    l2 = [i for i in l2 if i]
    inter = [i for i in l2 if i in l1]
    return [len(l1), len(l2), len(inter)]


class RangeField:
    def __init__(self, gte, lte):
        self.gte = gte
        self.lte = lte

    def in_range(self, x) -> bool:
        if self.gte <= x <= self.lte:
            return True
        else:
            return False

    def to_json(self):
        return {"gte": self.gte, "lte": self.lte}


def range_match(range_field, x) -> bool:
    if not range_field:
        return True
    if range_field.gte is not None:
        if range_field.lte is not None:
            if range_field.gte <= x <= range_field.lte:
                return True
            else:
                return False
        else:
            if x >= range_field.gte:
                return True
            else:
                return False
    else:
        return False


class JobOrderMatchUtil:
    def __init__(self, jd: JobOrderFeatures, need_companies: list,
                 need_school: list, custom_tags: list, need_985_211: bool):
        self.jd: JobOrderFeatures = jd
        self.cp: CandidatePreferenceDoc = self.jd.candidate_preference

        self.need_companies: set = set(need_companies +
                                       list(self.jd.recommend_company or []))
        self.need_school: set = set(need_school)
        self.need_custom_tags: set = set(custom_tags)
        self.need_985_211: bool = self.jd.need_985_211 or need_985_211

        self.title_set = {i['index'] for i in self.jd.title_prob_weight}
        self.title_parent_set = {get_title2parent(i) for i in self.title_set}

        self.lda_weight_dict = VectorFieldMeta.index_value_to_dict(
            self.jd.lda_weight)
        self.poi_weight_dict = VectorFieldMeta.index_value_to_dict(
            self.jd.poi_weight)

        self.cp_lda_weight_dict = VectorFieldMeta.index_value_to_dict(
            self.jd.candidate_preference.lda_weight)
        self.cp_poi_weight_dict = VectorFieldMeta.index_value_to_dict(
            self.jd.candidate_preference.poi_weight)

        self.cp_major_weight_dict = VectorFieldMeta.index_value_to_dict(
            self.jd.candidate_preference.major_weight)
        self.cp_title_weight_dict = VectorFieldMeta.index_value_to_dict(
            self.jd.candidate_preference.title_weight)
        self.cp_parent_title_weight_dict = VectorFieldMeta.index_value_to_dict(
            self.jd.candidate_preference.parent_title_weight)

    def age_match(self, age) -> List[int]:
        # jd age 缺失 [1, 0, 0, 0, 0]
        # resume age 缺失 [0, 1, 0, 0, 0]
        # 匹配    [0, 0, 0, 1, 0]
        # 不匹配   [0, 0, 0, 0, 1]
        if not self.jd.age_range.gte:
            return [1, 0, 0, 0, 0]
        if not age:
            return [0, 1, 0, 0, 0]
        if range_match(self.jd.age_range, age):
            return [0, 0, 1, 1, 0]
        else:
            return [0, 0, 1, 0, 1]

    def cp_age_match(self, age):
        if not self.cp.age_avg:
            return [1, 0, 0, 0]
        if not age:
            return [0, 1, 0, 0]
        return [0, 0, 1, age - self.cp.age_avg]

    def salary_match(self, salary) -> List[int]:
        # 薪资范围匹配
        # jd salary 缺失 [1, 0, 0, 0, 0]
        # resume salary 缺失 [0, 1, 0, 0, 0]
        # 匹配    [0, 0, 1, 1, 0]
        # 不匹配  [0, 0, 1, 0, 1]
        if not self.jd.salary_range.gte:
            return [1, 0, 0, 0, 0]
        if not salary:
            return [0, 1, 0, 0, 0]
        if range_match(self.jd.salary_range, salary):
            return [0, 0, 1, 1, 0]
        else:
            return [0, 0, 1, 0, 1]

    def cp_salary_match(self, salary):
        if not self.cp.salary_avg:
            return [1, 0, 0, 0]
        if not salary:
            return [0, 1, 0, 0]
        return [0, 0, 1, salary - self.cp.salary_avg]

    def location_match(self, locations):
        if not self.jd.cities:
            return [1, 0, 0, 0]
        if not locations:
            return [0, 1, 0, 0]

        x = 0
        for loc in locations:
            if loc in self.jd.cities:
                x = 1
                break
        return [0, 0, 1, x]

    def degree_list_match(self, degree_list: list) -> list:
        # 学历匹配
        # 如果没有学历要求则都符合
        if not self.jd.lowest_degree:
            return [1, 0, 0, 0]
        if not degree_list:
            return [0, 1, 0, 0]
        for degree in degree_list:
            if self.degree_match(degree):
                return [0, 0, 1, 0]
            else:
                continue
        else:
            return [0, 0, 0, 1]

    def major_list_match(self, major_list) -> list:
        if not self.jd.need_majors:
            return [1, 0, 0, 0]
        if not major_list:
            return [0, 1, 0, 0]
        for major in major_list:
            if self.major_match(major):
                return [0, 0, 1, 0]
        else:
            return [0, 0, 0, 1]

    def cp_major_list_match(self, major_list) -> list:
        if not self.cp_major_weight_dict:
            return [1, 0, 0, 0]
        if not major_list:
            return [0, 1, 0, 0]
        _x = 0
        for major in major_list:
            _x += self.cp_major_weight_dict.get(major, 0)
        return [0, 0, 1, _x]

    def match_985_211(self, is_985_211) -> list:
        if not self.need_985_211:
            return [1, 0, 0]
        if is_985_211:
            return [0, 1, 0]
        else:
            return [0, 0, 1]

    def title_list_match(self, title_list) -> list:
        if not self.title_set:
            return [1, 0, 0, 0]
        if not title_list:
            return [0, 1, 0, 0]
        for title in title_list:
            if self.title_match(title):
                return [0, 0, 1, 0]
        else:
            return [0, 0, 0, 1]

    def parent_title_list_match(self, parent_title_list) -> list:
        if not self.title_parent_set:
            return [1, 0, 0, 0]
        if not parent_title_list:
            return [0, 1, 0, 0]
        for parent_title in parent_title_list:
            if self.parent_title_match(parent_title):
                return [0, 0, 1, 0]
        else:
            return [0, 0, 0, 1]

    def title_level_match(self, highest_level: int) -> list:
        if abs(highest_level - self.jd.title_level) <= 1:
            # or abs(highest_level - self.jd.candidate_preference.title_level_avg) <= 0.5:
            return [1, 0, 0]
        elif highest_level >= self.jd.title_level:
            return [0, 1, 0]
        else:
            return [0, 0, 1]

    def working_year_match(self, work_year) -> list:
        # 工作年限匹配
        # jd age 缺失 [1, 0, 0, 0, 0]
        # resume age 缺失 [0, 1, 0, 0, 0]
        # 匹配    [0, 0, 0, 1, 0]
        # 不匹配   [0, 0, 0, 0, 1]
        if not self.jd.working_years_range.gte:
            return [1, 0, 0, 0, 0, 0]
        if not work_year:
            return [0, 1, 0, 0, 0, 0]
        if range_match(self.jd.working_years_range, work_year):
            return [
                0, 0, 1, 1, 0,
                abs(work_year - self.jd.working_years_range.gte)
            ]
        else:
            return [
                0, 0, 1, 0, 1,
                abs(work_year - self.jd.working_years_range.gte)
            ]

    def industry_match(self, industry):
        # 行业是否匹配
        if industry in self.jd.need_industries:
            return True
        else:
            return False

    def cp_working_year_match(self, work_month) -> list:
        # 偏好工作年限匹配
        if not self.cp.work_duration_month_avg:
            return [1, 0, 0]
        else:
            return [0, 1, (self.cp.work_duration_month_avg - work_month) / 12]

    def major_match(self, major):
        # 专业匹配
        if major in self.jd.need_majors:
            return True
        else:
            return False

    def school_match(self, school):
        if school in self.need_school:
            return True
        else:
            return False

    def school_list_match(self, school_list):
        # 目标学校
        for school in school_list:
            if self.school_match(school):
                return True
        else:
            return False

    def degree_match(self, degree):
        i1 = get_degree_id(self.jd.lowest_degree)
        i2 = get_degree_id(degree)
        if i2 >= i1:
            return True
        else:
            return False

    def cp_degree_list_match(self, degree_list) -> list:
        if not self.cp.degree_avg:
            return [1, 0, 0, 0]
        if not degree_list:
            return [0, 1, 0, 0]
        degree_list = [get_degree_id(degree) for degree in degree_list]
        d_max = max(degree_list)
        return [0, 0, 1, self.cp.degree_avg - d_max]

    def company_match(self, company):
        if company in self.need_companies:
            return True
        else:
            return False

    def company_list_match(self, company_list):
        for company in company_list:
            if self.company_match(company):
                return True
        else:
            return False

    def title_match(self, title) -> int:
        if title in self.title_set:
            return 1
        else:
            return 0

    def parent_title_match(self, parent_title) -> int:
        if parent_title in self.title_parent_set:
            return 1
        else:
            return 0

    def cp_title_level_match(self, highest_level: int) -> list:
        if not self.cp.title_level_avg:
            return [1, 0, 0]
        else:
            return [0, 1, self.cp.title_level_avg - highest_level]

    def poi_match(self, poi_weight: dict) -> list:
        return [
            len(self.poi_weight_dict),
            len(poi_weight),
            len(self.poi_weight_dict.keys() & poi_weight.keys()),
            cosine_dict(self.poi_weight_dict, poi_weight)
        ]

    def lda_match(self, lda_weight: dict) -> list:
        return [
            len(self.lda_weight_dict),
            len(lda_weight),
            len(self.lda_weight_dict.keys() & lda_weight.keys()),
            cosine_dict(self.lda_weight_dict, lda_weight)
        ]

    def cp_poi_match(self, poi_weight: dict) -> list:
        n = len(self.cp_poi_weight_dict.keys() & poi_weight.keys())
        v = cosine_dict(self.cp_poi_weight_dict, poi_weight)
        return [len(self.poi_weight_dict), len(poi_weight), n, v]

    def cp_lda_match(self, lda_weight: dict) -> list:
        n = len(self.cp_lda_weight_dict.keys() & lda_weight.keys())
        v = cosine_dict(self.cp_lda_weight_dict, lda_weight)
        return [len(self.cp_lda_weight_dict), len(lda_weight), n, v]

    def cp_title_list_match(self, title_list: list) -> list:
        if not self.cp_title_weight_dict:
            return [1, 0, 0, 0]
        if not title_list:
            return [0, 1, 0, 0]
        _x = 0
        for title in title_list:
            _x += self.cp_title_weight_dict.get(title, 0)
        return [0, 0, 1, _x]

    def cp_parent_title_list_match(self, parent_title_list: list) -> list:
        if not self.cp_parent_title_weight_dict:
            return [1, 0, 0, 0]
        if not parent_title_list:
            return [0, 1, 0, 0]
        _x = 0
        for parent_title in parent_title_list:
            _x += self.cp_parent_title_weight_dict.get(parent_title, 0)
        return [0, 0, 1, _]
