from collections import defaultdict
from typing import Dict, List

from gllue_ai_libs.angelo_tool.analyze.algorithm import unitvec_dict
from gllue_ai_libs.angelo_tool.nlp.base_func import sub_brackets

from common.jd_index.jd_util import RangeField
from database.elasticsearch.models import ResumeFeatures
from database.mongodb import jobsubmission_find
from preprocess import FillInfo, ToJsonMixin, normalize_dict
from preprocess.company import extract_company_keyword
from preprocess.degree import get_degree_id, normalize_degree
from preprocess.description import (extract_description_poi_with_title,
                                    extract_jd_content_info,
                                    predict_description_lda_with_title)
from preprocess.major import lowest_degree_major_predict
from preprocess.school import normalize_school_aid
from preprocess.title import normalize_title_level, title_multiple_predict


class CandidatePreference(ToJsonMixin, FillInfo):
    def to_json(self):
        result = dict()
        result['degree_avg'] = self.degree_avg
        result['title_level_avg'] = self.title_level_avg
        result['work_duration_month_avg'] = self.work_duration_month_avg
        result['salary_avg'] = self.salary_avg
        result['age_avg'] = self.age_avg
        result['lda_weight'] = self.lda_weight
        result['poi_weight'] = self.poi_weight
        result['title_weight'] = self.title_weight
        result['parent_title_weight'] = self.parent_title_weight
        result['major_weight'] = self.major_weight
        return result

    def __init__(self, tenant, jd_aid):
        self.tenant = tenant
        self.jd_aid = jd_aid

        self.degree_avg: float = 0
        self.title_level_avg: float = 0
        self.work_duration_month_avg: float = 0
        self.salary_avg: float = 0
        self.age_avg: float = 0

        self.lda_weight: Dict[int, float] = dict()
        self.poi_weight: Dict[str, float] = dict()
        self.title_weight: Dict[str, float] = dict()
        self.parent_title_weight: Dict[str, float] = dict()
        self.major_weight: Dict[str, float] = dict()

        self.fill_info()

    def fill_info(self):
        self.get_candidate_preference_info()

    def get_candidate_preference_info(self):
        data = jobsubmission_find(
            query={
                'client_key': self.tenant,
                'joborder': self.jd_aid,
                'max_status': {
                    '$ne': None
                }
            },
            projection={'candidate': 1})
        resume_ids = [
            i['candidate'] for i in data if i['candidate'] is not None
        ]

        resume_query = ResumeFeatures.search().filter(
            'term', tenant=self.tenant).filter(
                'terms', aid=resume_ids).source([
                    'basic_info.age', 'educations.edu_list.major_stand',
                    'educations.last_degree',
                    'experiences.work_list.title_stand',
                    'experiences.work_list.title_parent',
                    'experiences.lda_weight', 'experiences.poi_weight',
                    'experiences.title_level_highest',
                    'experiences.total_duration_month'
                ])

        lda_weight = defaultdict(float)
        poi_weight = defaultdict(float)
        title_weight = defaultdict(float)
        parent_title_weight = defaultdict(float)
        major_weight = defaultdict(float)

        age_sum = 0
        age_len = 0

        salary_sum = 0
        salary_len = 0

        degree_sum = 0
        degree_len = 0

        work_duration_month_sum = 0
        work_duration_month_len = 0

        title_level_sum = 0
        title_level_len = 0

        for resume in resume_query.scan():
            # 年龄
            if resume.basic_info.age:
                age_sum += resume.basic_info.age
                age_len += 1

            # 薪资
            if resume.basic_info.current_salary:
                salary_sum += resume.basic_info.current_salary
                salary_len += 1

            # 学历
            if resume.educations.last_degree:
                degree_sum += get_degree_id(resume.educations.last_degree)
                degree_len += 1

            # 专业
            for edu in resume.educations.edu_list:
                major_weight[edu.major_stand] += 1

            # 工龄
            if resume.experiences.total_duration_month:
                work_duration_month_sum += resume.experiences.total_duration_month
                work_duration_month_len += 1

            # 职级
            if resume.experiences.title_level_highest:
                title_level_sum += resume.experiences.title_level_highest
                title_level_len += 1

            # 职位
            for exp in resume.experiences.work_list:
                if exp.title_stand:
                    title_weight[exp.title_stand] += 1
                if exp.title_parent:
                    parent_title_weight[exp.title_parent] += 1

            # poi
            for poi in resume.experiences.poi_weight:
                poi_weight[poi['index']] += poi['value']

            # lda
            for lda in resume.experiences.lda_weight:
                lda_weight[lda['index']] += lda['value']

        self.degree_avg = degree_sum / degree_len if degree_len else 0
        self.title_level_avg = title_level_sum / title_level_len if title_level_len else 0
        self.work_duration_month_avg = work_duration_month_sum / work_duration_month_len if work_duration_month_len else 0
        self.salary_avg = salary_sum / salary_len if salary_len else 0
        self.age_avg = age_sum / age_len if age_len else 0

        self.title_weight = normalize_dict(
            unitvec_dict(title_weight, return_dict=True))
        self.parent_title_weight = normalize_dict(
            unitvec_dict(parent_title_weight, return_dict=True))
        self.major_weight = normalize_dict(
            unitvec_dict(major_weight, return_dict=True))
        self.lda_weight = normalize_dict(
            unitvec_dict(lda_weight, return_dict=True))
        self.poi_weight = normalize_dict(
            unitvec_dict(poi_weight, return_dict=True))


class JobDescription(FillInfo):
    def __init__(self, jd_info):
        self.aid: int = jd_info['aid']
        self.tenant: str = jd_info['tenant']
        self.version: int = jd_info['version']
        self.is_deleted: bool = jd_info['is_deleted']

        self.jd_info = jd_info

        self.title: str = ""
        self.title_clean: str = ""
        self.title_stand: str = ""
        self.title_prob_weight: Dict[str, float] = dict()
        self.title_level: int = 0

        self.content: str = ""
        self.content_clean: str = ""

        self.cities: list = list()
        self.company: str = ""
        self.company_keyword: str = ""
        self.recommend_company: List[str] = list()

        self.lowest_degree: str = ""
        self.need_majors: List[str] = list()
        self.need_985_211: bool = False
        self.need_industries: List[str] = []

        self.working_years_range: RangeField = None
        self.age_range: RangeField = None
        self.salary_range: RangeField = None

        self.lda_weight: Dict[int, float] = dict()
        self.poi_weight: Dict[str, float] = dict()

        self.candidate_preference: CandidatePreference = None

    def check_title(self, title):
        self.title = title
        self.title_clean = sub_brackets(title)

        title_rst = title_multiple_predict([title])[0]
        if title_rst:
            self.title_stand = title_rst['pred']
            self.title_prob_weight = title_rst['pred_prob']
        self.title_level = normalize_title_level(title)

    def process(self, jd_info):
        title = jd_info['title']
        content = jd_info['content']
        company = jd_info['company']
        salary_range = jd_info['salary_range']
        lowest_degree = jd_info['lowest_degree']
        working_years_range = jd_info['working_years_range']

        self.cities = jd_info['cities']

        self.check_title(title)
        self.company = company

        jd_content_info = extract_jd_content_info(content)

        self.content = content
        self.content_clean = jd_content_info['content_clean']

        self.need_985_211 = jd_content_info['need_985_211']
        if lowest_degree:
            self.lowest_degree = normalize_degree(lowest_degree)
        else:
            self.lowest_degree = jd_content_info['lowest_degree']

        self.need_majors = list(
            lowest_degree_major_predict(
                major_list=jd_content_info['need_majors'],
                lowest_degree=jd_content_info['lowest_degree']))

        if working_years_range:
            self.working_years_range = RangeField(
                gte=working_years_range['gte'], lte=working_years_range['lte'])
        else:
            self.working_years_range = RangeField(
                gte=jd_content_info['working_year_range']['gte'],
                lte=jd_content_info['working_year_range']['lte'])

        self.age_range = RangeField(
            gte=jd_content_info['age_range']['gte'],
            lte=jd_content_info['age_range']['lte'])
        self.salary_range = RangeField(
            gte=salary_range['gte'], lte=salary_range['lte'])

        self.lda_weight = normalize_dict(
            unitvec_dict(
                predict_description_lda_with_title(self.title_clean,
                                                   self.content_clean),
                return_dict=True))
        self.poi_weight = normalize_dict(
            unitvec_dict(
                extract_description_poi_with_title(self.title_clean,
                                                   self.content_clean),
                return_dict=True))

    def fill_info(self):
        self.process(self.jd_info)
        self.candidate_preference = CandidatePreference(self.tenant, self.aid)
        self.repair_from_candidate_preference()

    @classmethod
    def fill_target_company_list(cls, company_list):
        cl = []
        for i in company_list:
            cl.append(i)
            cl.append(extract_company_keyword(i))
        else:
            cl = [i for i in cl if i]
        return cl

    @classmethod
    def fill_target_school_list(cls, school_list):
        return [normalize_school_aid(s) for s in school_list]

    def get_recommend_company(self):
        self.recommend_company = get_similar_company(self.company)

    def repair_from_candidate_preference(self):
        if not self.candidate_preference:
            return
        poi_weight = defaultdict(float)
        lda_weight = defaultdict(float)

        for k, v in self.candidate_preference.poi_weight.items():
            poi_weight[k] += v
        for k, v in self.candidate_preference.lda_weight.items():
            lda_weight[k] += v

        for k, v in self.poi_weight.items():
            poi_weight[k] += v

        for k, v in self.lda_weight.items():
            lda_weight[k] += v

        lda_weight = normalize_dict(
            unitvec_dict({k: v / 2
                          for k, v in lda_weight.items()},
                         return_dict=True))
        poi_weight = normalize_dict(
            unitvec_dict({k: v / 2
                          for k, v in poi_weight.items()},
                         return_dict=True))
        self.poi_weight = poi_weight
        self.lda_weight = lda_weight

    def to_json(self):
        result = dict()
        result['aid'] = self.aid
        result['tenant'] = self.tenant
        result['version'] = self.version
        result['is_deleted'] = self.is_deleted

        result['title'] = self.title
        result['title_clean'] = self.title_clean
        result['title_stand'] = self.title_stand
        result['title_prob_weight'] = self.title_prob_weight
        result['title_level'] = self.title_level

        result['content'] = self.content
        result['content_clean'] = self.content_clean

        result['cities'] = self.cities
        result['company'] = self.company
        result['company_keyword'] = self.company_keyword
        result['recommend_company'] = self.recommend_company

        result['lowest_degree'] = self.lowest_degree
        result['need_majors'] = self.need_majors
        result['need_985_211'] = self.need_985_211

        result['working_years_range'] = self.working_years_range.to_json()
        result['age_range'] = self.age_range.to_json()
        result['salary_range'] = self.salary_range.to_json()

        result['need_industries'] = self.need_industries
        result['lda_weight'] = self.lda_weight
        result['poi_weight'] = self.poi_weight

        result['candidate_preference'] = self.candidate_preference.to_json()
        return result

    @classmethod
    def init_from_jd_info(cls, jd_info: dict) -> "JobDescription":
        jd = JobDescription(jd_info)
        jd.fill_info()
        return jd
