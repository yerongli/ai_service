from collections import defaultdict
from typing import DefaultDict, Dict, List

import pendulum
from gllue_ai_libs.angelo_tool.analyze.algorithm import unitvec_dict

from preprocess import (FillInfo, ToJsonMixin, normalize_dict,
                        school_weight_cooling, work_exp_weight_cooling)
from preprocess.basic_info import get_norm_monthly_salary
from preprocess.company import extract_company_keyword
from preprocess.datetime import (STANDARD_TIME, Date, get_date_duration_month,
                                 stand_date)
from preprocess.degree import normalize_degree
from preprocess.description import (extract_description_poi_with_title,
                                    predict_description_lda_with_title)
from preprocess.major import normalize_major
from preprocess.school import get_school_info_dict, normalize_school_aid
from preprocess.title import (get_title2parent, normalize_title,
                              normalize_title_level, title_multiple_predict)


class BasicInfo:
    def __init__(self, basic_info):
        self.name: str = basic_info['name']
        self.gender: str = basic_info['gender']
        self.mobile: str = basic_info['mobile']
        self.email: str = basic_info['email']
        self.linkedin: str = basic_info['linkedin']
        self.birth_day: Date = basic_info['birth_day']
        self.age: int = 0
        self.current_salary: float = basic_info['current_salary']
        self.locations: list = basic_info['locations']

    def stand_birth_day(self):
        self.birth_day = stand_date(self.birth_day)

    def stand_current_salary(self):
        self.current_salary = get_norm_monthly_salary(self.current_salary)

    def stand_age(self):
        if self.birth_day:
            period = pendulum.today().date() - self.birth_day
            self.age = period.years

    def fill_info(self):
        self.stand_birth_day()
        self.stand_age()
        self.stand_current_salary()

    @classmethod
    def init_from_basic_info(cls, basic_info) -> "BasicInfo":
        basic_info = cls(basic_info)
        basic_info.fill_info()
        return basic_info

    def to_json(self):
        _d = {
            "name": self.name,
            "gender": self.gender,
            "mobile": self.mobile,
            "email": self.email,
            "linkedin": self.linkedin,
            "birth_day": self.birth_day,
            "age": self.age,
            "current_salary": self.current_salary,
            "locations": self.locations
        }
        return _d


class EducationExperience:
    def __init__(self, edu_info: dict):
        self.is_recent = edu_info['is_recent']
        self.is_current = edu_info['is_current']
        self.weight = 0

        self.school: str = edu_info['school']
        self.school_stand_id: int = 0

        self.major: str = edu_info['major']
        self.major_stand: str = ""

        self.degree: str = edu_info['degree']
        self.degree_stand: str = ""

        self.start: Date = edu_info['start']
        self.end: Date = edu_info['end']

        self.duration_month: int = 0

        # 专业合法
        self.is_major_legal: bool = True
        # 教育时间合法
        self.is_time_legal: bool = True
        # 教育经历合法
        self.is_edu_legal: bool = True

        # 985院校
        self.is_985: bool = False
        # 211院校
        self.is_211: bool = False
        self.is_985_211: bool = False
        # 海外院校
        self.is_abroad: bool = False
        # qs200院校
        self.is_qs200: bool = False

    def stand_school(self):
        self.school_stand_id = normalize_school_aid(self.school)

    def stand_degree(self):
        self.degree_stand = normalize_degree(self.degree)

    def stand_major(self):
        self.major_stand = normalize_major(
            major=self.major, degree=self.degree_stand)

    def stand_start(self):
        self.start = stand_date(self.start)

    def stand_end(self):
        self.end = stand_date(self.end)

    def stand_duration_month(self):
        self.duration_month = get_date_duration_month(self.start, self.end)

    def explore_info(self):
        si = get_school_info_dict(self.school_stand_id)
        if si:
            self.is_985 = si['is_985']
            self.is_211 = si['is_211']
            self.is_985_211 = self.is_985 or self.is_211
            self.is_abroad = si['is_abroad']
            self.is_qs200 = si['is_qs200']

    def fill_info(self):
        self.stand_school()
        self.stand_degree()
        self.stand_major()
        self.stand_start()
        self.stand_end()
        self.stand_duration_month()
        self.explore_info()

    def to_json(self):
        _d = {
            "is_recent": self.is_recent,
            "is_current": self.is_current,
            "weight": self.weight,
            "school": self.school,
            "school_stand_id": self.school_stand_id,
            "major": self.major,
            "major_stand": self.major_stand,
            "degree": self.degree,
            "degree_stand": self.degree_stand,
            "start": self.start,
            "end": self.end,
            "duration_month": self.duration_month,
            "is_major_legal": self.is_major_legal,
            "is_time_legal": self.is_time_legal,
            "is_edu_legal": self.is_edu_legal,
            "is_985": self.is_985,
            "is_211": self.is_211,
            "is_985_211": self.is_985_211,
            "is_abroad": self.is_abroad,
            "is_qs200": self.is_qs200
        }
        return _d


class EducationExperienceList:
    def __init__(self):
        # 最近学校
        self.last_school: int = None
        # 最近专业
        self.last_major: str = None
        # 最近学历
        self.last_degree: str = None
        # 最近毕业时间
        self.last_graduation_time: Date = None

        self.total_duration_month: int = 0

        # 学历中断
        self.is_education_interrupt: bool = False
        # 教育信息不合法
        self.is_education_illegal: bool = False

        # 985
        self.is_985: bool = False
        # 211
        self.is_211: bool = False
        self.is_985_211: bool = False
        # qs200
        self.is_qs200: bool = False
        # 海外院校: 0-国内，1-海外
        self.is_abroad: bool = False

        # 第一学历 985
        self.first_is_985: bool = False
        # 第一学历 211
        self.first_is_211: bool = False
        self.first_is_985_211: bool = False
        # 第一学历 qs200
        self.first_is_qs200: bool = False
        # 第一学历 海外
        self.first_is_abroad: bool = False

        self.elements: List[EducationExperience] = []

    def check_985(self):
        self.is_985 = any([i.is_985 for i in self.elements])
        if self.elements:
            self.first_is_985 = self.elements[0].is_985

    def check_211(self):
        self.is_211 = any([i.is_211 for i in self.elements])
        if self.elements:
            self.first_is_211 = self.elements[0].is_211

    def check_985_211(self):
        self.is_985_211 = any([i.is_985_211 for i in self.elements])
        if self.elements:
            self.first_is_985_211 = self.elements[0].is_985_211

    def check_qs200(self):
        self.is_qs200 = any([i.is_qs200 for i in self.elements])
        if self.elements:
            self.first_is_qs200 = self.elements[0].is_qs200

    def check_abroad(self):
        self.is_abroad = any([i.is_abroad for i in self.elements])
        if self.elements:
            self.first_is_abroad = self.elements[0].is_abroad

    def check_legal(self):
        elements_length = len(self.elements)
        max_elements_iter = elements_length - 1

        for i, edu in enumerate(self.elements):
            # 检查每段教育经历合法性
            self.is_education_illegal = all(
                [edu.is_major_legal, edu.is_time_legal, edu.is_edu_legal])

            # 当前开始时间与上一次结束时间 差一年以上
            try:
                if i != max_elements_iter:
                    if (edu.start.year - self.elements[i + 1].end.year) > 1:
                        self.is_education_interrupt = True
            except AttributeError:
                self.is_education_interrupt = True

    def check_total_duration_month(self):
        self.total_duration_month = [
            edu.duration_month for edu in self.elements if edu.duration_month
        ]

    def check_other_info(self):
        for i, edu in enumerate(self.elements):
            edu_weight = school_weight_cooling(i) / 100
            self.elements[i].weight = edu_weight

        if self.elements:
            last_edu = self.elements[0]
            self.last_school = last_edu.school_stand_id
            self.last_major = last_edu.major_stand
            self.last_degree = last_edu.degree_stand
            self.last_graduation_time = last_edu.end

    def fill_info(self):
        self.check_985()
        self.check_211()
        self.check_985_211()
        self.check_qs200()
        self.check_abroad()
        self.check_legal()
        self.check_total_duration_month()
        self.check_other_info()

    def sort(self):
        self.elements.sort(
            key=lambda x: x.start or STANDARD_TIME, reverse=True)

    def compile(self):
        self.sort()
        self.fill_info()

    def to_json(self):
        _d = dict()
        _d['last_school'] = self.last_school
        _d['last_major'] = self.last_major
        _d['last_degree'] = self.last_degree
        _d['last_graduation_time'] = self.last_graduation_time
        _d['total_duration_month'] = self.total_duration_month
        _d['is_education_interrupt'] = self.is_education_interrupt
        _d['is_education_illegal'] = self.is_education_illegal
        _d['is_985'] = self.is_985
        _d['is_211'] = self.is_211
        _d['is_985_211'] = self.is_985_211
        _d['is_qs200'] = self.is_qs200
        _d['is_abroad'] = self.is_abroad
        _d['first_is_985'] = self.first_is_985
        _d['first_is_211'] = self.first_is_211
        _d['first_is_985_211'] = self.first_is_985_211
        _d['first_is_qs200'] = self.first_is_qs200
        _d['first_is_abroad'] = self.first_is_abroad
        _d['edu_list'] = [i.to_json() for i in self.elements]
        return _d

    @classmethod
    def init_from_educations(
            cls, educations: List[Dict]) -> "EducationExperienceList":
        eel = EducationExperienceList()
        for education in educations:
            # school, major, degree, start, end
            ee = EducationExperience(education)
            ee.fill_info()
            eel.elements.append(ee)
        else:
            eel.compile()
        return eel


class WorkExperience:
    def __init__(self, work_info: dict):
        self.is_recent: bool = work_info['is_recent']
        self.is_current: bool = work_info['is_current']
        self.weight: float = 0.

        self.company: str = work_info['company']
        self.company_keyword: str = ""

        self.title: str = work_info['title']
        self.title_parent: str = ""
        self.title_stand: str = work_info['title_stand']
        self.title_level: int = 0
        self.title_prob_weight: Dict[str, float] = work_info[
            'title_prob_weight']

        self.start: Date = work_info['start']
        self.end: Date = work_info['end']

        # 工作时长, 单位为 月
        self.duration_month: int = 0
        # 公司行业
        self.company_industry_list: List[str] = []
        # 行业内知名
        self.is_industry_famous: bool = False
        # # 工作内容分类
        # self.description_industry_list: List[str] = []

        self.description: str = work_info['description']
        # 工作描述技能点
        self.poi_weight: Dict[str, float] = dict()
        # 工作描述 lda
        self.lda_weight: Dict[int, float] = dict()

    def stand_company(self):
        self.company_keyword = extract_company_keyword(self.company)

    def stand_title(self):
        self.title_level = normalize_title_level(self.title)
        self.title_parent = get_title2parent(self.title)

    def stand_start(self):
        self.start = stand_date(self.start)

    def stand_end(self):
        self.end = stand_date(self.end)

    def stand_duration_month(self):
        self.duration_month = get_date_duration_month(self.start, self.end)

    def explore_info(self):
        self.poi_weight = normalize_dict(
            unitvec_dict(
                extract_description_poi_with_title(self.title,
                                                   self.description),
                return_dict=True))
        self.lda_weight = unitvec_dict(
            predict_description_lda_with_title(self.title, self.description),
            return_dict=True)

    def fill_info(self):
        self.stand_company()
        self.stand_title()
        self.stand_start()
        self.stand_end()
        self.stand_duration_month()
        self.explore_info()

    def to_json(self):
        _d = dict()
        _d["is_recent"] = self.is_recent
        _d["is_current"] = self.is_current
        _d["weight"] = self.weight
        _d["company"] = self.company
        _d["company_keyword"] = self.company_keyword
        _d["title"] = self.title
        _d["title_parent"] = self.title_parent
        _d["title_stand"] = self.title_stand
        _d["title_level"] = self.title_level
        _d["title_prob_weight"] = self.title_prob_weight
        _d["start"] = self.start
        _d["end"] = self.end
        _d["duration_month"] = self.duration_month
        _d["company_industry_list"] = self.company_industry_list
        _d["is_industry_famous"] = self.is_industry_famous
        _d["description"] = self.description
        _d["poi_weight"] = self.poi_weight
        _d["lda_weight"] = self.lda_weight
        return _d


class WorkExperienceList:
    def __init__(self):
        # 职位列表
        self.title_weight: DefaultDict[str, float] = defaultdict(float)
        self.title_duration_month: DefaultDict[str, int] = defaultdict(int)

        # 最高职级
        self.title_level_highest: int = 0
        # 总工作时长
        self.total_duration_month: int = 0

        # 公司行业相关
        self.company_industry_duration_month: DefaultDict[
            str, int] = defaultdict(int)

        # 工作描述poi相关
        self.poi_weight: DefaultDict[str, float] = defaultdict(float)
        self.poi_duration_month: DefaultDict[str, int] = defaultdict(int)
        # 工作描述 lda 向量
        self.lda_weight: DefaultDict[int, float] = defaultdict(float)

        # 工作信息是否异常: 工作时间交叉，工作时间断档半年以上
        self.is_work_exp_across: bool = False
        self.is_work_exp_severed: bool = False

        self.elements: List[WorkExperience] = []

    def check_legal(self):
        # 检查合法
        elements_length = len(self.elements)
        max_elements_iter = elements_length - 1

        for i, work in enumerate(self.elements):
            if i != max_elements_iter:
                if not work.start or not self.elements[i + 1].end:
                    self.is_work_exp_severed = True
                    continue
                if work.start < self.elements[i + 1].end:
                    self.is_work_exp_across = True
                if (work.start - self.elements[i + 1].end).months > 6:
                    self.is_work_exp_severed = True

    def fill_info(self):
        title_level_list = list()

        for i, work in enumerate(self.elements):
            work_exp_a = work_exp_weight_cooling(i) / 100
            self.elements[i].weight = work_exp_a

            if work.duration_month:
                duration_month = work.duration_month
            else:
                duration_month = 0
            title_level_list.append(work.title_level)

            # title
            if work.title_stand:
                self.title_weight[work.title_stand] += 1 * work_exp_a
                self.title_duration_month[work.title_stand] += duration_month

            # duration
            self.total_duration_month += duration_month

            # 公司行业
            for t in work.company_industry_list:
                self.company_industry_duration_month[t] += duration_month

            # 工作描述poi
            for k, v in work.poi_weight.items():
                self.poi_weight[k] += v * work_exp_a * 1
                self.poi_duration_month[k] += duration_month

            # 工作描述lda
            for k, v in work.lda_weight.items():
                self.lda_weight[k] += (float(v) * work_exp_a * 1)

        else:
            if title_level_list:
                self.title_level_highest = max(title_level_list)
            else:
                self.title_level_highest = 0

            if len(self.lda_weight) > 1:
                self.lda_weight = unitvec_dict(
                    self.lda_weight, return_dict=True)

            try:
                if len(self.poi_weight) > 1:
                    self.poi_weight = normalize_dict(
                        unitvec_dict(self.poi_weight, return_dict=True))
            except AssertionError as e:
                print(self.poi_weight)
                raise e

    def to_json(self):
        _d = dict()
        _d['title_weight'] = self.title_weight
        _d['title_duration_month'] = self.title_duration_month
        _d['title_level_highest'] = self.title_level_highest
        _d['total_duration_month'] = self.total_duration_month
        _d['company_industry_duration_month'] = self.company_industry_duration_month
        _d['poi_weight'] = self.poi_weight
        _d['poi_duration_month'] = self.poi_duration_month
        _d['lda_weight'] = self.lda_weight
        _d['is_work_exp_across'] = self.is_work_exp_across
        _d['is_work_exp_severed'] = self.is_work_exp_severed
        _d['work_list'] = [i.to_json() for i in self.elements]
        return _d

    def sort(self):
        self.elements.sort(
            key=lambda x: x.start or STANDARD_TIME, reverse=True)

    def compile(self):
        self.sort()
        self.fill_info()

    @classmethod
    def init_from_experiences(cls,
                              experiences: List[dict]) -> "WorkExperienceList":
        wel = WorkExperienceList()
        title_list = [i['title'] for i in experiences]
        title_rst = title_multiple_predict(title_list)

        for i, work in enumerate(experiences):
            # company, title, description, start, end
            _title_pred = title_rst[i]
            try:
                work['title_stand'] = _title_pred[
                    'pred'] if _title_pred else None
                work['title_prob_weight'] = _title_pred[
                    'pred_prob'] if _title_pred else None
                we = WorkExperience(work)
                we.fill_info()
                wel.elements.append(we)
            except Exception as e:
                raise e
        else:
            wel.compile()
        return wel


def index_single_resume(resume_info: dict) -> dict:
    resume_index = dict()

    resume_index['aid'] = resume_info['aid']
    resume_index['tenant'] = resume_info['tenant']
    resume_index['is_deleted'] = resume_info['is_deleted']

    basic_info = BasicInfo.init_from_basic_info(
        resume_info['basic_info']).to_json()
    educations = EducationExperienceList.init_from_educations(
        resume_info["educations"]).to_json()
    experiences = WorkExperienceList.init_from_experiences(
        resume_info["experiences"]).to_json()

    resume_index['basic_info'] = basic_info
    resume_index['educations'] = educations
    resume_index['experiences'] = experiences
    resume_index['custom_tags'] = resume_info['custom_tags']

    return resume_index
