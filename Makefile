.PHONY: clean clean-test clean-pyc docs help build build_base deploy
.DEFAULT_GOAL := help

define BROWSER_PYSCRIPT
import os, webbrowser, sys

try:
	from urllib import pathname2url
except:
	from urllib.request import pathname2url

webbrowser.open("file://" + pathname2url(os.path.abspath(sys.argv[1])))
endef
export BROWSER_PYSCRIPT

define PRINT_HELP_PYSCRIPT
import re, sys

for line in sys.stdin:
	match = re.match(r'^([a-zA-Z_-]+):.*?## (.*)$$', line)
	if match:
		target, help = match.groups()
		print("%-20s %s" % (target, help))
endef
export PRINT_HELP_PYSCRIPT

BROWSER := python -c "$$BROWSER_PYSCRIPT"

help:
	@python -c "$$PRINT_HELP_PYSCRIPT" < $(MAKEFILE_LIST)

clean: clean-pyc clean-test clean-build ## remove all test, coverage and Python artifacts

clean-pyc: ## remove Python file artifacts
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -fr {} +

clean-test: ## remove test and coverage artifacts
	rm -fr .pytest_cache
	rm -fr htmlcov/
	rm -fr dist/
	rm -f .coverage

clean-build: ## remove build artifacts
	rm -fr build/
	rm -fr dist/
	rm -fr .eggs/
	find . -name '*.egg-info' -exec rm -fr {} +
	find . -name '*.egg' -exec rm -f {} +

coverage: ## check code coverage quickly with the default Python
	coverage run --source services -m unittest discover -s tests
	coverage report -m
	coverage html
	$(BROWSER) htmlcov/index.html

docs: ## generate Sphinx HTML documentation, including API docs
	$(MAKE) -C docs clean
	$(MAKE) -C docs html
	$(BROWSER) docs/_build/html/index.html

test: ## run tests quickly with the default Python
	python -m unittest discover -s tests

servedocs: docs ## compile the docs watching for changes
	watchmedo shell-command -p '*.rst' -c '$(MAKE) -C docs html' -R -D .

celery: ## run celery
	celery -E -A sync_server.worker worker

run-all: ## run all AI engine services
	python -m engine_services.company_engine &
	python -m engine_services.description_engine &
	python -m engine_services.jd_engine &
	python -m engine_services.major_engine &
	python -m engine_services.school_engine &
	python -m engine_services.title_engine &
	python -m engine_services.jd_index_engine &
	python -m engine_services.resume_index_engine &
	python -m engine_services.splitter_engine &
	python -m engine_services.resume_recommend_engine &
	python -m engine_services.feedback_engine &
	while true; do trap 'ps -ax | grep engine_services | grep -v grep | cut -b 1-6 | xargs kill -9' 2; done

init: ## init development environment
	conda-env create -f environment.yml
	docker volume create mongodata
	docker run --name mongodb -p 27017:27017 -v mongodata:/data/db -d mongo
	bash -c "source activate ai-service && pre-commit install"

release: ## package and push a release
	bumpversion patch
	git push
	git push --tags

# ======================
# Build & Deploy Related
# ======================

# Docker registy & image & container settings
PUBLIC_REGISTRY  = registry.cn-hangzhou.aliyuncs.com/gllue
PRIVATE_REGISTRY = registry-vpc.cn-hangzhou.aliyuncs.com/gllue

DOCKER_IMAGE_NAME      =  ai_service
DOCKER_BASE_IMAGE_NAME =  ai_service_base
DOCKER_CONTAINER_NAME  =  ai_service

# Exposed environment to control build & deploy process
IMAGE_TAG              ?= $(shell git rev-parse HEAD)
BASE_IMAGE_TAG         ?= v1
DEPLOY_TAG             ?= latest
DEPLOY_ENV_TYPE        ?= Production
CONFIG_PATH            ?= /opt/web/ai_service_config.json
MODEL_GIT_PATH         ?= /ai_models
MODEL_GIT_REF          ?= master
CONTAINER_NAME_SUFFIX  ?= 
DOCKER_RUN_COMMAND     ?= echo helloworld
DOCKER_BUILD_ARGS      ?=
IF_UPLOAD_TO_REGISTRY  ?= 0 # Determine whether upload after build

# For DRY
PUBLIC_IMAGE_FULL_NAME       = $(PUBLIC_REGISTRY)/$(DOCKER_IMAGE_NAME):$(IMAGE_TAG)
PRIVATE_IMAGE_FULL_NAME      = $(PRIVATE_REGISTRY)/$(DOCKER_IMAGE_NAME):$(IMAGE_TAG)
PUBLIC_IMAGE_LATEST_NAME     = $(PUBLIC_REGISTRY)/$(DOCKER_IMAGE_NAME):latest
PRIVATE_IMAGE_LATEST_NAME    = $(PRIVATE_REGISTRY)/$(DOCKER_IMAGE_NAME):latest
PUBLIC_BASE_IMAGE_FULL_NAME  = $(PUBLIC_REGISTRY)/$(DOCKER_BASE_IMAGE_NAME):$(BASE_IMAGE_TAG)
PRIVATE_BASE_IMAGE_FULL_NAME = $(PRIVATE_REGISTRY)/$(DOCKER_BASE_IMAGE_NAME):$(BASE_IMAGE_TAG)
DEPLOY_IMAGE_FULL_NAME       = $(PUBLIC_REGISTRY)/$(DOCKER_IMAGE_NAME):$(DEPLOY_TAG)
DEPLOY_CONTAINER_NAME        = $(DOCKER_CONTAINER_NAME)_${CONTAINER_NAME_SUFFIX}


build_base: ## build base docker image & push to registry IF_UPLOAD_TO_REGISTRY is set
	docker build $(DOCKER_BUILD_ARGS) -t $(PUBLIC_BASE_IMAGE_FULL_NAME) -f deploy/Dockerfile.base .
	docker image tag $(PUBLIC_BASE_IMAGE_FULL_NAME) $(PRIVATE_BASE_IMAGE_FULL_NAME) 
	@if [ $(IF_UPLOAD_TO_REGISTRY) != 0 ]; then \
		docker push $(PUBLIC_BASE_IMAGE_FULL_NAME); \
		docker push $(PRIVATE_BASE_IMAGE_FULL_NAME) || true; \
	fi;

build: ## build docker image & push to registry is IF_UPLOAD_TO_REGISTRY is set
	docker build $(DOCKER_BUILD_ARGS) -t $(PUBLIC_IMAGE_FULL_NAME) -f deploy/Dockerfile.final .
	docker image tag $(PUBLIC_IMAGE_FULL_NAME) $(PUBLIC_IMAGE_LATEST_NAME)
	docker image tag $(PUBLIC_IMAGE_FULL_NAME) $(PRIVATE_IMAGE_FULL_NAME)
	docker image tag $(PUBLIC_IMAGE_FULL_NAME) $(PRIVATE_IMAGE_LATEST_NAME)
	@if [ $(IF_UPLOAD_TO_REGISTRY) != 0 ]; then \
		docker push $(PUBLIC_IMAGE_FULL_NAME); \
		docker push $(PUBLIC_IMAGE_LATEST_NAME); \
		docker push $(PRIVATE_IMAGE_FULL_NAME) || true; \
		docker push $(PRIVATE_IMAGE_LATEST_NAME) || true; \
	fi;


