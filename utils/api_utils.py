def paging_items(items: list, size: int = 20, offset: int = 0):
    if not size:
        size = 20
    if not offset:
        offset = 0
    total = len(items)
    items = items[offset:offset + size]
    return items, total, size
