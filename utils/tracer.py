from sys import exc_info
from traceback import format_exc

from grpcalchemy import current_rpc
from grpcalchemy.config import Config as gRRCAlchemyConfig
from grpcalchemy.ctx import AppContext
from grpcalchemy.globals import LocalProxy, LocalStack
from jaeger_client import Config
from jaeger_client.tracer import Span, Tracer

from utils import message_to_dict

_span_ctx_stack = LocalStack()


def _find_span():
    return _span_ctx_stack.top


current_span: Span = LocalProxy(_find_span)


class AITracer(AppContext):
    tracer: Tracer

    def init_tracer(self, config: gRRCAlchemyConfig):
        config = Config(
            config={  # usually read from some yaml config
                'local_agent': {
                    'reporting_host': config['JAEGER_HOST'],
                },
                'sampler': {
                    'type': 'const',
                    'param': 1,
                },
                'logging': False,
            },
            service_name='ai-service',
            validate=True,
        )
        self.tracer: Tracer = config.initialize_tracer()

    def close(self):
        self.tracer.close()

    def start_span(self,
                   operation_name=None,
                   child_of=None,
                   references=None,
                   tags=None,
                   start_time=None):
        span = self.tracer.start_span(operation_name=operation_name,
                                      child_of=child_of,
                                      references=references,
                                      tags=tags,
                                      start_time=start_time)
        _span_ctx_stack.push(span)
        return span

    def push(self):
        super().push()
        span = self.start_span(f'{current_rpc.server_name}/{current_rpc.name}')
        span.log_kv(message_to_dict(current_rpc.origin_request))
        _span_ctx_stack.push(span)

    def pop(self):
        exc_type, exc_val, exc_tb = exc_info()
        if any([exc_type, exc_val, exc_tb]):
            current_span.set_tag('error', True)
            current_span.set_tag('exception', format_exc())
        current_span.finish()
        _span_ctx_stack.pop()
        super().pop()
