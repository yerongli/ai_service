import requests
from gllue_rpc.http_channel import HttpChannel

from config import ai_service_config


class RequestException(Exception):
    ...


http_channel = HttpChannel(ai_service_config)

DEFAULT_FILE_NAME = "any"


@http_channel.after_call
def after_call(response: requests.Response) -> dict:
    if not response.ok:
        raise RequestException(f"{response.request.url}:{response.status_code}")
    return response.json()


def call_ocr(base64_data: str, type: str) -> str:
    result = http_channel.call_resume_extractor(
        "ocr_img", {"content": base64_data, "ext": "jpeg", "type_here": type}
    )
    if result.get("status"):
        return result.get("result", {}).get("target_result", "")
    else:
        return ""


def call_extract(base64_data: str) -> dict:
    result = http_channel.call_resume_extractor(
        "extract",
        json={
            "file_content": base64_data,
            "file_name": DEFAULT_FILE_NAME,
            "is_base64": True,
            "extra_param": '{"is_zlib_compress_data":true}',
        },
    )
    if result.get("status"):
        return _mapping_to_resume_info(result.get("result", {}))
    else:
        return _mapping_to_resume_info({})


def _mapping_to_resume_info(extract_result: dict) -> dict:
    basic_info = {
        "name": extract_result.get("name", ""),
        "gender": extract_result.get("gender", ""),
        "mobile": extract_result.get("mobile", ""),
        "email": extract_result.get("email", ""),
        "linkedin": extract_result.get("linkedin", ""),
        "birth_day": extract_result.get("dateOfBirth", ""),
    }
    educations = []
    for education in extract_result.get("education", []) or []:
        educations.append(
            {
                "school": education.get("school", ""),
                "major": education.get("major", ""),
                "degree": education.get("degree", ""),
                "start": education.get("start", ""),
                "end": education.get("end", ""),
            }
        )
    experiences = []
    for experience in extract_result.get("experiences", []) or []:
        experiences.append(
            {
                "company": experience.get("company", ""),
                "title": experience.get("title", ""),
                "description": experience.get("description", ""),
                "start": experience.get("start", ""),
                "end": experience.get("end", ""),
            }
        )
    return {
        "basic_info": basic_info,
        "educations": educations,
        "experiences": experiences,
    }
