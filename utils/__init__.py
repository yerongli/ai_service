from functools import partial

from google.protobuf.json_format import MessageToDict


def message_to_dict(message,
                    including_default_value_fields=True,
                    preserving_proto_field_name=True,
                    use_integers_for_enums=False) -> dict:
    return MessageToDict(message, including_default_value_fields,
                         preserving_proto_field_name, use_integers_for_enums)


class LazyLoad:
    def __init__(self, func: partial):
        self._func = func
        self._attr = None

    @property
    def _load(self):
        if self._attr is None:
            self._attr = self._func()
        return self._attr

    def __getattr__(self, item):
        return getattr(self._load, item)

    __len__ = lambda x: len(x._load)
