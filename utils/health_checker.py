from pprint import pprint

from service_checker import Snooper
from service_checker.contrib import mongo, redis

from config import ai_service_config

snooper = Snooper()

redis.install(snooper=snooper, host=ai_service_config['CELERY_BROKER_URL'])
mongo.install(
    snooper=snooper,
    db=ai_service_config['MONGO_DB'],
    connect_kwargs=dict(
        host=ai_service_config['MONGO_HOST'],
        port=ai_service_config['MONGO_PORT'],
        username=ai_service_config['MONGO_USER'],
        password=ai_service_config['MONGO_PASSWORD'],
        authentication_source=ai_service_config['MONGO_AUTH_DB'],
        serverSelectionTimeoutMS=1000))

if __name__ == '__main__':
    pprint(snooper.check())
