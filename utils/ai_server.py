import logging

import sentry_sdk
from grpcalchemy import Server

from utils.tracer import AITracer


class ai_server(Server):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.listener("before_server_start", before_server_start)
        self.listener("after_server_stop", after_server_stop)
        if self.config["SENTRY_DSN"]:
            sentry_sdk.init(dsn=self.config["SENTRY_DSN"])
        self.app_context = AITracer(self)
        self.logger.setLevel(logging.INFO)


def before_server_start(app: ai_server):
    app.app_context.init_tracer(config=app.config)


def after_server_stop(app: ai_server):
    app.app_context.close()
