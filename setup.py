"""The setup script."""

from setuptools import find_packages, setup

readme = '''
AI Service Proto Buffer File

文档
=============

* Documentation: `TODO`
'''

requirements = ["grpcio", "protobuf", "grpcio-tools"]

setup_requirements = []

test_requirements = []

setup(
    author="Gullue",
    author_email='contact@gllue.com',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ],
    description="The Proto buffer files of AI Service",
    install_requires=requirements,
    license="MIT license",
    long_description=readme,
    include_package_data=True,
    keywords='AI Service Proto Files',
    name='ai_service_protos',
    packages=['ai_service_protos'],
    setup_requires=setup_requirements,
    test_suite='tests',
    tests_require=test_requirements,
    url='https://gitlab.gllue.com/tony.li/ai_service',
    python_requires='>=3.6.0',
    version='1.3.0',
    zip_safe=False,
)
