import logging

from ai_service_protos import base_pb2
from database.redis.cache import get_company_cache, set_company_cache
from engine_services.company_engine.channel import company_keyword_engine_stub


def extract_company_keyword(company: str) -> str:
    # 提取公司名称的关键词
    if not company:
        return ""

    cached_result = get_company_cache(company)
    if cached_result:
        return cached_result

    try:
        response = company_keyword_engine_stub.ExtractKeyword(
            base_pb2.NormalTextRequest(text=company))
    except Exception:
        logging.exception("公司关键字提取失败", extra={"company": company})
        return ""

    set_company_cache(company, response.text)
    return response.text
