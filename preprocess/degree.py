import re
from typing import Union

from gllue_ai_libs.angelo_tool.nlp.base_func import traditional2simplified

MBA_REGEX = re.compile('(mba)')
DOCTOR_REGEX = re.compile('(博士)|(doctor)|(phd)')
MASTER_REGEX = re.compile('(硕士)|(master)|(学硕)|(专硕)|(研究生)|(ms)|(sm)|(am)|(ma)')
BACHELOR_REGEX = re.compile(
    '(本科)|(bachelor)|(大学)|(学士)|(diploma)|(bs)|(sb)|(ab)|(ba)')
COLLEGE_REGEX = re.compile('(专科)|(大专)|(college)|(中专)|(中技)|(special)')
HIGH_REGEX = re.compile('(高中)|(初中)')


def normalize_degree(degree: str) -> str:
    # 预处理 degree
    if not degree:
        return ""

    degree = degree.strip()
    degree = degree.lower()
    degree = traditional2simplified(degree)

    if re.findall(MBA_REGEX, degree):
        return 'MBA'
    if re.findall(DOCTOR_REGEX, degree):
        return '博士'
    if re.findall(MASTER_REGEX, degree):
        return '硕士'
    if re.findall(BACHELOR_REGEX, degree):
        return '本科'
    if re.findall(COLLEGE_REGEX, degree):
        return '专科'
    if re.findall(HIGH_REGEX, degree):
        return '高中'
    return ""


_degree2id = {
    'MBA': 6,
    '博士': 5,
    '硕士': 4,
    '本科': 3,
    '专科': 2,
    '高中': 1,
}
num_degree = len(_degree2id) + 1


def get_degree_id(degree: str) -> int:
    return _degree2id.get(degree, 0)
