import re
from typing import List

RE_BLANK = re.compile('\s')
RE_NON_NUM = re.compile('保密|面议|面谈')
RE_SEP_NUM = re.compile('\d*\.?\d+|(?<=\d)\D+')
RE_SEP_SUFFIX = re.compile(
    '^((k|thousand|千)|(万))?(.{0,10}?((个月)|(月|month)|(年|year))|)(.{0,10}?([-到至])?).*$',
    re.IGNORECASE)


def normalize_default(content: List[str]):
    c_set = set(content)
    c_list = list(
        filter(lambda _: _ in c_set and not c_set.remove(_), content))
    return c_list


def normalize_salary(content: str) -> dict:
    content = [content]

    res_lst = []
    content = normalize_default(content)
    for salary in content:
        res = {
            'monthly': [],
            'yearly': [],
            'period': 12,
            'non_num': None,
        }
        salary = RE_BLANK.sub('', salary)
        sal_lst = RE_SEP_NUM.findall(salary)
        if not sal_lst:
            non_num = RE_NON_NUM.search(salary)
            if non_num:
                res['non_num'] = non_num.group()
                if not res_lst:
                    res_lst.append(res)
        else:
            sal_seq = []
            for i in range(0, len(sal_lst), 2):
                elem = {
                    'num': float(sal_lst[i]),
                    'type': -2,  # {month_count: -1, month: 0, year: 1}
                    'multiple': 0,
                    'if_joint': 0
                }
                if i < len(sal_lst) - 1:
                    suffix = RE_SEP_SUFFIX.findall(sal_lst[i + 1])[0]
                    if suffix[1]:
                        elem['multiple'] = 1000
                    elif suffix[2]:
                        elem['multiple'] = 10000
                    if suffix[5]:
                        elem['type'] = -1
                    elif suffix[6]:
                        elem['type'] = 0
                    elif suffix[7]:
                        elem['type'] = 1
                    if suffix[9]:
                        elem['if_joint'] = 1
                sal_seq.append(elem)
            sal_rest = []
            for i, sal in enumerate(sal_seq):
                if sal['if_joint'] > 0 and i < len(sal_seq) - 1:
                    sal_seq[i + 1]['if_joint'] = -1
                    if sal['multiple'] > sal_seq[i + 1]['multiple']:
                        sal_seq[i + 1]['multiple'] = sal['multiple']
                    elif sal['multiple'] < sal_seq[i + 1]['multiple']:
                        sal['multiple'] = sal_seq[i + 1]['multiple']
                    if sal['type'] > sal_seq[i + 1]['type']:
                        sal_seq[i + 1]['type'] = sal['type']
                    elif sal['type'] < sal_seq[i + 1]['type']:
                        sal['type'] = sal_seq[i + 1]['type']
                if sal['type'] == -2:
                    sal['num'] = sal['num'] * sal['multiple'] if sal[
                        'multiple'] else sal['num']
                    sal['multiple'] = 0
                    sal_rest.append(sal)
                elif sal['type'] == -1 and sal['num']:
                    res['period'] = sal['num']
                elif sal['type'] >= 0:
                    key = ('monthly', 'yearly')[sal['type']]
                    sal_num = sal['num'] * sal['multiple'] if sal[
                        'multiple'] else sal['num']
                    if sal['if_joint'] > 0:
                        res[key] = [sal_num, sal_num]
                    elif sal['if_joint'] < 0:
                        if sal_num > res[key][1]:
                            res[key][1] = sal_num
                    else:
                        res[key] = [sal_num]
            for sal in sal_rest:
                if not res['monthly'] and not res['yearly']:
                    key = ('monthly', 'yearly')[sal['num'] >= 100000]
                    if sal['if_joint'] > 0:
                        res[key] = [sal['num'], sal['num']]
                    else:
                        res[key] = [sal['num']]
                elif sal['if_joint'] < 0:
                    key = ('monthly', 'yearly')[bool(res['yearly'])]
                    if sal['num'] > res[key][-1]:
                        res[key][-1] = sal['num']
            if res['monthly'] and res['yearly']:
                res['yearly'] = [num * res['period'] for num in res['monthly']]
            elif res['monthly']:
                res['yearly'] = [num * res['period'] for num in res['monthly']]
            elif res['yearly']:
                res['monthly'] = [num / res['period'] for num in res['yearly']]
            else:
                continue
            if_append = True
            for _r in res_lst:
                if _r['non_num']:
                    res_lst = [res]
                elif res['monthly'][0] - _r['monthly'][0] <= 1000:
                    if_append = False
            if if_append:
                res_lst.append(res)
    if res_lst:
        return res_lst[0]
    else:
        return None


def get_norm_monthly_salary(salary):
    if not salary:
        return 0
    salary = normalize_salary(salary)
    if salary and salary['monthly']:
        return salary['monthly'][0]
    else:
        return 0
