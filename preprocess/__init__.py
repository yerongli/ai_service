import pendulum
from functools import partial
from gllue_ai_libs.angelo_tool.analyze.algorithm import get_newton_law_of_cooling_a, newton_law_of_cooling

# 工作经验的系数
work_exp_weight_cooling_a = get_newton_law_of_cooling_a(100, 1, 0, 10)
work_exp_weight_cooling = partial(newton_law_of_cooling, 100, 0,
                                  work_exp_weight_cooling_a)

# 教育经验自身系数
school_weight_cooling_a = get_newton_law_of_cooling_a(100, 1, 0, 10)
school_weight_cooling = partial(newton_law_of_cooling, 100, 0,
                                school_weight_cooling_a)

# 教育经验较工作经验的系数
school_exp_weight_cooling_a = get_newton_law_of_cooling_a(100, 1, 0, 50)
school_exp_weight_cooling = partial(newton_law_of_cooling, 100, 0,
                                    school_exp_weight_cooling_a)


def normalize_dict(vec_dict: dict, return_dict=True):
    if not vec_dict:
        return vec_dict
    keys = vec_dict.keys()
    values = vec_dict.values()
    maxl = max(values)
    minl = min(values)
    diffl = (maxl - minl)
    if diffl == 0:
        return vec_dict

    rst = [(i - minl) / diffl for i in values]
    if return_dict:
        rst = dict(zip(keys, rst))
    else:
        rst = list(zip(keys, rst))
    return rst


class ToJsonMixin:
    __json_fields__: tuple = ()

    @staticmethod
    def to_json_help(ob):
        result = dict()
        for k in ob.__json_fields__:
            v = getattr(ob, k)
            if isinstance(v, pendulum.Date) or isinstance(
                    v, pendulum.DateTime):
                v = str(v)
            result[k] = v
        return result

    def to_json(self):
        return self.to_json_help(self)


class FillInfo:
    def fill_info(self):
        pass
