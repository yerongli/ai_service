import logging

from google.protobuf.json_format import MessageToDict

from ai_service_protos import base_pb2, resume_index_pb2
from engine_services.resume_index_engine.channel import \
    resume_process_engine_stub


def ident_update_resume(ident: str):
    try:
        request = base_pb2.NormalIdentRequest(ident=ident)
        response = resume_process_engine_stub.IdentUpdateResume(request)
        result = MessageToDict(
            response,
            including_default_value_fields=True,
            preserving_proto_field_name=True)
        return result
    except Exception as e:
        logging.exception(
            f"update resume_index fail [{e}]", extra={"ident": ident})
        return None


def index_single_resume(resume_info: dict):
    try:
        request = resume_index_pb2.ResumeMessage(**resume_info)
        response = resume_process_engine_stub.IndexSingleResume(
            request, timeout=5)
        result = MessageToDict(
            response,
            including_default_value_fields=True,
            preserving_proto_field_name=True)
        return result
    except Exception as e:
        logging.exception(
            f"index resume_index fail [{e}]",
            extra={"resume_index": resume_info})
        return None


def delete_single_resume(resume_info: dict):
    try:
        request = base_pb2.NormAidTenantRequest(**resume_info)
        response = resume_process_engine_stub.DeleteSingleResume(request)
        result = MessageToDict(
            response,
            including_default_value_fields=True,
            preserving_proto_field_name=True)
        return result
    except Exception as e:
        logging.exception(
            f"delete resume_index fail [{e}]",
            extra={"resume_index": resume_info})
        return None


def all_reindex_resume(type_):
    try:
        request = base_pb2.NormalTextRequest(text=type_)
        response = resume_process_engine_stub.ReindexAllResume(request)
        result = MessageToDict(
            response,
            including_default_value_fields=True,
            preserving_proto_field_name=True)
        return result
    except Exception as e:
        logging.exception(f"reindex all resume_index fail [{e}]")
        return None


def tenant_reindex_resume(tenant: str):
    try:
        request = base_pb2.NormTenantRequest(tenant=tenant)
        response = resume_process_engine_stub.ReindexResumeUseTenant(request)
        result = MessageToDict(
            response,
            including_default_value_fields=True,
            preserving_proto_field_name=True)
        return result
    except Exception as e:
        logging.exception(
            f"reindex tenant resume_index fail [{e}]",
            extra={"tenant": tenant})
        return None
