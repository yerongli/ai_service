import logging
import re
from typing import List

from google.protobuf.json_format import MessageToDict

from database.redis.cache import get_title_cache, set_title_cache
from grpc import insecure_channel
from ai_service_protos import base_pb2, title_multi_classify_engine_pb2_grpc
from engine_services.description_engine.channel import \
    title_multi_classify_engine_stub
from config import ai_service_config

LEVEL_1_REGEX = re.compile(
    r'(初级)|(junior)|(专员)|(助理)|(assistant)|(实习)|(见习)|(intern)|(兼职)',
    flags=re.IGNORECASE)
LEVEL_2_REGEX = re.compile(
    r'(中级)|(主管)|(supervisor)|(经理)|(manager)|(组长)|(高级)|(资深)|(senior)|(主任)|(负责人)|(部长)',
    flags=re.IGNORECASE)
LEVEL_3_REGEX = re.compile(
    r'(总监)|(director)|(专家)|(expert)|(总经理)|(副总)|(创始人)|(董事)|(founder)|(chairman)|'
    r'(合伙人)|(vp)|(c.o)|(总裁)|(president)|(首席)|(chief)',
    flags=re.IGNORECASE)


def normalize_title(title: str) -> str:
    if not title:
        return ""

    cached_result = get_title_cache(title)
    if cached_result:
        return cached_result

    try:
        result = title_multi_classify_engine_stub.Predict(
            base_pb2.NormalTextRequest(text=title)).text
    except Exception:
        logging.exception('正则化职位名字失败', extra={'title': title})
        return ""

    set_title_cache(title, result)
    return result


def predict_title_prob(title: str) -> List[str]:
    if not title:
        return []
    try:
        result = list(
            title_multi_classify_engine_stub.PredictProbTop(
                base_pb2.NormalTextRequest(text=title)).result)
        if not result:
            result = title_multi_classify_engine_stub.Predict(
                base_pb2.NormalTextRequest(text=title)).text
            if result:
                processed_title = [result]
            else:
                processed_title = []
        else:
            processed_title = result
    except Exception:
        logging.exception('职位名称prob失败', extra={'title': title})
        return []
    return processed_title


def title_multiple_predict_(title_list: list):
    cache_list = []
    for title in title_list:
        if title:
            t = get_title_cache(title)
            if t and isinstance(t, dict):
                cache_list.append(t)
            else:
                break
        else:
            cache_list.append(None)
    _title_list = []
    for title in title_list:
        _title_list.append(title if title else "")
    title_list = _title_list

    if len(cache_list) == len(title_list):
        # 如果全部由缓存，则返回缓存
        return cache_list
    else:
        # 如果存在无缓存，则全部预测。因为一个batch预测耗时差不多。
        request = base_pb2.NormTextListMessage(text_list=title_list)
        response = title_multi_classify_engine_stub.MultiplePredict(request)
        result = MessageToDict(
            response,
            including_default_value_fields=True,
            preserving_proto_field_name=True)
        items: list = result['items']

        # 更新缓存
        for i, v in enumerate(title_list):
            set_title_cache(v, items[i])
        return items


# 预留
def title_multiple_predict(title_list: list):
    cache_list = [0] * len(title_list)
    predict_list = []
    for i, title in enumerate(title_list):
        if not title:
            cache_list[i] = None
        else:
            t = get_title_cache(title)
            if t and isinstance(t, dict):
                cache_list[i] = t
            else:
                predict_list.append(title)
    if predict_list:
        request = base_pb2.NormTextListMessage(text_list=predict_list)
        response = title_multi_classify_engine_stub.MultiplePredict(request)
        result = MessageToDict(
            response,
            including_default_value_fields=True,
            preserving_proto_field_name=True)
        items: list = result['items']
        for i, v in enumerate(predict_list):
            set_title_cache(v, items[i])

        for i, v in enumerate(cache_list):
            if v == 0:
                cache_list[i] = items.pop(0)

    return cache_list


def normalize_title_level(title) -> int:
    if not title:
        return 0
    if LEVEL_1_REGEX.findall(title, ):
        return 1
    elif LEVEL_2_REGEX.findall(title):
        return 2
    elif LEVEL_3_REGEX.findall(title):
        return 3
    else:
        return 1


def get_title2id_dict() -> dict:
    with insecure_channel(
            target=ai_service_config['DESCRIPTION_ENGINE_ADDRESS']
    ) as description_engine_channel:
        tmces = title_multi_classify_engine_pb2_grpc.title_multi_classify_engineStub(
            description_engine_channel)
        request = base_pb2.NormalTextRequest()
        response = tmces.GetTitle2Id(request)
        result = MessageToDict(
            response,
            including_default_value_fields=True,
            preserving_proto_field_name=True)
        return result['result']


def get_title_parent2id_dict() -> dict:
    with insecure_channel(
            target=ai_service_config['DESCRIPTION_ENGINE_ADDRESS']
    ) as description_engine_channel:
        tmces = title_multi_classify_engine_pb2_grpc.title_multi_classify_engineStub(
            description_engine_channel)
        request = base_pb2.NormalTextRequest()
        response = tmces.GetTitleParent2Id(request)
        result = MessageToDict(
            response,
            including_default_value_fields=True,
            preserving_proto_field_name=True)
        return result['result']


def get_title2parent_dict() -> dict:
    with insecure_channel(
            target=ai_service_config['DESCRIPTION_ENGINE_ADDRESS']
    ) as description_engine_channel:
        tmces = title_multi_classify_engine_pb2_grpc.title_multi_classify_engineStub(
            description_engine_channel)
        request = base_pb2.NormalTextRequest()
        response = tmces.GetTitle2Parent(request)
        result = MessageToDict(
            response,
            including_default_value_fields=True,
            preserving_proto_field_name=True)
        return result['result']


_title2id = get_title2id_dict()
_title_parent2id = get_title_parent2id_dict()
_title2parent = get_title2parent_dict()

num_titles = len(_title2id) + 1
num_title_parent = len(_title_parent2id) + 1
num_title_level = 4


def get_title_id(title: str) -> int:
    return _title2id.get(title, 0)


def get_title_parent_id(title_parent: str) -> int:
    return _title_parent2id.get(title_parent, 0)


def get_title2parent(title: str) -> str:
    return _title2parent.get(title, "")
