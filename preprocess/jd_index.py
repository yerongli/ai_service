import logging

from google.protobuf.json_format import MessageToDict

from ai_service_protos import base_pb2
from engine_services.jd_index_engine.channel import jd_process_engine_stub


def ident_update_jd_info(ident: str):
    try:
        request = base_pb2.NormalIdentRequest(ident=ident)
        response = jd_process_engine_stub.IdentUpdateJdInfo(request)
        result = MessageToDict(
            response,
            including_default_value_fields=True,
            preserving_proto_field_name=True)
        return result
    except Exception:
        logging.exception("update jd_info fail", extra={"ident": ident})
        return None


def delete_single_jd_info(jd_info: dict):
    try:
        request = base_pb2.NormAidTenantRequest(**jd_info)
        response = jd_process_engine_stub.DeleteSingleJdInfo(request)
        result = MessageToDict(
            response,
            preserving_proto_field_name=True,
            including_default_value_fields=True)
        return result
    except Exception:
        logging.exception("delete jd_info fail", extra={"jd_info": jd_info})
        return None


def reindex_all_jd():
    try:
        request = base_pb2.NormTenantRequest()
        response = jd_process_engine_stub.ReindexAllJd(request)
        result = MessageToDict(
            response,
            preserving_proto_field_name=True,
            including_default_value_fields=True)
        return result
    except Exception as e:
        logging.exception("reindex all jd_index fail", extra={"error": str(e)})
        return None


def reindex_jd_use_tenant(tenant: str):
    try:
        request = base_pb2.NormTenantRequest(tenant=tenant)
        response = jd_process_engine_stub.ReindexJdUseTenant(request)
        result = MessageToDict(
            response,
            including_default_value_fields=True,
            preserving_proto_field_name=True)
        return result
    except Exception:
        logging.exception(
            f"reindex tenant [{tenant}] jd_index fail",
            extra={"tenant": tenant})
        return None
