import datetime
from typing import Union

import pendulum
import pendulum.exceptions
from pendulum.datetime import Date, DateTime

STANDARD_TIME = Date(year=1970, month=1, day=1)


def stand_time(t) -> Union[DateTime, None]:
    if not t:
        return None
    if isinstance(t, str):
        try:
            return pendulum.parse(t)
        except pendulum.exceptions.ParserError:
            if '至今' in t:
                return pendulum.now()
            else:
                return None
    elif isinstance(t, datetime.datetime):
        return pendulum.DateTime(year=t.year, month=t.month, day=t.day)
    else:
        return None


def stand_date(t) -> Union[Date, None]:
    if not t:
        return None
    if isinstance(t, str):
        try:
            return pendulum.parse(t).date()
        except pendulum.exceptions.ParserError:
            if '至今' in t:
                return pendulum.now().date()
            else:
                return None
    elif isinstance(t, datetime.date):
        return pendulum.Date(year=t.year, month=t.month, day=t.day)
    else:
        return None


def get_date_duration_month(start: Date, end: Date):
    try:
        t = (end or pendulum.now().date()) - start
        duration_month = t.in_months()
        return duration_month
    except Exception as e:
        return None
