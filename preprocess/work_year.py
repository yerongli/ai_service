WORK_YEAR_MAPPING = {
    "work_year_0": {
        "gte": 0,
        "lte": 99
    },
    "work_year_1": {
        "gte": 0,
        "lte": 1
    },
    "work_year_2": {
        "gte": 1,
        "lte": 3
    },
    "work_year_3": {
        "gte": 3,
        "lte": 5
    },
    "work_year_4": {
        "gte": 5,
        "lte": 10
    },
    "work_year_5": {
        "gte": 10,
        "lte": 99
    },
    "work_year_6": {
        "gte": 10,
        "lte": 99
    }
}
