import logging
from typing import List, Set

from google.protobuf.json_format import MessageToDict

from ai_service_protos import base_pb2, major_pb2
from database.redis.cache import get_major_cache, set_major_cache
from engine_services.description_engine.channel import \
    major_classify_engine_stub
from grpc import insecure_channel
from ai_service_protos import major_classify_engine_pb2_grpc
from config import ai_service_config


def normalize_major(major: str, degree: str) -> str:
    if not major:
        return '未知专业'

    cache_name = str(degree) + '-' + str(major)
    cached_result = get_major_cache(cache_name)
    if cached_result:
        return cached_result

    try:
        processed_name = process_major_with_degree(major=major, degree=degree)
    except Exception:
        logging.exception('正则化专业名字失败', extra={'major': major})
        return '未知专业'

    set_major_cache(cache_name, processed_name)
    return processed_name


def process_major_with_degree(major: str, degree: str = '本科') -> str:
    request = major_pb2.CellPredictRequest(level="", major=major)

    degree_dict = {
        "专科": "专科",
        "本科": "本科",
        "硕士": "硕士",
        "博士": "硕士",
        "MBA": "硕士",
        "": "本科",
        None: "本科"
    }

    degree = degree_dict.get(degree)
    if degree:
        request.level = degree
        return major_classify_engine_stub.CellPredict(request).result
    else:
        return '未知专业'


def lowest_degree_major_predict(major_list: List,
                                lowest_degree: str) -> Set[str]:
    if not major_list:
        major_list = []
    if not lowest_degree:
        lowest_degree = ""
    request = major_pb2.LowestDegreeMajorPredictRequest(
        major_list=major_list, lowest_degree=lowest_degree)
    response = major_classify_engine_stub.LowestDegreeMajorPredict(request)
    result = set(response.result)
    return result


def get_major2id_dict() -> dict:
    with insecure_channel(
            target=ai_service_config['DESCRIPTION_ENGINE_ADDRESS']
    ) as description_engine_channel:
        mces = major_classify_engine_pb2_grpc.major_classify_engineStub(
            description_engine_channel)
        request = base_pb2.NormalTextRequest()
        response = mces.GetMajor2Id(request)
        result = MessageToDict(
            response,
            including_default_value_fields=True,
            preserving_proto_field_name=True)
        return result['result']


def get_major_parent2id_dict() -> dict:
    with insecure_channel(
            target=ai_service_config['DESCRIPTION_ENGINE_ADDRESS']
    ) as description_engine_channel:
        mces = major_classify_engine_pb2_grpc.major_classify_engineStub(
            description_engine_channel)
        request = base_pb2.NormalTextRequest()
        response = mces.GetMajor2Id(request)
        result = MessageToDict(
            response,
            including_default_value_fields=True,
            preserving_proto_field_name=True)
        return result['result']


_major2id: dict = get_major2id_dict()
_major_parent2id: dict = get_major_parent2id_dict()

num_majors = len(_major2id) + 1
num_major_parents = len(_major_parent2id) + 1


def get_major2id(major):
    return _major2id.get(major, 0)


def get_major_parent2id(parent):
    return _major_parent2id.get(parent, 0)
