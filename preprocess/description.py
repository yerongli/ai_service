import logging
from typing import Dict

from google.protobuf.json_format import MessageToDict

from ai_service_protos import base_pb2
from engine_services.description_engine.channel import (
    extract_poi_engine_stub, lda_predict_engine_stub)
from engine_services.jd_engine.channel import jd_engine_stub


def extract_description_poi_with_title(title: str,
                                       description: str) -> Dict[str, float]:
    try:
        request = base_pb2.NormalTitleDescriptionRequest(
            title=title, description=description)

        poi_weight = extract_poi_engine_stub.DescriptionPoiSortWithTitle(
            request).result
        poi_weight = {i.index: i.value for i in poi_weight}
        return poi_weight
    except Exception:
        logging.exception(
            '提取poi错误', extra={
                'title': title,
                'description': description
            })
        return dict()


def predict_description_lda_with_title(title: str,
                                       description: str) -> Dict[int, float]:
    if not title:
        return {}
    try:
        request = base_pb2.NormalTitleDescriptionRequest(
            title=title, description=description)
        lda_vector = lda_predict_engine_stub.GetTextLda(request).result
        lda_vector = {i.index: i.value for i in lda_vector}
        return lda_vector
    except Exception:
        logging.exception(
            f'lda错误]', extra={
                'title': title,
                'description': description,
            })
        return dict()


def extract_jd_content_info(content: str) -> dict:
    jd_content_info = jd_engine_stub.ExtractContentInfo(
        base_pb2.NormalTextRequest(text=content))
    jd_content_info = MessageToDict(
        jd_content_info,
        including_default_value_fields=True,
        preserving_proto_field_name=True)
    return jd_content_info


def get_poi2id_dict() -> dict:
    request = base_pb2.NormalTextRequest()
    response = extract_poi_engine_stub.GetPoi2Id(request)
    result = MessageToDict(
        response,
        including_default_value_fields=True,
        preserving_proto_field_name=True)
    return result['result']


_poi2id = get_poi2id_dict()
num_poi = len(_poi2id) + 1


def get_poi2id(poi):
    return _poi2id.get(poi, 0)
