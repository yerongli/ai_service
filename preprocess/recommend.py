import datetime
import logging

import tqdm
from google.protobuf.json_format import MessageToDict

from ai_service_protos import base_pb2, resume_recommend_pb2
from database.mongodb import data_center_client, joborder_find
from engine_services.resume_recommend_engine.channel import \
    resume_recommend_stub


def recommend_resume_from_jd_info(jd_info: dict,
                                  refresh: bool = True,
                                  topn: int = 100,
                                  min_score: float = 0.4,
                                  size: int = 20,
                                  offset: int = 0):
    request = resume_recommend_pb2.RecommendResumeFromJdInfoRequest(
        jd_info=jd_info,
        refresh=refresh,
        topn=topn,
        min_score=min_score,
        size=size,
        offset=offset)

    response = resume_recommend_stub.RecommendResumeFromJdInfo(request)
    result = MessageToDict(
        response,
        including_default_value_fields=True,
        preserving_proto_field_name=True)
    return result


# 为一个租户的所有 jd_index 更新推荐
def tenant_re_recommend_resume_from_jd(tenant: str):
    logging.info(
        f"[{datetime.datetime.now()}] - tenant_re_recommend_resume_from_jd [{tenant}]]"
    )

    data = joborder_find(
        query={"client_key": tenant},
        projection={
            "id": 1,
            'current_version': 1
        })

    for j in tqdm.tqdm(data):
        try:
            aid = j['id']
            jd_info = {
                "tenant": tenant,
                "aid": aid,
                "version": j['current_version'],
                "custom_prefer_list": []
            }
            recommend_resume_from_jd_info(jd_info)
        except Exception:
            logging.exception(
                f"re_recommend_resume_from_jd error, tenant [{tenant}]",
                extra={"aid": j['id']})


# 为所有租户的所有 jd_index 更新推荐
def all_re_recommend_resume_from_jd():
    tenant_list = data_center_client.get_client_keys(timeout=5)
    logging.info(
        f"[{datetime.datetime.now()}] - all_re_recommend_resume_from_jd {tenant_list}"
    )
    for tenant in tenant_list:
        tenant_re_recommend_resume_from_jd(tenant)


# 为一个租户刷新首页推荐
def tenant_refresh_recommend_resume_home_page(tenant: str, topn: int = 100):
    logging.info(
        f"[{datetime.datetime.now()}] - tenant_refresh_recommend_resume_home_page [{tenant}]]"
    )
    request = resume_recommend_pb2.RecommendResumeHomePageRequest(
        tenant=tenant, topn=topn)
    response = resume_recommend_stub.GetRecommendResumeHomePage(request)
    result = MessageToDict(
        response,
        including_default_value_fields=True,
        preserving_proto_field_name=True)
    return result


# 为所有租户刷新首页推荐
def all_refresh_recommend_resume_home_page():
    tenant_list = data_center_client.get_client_keys(timeout=5)
    for tenant in tenant_list:
        tenant_refresh_recommend_resume_home_page(tenant)
