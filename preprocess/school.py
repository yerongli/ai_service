import logging
from typing import Union

from google.protobuf.json_format import MessageToDict

from ai_service_protos import base_pb2, school_pb2
from database.redis.cache import get_school_cache, set_school_cache
from engine_services.school_engine.channel import school_engine_stub


def normalize_school_aid(school: str) -> int:
    if not school:
        return 0

    cached_result = get_school_cache(school)
    if cached_result:
        return int(cached_result)

    try:
        request = base_pb2.NormalTextRequest(text=school)
        result = school_engine_stub.SchoolSearch(request)
        aid = result.aid
        if not aid:
            aid = 0
    except Exception:
        logging.exception('获取学校aid失败', extra={'school': school})
        return 0

    set_school_cache(school, aid)
    return aid


def get_school_info_dict(aid: int) -> Union[dict, None]:
    if aid is None:
        return None
    try:
        request = school_pb2.SchoolAidMessage(aid=aid)
        result = school_engine_stub.GetSchoolInfoDict(request)
        if result:
            result = {
                "is_985": result.is_985,
                "is_211": result.is_211,
                "is_abroad": result.is_abroad,
                "is_qs200": result.is_qs200
            }
        return result
    except Exception:
        logging.exception('获取学校信息失败', extra={'aid': aid})
        return None


def get_school2id_dict() -> dict:
    request = base_pb2.NormalTextRequest()
    response = school_engine_stub.GetSchool2Id(request)
    result = MessageToDict(
        response,
        including_default_value_fields=True,
        preserving_proto_field_name=True)
    return result['result']


_school2id = get_school2id_dict()
num_schools = len(_school2id) + 1


def get_school2id(school):
    return _school2id.get(school, 0)
