import os
import tqdm
import itertools
import random
import pickle
from common.resume_duplicate_check.ranker import *
from common.resume_duplicate_check.manage import DuplicateResume
from gllue_ai_libs.angelo_tool.analyze.efficient import multi_worker
from sklearn.model_selection import train_test_split
from sklearn import metrics

with open('mobile_list.bin', 'rb') as f:
    mobile_list = pickle.load(f)

with open('tenant_id_dict.bin', 'rb') as f:
    tenant_id_dict = pickle.load(f)

if os.path.exists('id_pairs.bin'):
    with open('id_pairs.bin', 'rb') as f:
        id_pairs = pickle.load(f)


def get_tenant(_id: str):
    return _id.split('_')[0]


def deal_single_mobile(mobile) -> list:
    if len(mobile) != 11:
        return []
    # 获取对应数据
    rfs = list(ResumeFeatures.search().filter("term", **{
        "basic_info.mobile": mobile
    }).source([
        'aid', 'experiences.work_list.title_stand',
        'educations.edu_list.school_stand_id'
    ]).scan())
    ids = []
    for rf in rfs:
        if len(rf.experiences.work_list) >= 1 and len(
                rf.educations.edu_list) >= 1:
            ids.append(rf.meta.id)
    if not ids:
        return []
    if len(ids) >= 30:
        return []
    rst = []
    # 排列组合
    for i, j in itertools.product(ids, ids):
        if i != j:
            rst.append((i, j, 1))
    # 候选K数量
    k = len(rst) // len(ids)
    ids = set(ids)

    for _id in ids:
        # tenant = get_tenant(_id)
        tenant, aid = _id.split("_")
        dr = DuplicateResume(tenant)
        # 召回
        cand_aid = list(
            dr.recall_for_resume_duplicate_checking(
                ResumeFeatures.get(id=_id).to_dict(skip_empty=False)))
        cand_aid = [f'{tenant}_{i}' for i in cand_aid]
        # 如果存在召回， 则从召回中选取
        if cand_aid:
            if len(cand_aid) >= k:
                for i in random.sample(cand_aid, k=min(len(cand_aid), k * 2)):
                    if i != _id:
                        rst.append((_id, i, 0))
            else:
                for i in cand_aid:
                    if i != _id:
                        rst.append((_id, i, 0))
        # 如果不存在召回， 则随机选取
        else:
            try:
                for i in random.sample(tenant_id_dict[tenant], k=k):
                    if i not in ids:
                        rst.append((_id, i, 0))
            except ValueError:
                print(tenant, len(tenant_id_dict[tenant]), k)
                continue
    return rst


def product_mobile_list(mobile_list: list) -> list:
    data = []
    for mobile in tqdm.tqdm(mobile_list):
        data.append(deal_single_mobile(mobile))
    return data


# 避免重要信息过拟合
name_choice = [[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]]

mobile_choice = [[0, 0, 1], [0, 1, 0], [1, 0, 0]]


def deal_id_coup(x) -> list:
    try:
        id1, id2, tag = x
        r1 = ResumeFeatures.get(id=id1)
        r2 = ResumeFeatures.get(id=id2)
        if len(r1.experiences.work_list) < 1 or len(
                r2.experiences.work_list) < 1:
            return []
        if len(r1.educations.edu_list) < 1 or len(r2.educations.edu_list) < 1:
            return []

        r1 = r1.to_dict(skip_empty=False)
        r2 = r2.to_dict(skip_empty=False)

        rst = compare_resume(r1, r2)
        rst.append(tag)
        if tag == 1:
            rst[0:4] = random.choice(name_choice)
            rst[4:7] = random.choice(mobile_choice)
        return rst
    except Exception as e:
        print(e)
        return []
