import tqdm
import random
from keras.models import Model
from keras.layers import Dense, BatchNormalization, Concatenate, Input
from ctr_recommend.ml.ranker import deal_single_resume, deal_single_resume_sub
from ctr_recommend.ml.manage import RecommendResume
from database.mongodb.operate import jobsubmission_find, candidate_find
from database.elasticsearch.models import JobOrderFeatures
from gllue_ai_libs.angelo_tool.analyze.efficient import multi_worker


def block_layer(in_layer, last_layer, dense_unit):
    dense = Dense(dense_unit, activation='relu')(last_layer)
    bn = BatchNormalization()(dense)
    ct = Concatenate()([in_layer, bn])
    return ct


def get_dnn_model(input_unit):
    input_layer = Input(shape=(input_unit, ))
    x = block_layer(input_layer, input_layer, input_unit * 4)
    x = block_layer(input_layer, x, input_unit * 2)
    x = block_layer(input_layer, x, input_unit)
    x = Dense(1, activation='sigmoid')(x)

    model = Model(inputs=input_layer, outputs=x)
    model.compile(loss='mse', optimizer='adam', metrics=['mse', 'accuracy'])
    return model


def get_candidate_ids(tenant):
    x = candidate_find(query={'client_key': tenant}, projection={"id": 1})
    x = [i['id'] for i in x]
    return x


def get_single_jd_ml_data(tenant, aid, candidate_ids=None):
    rr = RecommendResume(tenant)
    jobsubmission_list = jobsubmission_find(
        query={
            'client_key': tenant,
            'joborder': aid
        },
        projection={
            'candidate': 1,
            'joborder': 1,
            'max_status': 1
        })

    half_aid_set = {
        i['candidate']
        for i in jobsubmission_list
        if not i['max_status'] and i['candidate'] is not None
    }
    one_aid_set = {
        i['candidate']
        for i in jobsubmission_list
        if i['max_status'] is not None and i['candidate'] is not None
    }
    if not one_aid_set:
        return None
    half_aid_set = list(half_aid_set)
    one_aid_set = list(one_aid_set)
    # zero_aid_set = list(random.sample(candidate_ids, k=len(one_aid_set)))
    # xxx = set(half_aid_set + one_aid_set)
    # zero_aid_set = [i for i in zero_aid_set if i not in one_aid_set]
    # zero_aid_set = list(random.sample(self.candidate_ids, k=len(one_aid_set)))
    # one_aid_set = one_aid_set
    # 获取获选简历
    one_data = rr._get_data_for_recommend_resume_from_jd(one_aid_set)
    half_data = rr._get_data_for_recommend_resume_from_jd(half_aid_set)
    # zero_data = rr._get_data_for_recommend_resume_from_jd(zero_aid_set)

    jd_info = {
        "tenant": tenant,
        "aid": aid,
    }
    jd_match_util = rr._get_and_upgrade_jd(jd_info)
    rst = []
    # print(
    #     f"[{aid} - {jd_match_util.jd.title}], [{len(one_aid_set)}, {len(half_aid_set)}, {0}]"
    # )
    for d in one_data:
        if len(d['experiences']['poi_weight']) <= 3:
            continue
        if len(d['experiences']['lda_weight']) <= 2:
            continue
        x = deal_single_resume(jd_match_util, d)
        x.append(1)
        rst.append(x)

        x = deal_single_resume_sub(jd_match_util, d)
        x.append(1)
        rst.append(x)

    for d in half_data:
        x = deal_single_resume(jd_match_util, d)
        x.append(0)
        rst.append(x)

        x = deal_single_resume_sub(jd_match_util, d)
        x.append(0)
        rst.append(x)

    # for d in zero_data:
    #     x = deal_single_resume(jd_match_util, d)
    #     x.append(0)
    #     rst.append(x)

    return rst


def get_single_jd_ml_data_prop(x):
    try:
        tenant, aid = x
        return get_single_jd_ml_data(tenant, aid)
    except Exception as e:
        return []


def get_all_ml_data(tenant_list, jobs=1):
    tenant_id_pairs = []
    for tenant in tenant_list:
        jd_id_list = [
            i.aid for i in JobOrderFeatures.search().filter(
                'term', tenant=tenant).source(['aid']).scan()
        ]
        for jd_id in jd_id_list:
            tenant_id_pairs.append([tenant, jd_id])

    else:
        print(f"start multi worker.....{len(tenant_id_pairs)}")
        random.shuffle(tenant_id_pairs)
        if jobs == 1:
            rst = []
            for tenant, aid in tqdm.tqdm(tenant_id_pairs):
                try:
                    x = get_single_jd_ml_data(tenant, aid)
                    if x:
                        rst.extend(x)
                except KeyboardInterrupt:
                    return rst
                except Exception as e:
                    print(e)
                    continue
        else:
            rst = multi_worker(
                tenant_id_pairs, get_single_jd_ml_data_prop, workers=jobs)
    return rst
