import unittest

from sync_server.worker import celery_app

celery_app.conf.update(task_always_eager=True)
tenant = '9a5584d4-1689-49ec-a049-a9c00af27101'


class CronTaskTestCase(unittest.TestCase):
    def all_re_recommend_resume_from_jd(self):
        celery_app.tasks['cron_task.all_re_recommend_resume_from_jd'].delay()


if __name__ == '__main__':
    unittest.main()
