import datetime
import json

import click
from bson import json_util

from sync_server.worker import celery_app


@click.group()
def cli():
    ...


@cli.group(help="joborder")
def joborder():
    ...


@joborder.command(help="insert")
def insert():
    ...


@joborder.command(help="update")
def update():
    ...


@cli.group(help="candidate")
def candidate():
    ...


@candidate.command(help="insert")
def insert():
    ...


@candidate.command(help="update")
def update():
    ...


if __name__ == '__main__':
    d = {
        "documentKey": {
            "_id": "9a5584d4-1689-49ec-a049-a9c00af27101"
        },
        "time": datetime.datetime.now()
    }
    document = json.dumps(d, default=json_util.default)
    celery_app.tasks['joborder.update'].delay(document=document)
