import datetime
import unittest
from concurrent.futures import ThreadPoolExecutor

from bson import Timestamp

from sync_server.__main__ import process_event
from sync_server.celery_task.candidate import candidate_insert, \
    candidate_update

# 一个真实的 update操作
update_event = {
    '_id': {
        '_data':
        '825CA4209A0000000129295A1004C9D86D256C34456E9CEDFF93069E84DA463C5F6964003C39613535383464342D313638392D343965632D613034392D6139633030616632373130315F3936353035000004'
    },
    'operationType': 'update',
    'clusterTime': Timestamp(1554260122, 1),
    'ns': {
        'db': 'gllue_data_test',
        'coll': 'candidate'
    },
    'documentKey': {
        '_id': '9a5584d4-1689-49ec-a049-a9c00af27101_96505'
    },
    'updateDescription': {
        'updatedFields': {
            'dateOfBirth':
            datetime.datetime(1992, 4, 12, 0, 0),
            'dayNull':
            False,
            'educations': [{
                'lang': 'default',
                'candidate': 96505,
                'start': '2011-09',
                'end': '2015-06',
                'school': '西南交通大学',
                'degree': 'Bachelor',
                'major': '材料成型及控制工程',
                'description': None,
                'is_current': False,
                'is_recent': True,
                'parent_major': '机械类',
                'ai_school': None,
                'id': 37437,
                'school_type': [1]
            }],
            'experiences': [
                {
                    'lang': 'default',
                    'start': '2016-03',
                    'end': None,
                    'candidate': 96505,
                    'client': 3,
                    'title': '销售经理',
                    'description': '呵呵呵',
                    'function': None,
                    'function1': None,
                    'function2': None,
                    'is_current': True,
                    'is_recent': True,
                    'department': None,
                    'standard_title': '销售经理',
                    'client_standard_name': '汉得信息',
                    'client_name': ['上海汉得信息技术股份有限公司'],
                    'id': 76987,
                    'gllueext_fk_1549464059711': None,
                    'gllueext_number_salary': None,
                    'gllueext_text_1551101801602': None,
                    'gllueext_text_1551101832237': None
                },
                {
                    'lang':
                    'default',
                    'start':
                    '2015-07',
                    'end':
                    None,
                    'candidate':
                    96505,
                    'client':
                    3,
                    'title':
                    '销售经理',
                    'description':
                    '职责业绩：\n参与“元祖食品”\nSAP项目上线实施\n参与“和利时”二期项目实施\n参与“辉丰股份”\nSAP项目上线实施',
                    'function':
                    None,
                    'function1':
                    None,
                    'function2':
                    None,
                    'is_current':
                    True,
                    'is_recent':
                    False,
                    'department':
                    None,
                    'standard_title':
                    '销售经理',
                    'client_standard_name':
                    '汉得信息',
                    'client_name': ['上海汉得信息技术股份有限公司'],
                    'id':
                    76988,
                    'gllueext_fk_1549464059711':
                    None,
                    'gllueext_number_salary':
                    None,
                    'gllueext_text_1551101801602':
                    None,
                    'gllueext_text_1551101832237':
                    None
                }
            ],
            'hide_joborder':
            None,
            'hide_level':
            'zero_hide_level',
            'hide_time':
            None,
            'hider':
            None,
            'highest_education':
            37437,
            'lastUpdateBy':
            60,
            'lastUpdateDate':
            datetime.datetime(2019, 4, 3, 0, 0),
            'lastUpdateStatusDate':
            datetime.datetime(2019, 4, 3, 0, 0),
            'monthNull':
            False,
            'projects': [
                {
                    'lang':
                    'default',
                    'candidate':
                    96505,
                    'start':
                    '2016-07',
                    'end':
                    '2016-10',
                    'name':
                    '辉丰股份',
                    'title':
                    'FICO顾问',
                    'description':
                    '辉丰股份切换ERP软件\n独立负责\nSAP财务的CO模块，调研，蓝图，系统实现，培训及上线工作。',
                    'id':
                    28464
                },
                {
                    'lang':
                    'default',
                    'candidate':
                    96505,
                    'start':
                    '2016-01',
                    'end':
                    '2016-07',
                    'name':
                    '和利时-预算费控合并项目',
                    'title':
                    'FICO顾问',
                    'description':
                    '主导和利时\nSAP与预算、费控、合并系统的对接，支持预算、费控、合并系统的上线\n独立负责\nSAP财务模块与预算、费控、合并系统的接口工作，保证系统上线运行成功',
                    'id':
                    28465
                }
            ]
        },
        'removedFields': []
    }
}


class CandidateTaskTestCase(unittest.TestCase):
    def test_candidate_insert(self):
        pass

    def test_candidate_update(self):
        thread_worker = ThreadPoolExecutor(max_workers=2)
        result = thread_worker.submit(process_event, event=update_event)
        self.assertIsNone(result.exception())


if __name__ == '__main__':
    unittest.main()
