import unittest

from sync_server.celery_task.joborder import joborder_insert, joborder_update


class JobOrderTaskTestCase(unittest.TestCase):
    def test_candidate_insert(self):
        joborder_insert(document={
            'documentKey': {
                '_id': '9a5584d4-1689-49ec-a049-a9c00af27101_5'
            }
        })

    def test_candidate_update(self):
        joborder_update(document={
            'documentKey': {
                '_id': '9a5584d4-1689-49ec-a049-a9c00af27101_5'
            }
        })


if __name__ == '__main__':
    unittest.main()
