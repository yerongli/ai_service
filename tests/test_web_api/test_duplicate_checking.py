import unittest
from base64 import encodebytes
from os.path import abspath, dirname, join

from utils import rpc
from web_api.app import app

rpc.DEFAULT_FILE_NAME = 'test.pdf'
email_file = abspath(join(dirname(__file__), 'file', 'email.jpg'))
mobile_file = abspath(join(dirname(__file__), 'file', 'mobile.jpg'))
resume_file = abspath(join(dirname(__file__), 'file', 'test.pdf'))


class DuplicateCheckingTestCase(unittest.TestCase):
    def test_check_without_jpg(self):
        with app.test_client() as client:
            with open(resume_file, 'rb') as f:
                file = encodebytes(f.read()).decode()
            res = client.post(
                '/ai/duplicate_checking/DuplicateChecking',
                json={
                    'file': file,
                },
                headers={
                    'authentication':
                    'scheme {"client_key": "9a5584d4-1689-49ec-a049-a9c00af27101"}'
                })
            self.assertGreaterEqual(len(res.json['general']['items']), 1)
            self.assertGreaterEqual(len(res.json['precisely']['items']), 1)

    def test_check_with_jpg(self):
        with app.test_client() as client:
            with open(email_file, 'rb') as f:
                email = encodebytes(f.read()).decode()
            with open(mobile_file, 'rb') as f:
                mobile = encodebytes(f.read()).decode()
            with open(resume_file, 'rb') as f:
                file = encodebytes(f.read()).decode()
            res = client.post(
                '/ai/duplicate_checking/DuplicateChecking',
                json={
                    'file': file,
                    'email': email,
                    'mobile': mobile,
                },
                headers={
                    'authentication':
                    'scheme {"client_key": "9a5584d4-1689-49ec-a049-a9c00af27101"}'
                })
            self.assertGreaterEqual(len(res.json['general']['items']), 1)
            self.assertGreaterEqual(len(res.json['precisely']['items']), 1)
