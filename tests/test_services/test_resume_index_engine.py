import unittest

from ai_service_protos import resume_index_pb2
from database import get_ident
from database.elasticsearch.models import ResumeFeatures
from engine_services.resume_index_engine.channel import \
    resume_process_engine_stub
from preprocess.resume_index import (delete_single_resume, ident_update_resume,
                                     index_single_resume)

resume_aid = 106634
tenant = "9a5584d4-1689-49ec-a049-a9c00af27101"
basic_info = {
    "name": "谷小露",
    "gender": "女",
    "mobile": "18721832991",
    "birth_day": '1995-12-20'
}
educations = [{
    "school": "清华大学",
    "major": "会计",
    "degree": "本科",
    "start": "2013-09",
    "end": "2017-07",
    "is_recent": True,
    "is_current": False
}]
experiences = [{
    "company":
    "谷露",
    "title":
    "python开发工程师",
    "description":
    """
                                 熟练运用Python语言，熟悉Python标准库和流行的第三方库
                                 熟悉Python面向对象思想，良好的编码习惯
                                 熟悉MVC/单例/装饰器等设计模式
                                 熟悉Python多线程，多进程
                                 熟悉版本管理工具Git的使用，能够使用Webhook搭建自动部署环境
                                 熟悉Linux常用指令，熟悉Shell脚本，能够进行自动化运维
                                 能够搭建高并发架构
                                 熟练运用Django框架和Flask框架，了解Tornado框架
                                 掌握HTML5和CSS3、BootStrap、Flex进行响应式布局
                                 熟悉原生JS、jQuery，能够用面向对象封装交互方法
                                 熟练使用npm包管理工具，以及gulp前端自动化构建工具
                                 了解微信小程序的开发
                                 熟悉MySQL数据库，能够使用ORM操作MySQL
                                 掌握Redis、Memcached和MongoDB数据库进行缓存
                                 熟悉Python爬虫框架Scrapy，及爬虫技术Urllib/Selenium
                                 熟悉常见的反爬虫策略
                                 熟练使用re/BeautifulSoup/Xpath工具解析网页数据
                                 了解numpy/matplotlib/pandas/scipy等数据分析工具
                                 了解tensorflow等开源深度学习框架""",
    "start":
    "2017-09",
    "end":
    "至今",
    "is_recent":
    True,
    "is_current":
    True
}]

custom_tags_1 = ["空杯心态", "泰然自若"]
custom_tags_2 = ["空杯心态", "泰然自若", "高高兴兴"]

sample_resume = {
    "aid": resume_aid,
    "tenant": tenant,
    "is_deleted": False,
    "basic_info": basic_info,
    "educations": educations,
    "experiences": experiences,
    "custom_tags": custom_tags_1
}

delete_resume_info = {"aid": resume_aid, "tenant": tenant}

resume_custom_tags = {"aid": 1, "tenant": tenant, "custom_tags": custom_tags_2}


class ResumeIndexTestCase(unittest.TestCase):
    def test_ident_update_resume(self):
        ident = get_ident(tenant, resume_aid)
        ident_update_resume(ident)
        rdm = ResumeFeatures.get_with_tenant_and_aid(tenant, resume_aid)
        self.assertEqual(rdm.meta.id, ident)

    def test_index_single_resume(self):
        result = index_single_resume(sample_resume)
        self.assertIn('basic_info', result.keys())

    def test_delete_single_resume(self):
        delete_single_resume(resume_info=delete_resume_info)
        rdm = ResumeFeatures.get_with_tenant_and_aid(
            tenant=tenant, aid=resume_aid)
        self.assertTrue(rdm.is_deleted)
        rdm.is_deleted = False
        rdm.save()

    def test_update_single_resume_custom_tags(self):
        request = resume_index_pb2.UpdateSingleResumeCustomTagsMessage(
            **resume_custom_tags)
        response = resume_process_engine_stub.UpdateSingleResumeCustomTags(
            request)
