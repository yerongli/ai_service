import unittest

from google.protobuf.json_format import MessageToDict

from ai_service_protos import base_pb2
from engine_services.school_engine.channel import school_engine_stub


class SchoolEngineTestCase(unittest.TestCase):
    def test_SchoolSearch(self):
        request = base_pb2.NormalTextRequest(text="清华大学")
        response = school_engine_stub.SchoolSearch(request)
        result = MessageToDict(
            response,
            including_default_value_fields=True,
            preserving_proto_field_name=True,
        )
        self.assertEqual(result['aid'], 111764)

    def test_BatchSchoolSearch(self):
        request = base_pb2.NormTextListMessage(
            text_list=['清华大学', '北京大学', '哈哈哈'])
        response = school_engine_stub.BatchSchoolSearch(request)

        result = MessageToDict(
            response,
            including_default_value_fields=True,
            preserving_proto_field_name=True,
        )

        self.assertEqual([111764, 110221, 0],
                         [i['aid'] for i in result['items']])
