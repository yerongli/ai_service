import unittest

from google.protobuf.json_format import MessageToDict

from ai_service_protos import base_pb2, major_pb2
from engine_services.description_engine.channel import \
    major_classify_engine_stub


class MajorEngineTestCase(unittest.TestCase):
    def test_CellPredict(self):
        request = major_pb2.CellPredictRequest(level="本科", major="经济管理")
        response = major_classify_engine_stub.CellPredict(request)
        result = MessageToDict(
            response,
            including_default_value_fields=True,
            preserving_proto_field_name=True)
        self.assertEqual(result['result'], '本科-管理学-农业经济管理类')

    def test_LowestDegreeMajorPredict(self):
        request = major_pb2.LowestDegreeMajorPredictRequest(
            major_list=['软件工程', '计算机', '通络通信'], lowest_degree="本科")
        response = major_classify_engine_stub.LowestDegreeMajorPredict(request)
        result = MessageToDict(
            response,
            including_default_value_fields=True,
            preserving_proto_field_name=True)
        self.assertIn('硕士-工学-软件工程', result['result'])

    def test_GetMajor2Id(self):
        request = base_pb2.NormalTextRequest()
        response = major_classify_engine_stub.GetMajor2Id(request)
        result = MessageToDict(
            response,
            including_default_value_fields=True,
            preserving_proto_field_name=True)
        self.assertIn(1, set(result['result'].values()))
        self.assertIn(2, set(result['result'].values()))

    def test_GetMajorParent2Id(self):
        request = base_pb2.NormalTextRequest()
        response = major_classify_engine_stub.GetMajor2Id(request)
        result = MessageToDict(
            response,
            including_default_value_fields=True,
            preserving_proto_field_name=True)
        self.assertIn(2, set(result['result'].values()))


if __name__ == '__main__':
    unittest.main()
