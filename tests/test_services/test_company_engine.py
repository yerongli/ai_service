import unittest

from ai_service_protos import base_pb2, company_pb2
from engine_services.company_engine.channel import company_keyword_engine_stub, \
    tenant_search_engine_stub


class CompanyEngineTestCase(unittest.TestCase):
    def test_SearchDuplicateCountWithCache(self):
        request = company_pb2.CompanySearchDuplicateCountRequest(
            id_name_pairs={1: "test"},
            tenant='068603cd-2c9f-44b7-ae0b-e01fe2cd3899')
        response = tenant_search_engine_stub.SearchDuplicateCountWithCache(
            request)
        print(response)

    def test_ExtractKeyword(self):
        keyword = "谷露软件"
        request = base_pb2.NormalTextRequest(text="上海谷露软件有限公司")
        response = company_keyword_engine_stub.ExtractKeyword(request)
        self.assertEqual(response.text, keyword)
