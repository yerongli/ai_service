import unittest

from google.protobuf.json_format import MessageToDict

from ai_service_protos import feedback_pb2
from engine_services.feedback_engine.channel import feedback_stub

tenant = '9a5584d4-1689-49ec-a049-a9c00af27101'


class FeedbackTestCase(unittest.TestCase):
    def test_FeedbackJdResumeMatch(self):
        request = feedback_pb2.JdResumeFeedbackMessage(
            tenant=tenant,
            jd_aid=1,
            resume_aid_list=[1036792, 1043909, 1039764])
        response = feedback_stub.FeedbackJdResumeMatch(request)
        result = MessageToDict(
            response,
            including_default_value_fields=True,
            preserving_proto_field_name=True)
        self.assertTrue(result['status'])

    def test_FeedbackJdResumeMisMatch(self):
        request = feedback_pb2.JdResumeFeedbackMessage(
            tenant=tenant,
            jd_aid=225,
            resume_aid_list=[1036792, 1043909, 1039764])
        response = feedback_stub.FeedbackJdResumeMismatch(request)
        result = MessageToDict(
            response,
            including_default_value_fields=True,
            preserving_proto_field_name=True)
        self.assertTrue(result['status'])
