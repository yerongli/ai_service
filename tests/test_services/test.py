import unittest

from ctr_recommend.ml import manage as resume_recommend_util
from database.elasticsearch.models import CompanyFeatures
jd_aid = 82
tenant = "56fbcd7c-2bb3-4a0a-84d7-062981df6369"
version = 1
custom_prefer_list = [
    {"field": "上海大学", "type": "target_school", "boost": 1},
    {"field": "上海爱福窝云技术有限公司", "type": "target_company", "boost": 1},
]

basic_info = {
    "name": "谷小露",
    "gender": "女",
    "mobile": "18721832991",
    "birth_day": "1995-12-20",
}

educations = [
    {
        "school": "清华大学",
        "major": "会计",
        "degree": "本科",
        "start": "2013-09",
        "end": "2017-07",
    }
]

experiences = [
    {
        "company": "谷露",
        "title": "python开发工程师",
        "description": """
                         熟练运用Python语言，熟悉Python标准库和流行的第三方库
                         熟悉Python面向对象思想，良好的编码习惯
                         熟悉MVC/单例/装饰器等设计模式
                         熟悉Python多线程，多进程
                         熟悉版本管理工具Git的使用，能够使用Webhook搭建自动部署环境
                         熟悉Linux常用指令，熟悉Shell脚本，能够进行自动化运维
                         能够搭建高并发架构
                         熟练运用Django框架和Flask框架，了解Tornado框架
                         掌握HTML5和CSS3、BootStrap、Flex进行响应式布局
                         熟悉原生JS、jQuery，能够用面向对象封装交互方法
                         熟练使用npm包管理工具，以及gulp前端自动化构建工具
                         了解微信小程序的开发
                         熟悉MySQL数据库，能够使用ORM操作MySQL
                         掌握Redis、Memcached和MongoDB数据库进行缓存
                         熟悉Python爬虫框架Scrapy，及爬虫技术Urllib/Selenium
                         熟悉常见的反爬虫策略
                         熟练使用re/BeautifulSoup/Xpath工具解析网页数据
                         了解numpy/matplotlib/pandas/scipy等数据分析工具
                         了解tensorflow等开源深度学习框架""",
        "start": "2017-09",
        "end": "至今",
    }
]

jd_info = {
    "aid": jd_aid,
    "tenant": tenant,
    "version": version,
    "custom_prefer_list": custom_prefer_list,
}

class CompanyEngineTestCase(unittest.TestCase):
    def test_SearchDuplicateCountWithCache(self):

        response = CompanyFeatures.search_list_(name="百度")
        print(response)

        response = CompanyFeatures.search_list_(name="美团")
        print(response)

        response = CompanyFeatures.search_list_(name="阿里巴巴")
        print(response)

        response = CompanyFeatures.search_list_(name="李叶嵘")
        print(response)

        response = CompanyFeatures.search_list_(name="上海隧道马来西子公司")  # 上海隧道马来西子公司
        print(response)

    def testJD(self):
        #jd_info = {};
        result = resume_recommend_util.recommend_resume_from_jd_info(jd_info);
        print(result);