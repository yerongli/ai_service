import unittest

from google.protobuf.json_format import MessageToDict

from ai_service_protos import base_pb2
from engine_services.jd_engine.channel import jd_engine_stub

jd_aid = 1
tenant = "9a5584d4-1689-49ec-a049-a9c00af27101"
version = 1
title = "python开发工程师"
company = "上海谷露软件有限公司"
content = """岗位职责：
            1、负责产品服务器端设计和代码实现 ；
            2、优化现有产品的功能和体验细节 ；
            3、积极调研新技术，随时准备用于产品 ；
            岗位要求：
            1、本科及以上学历，计算机、软件工程、信息管理、计算机应用等理工专业 ；
            2、3年以上Web服务器端开发经验， 2年以上python服务端开发经验，做过saas平台，大并发经验优先 ；
            3、有丰富的数据库建模经验，有MySQL、MongoDB、Redis使用经验者优先 ；
            4、有扎实的技术功底，做到知其然并知其所以然，能读懂python主流框架源码 ；
            5、熟悉分布式系统的设计和应用，熟悉分布式、缓存、消息等机制；能对分布式常用技术进行合理应用，解决问题；
            6、具有一定的技术架构思维，确保设计的技术方案、开发的代码有较高性能、质量保障、扩展性，前瞻性 。"""
salary_range = {"gte": 10000., "lte": 30000.}

sample_jd_info = {
    "aid": jd_aid,
    "tenant": tenant,
    "version": version,
    "title": title,
    "content": content,
    "company": "上海谷露软件有限公司",
    "salary_range": salary_range
}


class JdEngineTestCase(unittest.TestCase):
    def test_ExtractContentInfo(self):
        request = base_pb2.NormalTextRequest(text=sample_jd_info['content'])
        response = jd_engine_stub.ExtractContentInfo(request)
        result = MessageToDict(
            response,
            including_default_value_fields=True,
            preserving_proto_field_name=True)
        for i in [
                'content_sentence', 'content_clean', 'lowest_degree',
                'need_majors', 'age_range', 'working_year_range',
                'need_985_211'
        ]:
            self.assertIn(i, result.keys())
