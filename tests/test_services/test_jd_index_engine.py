import unittest

from database import get_ident
from database.elasticsearch.models import JobOrderFeatures
from database.mongodb import get_single_joborder
from preprocess.jd_index import delete_single_jd_info, \
    ident_update_jd_info

jd_aid = 2
tenant = "9a5584d4-1689-49ec-a049-a9c00af27101"

delete_jd_info = {"aid": jd_aid, "tenant": tenant}


class JdIndexTestCase(unittest.TestCase):
    def test_ident_update_jd_info(self):
        ident = get_ident(tenant, jd_aid)
        ident_update_jd_info(ident)
        jdm = JobOrderFeatures.get_with_tenant_and_aid(tenant, jd_aid)
        x = get_single_joborder(ident=ident)
        self.assertEqual(jdm.title, x['jobTitle'])

    def test_delete_single_jd_info(self):
        delete_single_jd_info(delete_jd_info)
        jdm = JobOrderFeatures.get_with_tenant_and_aid(
            tenant=tenant, aid=jd_aid)
        self.assertTrue(jdm.is_deleted)
