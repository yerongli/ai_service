import unittest

from google.protobuf.json_format import MessageToDict

from ai_service_protos import base_pb2
from engine_services.description_engine.channel import extract_poi_engine_stub, \
    lda_predict_engine_stub

title = "python开发工程师"
description = """岗位职责：
            1、负责产品服务器端设计和代码实现 ；
            2、优化现有产品的功能和体验细节 ；
            3、积极调研新技术，随时准备用于产品 ；
            岗位要求：
            1、本科及以上学历，计算机、软件工程、信息管理、计算机应用等理工专业 ；
            2、3年以上Web服务器端开发经验， 2年以上python服务端开发经验，做过saas平台，大并发经验优先 ；
            3、有丰富的数据库建模经验，有MySQL、MongoDB、Redis使用经验者优先 ；
            4、有扎实的技术功底，做到知其然并知其所以然，能读懂python主流框架源码 ；
            5、熟悉分布式系统的设计和应用，熟悉分布式、缓存、消息等机制；能对分布式常用技术进行合理应用，解决问题；
            6、具有一定的技术架构思维，确保设计的技术方案、开发的代码有较高性能、质量保障、扩展性，前瞻性 。"""

title_description = {"title": title, "description": description}


class DescriptionEngineTestCase(unittest.TestCase):
    def test_extract_description_poi_with_title(self):
        request = base_pb2.NormalTitleDescriptionRequest(**title_description)
        response = extract_poi_engine_stub.DescriptionPoiSortWithTitle(request)
        result = MessageToDict(
            response,
            including_default_value_fields=False,
            preserving_proto_field_name=False,
        )
        self.assertIn("python", [i['index'] for i in result['result']])

    def test_predict_description_lda_with_title(self):
        request = base_pb2.NormalTitleDescriptionRequest(**title_description)
        response = lda_predict_engine_stub.GetTextLda(request)

        result = MessageToDict(
            response,
            including_default_value_fields=False,
            preserving_proto_field_name=False,
        )
        self.assertIn(212, [i['index'] for i in result['result']])


if __name__ == '__main__':
    unittest.main()
