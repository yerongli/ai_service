import unittest

from google.protobuf.json_format import MessageToDict

from ai_service_protos import base_pb2, resume_recommend_pb2
from ai_service_protos.meta_pb2 import BaseAidTenantMessage
from engine_services.resume_recommend_engine.channel import (
    duplicate_checking_stub, resume_features_stub, resume_recommend_stub)

tenant = "9a5584d4-1689-49ec-a049-a9c00af27101"
resume_aid = 108565

resume_info = {"aid": resume_aid, "tenant": tenant}
similar_resume_from_resume_id_info = {
    "resume_info": resume_info,
    "topn": 10,
    "min_score": 0
}

basic_info = {
    "name": "谷小露",
    "gender": "女",
    "mobile": "18721832991",
    "birth_day": '1995-12-20'
}
educations = [{
    "school": "清华大学",
    "major": "会计",
    "degree": "本科",
    "start": "2013-09",
    "end": "2017-07"
}]
experiences = [{
    "company":
    "谷露",
    "title":
    "python开发工程师",
    "description":
    """
         熟练运用Python语言，熟悉Python标准库和流行的第三方库
         熟悉Python面向对象思想，良好的编码习惯
         熟悉MVC/单例/装饰器等设计模式
         熟悉Python多线程，多进程
         熟悉版本管理工具Git的使用，能够使用Webhook搭建自动部署环境
         熟悉Linux常用指令，熟悉Shell脚本，能够进行自动化运维
         能够搭建高并发架构
         熟练运用Django框架和Flask框架，了解Tornado框架
         掌握HTML5和CSS3、BootStrap、Flex进行响应式布局
         熟悉原生JS、jQuery，能够用面向对象封装交互方法
         熟练使用npm包管理工具，以及gulp前端自动化构建工具
         了解微信小程序的开发
         熟悉MySQL数据库，能够使用ORM操作MySQL
         掌握Redis、Memcached和MongoDB数据库进行缓存
         熟悉Python爬虫框架Scrapy，及爬虫技术Urllib/Selenium
         熟悉常见的反爬虫策略
         熟练使用re/BeautifulSoup/Xpath工具解析网页数据
         了解numpy/matplotlib/pandas/scipy等数据分析工具
         了解tensorflow等开源深度学习框架
        """,
    "start":
    "2017-09",
    "end":
    "至今"
}]
sample_resume = {
    "tenant": tenant,
    "basic_info": basic_info,
    "educations": educations,
    "experiences": experiences
}
similar_resume_from_resume_info = {
    "resume_info": sample_resume,
    "topn": 10,
    "min_score": 0
}


class ResumeSimilarTestCase(unittest.TestCase):
    def test_similar_resume_from_resume_id(self):
        request = resume_recommend_pb2.SimilarResumeFromResumeIdMessage(
            **similar_resume_from_resume_id_info)
        response = resume_recommend_stub.SimilarResumeFromResumeId(request)
        result = MessageToDict(
            response,
            including_default_value_fields=True,
            preserving_proto_field_name=True)
        print(result)
        self.assertTrue(
            len(result['items']) <= similar_resume_from_resume_id_info['topn'])
        self.assertTrue(result['items'][-1]['score'] >=
                        similar_resume_from_resume_id_info['min_score'])
        self.assertNotIn(
            similar_resume_from_resume_id_info['resume_info']['aid'],
            [i['aid'] for i in result['items']])

    def test_similar_resume_from_resume(self):
        request = resume_recommend_pb2.SimilarResumeFromResumeMessage(
            **similar_resume_from_resume_info)
        response = resume_recommend_stub.SimilarResumeFromResume(request)
        result = MessageToDict(
            response,
            including_default_value_fields=True,
            preserving_proto_field_name=True)
        self.assertTrue(
            len(result['items']) <= similar_resume_from_resume_info['topn'])
        self.assertTrue(result['items'][-1]['score'] >=
                        similar_resume_from_resume_info['min_score'])
