import unittest

from google.protobuf.json_format import MessageToDict

from ai_service_protos import base_pb2
from engine_services.description_engine.channel import \
    title_multi_classify_engine_stub

title = "python研发"


class TitleEngineTestCase(unittest.TestCase):
    def test_Predict(self):
        request = base_pb2.NormalTextRequest(text=title)
        response = title_multi_classify_engine_stub.Predict(request)
        result = MessageToDict(
            response,
            including_default_value_fields=True,
            preserving_proto_field_name=True,
        )
        self.assertEqual(result['text'], '互联网/软件/游戏-技术-后端开发-python工程师')

    def test_PredictProbTop(self):
        request = base_pb2.NormalTextRequest(text=title)
        response = title_multi_classify_engine_stub.PredictProbTop(request)
        result = MessageToDict(
            response,
            including_default_value_fields=True,
            preserving_proto_field_name=True,
        )
        self.assertEqual(result['result'], ['互联网/软件/游戏-技术-后端开发-python工程师'])

    def test_MultiplePredict(self):
        request = base_pb2.NormTextListMessage(text_list=['Python开发工程师'])
        response = title_multi_classify_engine_stub.MultiplePredict(request)
        result = MessageToDict(
            response,
            including_default_value_fields=True,
            preserving_proto_field_name=True)
        result = result['items']
        self.assertEqual(result[0]['pred'], '互联网/软件/游戏-技术-后端开发-python工程师')

    def test_GetTitle2Id(self):
        request = base_pb2.NormalTextRequest()
        response = title_multi_classify_engine_stub.GetTitle2Id(request)
        result = MessageToDict(
            response,
            including_default_value_fields=True,
            preserving_proto_field_name=True)
        self.assertIn(1, set(result['result'].values()))
        self.assertIn(2, set(result['result'].values()))

    def test_GetTitleParent2Id(self):
        request = base_pb2.NormalTextRequest()
        response = title_multi_classify_engine_stub.GetTitleParent2Id(request)
        result = MessageToDict(
            response,
            including_default_value_fields=True,
            preserving_proto_field_name=True)
        self.assertIn(1, set(result['result'].values()))
        self.assertIn(2, set(result['result'].values()))

    def test_GetTitle2Parent(self):
        request = base_pb2.NormalTextRequest()
        response = title_multi_classify_engine_stub.GetTitle2Parent(request)
        result = MessageToDict(
            response,
            including_default_value_fields=True,
            preserving_proto_field_name=True)
        print(result)
        self.assertEqual(result['result']['互联网/软件/游戏-技术-后端开发-.NET工程师'],
                         '互联网/软件/游戏-技术-后端开发')


if __name__ == '__main__':
    unittest.main()
