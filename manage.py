#!/usr/bin/env python
import logging
from importlib import reload
from json import dump
from os import remove
from pprint import pformat, pprint

import click

import config as config_module
from database.elasticsearch import (es_company_index_init, es_jd_index_init,
                                    es_resume_index_init, es_school_index_init)
from database.mongodb.models import SyncLog
from sync_server.worker import celery_app
from utils.health_checker import snooper


@click.group()
def cli():
    ...


@cli.group(help="日志相关")
def log():
    ...


@log.command(help="查询同步日志")
@click.option("--ident", "-i")
def search(ident):
    sync_log: SyncLog = SyncLog.objects(ident=ident).first()
    if sync_log:
        print(sync_log.to_json())
    else:
        print('not found')


@cli.group(help="服务健康状态")
def health():
    ...


@health.command(help="健康检查")
def check():
    pprint(snooper.check())


@cli.group(help="es相关操作")
def es():
    ...


@cli.group(help="数据初始化")
def data():
    ...


@cli.group(help="单个租户的操作")
def tenant():
    ...


@es.command(help="初始化es数据库的结构，在首次上线或者es结构有变化是使用")
def init():
    es_jd_index_init()
    es_resume_index_init()
    es_company_index_init()
    es_school_index_init()


@data.command(help="初始化所有租户的jd和简历数据")
def init():
    celery_app.tasks['command.all_reindex_resume'].delay()
    celery_app.tasks['command.all_reindex_jd'].delay()


@data.command(help="对所有租户的jd重新索引")
def reindex_jd():
    celery_app.tasks['command.all_reindex_jd'].delay()


@data.command(help="对所有租户的resume重新索引")
@click.option("--type", "-t")
def reindex_resume(type: str = ''):
    celery_app.tasks['command.all_reindex_resume'].delay(type_=type)


@data.command(help="为所有租户的所有jd重新推荐, home page 更新")
def re_recommend():
    celery_app.tasks['command.all_re_recommend_resume_from_jd'].delay()


@tenant.command(help="初始化单个租户的 jd_index 和 简历数据")
@click.option("--client_key", "-ck")
def init(client_key):
    celery_app.tasks['command.tenant_reindex_resume'].delay(tenant=client_key)
    celery_app.tasks['command.tenant_reindex_jd'].delay(tenant=client_key)


@tenant.command(help="对单个租户的jd重新索引")
@click.option("--client_key", "-ck")
def reindex_jd(client_key):
    celery_app.tasks['command.tenant_reindex_jd'].delay(tenant=client_key)


@tenant.command(help="对单个租户的resume重新索引")
@click.option("--client_key", "-ck")
def reindex_resume(client_key):
    celery_app.tasks['command.tenant_reindex_resume'].delay(tenant=client_key)


@tenant.command(help="为单个租户的所有jd重新推荐, home page 更新")
@click.option("--client_key", "-ck")
def re_recommend(client_key):
    celery_app.tasks['command.tenant_re_recommend_resume_from_jd'].delay(
        tenant=client_key)


@cli.group(help='配置信息模块')
def config():
    ...


def init_config():
    with open(config_module.ai_service_config['CONFIG_FILE'], 'w') as f:
        dump(config_module.ai_service_config, f, sort_keys=True, indent='    ')


@config.command(help='初始化配置文件')
def init():
    logging.info(
        f"尝试移除配置文件 -> {config_module.ai_service_config['CONFIG_FILE']}")
    try:
        remove(config_module.ai_service_config['CONFIG_FILE'])
    except FileNotFoundError:
        pass
    reload(config_module)
    init_config()


@config.command(help='打印当前配置')
def show():
    click.echo(pformat(config_module.ai_service_config))


@config.command(help='编辑前配置')
def edit():
    from prompt_toolkit import prompt
    from prompt_toolkit.completion import WordCompleter
    config_key_completer = WordCompleter(
        config_module.ai_service_config.keys())
    while True:
        key = prompt('配置名称（exit 退出）：', completer=config_key_completer)
        if key == 'exit':
            break
        if key not in config_module.ai_service_config:
            raise KeyError(key)
        else:
            value = prompt('修改值：')
            config_module.ai_service_config[key] = value
            init_config()


if __name__ == '__main__':
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    logger.addHandler(logging.StreamHandler())
    cli()
