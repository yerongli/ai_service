import logging
from typing import Any, Dict, Optional

import grpc
import sentry_sdk
from flask import Flask, abort, jsonify, request
from grpc._channel import _Rendezvous
from sentry_sdk.integrations.flask import FlaskIntegration

from ai_service_protos.base_pb2 import NormAidTenantRequest
from ai_service_protos.feedback_pb2 import JdResumeFeedbackMessage
from ai_service_protos.meta_pb2 import BaseAidTenantMessage
from ai_service_protos.resume_recommend_pb2 import (
    DuplicateResumeCheckingFromResumeIdMessage,
    DuplicateResumeCheckingFromResumeMessage,
    DuplicateResumeCheckingPreciselyMessage, RecommendResumeFromJdInfoRequest,
    RecommendResumeHomePageRequest, ScoreJdCVMessage,
    SimilarResumeFromResumeIdMessage, SimilarResumeFromResumeMessage)
from config import ai_service_config, logger
from engine_services.feedback_engine.channel import feedback_stub
from engine_services.resume_recommend_engine.channel import (
    duplicate_checking_stub, resume_features_stub, resume_recommend_stub)
from utils import message_to_dict
from utils.health_checker import snooper
from utils.rpc import call_extract, call_ocr, http_channel
from web_api.authorization import current_user, login_required


@http_channel.prepare_headers
def add_authentication_info(
        url: str,
        json_data: Optional[dict],
) -> Dict[str, str]:
    return {'authentication': request.headers.get('request')}


app = Flask(__name__)

sentry_sdk.init(
    dsn=ai_service_config["SENTRY_DSN"], integrations=[FlaskIntegration()])

# gllue web 只支持 POST 请求，所有HTTP请求语义只能设置成POST

print('test print')
logger.info('test logger')


@app.errorhandler(ValueError)
@app.errorhandler(TypeError)
def special_exception_handler(error):
    return str(error), 400


@app.errorhandler(KeyError)
def special_exception_handler(error):
    return f'No {str(error)} field', 400


@app.errorhandler(_Rendezvous)
def special_exception_handler(error: _Rendezvous):
    if error.code() == grpc.StatusCode.DEADLINE_EXCEEDED:
        logging.exception('服务超时')
    return f'服务器异常，请稍后再试', 500


@app.route('/ai/resume_recommend/RecommendResumeFromJdInfo', methods=['POST'])
@login_required
def RecommendResumeFromJdInfo():
    request_json = request.get_json(force=True)
    request_json['jd_info']['tenant'] = current_user.client_key
    response = message_to_dict(
        resume_recommend_stub.RecommendResumeFromJdInfo(
            RecommendResumeFromJdInfoRequest(**request_json), timeout=25))
    return jsonify(response)


@app.route("/ai/resume_recommend/GetRecommendResumeHomePage", methods=['POST'])
@login_required
def GetRecommendResumeHomePage():
    request_json = request.get_json(force=True)
    request_json['tenant'] = current_user.client_key
    response = message_to_dict(
        resume_recommend_stub.GetRecommendResumeHomePage(
            RecommendResumeHomePageRequest(**request_json), timeout=5))
    return jsonify(response)


@app.route('/ai/resume_recommend/SimilarResumeFromResumeId', methods=['POST'])
@login_required
def SimilarResumeFromResumeId():
    request_json = request.get_json(force=True)
    request_json['resume_info']['tenant'] = current_user.client_key
    response = message_to_dict(
        resume_recommend_stub.SimilarResumeFromResumeId(
            SimilarResumeFromResumeIdMessage(**request_json), timeout=5))
    return jsonify(response)


@app.route('/ai/resume_recommend/SimilarResumeFromResume', methods=['POST'])
@login_required
def SimilarResumeFromResume():
    request_json = request.get_json(force=True)
    request_json['resume_info']['tenant'] = current_user.client_key
    response = message_to_dict(
        resume_recommend_stub.SimilarResumeFromResume(
            SimilarResumeFromResumeMessage(**request_json), timeout=5))
    return jsonify(response)


@app.route('/ai/resume_recommend/ScoreJdCV', methods=['POST'])
@login_required
def ScoreJdCV():
    request_json = request.get_json(force=True)
    request_json['resume_info']['tenant'] = current_user.client_key
    response = message_to_dict(
        resume_recommend_stub.ScoreJdCV(
            ScoreJdCVMessage(**request_json), timeout=5))
    return jsonify(response)


@app.route('/ai/resume_recommend/BatchScoreJdCV', methods=['POST'])
@login_required
def BatchScoreJdCV():
    request_json = request.get_json(force=True)
    request_json['tenant'] = current_user.client_key
    response = message_to_dict(
        resume_recommend_stub.BatchScoreJdCV(
            BaseAidTenantMessage(**request_json), timeout=5))
    return jsonify(response)


@app.route('/ai/duplicate_checking/FromResumeId', methods=['POST'])
@login_required
def DuplicateResumeCheckingFromResumeId(force=True):
    request_json = request.get_json(force=True)
    request_json['resume_info']['tenant'] = current_user.client_key
    response = message_to_dict(
        duplicate_checking_stub.FromResumeId(
            DuplicateResumeCheckingFromResumeIdMessage(**request_json),
            timeout=10))
    return jsonify(response)


@app.route('/ai/duplicate_checking/FromResume', methods=['POST'])
@login_required
def DuplicateResumeCheckingFromResume():
    request_json = request.get_json(force=True)
    request_json['resume_info']['tenant'] = current_user.client_key
    response = message_to_dict(
        duplicate_checking_stub.FromResume(
            DuplicateResumeCheckingFromResumeMessage(**request_json),
            timeout=5))
    return jsonify(response)


@app.route('/ai/duplicate_checking/FromResume', methods=['POST'])
@login_required
def PreciselyFromResume():
    request_json = request.get_json(force=True)
    request_json['tenant'] = current_user.client_key
    response = message_to_dict(
        duplicate_checking_stub.PreciselyFromResume(
            DuplicateResumeCheckingPreciselyMessage(**request_json),
            timeout=5))
    return jsonify(response)


@app.route('/ai/resume_features/GetResumePoiList', methods=['POST'])
@login_required
def GetResumePoiList():
    request_json = request.get_json(force=True)
    request_json['tenant'] = current_user.client_key
    response = message_to_dict(
        resume_features_stub.GetResumePoiList(
            NormAidTenantRequest(**request_json), timeout=5))
    return jsonify(response)


@app.route('/ai/feedback/FeedbackJdResumeMatch', methods=['POST'])
@login_required
def FeedbackJdResumeMatch():
    request_json = request.get_json(force=True)
    request_json['tenant'] = current_user.client_key
    response = message_to_dict(
        feedback_stub.FeedbackJdResumeMatch(
            JdResumeFeedbackMessage(**request_json), timeout=5))
    return jsonify(response)


@app.route('/ai/feedback/FeedbackJdResumeMismatch', methods=['POST'])
@login_required
def FeedbackJdResumeMismatch():
    request_json = request.get_json(force=True)
    request_json['tenant'] = current_user.client_key
    response = message_to_dict(
        feedback_stub.FeedbackJdResumeMismatch(
            JdResumeFeedbackMessage(**request_json), timeout=5))
    return jsonify(response)


@app.route('/ai/duplicate_checking/DuplicateChecking', methods=['POST'])
@login_required
def DuplicateChecking():
    file_content = request.json['file']
    if not file_content:
        abort(400)

    extract_data = call_extract(file_content)
    resume_info: Dict[str, Any] = {'tenant': current_user.client_key}
    resume_info.update(extract_data)
    checked_resume_info = {
        "resume_info": resume_info,
        "topn": 10,
        "min_score": 0.7
    }
    email = request.json.get('email')
    mobile = request.json.get('mobile')
    if email:
        resume_info['basic_info']['email'] = call_ocr(email, 'email')
    if mobile:
        resume_info['basic_info']['mobile'] = call_ocr(mobile, 'mobile')
    # 模糊查重
    general_response = message_to_dict(
        duplicate_checking_stub.FromResume(
            DuplicateResumeCheckingFromResumeMessage(**checked_resume_info),
            timeout=5))

    # 精准查重
    message = DuplicateResumeCheckingPreciselyMessage(
        tenant=current_user.client_key,
        mobile=resume_info['basic_info']['mobile'],
        email=resume_info['basic_info']['email'],
        linkedin=resume_info['basic_info']['linkedin'])
    precisely_response = message_to_dict(
        duplicate_checking_stub.PreciselyFromResume(message, timeout=5))
    return jsonify({
        'general': general_response,
        'precisely': precisely_response
    })


@app.route('/ai/health/check', methods=['GET'])
def health_check():
    return jsonify(snooper.check())
