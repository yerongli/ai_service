from service_checker.contrib import grpc as grpc_check

from config import ai_service_config
from utils.health_checker import snooper
from web_api.app import app

grpc_check.install(snooper=snooper,
                   name='COMPANY_ENGINE'.lower(),
                   address=ai_service_config['COMPANY_ENGINE_ADDRESS'])
grpc_check.install(snooper=snooper,
                   name='DESCRIPTION_ENGINE'.lower(),
                   address=ai_service_config['DESCRIPTION_ENGINE_ADDRESS'])
grpc_check.install(snooper=snooper,
                   name='JD_ENGINE'.lower(),
                   address=ai_service_config['JD_ENGINE_ADDRESS'])
grpc_check.install(snooper=snooper,
                   name='JD_INDEX_ENGINE'.lower(),
                   address=ai_service_config['JD_INDEX_ENGINE_ADDRESS'])
grpc_check.install(snooper=snooper,
                   name='RESUME_INDEX_ENGINE'.lower(),
                   address=ai_service_config['RESUME_INDEX_ENGINE_ADDRESS'])
grpc_check.install(
    snooper=snooper,
    name='RESUME_RECOMMEND_ENGINE'.lower(),
    address=ai_service_config['RESUME_RECOMMEND_ENGINE_ADDRESS'])
grpc_check.install(snooper=snooper,
                   name='FEEDBACK_ENGINE'.lower(),
                   address=ai_service_config['FEEDBACK_ENGINE_ADDRESS'])
grpc_check.install(snooper=snooper,
                   name='SCHOOL_ENGINE'.lower(),
                   address=ai_service_config['SCHOOL_ENGINE_ADDRESS'])
