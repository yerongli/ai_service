对外暴露API接口
======================

.. note:: 所有 ``tenant`` 字段不需要调用方提供，皆由认证服务器填充。

Resume Recommend Engine
-----------------------------

.. note:: 返回值中 ``recommend_id`` 字段为记录性ID检索字段，并无其他意义。

简历推荐 接口
::::::::::::::::::::::::::::::::::::::::::::::::

.. note::  ``refresh`` 用来标志是否重新推荐，翻页时设置 false

.. sourcecode:: http

    POST /ai/resume_recommend/RecommendResumeFromJdInfo HTTP/1.1
    Content-Type: application/json
    Accept: application/json

    {
        "jd_info": {
            "aid": 1,
            "tenant": "b1261b3f-663a-4955-a416-8e155bff3db1",
            "version": 1,
            "custom_prefer_list": [
                {
                    "field": "清华大学",
                    "type": "target_school",
                    "boost": 1
                }
            ]
        },
        "refresh": true,
        "topn": 100,
        "min_score": 0.35,
        "size": 20,
        "offset": 0
    }

返回值：

.. sourcecode:: http

    HTTP/1.1 200 OK
    Content-Type: application/json

    {
        "recommend_id": "b1261b3f-663a-4955-a416-8e155bff3db2",
        "total": 100,
        "size": 20,
        "items": [
            {
                "aid": 1,
                "tenant": "b1261b3f-663a-4955-a416-8e155bff3db1",
                "score": 0.45,
                "reason": ["长得好看"]
            }
        ]
    }


首页人才推荐 接口
::::::::::::::::::::::::::::::::::::::::::::::::

user - 当前用户的id

.. sourcecode:: http

    POST /ai/resume_recommend/GetRecommendResumeHomePage HTTP/1.1
    Content-Type: application/json
    Accept: application/json

    {
        "user": 1,
        "tenant": "b1261b3f-663a-4955-a416-8e155bff3db1",
        "topn": 5
    }

返回值：

.. sourcecode:: http

    HTTP/1.1 200 OK
    Content-Type: application/json

    {
        "items": [
            {
                "jd_aid": 1
                "resume_aid": 1
                "score": 0.9
                "tenant": "b1261b3f-663a-4955-a416-8e155bff3db1"
                "reason": ["温柔善良"]
            }
        ]
    }


相似简历推荐（使用已存在简历） 接口
::::::::::::::::::::::::::::::::::::::::::::::::

.. sourcecode:: http

    POST /ai/resume_recommend/SimilarResumeFromResumeId HTTP/1.1
    Content-Type: application/json
    Accept: application/json

    {
        "resume_info": {
            "aid": 1,
            "tenant": "b1261b3f-663a-4955-a416-8e155bff3db1"
        },
        "topn": 5,
        "min_score": 0.35,
    }

返回值：

.. sourcecode:: http

    HTTP/1.1 200 OK
    Content-Type: application/json

    {
        "recommend_id": "b1261b3f-663a-4955-a416-8e155bff3db2",
        "items": [
            {
                "aid": 1,
                "tenant": "b1261b3f-663a-4955-a416-8e155bff3db1",
                "score": 0.45
            }
        ]
    }


相似简历推荐（使用新简历） 接口
::::::::::::::::::::::::::::::::::::::::::::::::

.. sourcecode:: http

    POST /ai/resume_recommend/SimilarResumeFromResume HTTP/1.1
    Content-Type: application/json
    Accept: application/json

    {
        "resume_info": {
            "tenant": "b1261b3f-663a-4955-a416-8e155bff3db1",
            "basic_info": {
                "name": "谷小露",
                "gender": "男",
                "mobile": "*****",
                "birth_day": "1980-01-02"
            },
            "educations": [
                {
                    "school": "学校",
                    "major": "专业",
                    "degree": "学历",
                    "start": "开始时间",
                    "end": "结束时间"
                }
            ],
            "experiences": [
                {
                    "company": "公司名称",
                    "title": "职位",
                    "description": "工作描述",
                    "start": "开始时间",
                    "end": "结束时间"
                }
            ]
        },
        "topn": 100,
        "min_score": 0.35,
    }

返回值：

.. sourcecode:: http

    HTTP/1.1 200 OK
    Content-Type: application/json

    {
        "recommend_id": "b1261b3f-663a-4955-a416-8e155bff3db2",
        "items": [
            {
                "aid": 1,
                "tenant": "b1261b3f-663a-4955-a416-8e155bff3db1",
                "score": 0.45
            }
        ]
    }


简历查重（使用已存在简历） 接口
::::::::::::::::::::::::::::::::::::::::::::::::

.. note::  min_score - 最低分推荐在0.7以上

.. sourcecode:: http

    POST /ai/duplicate_checking/FromResumeId HTTP/1.1
    Content-Type: application/json
    Accept: application/json

    {
        "resume_info": {
            "aid": 1,
            "tenant": "b1261b3f-663a-4955-a416-8e155bff3db1"
        },
        "topn": 5,
        "min_score": 0.70
    }

返回值：

.. sourcecode:: http

    HTTP/1.1 200 OK
    Content-Type: application/json

    {
        "recommend_id": "b1261b3f-663a-4955-a416-8e155bff3db2",
        "items": [
            {
                "aid": 1,
                "tenant": "b1261b3f-663a-4955-a416-8e155bff3db1",
                "score": 0.75
            }
        ]
    }


简历查重（通过简历html文件）- 功能合并
::::::::::::::::::::::::::::::::::::::::::::::::

合并调用 模糊查重 以及 精准查重 结果

.. py:data:: file

    zlib压缩后并且经过base64编码后的简历内容

.. py:data:: email

    对于猎聘-猎头版的简历, 需要提供邮件对应的base64编码的图片

.. py:data:: mobile

    对于猎聘-猎头版的简历, 需要提供电话号码对应的base64编码的图片

.. sourcecode:: http

    POST /ai/duplicate_checking/DuplicateChecking HTTP/1.1
    Content-Type: application/json
    Accept: application/json

    {
        "file": "",
        "email": "",
        "mobile": ""
    }

返回值：

.. sourcecode:: http

    HTTP/1.1 200 OK
    Content-Type: application/json

    {
        "general": {
            "recommend_id": "b1261b3f-663a-4955-a416-8e155bff3db2",
            "items": [
                {
                    "aid": 1,
                    "tenant": "b1261b3f-663a-4955-a416-8e155bff3db1",
                    "score": 0.45
                }
            ]
        },
        "precisely": {
            "recommend_id": "b1261b3f-663a-4955-a416-8e155bff3db2",
            "items": [
                {
                    "aid": 1,
                    "tenant": "b1261b3f-663a-4955-a416-8e155bff3db1",
                    "score": 0.0
                }
            ]
        }
    }


简历查重（使用新简历） 接口 - 模糊查重
::::::::::::::::::::::::::::::::::::::::::::::::

.. note::  min_score - 最低分推荐在0.7以上

.. sourcecode:: http

    POST /ai/duplicate_checking/FromResume HTTP/1.1
    Content-Type: application/json
    Accept: application/json

    {
        "resume_info": {
            "tenant": "b1261b3f-663a-4955-a416-8e155bff3db1",
            "basic_info": {
                "name": "谷小露",
                "gender": "男",
                "mobile": "*****",
                "email": "xx@gllue.com",
                "linkedin": "xx@gllue.com",
                "birth_day": "1980-01-02",
                "age": 17,
                "current_salary": "0.1",
                "locations": [102]
            },
            "educations": [
                {
                    "school": "学校",
                    "major": "专业",
                    "degree": "学历",
                    "start": "开始时间",
                    "end": "结束时间",
                    "is_recent": "最近的",
                    "is_current": "现在的"
                }
            ],
            "experiences": [
                {
                    "company": "公司名称",
                    "title": "职位",
                    "description": "工作描述",
                    "start": "开始时间",
                    "end": "结束时间",
                    "is_recent": "最近的",
                    "is_current": "现在的"
                }
            ]
        },
        "topn": 5,
        "min_score": 0.70
    }

返回值：

.. sourcecode:: http

    HTTP/1.1 200 OK
    Content-Type: application/json

    {
        "recommend_id": "b1261b3f-663a-4955-a416-8e155bff3db2",
        "items": [
            {
                "aid": 1,
                "tenant": "b1261b3f-663a-4955-a416-8e155bff3db1",
                "score": 0.45
            }
        ]
    }


简历-jd打分（使用已存在简历和jd） 接口
::::::::::::::::::::::::::::::::::::::::::::::::

.. sourcecode:: http

    POST /ai/resume_recommend/ScoreJdCV HTTP/1.1
    Content-Type: application/json
    Accept: application/json

    {
        "jd_info": {
            "aid": 1,
            "tenant": "b1261b3f-663a-4955-a416-8e155bff3db1",
            "version": 1,
            "custom_prefer_list": [
                {
                    "field": "清华大学",
                    "type": "target_school",
                    "boost": 1
                }
            ]
        }
        "resume_info": {
            "aid": 1,
            "tenant": "b1261b3f-663a-4955-a416-8e155bff3db1"
        }
    }

返回值：

.. sourcecode:: http

    HTTP/1.1 200 OK
    Content-Type: application/json

    {
        "score": 0.5
    }



简历-jd批量打分接口
::::::::::::::::::::::::::::::::::::::::::::::::

.. note:: aid 为 ``joborder`` 的 id

.. sourcecode:: http

    POST /ai/resume_recommend/BatchScoreJdCV HTTP/1.1
    Content-Type: application/json
    Accept: application/json

    {
        "aid": 2,
        "tenant": "b1261b3f-663a-4955-a416-8e155bff3db1"
    }

返回值：

.. note:: aid 为 ``candidate`` 的 id

.. sourcecode:: http

    HTTP/1.1 200 OK
    Content-Type: application/json

    {
        "result": [
            {
                "aid": 9527,
                "tenant": "b1261b3f-663a-4955-a416-8e155bff3db1",
                "score": 0.6909677386283875
            }
        ]
    }


获取 简历 POI （使用已存在简历和jd） 接口
::::::::::::::::::::::::::::::::::::::::::::::::

.. sourcecode:: http

    POST /ai/resume_features/GetResumePoiList HTTP/1.1
    Content-Type: application/json
    Accept: application/json

    {
        "aid": 1,
        "tenant": "b1261b3f-663a-4955-a416-8e155bff3db1"
    }

返回值：

.. sourcecode:: http

    HTTP/1.1 200 OK
    Content-Type: application/json

    {
        "items": [
            {
                "index": "需求分析",
                "value": 0.6909677386283875
            }
        ]
    }


用户反馈接口 jd-resume 匹配
::::::::::::::::::::::::::::::::::::::::::::::::

.. sourcecode:: http

    POST /ai/feedback/FeedbackJdResumeMatch HTTP/1.1
    Content-Type: application/json
    Accept: application/json

    {
        "jd_aid": 1,
        "tenant": "b1261b3f-663a-4955-a416-8e155bff3db1",
        "resume_aid_list": [1, 2]
    }

返回值：

.. sourcecode:: http

    HTTP/1.1 200 OK
    Content-Type: application/json

    {
        "status": true,
        "result": "用户反馈成功"
    }


用户反馈接口 jd-resume 不匹配
::::::::::::::::::::::::::::::::::::::::::::::::

.. sourcecode:: http

    POST /ai/feedback/FeedbackJdResumeMismatch HTTP/1.1
    Content-Type: application/json
    Accept: application/json

    {
        "jd_aid": 1,
        "tenant": "b1261b3f-663a-4955-a416-8e155bff3db1",
        "resume_aid_list": [1, 2]
    }

返回值：

.. sourcecode:: http

    HTTP/1.1 200 OK
    Content-Type: application/json

    {
        "status": true,
        "result": "用户反馈成功"
    }
