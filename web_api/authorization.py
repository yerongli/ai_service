import grpc
import json
import logging
from functools import wraps
from typing import Dict, Any
from gllue_userauth_client import AuthClient
from flask import _request_ctx_stack, abort, has_request_context, request
from grpc._channel import _Rendezvous
from werkzeug.local import LocalProxy

from config import ai_service_config

auth_client: AuthClient = AuthClient(ai_service_config['AUTH_SERVER_ADDRESS'])


class User:
    is_authenticated: bool  # 是否登录
    web_user_id: int  # gllueweb user表对应的id
    base_user_id: int  # gllueweb baseuser 表对应的id
    base_user_hub_id: int  # gllueweb baseuser表上的hub_id, hub-user表上id
    client_key: str  #clientkey，客户唯一标识
    auth_mode: int  # gllueweb-baseuser表 auth_mode， 用来标记用户身份，如系统用户， 直线经理
    user_remote_addr: str  # 用户浏览器所在ip地址
    register_request_host: str  # 用户请求gllueweb的地址， 如*.gllue.com
    register_addr: str  # gllueweb进程所在的IP地址

    def __init__(self, user_data: Dict[str, Any]):
        for k, v in user_data.items():
            setattr(self, k, v)


current_user: User = LocalProxy(lambda: _get_user())
EXEMPT_METHODS = {'OPTIONS'}


def load_user() -> User:
    uuid = request.cookies.get('uid')

    if uuid:
        try:
            user = auth_client.auth_by_cookie(uuid, timeout=3)
        except _Rendezvous as rpc_error:
            if rpc_error.code() != grpc.StatusCode.UNAUTHENTICATED:
                logging.exception(rpc_error)
            return User({'is_authenticated': False})
        else:
            return User({
                'is_authenticated': True,
                'client_key': user.client_key
            })

    authentication: str = request.headers.get('authentication', 'scheme {}')
    try:
        _, user_data_str = authentication.split(None, 1)
        user_data: dict = json.loads(user_data_str)
        if user_data:
            user_data.update({'is_authenticated': True})
            return User(user_data)
        else:
            return User({'is_authenticated': False})
    except:
        logging.exception('加载用户失败')
        return User({'is_authenticated': False})


def _get_user():
    if has_request_context() and not hasattr(_request_ctx_stack.top, 'user'):
        ctx = _request_ctx_stack.top
        ctx.user = load_user()

    return getattr(_request_ctx_stack.top, 'user', None)


def login_required(func):
    '''
    If you decorate a view with this, it will ensure that the current user is
    logged in and authenticated before calling the actual view. (If they are
    not, it calls the :attr:`LoginManager.unauthorized` callback.) For
    example::

        @app.route('/post')
        @login_required
        def post():
            pass

    If there are only certain times you need to require that your user is
    logged in, you can do so with::

        if not current_user.is_authenticated:
            return current_app.login_manager.unauthorized()

    ...which is essentially the code that this function adds to your views.

    It can be convenient to globally turn off authentication when unit testing.
    To enable this, if the application configuration variable `LOGIN_DISABLED`
    is set to `True`, this decorator will be ignored.

    .. Note ::

        Per `W3 guidelines for CORS preflight requests
        <http://www.w3.org/TR/cors/#cross-origin-request-with-preflight-0>`_,
        HTTP ``OPTIONS`` requests are exempt from login checks.

    :param func: The view function to decorate.
    :type func: function
    '''

    @wraps(func)
    def decorated_view(*args, **kwargs):
        if request.method in EXEMPT_METHODS:
            return func(*args, **kwargs)
        elif not current_user.is_authenticated:
            return abort(401)
        return func(*args, **kwargs)

    return decorated_view
