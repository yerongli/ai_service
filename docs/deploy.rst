AI服务部署
======================================

项目外部依赖：

- **AI Model File** - https://gitlab.gllue.com/angelo.li/ai_model_file
- **MongoDB**
- **Redis**
- **ES**
- **jaeger**
- **gllue data** - https://gitlab.gllue.com/gllue/gllue_data
- **gllue auth server** - https://gitlab.gllue.com/kai.wang/gllueuserauth

.. warning:: 新环境部署时需要单独初始化ES数据库结构，初始化数据, 初始化任务将会分发至celery异步队列。

.. code-block:: shell

    python manage.py es init

    python manage.py data init

.. warning:: 新部署一家客户时，需要做以下操作

- **client hub** - 中为当前客户添加一个配置 **ES_DUMP_STRATEGY**（注意：更新config后需要点更新config）。
    - es - 关闭数据同步，
    - mix - 开启数据同步

- 如果开启数据同步，需要跑数据同步脚本。

.. code-block:: shell

    ./gllue cron rebuild_index_by_db --dump_strategy gllue_data

- 为当前用户初始化数据，需要再ai服务中跑下面的命令。

.. code-block:: shell

    python manage.py tenant init --client_key client_key


angelo_tool_engine
---------------------

.. warning:: **暂时弃用，不需要部署**

.. code-block:: shell

    python -m engine_services.angelo_tool_engine

company_engine
---------------------
.. code-block:: shell

    python -m engine_services.company_engine

description_engine
---------------------
.. code-block:: shell

    python -m engine_services.description_engine

jd_engine
---------------------
.. code-block:: shell

    python -m engine_services.jd_engine

school_engine
---------------------
.. code-block:: shell

    python -m engine_services.school_engine

jd_index_engine
---------------------
.. code-block:: shell

    python -m engine_services.jd_index_engine

resume_index_engine
---------------------
.. code-block:: shell

    python -m engine_services.resume_index_engine

splitter_engine
---------------------

.. warning:: **暂时弃用，不需要部署**

.. code-block:: shell

    python -m engine_services.splitter_engine

resume_recommend_engine
-------------------------
.. code-block:: shell

    python -m engine_services.resume_recommend_engine

feedback_engine
-------------------------
.. code-block:: shell

    python -m engine_services.feedback_engine

同步服务
-------------------------
.. code-block:: shell

    python -m sync_server

同步服务celery队列
-------------------------
.. code-block:: shell

    celery worker -A sync_server.worker -Ofair -l info

同步服务celery队列监控
-------------------------
.. code-block:: shell

    celery flower -A sync_server.worker --port=5555

定时任务
-------------------------
.. code-block:: shell

    celery beat -A sync_server.worker -l info

web_api
--------------
.. code-block:: shell

    uwsgi --http :5555 -w web_api:app --processes 4 --threads 8 --http-timeout 30


