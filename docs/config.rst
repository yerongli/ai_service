AI 服务器配置信息
=================


.. py:data:: ENV_PREFIX

    环境变量前缀, 通过设置 ENV_PREFIX + KEY 来覆盖对于KEY的配置信息。

    .. note:: 可使用环境变量 ``AI_SERVICE_CONFIG_FILE`` 来指定配置文件地址。

    Value: ``'AI_SERVICE_'``

配置文件模板:

.. code-block:: json

    {
        "ANGELO_TOOL_ENGINE_ADDRESS": "10.0.0.20:50051",
        "ANGELO_TOOL_ENGINE_PORT": 50051,
        "AUTH_SERVER_ADDRESS": "172.16.0.238:12333",
        "CELERY_BROKER_URL": "redis://localhost:6379/1",
        "CELERYD_MAX_TASKS_PER_CHILD": 5000,
        "CELERY_TASK_ALWAYS_EAGER": false,
        "COMPANY_ENGINE_ADDRESS": "10.0.0.20:50052",
        "COMPANY_ENGINE_PORT": 50052,
        "DESCRIPTION_ENGINE_ADDRESS": "10.0.0.20:50053",
        "DESCRIPTION_ENGINE_PORT": 50053,
        "ES_HOST": "10.0.0.20",
        "ES_JD_FEATURE_INDEX": "jd_features",
        "ES_PORT": 9200,
        "ES_RESUME_FEATURE_INDEX": "resume_features",
        "GLLUE_DATA_HOST": "service-proxy.gllue.com",
        "GLLUE_DATA_PORT": 50051,
        "JAEGER_HOST": "10.0.0.5",
        "JD_ENGINE_ADDRESS": "10.0.0.20:50054",
        "JD_ENGINE_PORT": 50054,
        "JD_INDEX_ENGINE_ADDRESS": "10.0.0.20:50060",
        "JD_INDEX_ENGINE_PORT": 50060,
        "MODEL_PATH": "",
        "MONGO_DB": "ai_search_engine",
        "MONGO_HOST": "127.0.0.1",
        "MONGO_PASSWORD": "",
        "MONGO_PORT": 27017,
        "MONGO_USER": "",
        "REDIS_URL": "redis://localhost/0",
        "RESUME_INDEX_ENGINE_ADDRESS": "10.0.0.20:50059",
        "RESUME_INDEX_ENGINE_PORT": 50059,
        "RESUME_RECOMMEND_ENGINE_ADDRESS": "10.0.0.20:50070",
        "RESUME_RECOMMEND_ENGINE_PORT": 50070,
        "SCHOOL_ENGINE_ADDRESS": "10.0.0.20:50056",
        "SCHOOL_ENGINE_PORT": 50056,
        "SENTRY_DSN": "",
        "SPLITTER_ENGINE_ADDRESS": "10.0.0.20:50058",
        "SPLITTER_ENGINE_PORT": 50058,
        "TEMPLATE_PATH": "ai_service_protos",
        "FEEDBACK_ENGINE_PORT": 50071,
        "FEEDBACK_ENGINE_ADDRESS": "10.0.0.20:50071"
    }


配置信息
-------------

.. py:data:: CELERY_TASK_ALWAYS_EAGER

    .. warning:: 该配置只在开发环境才允许为 **True**, 所有线上环境必须配置为 **False**。

    Default: ``True``

.. py:data:: CONFIG_FILE

    用于定义配置文件配置，配置文件里所配置的参数将覆盖当前类中的配置。

    Default: ``'/opt/web/ai_service_config.json'``

.. py:data:: MODEL_PATH

    模型储存位置 - 项目地址： https://gitlab.gllue.com/angelo.li/ai_model_file

    Default: ``''``

MongoDB相关
::::::::::::::

.. py:data:: MONGO_HOST

    mongo host

    Default: ``'127.0.0.1'``

.. py:data:: MONGO_PORT

    mongo port

    Default: ``27017``

.. py:data:: MONGO_DB

    mongo db

    Default: ``'ai_search_engine'``

.. py:data:: MONGO_USER

    mongo user

    Default: ``''``

.. py:data:: MONGO_PASSWORD

    mongo password

    Default: ``''``

.. py:data:: SENTRY_DSN

    sentry dsn

    Default: ``''``

Redis相关
::::::::::::::

.. py:data:: REDIS_URL

    redis url: redis 版本 3.2.0

    Default: ``'redis://localhost/0'``

ES相关
::::::::::::::

.. py:data:: ES_HOST

    Default: ``'10.0.0.20'``

.. py:data:: ES_PORT

    Default: ``9200``

.. py:data:: ES_RESUME_FEATURE_INDEX

    Default: ``'resume_features'``

.. py:data:: ES_JD_FEATURE_INDEX

    Default: ``'jd_features'``

Celery相关
::::::::::::::

.. py:data:: CELERY_BROKER_URL

    celery broker url

    Default: ``'redis://localhost/1'``

Jaeger相关
::::::::::::::

.. py:data:: JAEGER_HOST

    Default: ``'10.0.0.5'``

Gllue Data Server相关
:::::::::::::::::::::::

.. py:data:: GLLUE_DATA_HOST

    gllue data 服务器地址

    Default: ``'service-proxy.gllue.com'``

.. py:data:: GLLUE_DATA_PORT

    gllue data 服务器端口

    Default: ``50051``

Gllue Auth Server相关
:::::::::::::::::::::::

.. py:data:: AUTH_SERVER_ADDRESS

    权限认证服务器地址

    Default: ``'172.16.0.238:12333'``

