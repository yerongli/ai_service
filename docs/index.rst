.. ai_service documentation master file, created by
   sphinx-quickstart on Mon Jan  7 17:03:19 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ai_service's documentation!
======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   readme
   config
   deploy
   web_api
   grpc_api
   contributing

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
