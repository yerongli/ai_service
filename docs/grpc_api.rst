grpc 远程调用示例
==================

.. note:: 所有gRPC调用的输入与输出参数皆为gRPC的message对象。

.. warning:: 下列所有接口的输入与输出都用json来描述，用来描述输入与输出的 ``Schema``，但是message对象行为并不同于Python的Dict。

.. code-block:: python

    from google.protobuf.json_format import MessageToDict


    def message_to_dict(message,
                        including_default_value_fields=True,
                        preserving_proto_field_name=True,
                        use_integers_for_enums=False) -> dict:
        return MessageToDict(message, including_default_value_fields,
                             preserving_proto_field_name, use_integers_for_enums)

    test = gRPCMessage(name='L')
    message_to_dict(test) # {'name': 'L'}
    test.name # 'L'

依赖安装
------------

* PyPi库 - `ai_service_protos`
* grpcio > 1.19.0

ScoreJdCV 接口示例
-------------------

.. code-block:: python

    from functools import partial

    from grpc import insecure_channel

    from ai_service_protos.resume_recommend_pb2 import ScoreJdCVMessage
    from ai_service_protos.resume_recommend_pb2_grpc import resume_recommendStub

    resume_recommend_engine_channel = LazyLoad(
    partial(insecure_channel, target='RESUME_RECOMMEND_ENGINE_ADDRESS'))

    resume_recommend_stub: resume_recommendStub = LazyLoad(
        partial(resume_recommendStub, resume_recommend_engine_channel))

    request = {}
    response = resume_recommend_stub.ScoreJdCV(ScoreJdCVMessage(**request))

.. py:function:: resume_recommend_stub.ScoreJdCV(request)

   简历-jd打分（使用已存在简历和jd）

   :param request: :ref:`ScoreJdCVMessage`

   :type request: ScoreJdCVMessage
   :return: :ref:`ScoreJdCVResponse`
   :rtype: ScoreJdCVResponse


.. _ScoreJdCVMessage:

ScoreJdCVMessage
:::::::::::::::::::

.. sourcecode:: json

    {
        "jd_info": {
            "aid": 1,
            "tenant": "b1261b3f-663a-4955-a416-8e155bff3db1",
            "version": 1,
            "custom_prefer_list": [
                {
                    "field": "清华大学",
                    "type": "target_school",
                    "boost": 1
                }
            ]
        }
        "resume_info": {
            "aid": 1,
            "tenant": "b1261b3f-663a-4955-a416-8e155bff3db1"
        }
    }

.. _ScoreJdCVResponse:

ScoreJdCVResponse
:::::::::::::::::::

.. sourcecode:: json


    {
        "score": 0.5
    }
