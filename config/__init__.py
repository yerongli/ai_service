import logging
from os import getcwd, chdir

import requests
from grpcalchemy.config import Config


def access_config_from_apollo(current_config: dict) -> dict:
    route = "configs"
    if current_config["APOLLO_USING_CACHE"]:
        route = "configfiles"
    url = (
        f"{current_config['APOLLO_SERVER_URL']}/{route}/{current_config['APOLLO_APP_ID']}/"
        f"{current_config['APOLLO_CLUSTER']}/{current_config['APOLLO_NAMESPACE']}"
    )
    response = requests.get(url)
    if response.ok:
        return response.json().get("configurations", {})
    else:
        raise Exception("加载配置失败")


class BaseConfig:
    """基础配置类"""

    #: 环境变量前缀
    ENV_PREFIX = "AI_SERVICE_"

    #: 使用apollo
    ENABLE_CONFIG_LIST = False

    #: 用于定义配置文件配置，配置文件里所配置的参数将覆盖当前类中的配置
    CONFIG_FILE = "/opt/web/ai_service_config.json"

    TEMPLATE_PATH = "ai_service_protos"  #: gRPC Proto文件相关位置

    MODEL_PATH = ""  #: 模型储存位置

    #: mongodb 相关配置
    MONGO_HOST = "127.0.0.1"
    MONGO_PORT = 27017
    MONGO_DB = "ai_search_engine"
    MONGO_USER = ""
    MONGO_PASSWORD = ""
    MONGO_AUTH_DB = "admin"
    MONGO_SCHOOL_COL = "school"

    CURRENT_VERSION = 1  #: 服务版本号

    UPDATE_PERCENT = 0.1  #: 每次版本升级时，client 每次自动更新的数据比例

    #: sentry 相关
    SENTRY_DSN = ""

    #: redis 相关
    REDIS_URL = "redis://localhost/0"  # 缓存使用的 redis
    TTL = 60 * 60 * 24 * 7  # redis 缓存失效时间 单位为妙

    #: es 相关
    ES_HOST = "10.0.0.20"
    ES_PORT = 9200
    ES_RESUME_FEATURE_INDEX = "resume_features_2"
    ES_JD_FEATURE_INDEX = "jd_features"
    ES_COMPANY_FEATURE_INDEX = "company_features"
    ES_SCHOOL_FEATURES_INDEX = "school_features"

    #: celery相关
    CELERY_BROKER_URL = "redis://localhost:6379/1"
    CELERY_REDIS_MAX_CONNECTIONS = 5
    CELERY_DEFAULT_QUEUE = "broker:ai"
    CELERY_DEFAULT_EXCHANGE = "broker:ai"
    CELERY_DEFAULT_ROUTING_KEY = "broker:ai"
    CELERYD_MAX_TASKS_PER_CHILD = 5000
    CELERY_TASK_ALWAYS_EAGER = False
    CELERY_RESULT_SERIALIZER = "json"
    CELERY_TIMEZONE = "Asia/Shanghai"

    #: service相关
    ANGELO_TOOL_ENGINE_PORT = 50051
    ANGELO_TOOL_ENGINE_ADDRESS = "10.0.0.20:50051"
    COMPANY_ENGINE_PORT = 50052
    COMPANY_ENGINE_ADDRESS = "10.0.0.20:50052"
    DESCRIPTION_ENGINE_PORT = 50053
    DESCRIPTION_ENGINE_ADDRESS = "10.0.0.20:50053"
    JD_ENGINE_PORT = 50054
    JD_ENGINE_ADDRESS = "10.0.0.20:50054"
    JD_INDEX_ENGINE_PORT = 50060
    JD_INDEX_ENGINE_ADDRESS = "10.0.0.20:50060"
    RESUME_INDEX_ENGINE_PORT = 50059
    RESUME_INDEX_ENGINE_ADDRESS = "10.0.0.20:50059"
    RESUME_RECOMMEND_ENGINE_PORT = 50070
    RESUME_RECOMMEND_ENGINE_ADDRESS = "10.0.0.20:50070"
    FEEDBACK_ENGINE_PORT = 50071
    FEEDBACK_ENGINE_ADDRESS = "10.0.0.20:50071"
    SCHOOL_ENGINE_PORT = 50056
    SCHOOL_ENGINE_ADDRESS = "10.0.0.20:50056"
    SPLITTER_ENGINE_PORT = 50058
    SPLITTER_ENGINE_ADDRESS = "10.0.0.20:50058"

    #: jaeger 相关
    JAEGER_HOST = "10.0.0.5"

    #: 中心数据库地址
    GLLUE_DATA_HOST = "service-proxy.gllue.net"
    GLLUE_DATA_PORT = 50051

    #: 认证服务器
    AUTH_SERVER_ADDRESS = "172.16.0.238:12333"

    #: apollo
    APOLLO_USING_CACHE = False
    APOLLO_SERVER_URL = ""
    APOLLO_APP_ID = ""
    APOLLO_CLUSTER = "default"
    APOLLO_NAMESPACE = "application"

    #: 简历解析相关
    RESUME_EXTRACTOR_SERVICE_HOST = "https://extract-service.public-rpc.gllue.net"


ai_service_config = Config(
    BaseConfig, sync_access_config_list=[access_config_from_apollo]
)

cwd = getcwd()
root_path = cwd[: cwd.rfind("ai_service") + len("ai_service")]
chdir(root_path)

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
logger.addHandler(logging.StreamHandler())
